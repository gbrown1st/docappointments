<?php

require_once('configzedmed.php');

$code_version = '140219';

if (isset($_GET['showVersion']))

{
	
	echo $code_version;
	
}

if(isset($_POST['makePassword']))
{

  $length = 6;
  // start with a blank password
  $password = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
    
  // set up a countermakleapp
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($password, $char)) { 
      $password .= $char;
      $i++;
    }

  }
					
  // done!
	echo json_encode($password);

}

if(isset($_POST['getDoctors']))
{

 $clinic_code = $_POST['clinic_code'];
 
 $sql = "SELECT 
  TREATING_DOCTORS.DOCTOR_CODE,
  TREATING_DOCTORS.TITLE,
  TREATING_DOCTORS.NAME,
  TREATING_DOCTORS.IS_ACTIVE,
  TREATING_DOCTOR_LOCATIONS.CLINIC_CODE
FROM
  TREATING_DOCTORS
  INNER JOIN TREATING_DOCTOR_LOCATIONS ON (TREATING_DOCTORS.DOCTOR_CODE = TREATING_DOCTOR_LOCATIONS.DOCTOR_CODE)
  WHERE 
  TREATING_DOCTOR_LOCATIONS.CLINIC_CODE = '$clinic_code'
  AND TREATING_DOCTORS.IS_ACTIVE = 'Y'";
	  
	$rs = ibase_query($Connect, $sql);
	
	$p = array();
	
		while ($row = ibase_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = $row->TITLE.' '.$row->NAME;
			$doctor->data = $row->DOCTOR_CODE;
			$doctor->DoctorID = $row->DOCTOR_CODE;
			$doctor->DoctorCode = $row->DOCTOR_CODE;
			$doctor->FullName =  $row->TITLE.' '.$row->NAME;
			$doctor->location = $row->CLINIC_CODE;
			$doctor->online_status = "";
			$p[] = $doctor;
		}

	echo json_encode($p);
	
}

if(isset($_POST['getAllDayAppointments']))
{

	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_day = $item['appt_day'];

		$patient_description = str_replace("*", "'", $item['patient_description']);
		$doctors = $item['doctors'];
		
		$start_point = $item['start_point'];
		
		$end_point = $item['end_point'];
		
		$clinic_code = $item['clinic_code'];
		
	}


 $appt_sql = "SELECT -1 AS APPOINTMENT_ID, A.DOCTOR_CODE AS DOCTOR_CODE, A.CLINIC_CODE AS CLINIC_CODE, A.START_POINT, A.END_POINT, 0 AS PATIENT_ID, 'AV' AS STATUS_CODE,
D.TITLE || D.NAME AS DOCTOR_NAME
 
FROM DOCTOR_APPT_SLOTS A
LEFT OUTER JOIN TREATING_DOCTORS D ON (A.DOCTOR_CODE=D.DOCTOR_CODE)
WHERE
    A.IS_BOOKED='N' 
AND A.APPOINTMENT_ACTIVITY_TYPE_ID=(SELECT APPOINTMENT_ACTIVITY_TYPE_ID FROM APPOINTMENT_TYPES WHERE APPOINTMENT_TYPE_ID = $appt_type_id)
AND A.APPTDAY = '$appt_day'
AND A.DOCTOR_CODE IN ($doctors)
AND A.CLINIC_CODE = '$clinic_code'
 
ORDER BY A.DOCTOR_CODE, A.START_POINT";
  
	$appt_rs = ibase_query($Connect, $appt_sql);
	
	$appointments = array();
	
		while ($appt_row = ibase_fetch_object($appt_rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerName = $appt_row->DOCTOR_NAME;
			$Appointment->PractitionerID = $appt_row->DOCTOR_CODE;
			$Appointment->Location = $appt_row->CLINIC_CODE;
			$Appointment->ApptID = $appt_row->APPOINTMENT_ID;
			$Appointment->StartTime = strtotime($appt_row->START_POINT);
			$Appointment->When = $appt_row->START_POINT;
			$Appointment->EndPoint = $appt_row->END_POINT;
			$Appointment->Length = strtotime($appt_row->END_POINT) - strtotime($appt_row->START_POINT);
			$Appointment->Descrip = 'Available';
			$appointments[] = $Appointment;
			
		}

	$booked_sql = "SELECT A.APPOINTMENT_ID, A.DOCTOR_CODE, A.CLINIC_CODE, A.START_POINT, A.END_POINT, A.NOTES, A.STATUS_CODE, D.TITLE || D.NAME AS DOCTOR_NAME
FROM UNIFIED_APPOINTMENTS A
LEFT OUTER JOIN TREATING_DOCTORS D ON (A.DOCTOR_CODE=D.DOCTOR_CODE)
WHERE
A.START_POINT>= '$start_point'
AND A.END_POINT<= '$end_point'
AND A.DOCTOR_CODE IN ($doctors)
AND A.CLINIC_CODE = '$clinic_code'
AND A.NOTES IN ($patient_description)
 
ORDER BY A.DOCTOR_CODE, A.START_POINT";
	  
	$booked_rs = ibase_query($Connect, $booked_sql);

		while ($booked_row = ibase_fetch_object($booked_rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerName = $booked_row->DOCTOR_NAME;
			$Appointment->PractitionerID = $booked_row->DOCTOR_CODE;
			$Appointment->Location = $appt_row->CLINIC_CODE;
			$Appointment->ApptID = (int) $booked_row->APPOINTMENT_ID;
			$Appointment->StartTime = strtotime($booked_row->START_POINT);
			$Appointment->When = $booked_row->START_POINT;
			$Appointment->EndPoint = $booked_row->END_POINT;
			$Appointment->Length = strtotime($booked_row->END_POINT) - strtotime($booked_row->START_POINT);
			$Appointment->Descrip = $booked_row->NOTES;
			$appointments[] = $Appointment;
			
		}
		


			$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "StartTime"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
			
			
	
	echo json_encode($appointments);
	
}

if(isset($_POST['getAllWeekAppointments']))
{
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_day = $item['appt_day'];
		$start_point = $item['start_point'];
		$end_point = $item['end_point'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$doctor = $item['doctor'];
		$clinic_code = $item['clinic_code'];
	}

 $appt_sql = "SELECT -1 AS APPOINTMENT_ID, A.DOCTOR_CODE AS DOCTOR_CODE, A.CLINIC_CODE AS CLINIC_CODE, A.START_POINT, A.END_POINT, 0 AS PATIENT_ID, 'AV' AS STATUS_CODE,
D.TITLE || D.NAME AS DOCTOR_NAME
 
FROM DOCTOR_APPT_SLOTS A
LEFT OUTER JOIN TREATING_DOCTORS D ON (A.DOCTOR_CODE=D.DOCTOR_CODE)
WHERE
    A.IS_BOOKED='N' 
AND A.APPOINTMENT_ACTIVITY_TYPE_ID=(SELECT APPOINTMENT_ACTIVITY_TYPE_ID FROM APPOINTMENT_TYPES WHERE APPOINTMENT_TYPE_ID = $appt_type_id)
AND A.APPTDAY IN ($appt_day)
AND A.DOCTOR_CODE = '$doctor'
AND A.CLINIC_CODE = '$clinic_code'
 
ORDER BY A.DOCTOR_CODE, A.START_POINT";
  
	$appt_rs = ibase_query($Connect, $appt_sql);
	
	$appointments = array();
	
		while ($appt_row = ibase_fetch_object($appt_rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerName = $appt_row->DOCTOR_NAME;
			$Appointment->PractitionerID = $appt_row->DOCTOR_CODE;
			$Appointment->Location = $appt_row->CLINIC_CODE;
			$Appointment->ApptID = $appt_row->APPOINTMENT_ID;
			$Appointment->StartTime = strtotime($appt_row->START_POINT);
			$Appointment->When = $appt_row->START_POINT;
			$Appointment->EndPoint = $appt_row->END_POINT;
			$Appointment->Length = strtotime($appt_row->END_POINT) - strtotime($appt_row->START_POINT);
			$Appointment->Descrip = 'Available';
			$appointments[] = $Appointment;
			
		}

	$booked_sql = "SELECT A.APPOINTMENT_ID, A.DOCTOR_CODE, A.CLINIC_CODE, A.START_POINT, A.END_POINT, A.NOTES, A.STATUS_CODE, D.TITLE || D.NAME AS DOCTOR_NAME
FROM UNIFIED_APPOINTMENTS A
LEFT OUTER JOIN TREATING_DOCTORS D ON (A.DOCTOR_CODE=D.DOCTOR_CODE)
WHERE
A.START_POINT>= '$start_point'
AND A.END_POINT<= '$end_point'
AND A.DOCTOR_CODE = '$doctor'
AND A.CLINIC_CODE = '$clinic_code'
AND A.NOTES IN ($patient_description)
ORDER BY A.DOCTOR_CODE, A.START_POINT";
	  
	$booked_rs = ibase_query($Connect, $booked_sql);

		while ($booked_row = ibase_fetch_object($booked_rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerName = $booked_row->DOCTOR_NAME;
			$Appointment->PractitionerID = $booked_row->DOCTOR_CODE;
			$Appointment->Location = $appt_row->CLINIC_CODE;
			$Appointment->ApptID = (int) $booked_row->APPOINTMENT_ID;
			$Appointment->StartTime = strtotime($booked_row->START_POINT);
			$Appointment->When = $booked_row->START_POINT;
			$Appointment->EndPoint = $booked_row->END_POINT;
			$Appointment->Length = strtotime($booked_row->END_POINT) - strtotime($booked_row->START_POINT);
			$Appointment->Descrip = $booked_row->NOTES;
			$appointments[] = $Appointment;
					
		}

			$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "StartTime"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
	
	echo json_encode($appointments);
	
}

if(isset($_POST['makeAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);

  	$data = json_decode($jsonString, true);
  	
	foreach ($data as $item) {

		$appt_id = (int) $item['appt_id'];
		$appt_time = $item['appt_time'];
		$delete_from_day = $item['delete_from_day'];
		$start_point = $item['start_point'];
		$end_point = $item['end_point'];
		$clinic_code = $item['clinic_code'];
		$patient_description = $item['patient_description'];
		$doctor = $item['doctor'];
		$doctor_name = $item['doctor_name'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$multiple_appointments = $item['multiple_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
		
	}
	
	$p = array();
	$StringResult = new StdClass();
		
	$check_appt_sql = "SELECT A.APPOINTMENT_ID, A.NOTES
FROM UNIFIED_APPOINTMENTS A
WHERE
A.START_POINT = '$start_point'
AND A.STATUS_CODE='CU' 
AND A.DOCTOR_CODE = '$doctor'
AND A.CLINIC_CODE = '$clinic_code'";
	  
	$check_appt_rs = ibase_query($Connect, $check_appt_sql);
	
	$number_of_rows = 0;
	
	while ($check_appt_row = ibase_fetch_object($check_appt_rs)) {

		if (strtolower(trim($check_appt_row->NOTES)) == 'internet' || strtolower(trim($check_appt_row->NOTES)) == 'internetwc') {

			$available = true;
			$appt_id = (int) $check_appt_row->APPOINTMENT_ID;

		} else {

			$available = false;

		}

		$number_of_rows ++;
	}
	
	if ($number_of_rows == 0) {
		
		$available = true;
		
	}
	
	if (!$available) {
		
		$StringResult->result_string = "This appointment is no longer available.\r\rPlease choose another."; 
		$StringResult->heading = "Appointment Unavailable";
		$StringResult->extra_data = 0;
		
	} else  {
		
		if ($multiple_appointments != 'yes') {
	
		$booked_sql = "SELECT A.APPOINTMENT_ID, A.DOCTOR_CODE, A.CLINIC_CODE, A.START_POINT, A.END_POINT, D.TITLE || D.NAME AS DOCTOR_NAME, A.NOTES
	FROM UNIFIED_APPOINTMENTS A 
	LEFT OUTER JOIN TREATING_DOCTORS D ON (A.DOCTOR_CODE=D.DOCTOR_CODE)
	WHERE
	A.START_POINT>= '$delete_from_day'
	AND A.STATUS_CODE='CU' 
	AND A.NOTES = '$patient_description'
	AND A.CLINIC_CODE = '$clinic_code'";
		  
		$booked_rs = ibase_query($Connect, $booked_sql);
	
			while ($booked_row = ibase_fetch_object($booked_rs)) {
	
				$booked_appt_id = $booked_row->APPOINTMENT_ID;
				$booked_clinic_code = $booked_row->CLINIC_CODE;
				$booked_start_point = $booked_row->START_POINT;
				$booked_doctor = $booked_row->DOCTOR_CODE;
				
				$sql1 = "UPDATE DOCTOR_APPT_SLOTS SET IS_BOOKED='N'
				WHERE CLINIC_CODE='$booked_clinic_code' 
				AND START_POINT>='$booked_start_point'
				AND DOCTOR_CODE='$booked_doctor';";
					  
				$rs1 = ibase_query($Connect, $sql1);
				
				$sql2 = "DELETE FROM UNIFIED_APPOINTMENTS WHERE APPOINTMENT_ID = $booked_appt_id";
					  
				$rs2 = ibase_query($Connect, $sql2);
	
			}

		}

	$next_id_sql = "EXECUTE PROCEDURE PR_APPOINTMENT_ID_GEN";
	  
	$next_id_rs = ibase_query($Connect, $next_id_sql);
	
		while ($next_id_row = ibase_fetch_object($next_id_rs)) {

			$nextApptID = $next_id_row->NEXT_APPOINTMENT_ID;

		}
	
	if ($appt_id == -1) {
		
	 	$sql = "INSERT INTO UNIFIED_APPOINTMENTS(APPOINTMENT_ID, STAFF_ID, DOCTOR_CODE, CLINIC_CODE, PATIENT_ID, START_POINT, END_POINT, STATUS_CODE, NOTES)
	VALUES ($nextApptID, '$doctor', '$doctor', '$clinic_code', NULL, '$start_point', '$end_point', 'CU', '$patient_description')";
		
	} else {
		
	 	$sql = "UPDATE UNIFIED_APPOINTMENTS SET NOTES = '$patient_description' WHERE APPOINTMENT_ID = $appt_id";
		
	}
 
	$rs = ibase_query($Connect, $sql);
		
		if ($rs) {
				
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = $sql;
					
				
			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = $booked_sql;
				
			}
		
} 
	$p[] = $StringResult;
	echo json_encode($p);
	
}

if(isset($_POST['cancelAppointment']))
{

	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);

  	$data = json_decode($jsonString, true);
  	
	foreach ($data as $item) {

		$appt_id = (int) $item['appt_id'];
		$appt_time = $item['appt_time'];
		$start_point = $item['start_point'];
		$end_point = $item['end_point'];
		$clinic_code = $item['clinic_code'];
		$show_all_appointments = $item['show_all_appointments'];
		$patient_description = $item['patient_description'];
		$doctor = $item['doctor'];
		$doctor_name = $item['doctor_name'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$include_in_notifications = $item['include_in_notifications'];
		
	}
	
	$p = array();
	$StringResult = new StdClass();

$sql1 = "UPDATE DOCTOR_APPT_SLOTS SET IS_BOOKED='N'
WHERE CLINIC_CODE ='$clinic_code' 
AND START_POINT = '$start_point' 
AND END_POINT = '$end_point' 
AND DOCTOR_CODE ='$doctor' 
AND IS_BOOKED='Y'";
	  
$rs1 = ibase_query($Connect, $sql1);


if ($show_all_appointments == 'no') {
	
	$sql2 = "UPDATE UNIFIED_APPOINTMENTS SET NOTES = 'Internet' WHERE APPOINTMENT_ID= $appt_id";
	
} else {
	
	$sql2 = "DELETE FROM UNIFIED_APPOINTMENTS WHERE APPOINTMENT_ID= $appt_id";
	
}

$rs2 = ibase_query($Connect, $sql2);
	
	if ($rs2) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = 0;
			
				
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;

		}

	$p[] = $StringResult;
	echo json_encode($p);
	
}



?>
