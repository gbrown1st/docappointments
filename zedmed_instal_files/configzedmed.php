<?php
 
$db = '127.0.0.1:C:\Zedmed\superplus';
$dbuser = 'SYSDBA';
$dbpass = 'xyzzy';
$dbcharset = 'UTF8';
$dbbuffers = 20;

$Connect = ibase_connect($db, $dbuser, $dbpass, $dbcharset, $dbbuffers) or die ('Unable to connect to database');

$config_smtp_host = '192.168.150.3';
$config_smtp_auth = false;
$config_smtp_port = 25;
$config_smtp_username = 'test@bigpond.com'; // Only applies if smtp_auth=true
$config_smtp_password = 'password';
$config_smtp_from = 'support@yam.com.au';
$config_smtp_replyto = 'dns@yam.com.au';

$config_docappointment_address = 'http://www.docappointment.com.au/test';   // No trailing forward slash
$config_clinic_name = 'Zedmed Test Clinic';
$config_clinic_email = 'david@yam.com.au';

$appt_type_id = 37;

$config_clinic_details = "
Please refer to our two locations below, for the address of your Doctor's clinic:

1 A Street, Somewhere – phone 03 1234 5678

If you cannot make it to your appointment and do not contact us, you will be charged a standard appointment fee.
				
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
	";


?>