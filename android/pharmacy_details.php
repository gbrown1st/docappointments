<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

/* $latidute = '-37.703655';//$_GET['latitude'];
$longitude = '145.119904';//$_GET['longitude'];
$distance = '10';//$_GET['distance']; */

$pharmacy_id = $_GET['pharmacy_id'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	`pharmacy_id` as pharmacy_id,
	`company_name` as company_name,
	`street` as street,
	`suburb` as suburb,
	`state` as state,
	`postcode` as postcode,
	`longitude` as longitude,
	`latitude` as latitude,
	`phone` as phone,
	`website` as website,
	`mobile` as mobile,
	`toll_free` as toll_free,
	`fax` as fax,
	`after_hours` as after_hours,
	`email` as email
FROM
	`pharmacies`
WHERE `pharmacy_id` = ?");

	  mysqli_stmt_bind_param($stmt, 'i', $pharmacy_id);

      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->pharmacy_id, $row->company_name, 
							  $row->street, $row->suburb, $row->state, $row->postcode, 
							  $row->longitude, $row->latitude, 
							  $row->phone, $row->website, $row->mobile, 
							  $row->toll_free, $row->fax, $row->after_hours, 
							  $row->email);
		
$json_string = '{"pharmacies": [
';

      while (mysqli_stmt_fetch($stmt)) {
		  
		   $json_string .= '{
		"pharmacy_id": "'.$row->pharmacy_id.'",
		"company_name": "'.$row->company_name.'",
		"street": "'.$row->street.'",
		"suburb": "'.$row->suburb.'",
		"state": "'.$row->state.'",
		"postcode": "'.$row->postcode.'",
		"latitude": "'.$row->latitude.'",
		"longitude": "'.$row->longitude.'",
		"phone": "'.$row->phone.'",
		"website": "'.$row->website.'",
		"mobile": "'.$row->mobile.'",
		"toll_free": "'.$row->toll_free.'",
		"fax": "'.$row->fax.'",
		"after_hours": "'.$row->after_hours.'",
		"email": "'.$row->email.'"
		},';
      }

	$len=strlen($json_string);
	$json_string=substr($json_string,0,($len-1));

	$json_string .= '
]}';


 mysqli_stmt_free_result($stmt);
 mysqli_close($mysqli);
	
	echo $json_string;



?>
