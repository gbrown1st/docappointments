<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surgeries.surgery_id,
	surgeries.surgery_name,
	surgeries.surgery_address_1,
	surgeries.surgery_address_2,
	surgeries.surgery_suburb_town,
	surgeries.surgery_state,
	surgeries.surgery_postcode,
	surgeries.contact_email,
	surgeries.contact_phone,
	surgeries.latitude,
	surgeries.longitude,
	surgery_types.surgery_type
FROM surgeries INNER JOIN surgery_types ON surgeries.surgery_type_id = surgery_types.surgery_type_id
WHERE
	surgery_status = 'Active'
ORDER BY
	surgeries.surgery_name ASC");


      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->surgery_id, $row->surgery_name, 
							  $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, 
							  $row->surgery_postcode, $row->contact_email, 
							  $row->contact_phone, $row->latitude, $row->longitude, 
							  $row->surgery_type);


	$json_string = '{"surgeries": [
';

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $json_string .= '
	{
		"surgery_id": '.$row->surgery_id.',
		"surgery_name": "'.$row->surgery_name.'",
		"surgery_address_1": "'.$row->surgery_address_1.'",
		"surgery_address_2": "'.$row->surgery_address_2.'",
		"surgery_suburb_town": "'.$row->surgery_suburb_town.'",
		"surgery_state": "'.$row->surgery_state.'",
		"surgery_postcode": "'.$row->surgery_postcode.'",
		"display_address_1": "'.$row->surgery_address_1.'",
		"display_address_2": "'.$row->surgery_suburb_town.' '.$row->surgery_state.' '.$row->surgery_postcode.'",
		"contact_email": "'.$row->contact_email.'",
		"contact_phone": "'.$row->contact_phone.'",
		"latitude": "'.$row->latitude.'",
		"longitude": "'.$row->longitude.'",
		"surgery_name": "'.$row->surgery_name.'"
	},';
      }

$len=strlen($json_string);
$json_string=substr($json_string,0,($len-1));

	$json_string .= '
]}';

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
echo $json_string;

?>



