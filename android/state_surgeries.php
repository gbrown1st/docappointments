<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

function normalize_str($str) {

    $invalid = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z',
    'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A',
    'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E',
    'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
    'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y',
    'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a',
    'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e',  'ë'=>'e', 'ì'=>'i', 'í'=>'i',
    'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
    'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y',  'ý'=>'y', 'þ'=>'b',
    'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "`" => "'", "´" => "'", '"' => ',', '`' => "'",
    '´' => "'", '"' => '\"', '"' => "\"", '´' => "'", "&acirc;€™" => "'", "{" => "",
    "~" => "", "–" => "-", "'" => "'","     " => " ");

    $str = str_replace(array_keys($invalid), array_values($invalid), $str);

    $remove = array("\n", "\r\n", "\r");
    $str = str_replace($remove, "\\n", trim($str));

      //$str = htmlentities($str,ENT_QUOTES);

    return htmlspecialchars($str);
}

$surgery_state = $_GET['surgery_state'];

$stmt = mysqli_prepare($mysqli,
          "SELECT surgeries.surgery_id, 
	surgeries.surgery_name, 
	surgeries.surgery_address_1, 
	surgeries.surgery_address_2, 
	surgeries.surgery_suburb_town, 
	surgeries.surgery_state, 
	surgeries.surgery_postcode, 
	surgeries.contact_email, 
	surgeries.contact_phone, 
	surgeries.latitude, 
	surgeries.longitude, 
	surgery_types.surgery_type, 
	surgery_text.surgery_front_screen, 
	surgery_text.surgery_make_appointment_alert, 
	surgery_text.surgery_cancel_appointment_alert
FROM surgeries INNER JOIN surgery_types ON surgeries.surgery_type_id = surgery_types.surgery_type_id
	 INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
WHERE
	(surgery_status = 'Active' OR surgeries.surgery_id IN (9,23,27,29,40,42,114,116,107,124,128,138))
AND (surgery_state = '$surgery_state' OR surgery_state_2 = '$surgery_state')
ORDER BY
	surgeries.surgery_name ASC");
	
      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->surgery_id, $row->surgery_name, 
							  $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, 
							  $row->surgery_postcode, $row->contact_email, 
							  $row->contact_phone, $row->latitude, $row->longitude, 
							  $row->surgery_type, $row->surgery_front_screen, $row->surgery_make_appointment_alert, $row->surgery_cancel_appointment_alert);


	$json_string = '{"surgeries": [
';

      while (mysqli_stmt_fetch($stmt)) {
		  
		  $surgery_front_screen = str_replace("[surgery_name]", $row->surgery_name, $row->surgery_front_screen);
		  $surgery_front_screen = str_replace("[surgery_phone]", $row->contact_phone, $surgery_front_screen);
		  $surgery_front_screen = normalize_str($surgery_front_screen);
		  $surgery_make_appointment_alert = normalize_str($row->surgery_make_appointment_alert);
		  $surgery_cancel_appointment_alert = normalize_str($row->surgery_cancel_appointment_alert);
		  
		  if ((int) $row->surgery_id == 9 || (int) $surgery_id == 23 || (int) $surgery_id == 29 || (int) $surgery_id == 40 || (int) $row->surgery_id == 114 || (int) $row->surgery_id == 116 || (int) $row->surgery_id == 107 || (int) $surgery_id == 124 || (int) $surgery_id == 128 || (int) $row->surgery_id == 138) {
			  $row->surgery_state = '';
			  $row->surgery_postcode = '';
		  }
		  
		  $json_string .= '
	{
		"surgery_id": '.$row->surgery_id.',
		"surgery_name": "'.$row->surgery_name.'",
		"surgery_address_1": "'.$row->surgery_address_1.'",
		"surgery_address_2": "'.$row->surgery_address_2.'",
		"surgery_suburb_town": "'.$row->surgery_suburb_town.'",
		"surgery_state": "'.$row->surgery_state.'",
		"surgery_postcode": "'.$row->surgery_postcode.'",
		"display_address_1": "'.$row->surgery_address_1.'",
		"display_address_2": "'.$row->surgery_suburb_town.' '.$row->surgery_state.' '.$row->surgery_postcode.'",
		"contact_email": "'.$row->contact_email.'",
		"contact_phone": "'.$row->contact_phone.'",
		"latitude": "'.$row->latitude.'",
		"longitude": "'.$row->longitude.'",
		"surgery_name": "'.$row->surgery_name.'",
		"surgery_front_screen": "'.$surgery_front_screen.'",
		"surgery_make_appointment_alert": "'.$surgery_make_appointment_alert.'",
		"surgery_cancel_appointment_alert": "'.$surgery_cancel_appointment_alert.'"
	},';
      }

$len=strlen($json_string);
$json_string=substr($json_string,0,($len-1));

	$json_string .= '
]}';

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
echo $json_string;

?>



