<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$company = $_POST['company'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$patient_name = $first_name.' '.$last_name;
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];//'03/02/1959';//$_POST['dob'];
$dob_array = explode('/', $dob);
$display_dob = $dob_array[2].'-'.$dob_array[1].'-'.$dob_array[0];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

function normalize_str($str) {

    $invalid = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z',
    'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A',
    'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E',
    'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
    'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y',
    'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a',
    'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e',  'ë'=>'e', 'ì'=>'i', 'í'=>'i',
    'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
    'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y',  'ý'=>'y', 'þ'=>'b',
    'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "`" => "'", "´" => "'", '"' => ',', '`' => "'",
    '´' => "'", '"' => '\"', '"' => "\"", '´' => "'", "&acirc;€™" => "'", "{" => "",
    "~" => "", "–" => "-", "'" => "'","     " => " ");

    $str = str_replace(array_keys($invalid), array_values($invalid), $str);

    $remove = array("\n", "\r\n", "\r");
    $str = str_replace($remove, "\\n", trim($str));

      //$str = htmlentities($str,ENT_QUOTES);

    return htmlspecialchars($str);
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);

$stat_surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT 
stat_doctors.surgery_id
FROM stat_doctors");
	
      mysqli_stmt_execute($stat_surgeries_stmt);

      mysqli_stmt_bind_result($stat_surgeries_stmt, $row->surgery_id);
	  
	  $stat_surgery_array = array();

      while (mysqli_stmt_fetch($stat_surgeries_stmt)) {
		 
		  $stat_surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($stat_surgeries_stmt);
	  	  
	  
$surgery_stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software,
	surgery_text.surgery_front_screen,
	surgery_text.surgery_make_appointment_alert,
	surgery_text.surgery_cancel_appointment_alert
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
INNER JOIN surgery_text ON server_url.surgery_id = surgery_text.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($surgery_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($surgery_stmt);

      mysqli_stmt_bind_result($surgery_stmt, $row->server_url, $row->surgery_name, $row->surgery_software, $row->surgery_front_screen, $row->surgery_make_appointment_alert, $row->surgery_cancel_appointment_alert);

      while (mysqli_stmt_fetch($surgery_stmt)) {
		 
		  $surgery_front_screen = str_replace("[surgery_name]", $row->surgery_name, $row->surgery_front_screen);
		  $surgery_front_screen = str_replace("[surgery_phone]", $row->contact_phone, $surgery_front_screen);
		  $surgery_front_screen = normalize_str($surgery_front_screen);
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  $surgery_make_appointment_alert = normalize_str($row->surgery_make_appointment_alert);
		  $surgery_cancel_appointment_alert = normalize_str($row->surgery_cancel_appointment_alert);
		  
      }

    mysqli_stmt_free_result($surgery_stmt);
	
		 $login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($login_stmt);
		 
		 mysqli_stmt_bind_result($login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($login_stmt);
		
		 if (mysqli_stmt_num_rows($login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			
			$json_string = '{"result": [{"heading": "Log In Successful!", "message": "Proceed with getting doctors.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'", "surgery_front_screen": "'.$surgery_front_screen.'", "surgery_make_appointment_alert": "'.$surgery_make_appointment_alert.'", "surgery_cancel_appointment_alert": "'.$surgery_cancel_appointment_alert.'"}]}';
			
		 } else {
			 
			  $add_user_stmt = mysqli_prepare($mysqli, "INSERT INTO users (surgery_id, user_type, first_name, last_name, dob, email, mobile, user_username, user_password, created_date) VALUES (?,?,?,?,?,?,?,?,AES_ENCRYPT(?, '0bfuscate'),?)");
			  
			  mysqli_stmt_bind_param($add_user_stmt, 'isssssssss', $surgery_id, $user_type, $first_name, $last_name, $dob, $email, $mobile, $username, $password, $now_date_time);
			  
			  if (mysqli_stmt_execute($add_user_stmt)) {
				  
				  $new_login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($new_login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($new_login_stmt);
		 
		 mysqli_stmt_bind_result($new_login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($new_login_stmt);

		 if (mysqli_stmt_num_rows($new_login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($new_login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			  
		 }
				  
				  $json_string = '{"result": [{"heading": "Log In Successful!", "message": "Proceed with getting doctors.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'", "surgery_front_screen": "'.$surgery_front_screen.'", "surgery_make_appointment_alert": "'.$surgery_make_appointment_alert.'", "surgery_cancel_appointment_alert": "'.$surgery_cancel_appointment_alert.'"}]}';
				  
			  } else {
				  
				  $json_string = '{"result": [{"heading": "Log In Failed!", "message": "Please try again.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'", "surgery_front_screen": "'.$surgery_front_screen.'", "surgery_make_appointment_alert": "'.$surgery_make_appointment_alert.'", "surgery_cancel_appointment_alert": "'.$surgery_cancel_appointment_alert.'"}]}';
				   
			  }
			  
		 }
		 
	
			
			 $user_stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "Android";
			$log_action = "Register/Log In";
			$log_action_comment = "Surgery: $surgery_name; Details: $patient_name [DOB: $dob] [Ph: $mobile]";
			
			mysqli_bind_param($user_stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($user_stmt);
			mysqli_stmt_free_result($user_stmt);
		
			mysqli_close($surgeries_stmt);
			mysqli_close($stat_surgeries_stmt);
			mysqli_close($surgery_stmt);
			mysqli_close($login_stmt);
			mysqli_close($add_user_stmt);
			mysqli_close($new_login_stmt);
			mysqli_close($user_stmt);
			
			mysqli_close($mysqli);
			
	

	echo $json_string;


?>



