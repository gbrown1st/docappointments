<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = '130';//$_POST['surgery_id'];
$doctor_id = '2';//$_POST['doctor_id'];

$username = 'gb';//$_POST['username'];
$password = 'gb';//$_POST['password'];
$user_type = 'patient';//$_POST['user_type'];

$pracsoftDescrip = '*Graham Brown [DOB: 03/02/1959] [Ph: 0448992599]*,*Tricia Brown [DOB: 25/02/1953] [Ph: 0408211198]*';//$_POST['pracsoftDescrip'];
$patient_string = str_replace(", ", ",", $pracsoftDescrip);
$patient_string = str_replace("*", "", $patient_string);
$appoitment_type = 'Internet';
$patient_description = '*Internet*,'.$pracsoftDescrip;

$fromDate = '2015-08-28';//$_POST['apptDate'];//'2012-08-21';//$_POST['apptDate'];
$toDate = '2015-08-31';//$_POST['apptDate'];//'2012-08-21';

$zedmed_start_point = $fromDate.' 00.00.00';
$zedmed_end_point = $toDate.' 23.59.59';

$dateMonthYearArr = array();
$dateMonthYearArrZedmed = array();
$fromDateTS = strtotime($fromDate);
$toDateTS = strtotime($toDate);

for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
// use date() and $currentDateTS to format the dates in between
$currentDateStr = date("Y-m-d",$currentDateTS);
$currentDateStrZedmed = date("m/d/Y",$currentDateTS);
$dateMonthYearArr[] = $currentDateStr;
$dateMonthYearArrZedmed[] = $currentDateStrZedmed;
//print $currentDateStr."<br />";
}

$dates_array_1 = array();
$dates_array_2 = array();
$dates_array_3 = array();
$dates_array_4 = array();
$dates_array_5 = array();
$dates_array_6 = array();

for ($d=0;$d<count($dateMonthYearArr);$d++) {
	
	if ($d < 8) {
		
		$dates_array_1[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 8 && $d < 15) {
		
		$dates_array_2[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 15 && $d < 22) {
		
		$dates_array_3[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 22 && $d < 29) {
		
		$dates_array_4[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 29 && $d < 36) {
		
		$dates_array_5[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 36 && $d < 42) {
		
		$dates_array_6[] = $dateMonthYearArr[$d];
		
	}
	
}

$zedmed_comma_separated = implode("','", $dateMonthYearArrZedmed);
$zedmed_comma_separated = "'".$zedmed_comma_separated."'";

$comma_separated = implode("','", $dateMonthYearArr);
$comma_separated = "'".$comma_separated."'";

$comma_separated_1 = implode("','", $dates_array_1);
$comma_separated_1 = "'".$comma_separated_1."'";
$comma_separated_2 = implode("','", $dates_array_2);
$comma_separated_2 = "'".$comma_separated_2."'";
$comma_separated_3 = implode("','", $dates_array_3);
$comma_separated_3 = "'".$comma_separated_3."'";
$comma_separated_4 = implode("','", $dates_array_4);
$comma_separated_4 = "'".$comma_separated_4."'";
$comma_separated_5 = implode("','", $dates_array_5);
$comma_separated_5 = "'".$comma_separated_5."'";
$comma_separated_6 = implode("','", $dates_array_6);
$comma_separated_6 = "'".$comma_separated_6."'";


/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	server_url.server_wsdl_url,
	surgeries.surgery_name,
	surgeries.surgery_software,
	surgeries.clinic_code,
	surgeries.surgery_state,
	surgeries.surgery_status
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software, $row->clinic_code, $row->surgery_state, $row->surgery_status);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  $clinic_code = $row->clinic_code;
		  $surgery_state = $row->surgery_state;
		  $surgery_status = $row->surgery_status;
      }
	  
	  switch ($surgery_state) {
	
	case 'ACT': date_default_timezone_set('Australia/Canberra');
	break;
	case 'NSW': date_default_timezone_set('Australia/Sydney');
	break;
	case 'NT': date_default_timezone_set('Australia/Darwin');
	break;
	case 'QLD': date_default_timezone_set('Australia/Brisbane');
	break;
	case 'SA': date_default_timezone_set('Australia/Adelaide');
	break;
	case 'TAS': date_default_timezone_set('Australia/Hobart');
	break;
	case 'TEST': date_default_timezone_set('Australia/Hobart');
	break;
	case 'VIC': date_default_timezone_set('Australia/Melbourne');
	break;
	case 'WA': date_default_timezone_set('Australia/Perth');
	break;
	
	
}

	mysqli_stmt_free_result($stmt);
	
	 if ($surgery_software == 'Zedmed') {
	$stmt_zm_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.clinic_code,
	zedmed_doctors.show_all_appointments
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_zm_doc, 'is', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_zm_doc);

      mysqli_stmt_bind_result($stmt_zm_doc, $row->clinic_code, $row->show_all_appointments);

      while (mysqli_stmt_fetch($stmt_zm_doc)) {
		 
		  $clinic_code = $row->clinic_code;
		  $show_all_appointments = $row->show_all_appointments;
		  
      }
	  
	  mysqli_stmt_free_result($stmt_zm_doc);
	  
	  } else if ($surgery_software != 'Zedmed' && in_array($surgery_id, $surgery_array)) {
		 
	$stmt_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.appointment_length
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_doc, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_doc);

      mysqli_stmt_bind_result($stmt_doc, $row->show_all_appointments, $row->appointment_book_id, $row->appointment_length);

      while (mysqli_stmt_fetch($stmt_doc)) {
		 
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  $appointment_length = $row->appointment_length;
	
      }
	  
	  mysqli_stmt_free_result($stmt_doc);
	  
	 } else {
		 
		 $stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id,
	doctor_new_patients.appointment_length
FROM
	doctor_new_patients
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->show_all_appointments, $row->appointment_book_id, $row->appointment_length);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  if ($row->show_all_appointments) {
			 
			  $show_all_appointments = $row->show_all_appointments;
			  
		  } else {
			  
			  $show_all_appointments = 'no';
		  }
		  
		  if ($row->appointment_book_id) {
			 
			  $appointment_book_id = $row->appointment_book_id;
			  
		  } else {
			  
			  $appointment_book_id = 0;
		  }
		  
		  $appointment_length =  $row->appointment_length;
      }
	  
		 mysqli_stmt_free_result($stmt2);
	 }
	  
	  
      mysqli_close($mysqli);

	  
	  $patients = '';
	  $patient_array = array();
	  $patient_details_array = array();
	  $AppInfo = '';
	  
	 if ($surgery_software == 'BestPractice' && !in_array($surgery_id, $surgery_array)) {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILEPHONE']);
		
		$getFamilyMembers= $_POST['getFamilyMembers'];
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_array[] = $response_array[$i]['INTERNALID'];
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILEPHONE']);
			
		}
	
		$post_array = array('getInternetPatientID' => 'true');
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$internet_patient_id = $response_array[0]['INTERNALID'];
		
		$patients = implode(",", $patient_array);
		
	}
	
		if ($surgery_software == 'BestPractice') {
					 
					 if (count($dates_array_1) > 0) {
						  
						  if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData1 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_1.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData1 = '[{"appt_day":"'.$comma_separated_1.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						  
						  
						$post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
					
					  }
					
					if (count($dates_array_2) > 0) {
							 
						if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData2 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_2.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData2 = '[{"appt_day":"'.$comma_separated_2.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						  
						$post_array2 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData2);
						
				 }
				 
					if (count($dates_array_3) > 0) {
						 
						 if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData3 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_3.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData3 = '[{"appt_day":"'.$comma_separated_3.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						  
						$post_array3 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData3);
					
					 }
					 
					 if (count($dates_array_4) > 0) {
						 
						 if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData4 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_4.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData4 = '[{"appt_day":"'.$comma_separated_4.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						  
						$post_array4 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData4);
					
					 }
				
					if (count($dates_array_5) > 0) {
						 
						 if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData5 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_5.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData5 = '[{"appt_day":"'.$comma_separated_5.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						 
						$post_array5 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData5);
						
					 }
					
					 if (count($dates_array_6) > 0) {
						 
						 if (!in_array($surgery_id, $surgery_array)) {
							  
						 	$jsonSendData6 = '[{"show_all_appointments":"'.$show_all_appointments.'","appointment_description":"Internet","appt_day":"'.$comma_separated_6.'","patient_description":"'.$pracsoftDescrip.'","doctor_id":'.$doctor_id.',"user_type":"patient","patient_id":"'.$patients.'"}]';
							
						  } else {
							  
							  $jsonSendData6 = '[{"appt_day":"'.$comma_separated_6.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
							  
						  }
						  
						$post_array6 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData6);
						
					 }
					
				 } else if ($surgery_software == 'PracSoft') {
					 
		 			if (count($dates_array_1) > 0) {
						
							$jsonSendData1 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_1.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
						
					}
					
					if (count($dates_array_2) > 0) {
						 $jsonSendData2 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_2.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array2 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData2);
						 
						
					 }
					
					if (count($dates_array_3) > 0) {
						
							
							$jsonSendData3 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_3.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array3 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData3);
						 
						
					 }
					 
					if (count($dates_array_4) > 0) {
						 
							$jsonSendData4 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_4.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array4 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData4);
						
						
					 }
					
					if (count($dates_array_5) > 0) {
						
							$jsonSendData5 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_5.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array5 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData5);
						 
					}
					
					if (count($dates_array_6) > 0) {
						 
						 
							$jsonSendData6 = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_6.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
							 $post_array6 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData6);
						
					 }
					
				 } else if ($surgery_software == 'Zedmed') {
				
					$jsonSendData1 = '[{"clinic_code":"'.$clinic_code.'","start_point":"'.$zedmed_start_point.'","patient_description":"'.$patient_description.'","appt_day":"'.$comma_separated.'","doctor":"'.$doctor_id.'","end_point":"'.$zedmed_end_point.'"}]';
					 $post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
					 
				 } else if ($surgery_software == 'Practice 2000') {
				
					$jsonSendData1 = '[{"patient_string":"'.$patient_string.'","show_all_appointments":"'.$show_all_appointments.'","patient_description":"'.$patient_description.'","appt_day":"'.$comma_separated.'","internet_id": -1,"doctor_id":"'.$doctor_id.'","user_type":"patient"}]';
					
					 $post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
					 
				 } else if ($surgery_software == 'Stat') {
					
					if (strtotime($fromDate) < strtotime('now')) {
						$from = date("Ymd",strtotime('now'));
					} else {
						$from = date("Ymd",strtotime($fromDate));
					}
					
					$to = date("Ymd",strtotime($toDate));
					
					$people = str_replace("*", "", $patient_description);
					
					$people_array = explode(',', $people);
					
					$params = array ('resourceId'=>$doctor_id, 'dateFrom'=>$from, 'dateTo'=>$to, 'personStringArray'=>$people_array);
					
					try {
						
					   	$client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
				
						$result = $client->GetAppointmentsWeek($params) ;
						
						$appointment_array = $result->GetAppointmentsWeekResult->DocAppAppointmentArray->DocAppAppointment;
						
						$stat_response_array = array();
						
						for ($i=0;$i<count($appointment_array);$i++) {
								
								$stat_response_array[$i]['ApptID'] = (int) $appointment_array[$i]->AppointmentId;
								$stat_response_array[$i]['PractitionerID'] = (int) $appointment_array[$i]->ResourceId;
								$stat_response_array[$i]['PatientID'] = 0;
								$stat_response_array[$i]['When'] = date("Y-m-d H:i.00.000",strtotime($appointment_array[$i]->AppointmentTime));
								$stat_response_array[$i]['Length'] = (int) $appointment_array[$i]->Length * 60;
								$stat_response_array[$i]['AppSecs'] =  0;
								$stat_response_array[$i]['Descrip'] =  $appointment_array[$i]->Description;
								$date = strtotime($stat_response_array[$i]['When']);
								$stat_response_array[$i]['Date'] = $date;
											
						}
						
					}
					catch (SoapFault $exception) {
						echo $exception->getMessage();
					}
					
				} else if ($surgery_software == 'Medilink') {
					
					if (strtotime($fromDate) < strtotime('now')) {
						$from = date('c', strtotime('now'));
					} else {
						$from = date('c',strtotime($fromDate));
					}
					
					$to = date('c',strtotime($toDate));
					
					$people = str_replace("*", "", $patient_description);
					$people_array = explode(',', $people);
					$short_array = array_shift($people_array);
					$p = array();
					for ($i=0;$i<count($people_array);$i++) {
						$person_array_1 = explode(' [DOB: ', $people_array[$i]);
						$person_array_2 = explode('] [Ph: ', $person_array_1[1]);
						$person_array_3 = explode(' ', $person_array_1[0]);
						$person = new stdClass();
						$person->Title = '';
						$person->FirstName = $person_array_3[0];
						$person->LastName = $person_array_3[1];
						$dob_array = explode('/',$person_array_2[0]);
						$dob = $person_array_2[0];
						$dob = $dob_array[2]."-".$dob_array[1]."-".$dob_array[0];
						$person->DOB = date('c', strtotime($dob));
						$mobile = $person_array_2[1];
						$mobile = str_replace(']','',$mobile);
						$person->PhoneNumber = $mobile;
						$p[] = $person;
					}
					
					//$params = array ('resourceId'=>$doctor_id, 'dateFrom'=>$from, 'dateTo'=>$to, 'personStringArray'=>$people_array);
					
					try {
						
						$client = new SoapClient($server_wsdl_url, array('login' => "admin", 'password' => "", "trace" => 1, "exception" => 0, 'cache_wsdl'   =>  WSDL_CACHE_BOTH, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
						
						$params = new stdClass;
						$params->ResourceIdArray = array($doctor_id);
						$params->Date = $from;
						$params->PersonStringArray = $p;
				
						$methodName = "GetAppointmentsDay";
						$resultName = $methodName . "Result";
						$result = $client->$methodName($params);

						$resultValue = $result->$resultName;
						$resourceArray = "AppointmentArray";
						$resource = "Appointment";
						$appts = $resultValue->$resourceArray->$resource;
		  
						print_r($params);
						print_r($appts);
						$stat_response_array = array();
						
						 for ($i=0;$i<count($appts);$i++) {
							$apptID = (int) $appts[$i]->AppointmentId;
							if ($apptID == 0) {
								$apptID = -1;
							}
							$stat_response_array[$i]['ApptID'] = $apptID;
							$stat_response_array[$i]['PractitionerID'] = (int) $appts[$i]->ResourceId;
							$stat_response_array[$i]['PatientID'] = 0;
							$stat_response_array[$i]['When'] = date("Y-m-d H:i.00.000",strtotime($appts[$i]->AppointmentTime));
							$stat_response_array[$i]['Length'] = (int) $appts[$i]->Length/60;
							$stat_response_array[$i]['AppSecs'] =  0;
							$stat_response_array[$i]['Descrip'] =  $appts[$i]->Description;
							$date = strtotime($stat_response_array[$i]['When']);
							$stat_response_array[$i]['Date'] = $date;
							
						 }  
					}
					catch (SoapFault $exception) {
						echo $exception->getMessage();
					}
				} 
			
				if (count($dates_array_1) > 0) {
					
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array1);
					$response1 = $r->send()->getBody();
					$response_array1 = json_decode($response1, true);
					
				}
				
				if (count($dates_array_2) > 0) {
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array2);
					$response2 = $r->send()->getBody();
					$response_array2 = json_decode($response2, true);
					
				}
				
				if (count($dates_array_3) > 0) {
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array3);
					$response3 = $r->send()->getBody();
					$response_array3 = json_decode($response3, true);
					
				}
				
				if (count($dates_array_4) > 0) {
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array4);
					$response4 = $r->send()->getBody();
					$response_array4 = json_decode($response4, true);
					
				}
				
				if (count($dates_array_5) > 0) {
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array5);
					$response5 = $r->send()->getBody();
					$response_array5 = json_decode($response5, true);
					
				}
				
				if (count($dates_array_6) > 0) {
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array6);
					$response6 = $r->send()->getBody();
					$response_array6 = json_decode($response6, true);
					
				}
				
				$response_array = array ();
				
				if (count($response_array1) > 0) {
					
					for ($i=0;$i<count($response_array1);$i++) {
						
						$date = strtotime($response_array1[$i]['When']);
						$response_array1[$i]['Date'] = $date;
						$response_array[] = $response_array1[$i];
						
					}
				
				}
				
				if (count($dates_array_2) > 0) {
				
					for ($i=0;$i<count($response_array2);$i++) {
						
						$date = strtotime($response_array2[$i]['When']);
						$response_array2[$i]['Date'] = $date;
						$response_array[] = $response_array2[$i];
						
					}
				
				}
				
				if (count($dates_array_3) > 0) {
				
					for ($i=0;$i<count($response_array3);$i++) {
						
						$date = strtotime($response_array3[$i]['When']);
						$response_array3[$i]['Date'] = $date;
						$response_array[] = $response_array3[$i];
						
					}
				
				}
				
				if (count($dates_array_4) > 0) {
					
					for ($i=0;$i<count($response_array4);$i++) {
						
						$date = strtotime($response_array4[$i]['When']);
						$response_array4[$i]['Date'] = $date;
						$response_array[] = $response_array4[$i];
						
					}
				
				}
				
				if (count($response_array5) > 0) {
				
					for ($i=0;$i<count($response_array5);$i++) {
						
						$date = strtotime($response_array5[$i]['When']);
						$response_array5[$i]['Date'] = $date;
						$response_array[] = $response_array5[$i];
						
					}
				
				}
				
				if (count($dates_array_6) > 0) {
					
					for ($i=0;$i<count($response_array6);$i++) {
						
						$date = strtotime($response_array6[$i]['When']);
						$response_array6[$i]['Date'] = $date;
						$response_array[] = $response_array6[$i];
						
					}
				
				}
				
		if ($surgery_software == 'Stat' || $surgery_software == 'Medilink') {
			
			$response_array = array();
			$response_array = $stat_response_array;
			
		}
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
		
		}
		
		$new_response_array = array();
		
		for ($i=0;$i<count($response_array);$i++) {
			
			if ($i == 0) {
				
				$new_response_array[] = $response_array[$i];
				
			} else {
				
				if ($response_array[$i]['Date'] != $response_array[($i-1)]['Date']) {
					
					$new_response_array[] = $response_array[$i];
					
				}
				
			}
			
		}
		
$response_array = $new_response_array;

$now = time();
	
$json_string = '{"appointments": [
';

 if ($surgery_status == 'Active') {
	 
      for ($i=0;$i<count($response_array);$i++) {
		
		  if (substr($response_array[$i]['When'], 0, 11) != 'Unavailable' && substr($response_array[$i]['Descrip'], 0, 11) != 'Unavailable') {
			  
			if (substr($response_array[$i]['Descrip'], 0, 9) == 'Available') {
				
				$response_array[$i]['Descrip'] = 'Internet';
					 
			}
				
		 $appInfo = '';
		  
		 $ApptDate_array = explode(" ", $response_array[$i]['When']);
		 
		 $date = strtotime($response_array[$i]['When']);

		 $appTime = date("D j M Y g:i A",$date);
		 
		  if ($surgery_software == 'BestPractice') {
			  
			  $appType = 'Internet';
			  
			  if (in_array($surgery_id, $surgery_array)) {
				  
				  if ($response_array[$i]['Descrip'] == 'Internet') {
					  
					  $appInfo = '';
					  
				  } else {
					  
					  $appInfo = $response_array[$i]['Descrip'];
					  
				  }
				  
			  } else {
				  
				  for ($n=0;$n<count($patient_details_array);$n++) {
	
					 if ((int) $patient_details_array[$n]['patient_id'] == (int) $response_array[$i]['PatientID']) {
						 
						 $dob = $patient_details_array[$n]['dob'];
						 $dob_array = explode('-', $dob);
						 $display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
						 
						 $appInfo = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$display_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
						 
					 } 
					 
				 }
				  
			  }
		 
		  } else  if ($surgery_software == 'PracSoft') {
			  
			  if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc' || $response_array[$i]['Descrip'] == 'Available') {
				  
				 $appType = 'Internet';
				  
			 } else {
				 
				 $appInfo = $response_array[$i]['Descrip'];
				 
			 }
			  
		  } else  if ($surgery_software == 'Practice 2000' || $surgery_software == 'Zedmed' || $surgery_software == 'Stat' || $surgery_software == 'Medilink') {
			  
			   if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc' || $response_array[$i]['Descrip'] == 'Available') {
				  
				 	$appType = 'Internet';
				  
				 } else {
					 
					 $appInfo = $response_array[$i]['Descrip'];
					 
				 }
		 
		  }
		
		
			 if ($show_all_appointments == 'no' && (int) $response_array[$i]['ApptID'] == -1) {
				  
				  continue;
				  
			  } else {
				  
				  if ($surgery_software == 'Practice 2000') {
					  
					  $apptPos = $response_array[$i]['Pos'];
					  
				  } else {
					  
					  $apptPos = '0';
				  }
				  
			if ($date > $now) {
				
				
				 if (($surgery_id == '42' && (int) $response_array[$i]['AppSecs'] >= 64800) || ($surgery_id == '56' && (int) $response_array[$i]['AppSecs'] >= 64800)) {
					 
					 continue;
					 
				 } else {
			
			  $json_string .= '{
			"PractitionerID": "'.$response_array[$i]['PractitionerID'].'",
			"DoctorNewPatients": "yes",
			"PatientID": "'.$response_array[$i]['PatientID'].'",
			"ApptDate": "'.$ApptDate_array[0].'",
			"ApptDescription": "'.$appTime.'",
			"ApptDateTime": "'.$response_array[$i]['When'].'",
			"ApptInfo": "'.$appInfo.'",
			"ApptType": "'.$appType.'",
			"ApptLength": "'.$response_array[$i]['Length'].'",
			"ApptPos": "'.$apptPos.'",
			"ApptSecs": "'.$response_array[$i]['AppSecs'].'",
			"ApptID": "'.$response_array[$i]['ApptID'].'"
	},';
	
			}
			
			}
			
			  }
	
			  }
	
	  }
	  
	$len=strlen($json_string);
	$json_string=substr($json_string,0,($len-1));
	
 }

	$json_string .= '
]}';

	
	echo $json_string;

?>

