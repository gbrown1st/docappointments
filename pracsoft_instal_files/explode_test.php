<?php

require_once "../config.php";
require_once('../PHPMailer_v5.1/class.phpmailer.php');

$code_version = '130805';

function getSeconds($startTime) {
	
	if (strlen($startTime) == 0) {
		
		return 60;
		
	} else {
		
		$startTime = explode (' ', $startTime);
		$startTime = explode ('.', $startTime[1]);
		$hms = $startTime[0];
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60) + 60;
		$seconds += (intval($s));
		return $seconds;
	}
	
}

if (isset($_GET['showVersion']))

{
	
	echo $code_version;
	
}

if(isset($_POST['makePassword']))
{

  $length = 6;
  // start with a blank password
  $password = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
    
  // set up a counter
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($password, $char)) { 
      $password .= $char;
      $i++;
    }

  }
					
  // done!
	echo json_encode($password);

}

if(isset($_POST['makeNewAppointmentRequest']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$appt_date = $item['appt_date'];
		$delete_from_day = $item['delete_from_day'];
		$patient_description = trim($item['patient_description']);
		$doctor_id = $item['doctor_id'];
		$doctor_code = $item['doctor_code'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$appointment_book_id = $item['appointment_book_id'];
		$multiple_appointments = $item['multiple_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
	
	}

	if ($multiple_appointments == "no") {
		$sql = "delete from APPT WHERE ([Descrip] = '$patient_description' and CONVERT(CHAR(10),[When],120) >= '$delete_from_day')";
		$rs = odbc_exec($Connect,$sql);
	}

	$p = array();
		
	$StringResult = new StdClass();

	if ((int) $appt_id > 0) {

		$sql2 = "update APPT set [Descrip] = '$patient_description' WHERE ApptID = $appt_id";

	} else {

		$sql2 = "
	INSERT INTO [HCN].[dbo].[APPT]
           ([PractitionerID]
           ,[PatientID]
           ,[Ur_No]
           ,[Type]
           ,[Descrip]
           ,[When]
           ,[Flag]
           ,[User]
           ,[ApptBookID])
     VALUES
           ($doctor_id
           ,0
           ,''
           ,0
           ,'$patient_description'
           ,'$appt_date'
           ,16
           ,'$doctor_code'
           ,$appointment_book_id)";

	}
	
	  
	$rs2 = odbc_exec($Connect,$sql2);

		if ($rs2) {
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = 0;
					
					$mail             = new PHPMailer();

					$body             = "Dear ".$patient_name.",
					
Thank you for your appointment with the " . $config_clinic_name . ".

Your appointment details are:

".$appt_time." with ".$doctor_name.".\n" . $config_clinic_details . "\n";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port;		// set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;       // SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Confirmation";
						//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				$mail->Body = $body;
				$address = $patient_email;
				$mail->AddAddress($address);
				$mail->Send();
				
				if ($include_in_notifications == 'y') {
					
					$notify_mail = new PHPMailer();
					
					$body = "Patient: ".$patient_name.",
				
Doctor: ".$doctor_name.",
	
Time: ".$appt_time.",
	
Status: Make Appointment Successful";
	
	
					$notify_mail->IsSMTP(); 
					
					$notify_mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
	
					$notify_mail->Host       = $config_smtp_host; // sets the SMTP server
	
					$notify_mail->Port       = $config_smtp_port;                    // set the SMTP port for the GMAIL server
	
					$notify_mail->Username   = $config_smtp_username; // SMTP account username
	
					$notify_mail->Password   = $config_smtp_password;        // SMTP account password
	
					$notify_mail->SetFrom($config_smtp_from);
	
					$notify_mail->AddReplyTo($config_smtp_replyto);
	
					$notify_mail->Subject    = $config_clinic_name . " Online Appointment Success Notification";
	
					$notify_mail->Body = $body;
	
					$address = $config_clinic_email;
	
					$notify_mail->AddAddress($address);
	
					$notify_mail->Send();
					
				}

			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = 0;
			
					if ($include_in_notifications == 'y') {
						
						$notify_mail = new PHPMailer();
		
						$body = "Patient: ".$patient_name.",
					
Doctor: ".$doctor_name.",
		
Time: ".$appt_time.",
		
Status: Make Appointment Failed";
		
		
						$notify_mail->IsSMTP(); 
						
						$notify_mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
		
						$notify_mail->Host       = $config_smtp_host; // sets the SMTP server
		
						$notify_mail->Port       = $config_smtp_port;                    // set the SMTP port for the GMAIL server
		
						$notify_mail->Username   = $config_smtp_username; // SMTP account username
		
						$notify_mail->Password   = $config_smtp_password;        // SMTP account password
		
						$notify_mail->SetFrom($config_smtp_from);
		
						$notify_mail->AddReplyTo($config_smtp_replyto);
		
						$notify_mail->Subject    = $config_clinic_name . " Online Appointment Failed Notification";
		
						$notify_mail->Body = $body;
		
						$address = $config_clinic_email;
		
						$notify_mail->AddAddress($address);
		
						$notify_mail->Send();
			
				}
			
			}

	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['cancelExistingAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$delete_from_day = $item['delete_from_day'];
		$appointment_description = $item['appointment_description'];
		$user_string = $item['user_string'];
		$user_type = $item['user_type'];
		$patient_description = $item['patient_description'];
		$doctor_id = $item['doctor_id'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		
	}

	if ($show_all_appointments == 'yes') {

		$sql = "delete from APPT WHERE [ApptID] = $appt_id";

	} else {
		
		$sql = "update APPT set [Descrip] = 'Internet' WHERE ApptID = $appt_id";

	}
	

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
		
	$StringResult = new StdClass();
	
	if ($rs) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = 0;
			$mail             = new PHPMailer();
			$body             = "Dear ".$patient_name.",
				
Your appointment ".$appt_time." with ".$doctor_name." has been cancelled.
			
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
				
";
				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                    // enables SMTP debug information (for testing)
															// 1 = errors and messages
															// 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;      // enable SMTP authentication
				$mail->Host       = $config_smtp_host;		// SMTP server
				$mail->Port       = $config_smtp_port;		// set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username;	// SMTP account username
				$mail->Password   = $config_smtp_password;	// SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Cancellation";
					//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				$mail->Body = $body;
				$address = $patient_email;
				$mail->AddAddress($address);
				$mail->Send();
				
				if ($include_in_notifications == 'y') {
					
					$notify_mail = new PHPMailer();
	
					$appt_mins = ((int) $appt_length / 60) . ' mins';
							
					$body = "Patient: ".$patient_name.",
				
Doctor: ".$doctor_name.",
	
Time: ".$appt_time.",
	
Status: Appointment Cancel Successful";
	
	
					$notify_mail->IsSMTP(); 
					
					$notify_mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
	
					$notify_mail->Host       = $config_smtp_host; // sets the SMTP server
	
					$notify_mail->Port       = $config_smtp_port;                    // set the SMTP port for the GMAIL server
	
					$notify_mail->Username   = $config_smtp_username; // SMTP account username
	
					$notify_mail->Password   = $config_smtp_password;        // SMTP account password
	
					$notify_mail->SetFrom($config_smtp_from);
	
					$notify_mail->AddReplyTo($config_smtp_replyto);
	
					$notify_mail->Subject    = $config_clinic_name . " Online Appointment Cancel Success Notification";
	
					$notify_mail->Body = $body;
	
					$address = $config_clinic_email;
	
					$notify_mail->AddAddress($address);
	
					$notify_mail->Send();
			
				}
				
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;
			
			if ($include_in_notifications == 'y') {
			
				$notify_mail = new PHPMailer();
	
				$appt_mins = ((int) $appt_length / 60) . ' mins';
							
				$body = "Patient: ".$patient_name.",
				
Doctor: ".$doctor_name.",
	
Time: ".$appt_time.",
	
Status: Appointment Cancel Failed";
	
	
				$notify_mail->IsSMTP(); 
					
				$notify_mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
	
				$notify_mail->Host       = $config_smtp_host; // sets the SMTP server
	
				$notify_mail->Port       = $config_smtp_port;                    // set the SMTP port for the GMAIL server
	
				$notify_mail->Username   = $config_smtp_username; // SMTP account username
	
				$notify_mail->Password   = $config_smtp_password;        // SMTP account password
	
				$notify_mail->SetFrom($config_smtp_from);
	
				$notify_mail->AddReplyTo($config_smtp_replyto);
	
				$notify_mail->Subject    = $config_clinic_name . " Online Appointment Cancel Failed Notification";
	
				$notify_mail->Body = $body;
	
				$address = $config_clinic_email;
	
				$notify_mail->AddAddress($address);
	
				$notify_mail->Send();
		
			}
		}
	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['getAppointmentBooksRequest']))
{

	$sql = "SELECT     [ApptBookID], [TimeSlotSize], [StartTime], [EndTime], [Description]
FROM         APPTBOOKSET
WHERE     ([Deleted] = 'N')";

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$AppointmentBook = new StdClass();
			$AppointmentBook->ApptBookID = (int) $row->ApptBookID;
			$AppointmentBook->TimeSlotSize = (int) $row->TimeSlotSize;
			$AppointmentBook->Description = trim($row->Description);
			$AppointmentBook->StartTime = trim($row->StartTime);
			$AppointmentBook->EndTime = trim($row->EndTime);
			$p[] = $AppointmentBook;
		}

	echo json_encode($p);

}

if(isset($_POST['makeAppointmentRequest']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$delete_from_day = $item['delete_from_day'];
		$appointment_description = $item['appointment_description'];
		$user_string = $item['user_string'];
		$user_type = $item['user_type'];
		$patient_description = $item['patient_description'];
		$doctor_id = $item['doctor_id'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
	
	}
	
		if ($user_type == "patient") {
		
		
			$sql = "update APPT set [Descrip] = 'Internet' WHERE ([Descrip] = '$user_string') and ([When] >= '$delete_from_day')";
	
			
		} else if ($user_type == "employer") {
		
		
			$sql = "update APPT set [Descrip] = 'Internetwc' WHERE ApptID = $appt_id";
				
		}
		
		$rs = odbc_exec($Connect,$sql);
	

	$p = array();
		
	$StringResult = new StdClass();

	if ($patient_description == "Internet" || $patient_description == "Internetwc") {
		
		if ($rs) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = 0;
			$mail             = new PHPMailer();
			$body             = "Dear ".$patient_name.",
				
Your appointment ".$appt_time." with ".$doctor_name." has been cancelled.
			
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
				
";
				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                    // enables SMTP debug information (for testing)
															// 1 = errors and messages
															// 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;      // enable SMTP authentication
				$mail->Host       = $config_smtp_host;		// SMTP server
				$mail->Port       = $config_smtp_port;		// set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username;	// SMTP account username
				$mail->Password   = $config_smtp_password;	// SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Confirmation";
					//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				$mail->Body = $body;
				$address = $patient_email;
				$mail->AddAddress($address);
				$mail->Send();
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;
		
		}
	
		$p[] = $StringResult;
		echo json_encode($p);
				
	} else {
	
		if ($rs) {
	
		$sql2 = "update APPT set [Descrip] = '$patient_description' WHERE ApptID = $appt_id";
	  
		$rs2 = odbc_exec($Connect,$sql2);
		
		if ($rs2) {
				
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = 0;
					
					$mail             = new PHPMailer();

					$body             = "Dear ".$patient_name.",
					
Thank you for your appointment with the " . $config_clinic_name . ".

Your appointment details are:

".$appt_time." with ".$doctor_name.".\n" . $config_clinic_details . "\n";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port;		// set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;       // SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Confirmation";
						//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				$mail->Body = $body;
				$address = $patient_email;
				$mail->AddAddress($address);
				$mail->Send();
			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = 0;
			
			}
			
		$p[] = $StringResult;
		echo json_encode($p);
			
		}
	
	}

}



if(isset($_POST['getWeekAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_day = $item['appt_day'];
		$PractitionerID = $item['PractitionerID'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$appointment_description = $item['appointment_description'];
		$user_type = $item['user_type'];
	
	}
	
	if ($user_type == "patient") {

 		$sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_day) and (LOWER([Descrip]) = 'internet' or [Descrip] in ($patient_description)) and [PractitionerID] = $PractitionerID";
	  
	} else {
	
		$patient_description .= '%';
		$sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_day) and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 'internetwc' or LOWER([Descrip]) like '$patient_description') and [PractitionerID] = $PractitionerID";
	
	}
	
	$rs = odbc_exec($Connect,$sql);
	
	 while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $row->ApptID;
			$Appointment->PractitionerID = (int) $row->PractitionerID;
			$Appointment->Descrip = trim($row->Descrip);
			$Appointment->When = trim($row->When);
			$p[] = $Appointment;
		}

		echo json_encode($p);
	
}

if(isset($_POST['checkAppointmentAvailability']))

{

	$jsonString = urldecode($_POST['jsonSendData']);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_date = $item['appt_date'];
		$doctor_id = $item['doctor_id'];

	}

$booked_sql = "SELECT ApptID, Descrip
FROM         APPT
WHERE     ([When] = '$appt_date') AND (PractitionerID = $doctor_id)";

			$booked_array = array();

			$booked_rs = odbc_exec($Connect,$booked_sql);

			if (odbc_num_rows($booked_rs) == 0) {

				$Booked = new stdClass();

				$Booked->is_available = 0;

				$booked_array[] = $Booked;

			} else {

				while ($booked_row = odbc_fetch_object($booked_rs)) {

					if (trim($booked_row->Descrip) == 'Internet' || trim($booked_row->Descrip) == 'Internetwc') {

						$Booked = new stdClass();

						$Booked->is_available = 0;

						$booked_array[] = $Booked;

					} else {

						$Booked = new stdClass();

						$Booked->is_available = 1;

						$booked_array[] = $Booked;

					}

				}

			}


	echo json_encode($booked_array);

}


if(isset($_POST['getAllWeekAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_id = (int) $item['doctor_id'];
		$appointment_book_id = (int) $item['appointment_book_id'];
		$appointment_length = (int) $item['appointment_length'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$show_all_appointments = $item['show_all_appointments'];
		$user_type = $item['user_type'];
		
	}

$patient_array = explode (',',$patient_string);

for ($i=0;$i<count($patient_array);$i++) {

	$patient_array[$i] = trim($patient_array[$i]);

}

$appt_date_cleared = str_replace("'","",$appt_date);

$days_array = explode (',',$appt_date_cleared);

$session_day_array = array();
$session_day_number_array = array();

for ($i=0;$i<count($days_array);$i++) {

	$session_day = intval(date('N', strtotime($days_array[$i])));

	$session_day ++;

	if ($session_day > 7) {

		$session_day -= 7;

	}

	$session_day_array[] = array("session_day" => $session_day, "session_date" => $days_array[$i]);
	$session_day_number_array[] = $session_day;
	
}
	
$appointments = array();
$days_away_array = array();
$sessions_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
      ,[PractitionerID]
      ,[Date] as theDate
      ,0 as DayOfWk
      ,[1Start] as start1
      ,[1End] as end1
      ,[2Start] as start2
      ,[2End] as end2
      ,[3Start] as start3
      ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
  FROM [HCN].[dbo].[TEMP_SESS]
  WHERE [PractitionerID] = $doctor_id
  AND [ApptBookID] = $appointment_book_id
  AND CONVERT(CHAR(10),[Date],120) in ($appt_date)
  ORDER BY Date";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

	if (odbc_num_rows($get_temp_sessions_rs) > 0) {

		while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {
			
			$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
			$theDate = $session_date_array[0];
			
			$temp_session_day = intval(date('N', strtotime($theDate)));

			$temp_session_day ++;
			
			if ($temp_session_day > 7) {
			
				$temp_session_day -= 7;
			
			}
					
				if (in_array($theDate, $days_array) && strlen($get_temp_sessions_row->start1) == 0) {
			
					$days_away_array[] = $theDate;
			
				} else {
					
					if ($show_all_appointments == 'yes') {
		
						if (in_array($theDate, $days_array) && strlen($get_temp_sessions_row->start1) > 0) {
				
							$startTime = getSeconds($get_temp_sessions_row->start1);
							$endTime = getSeconds($get_temp_sessions_row->end1);
							
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
								
							$startTime = getSeconds($get_temp_sessions_row->start2);
							$endTime = getSeconds($get_temp_sessions_row->end2);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
				
							$startTime = getSeconds($get_temp_sessions_row->start3);
							$endTime = getSeconds($get_temp_sessions_row->end3);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start4);
							$endTime = getSeconds($get_temp_sessions_row->end4);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start5);
							$endTime = getSeconds($get_temp_sessions_row->end5);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start6);
							$endTime = getSeconds($get_temp_sessions_row->end6);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
				
						}
						
					}
				
				}
			
			}

		}
		
if ($show_all_appointments == 'yes') {

	$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] = $doctor_id
	  AND [ApptBookID] = $appointment_book_id";

		$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);
	
		while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {
			
					$startTime = getSeconds($get_sessions_row->start1);
					$endTime = getSeconds($get_sessions_row->end1);
			
				if (in_array($get_sessions_row->DayOfWk, $session_day_number_array)) {
				
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
							
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
							
						}
							
						$startTime = getSeconds($get_sessions_row->start2);
						$endTime = getSeconds($get_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_sessions_row->start3);
						$endTime = getSeconds($get_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start4);
						$endTime = getSeconds($get_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start5);
						$endTime = getSeconds($get_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start6);
						$endTime = getSeconds($get_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
					}
		
				}
		
		for ($i=0;$i<count($sessions_array);$i++) {
				
			$day = (int) $sessions_array[$i]->DayOfWeek;

			$s = $sessions_array[$i]->StartTime;

			$e =  $sessions_array[$i]->EndTime;

			$l = $sessions_array[$i]->Length;

			while ($s < $e) {

				for ($d=0;$d<count($session_day_array);$d++) {
	
						if ($day == (int) $session_day_array[$d]['session_day'] && !in_array($session_day_array[$d]['session_date'], $days_away_array)) {
	
							$Appointment = new stdClass();
	
							$Appointment->ApptID = -1;
	
							$Appointment->PractitionerID = $doctor_id;
	
							$Appointment->PatientID = 0;
	
							$elapsed_hours = floor ($s / 3600);
	
							$remaining = $s - ($elapsed_hours * 3600);
	
							$elapsed_minutes = floor ($remaining / 60);
	
							$remaining = $remaining - ($elapsed_minutes * 60);
	
							if ($elapsed_hours < 10)
	
							{
	
								$hours = "0" . $elapsed_hours;
	
							}
	
							else
	
							{
	
								$hours = $elapsed_hours;
	
							}
	
							if ($elapsed_minutes < 10)
	
							{
	
								$minutes = "0" . $elapsed_minutes;
	
							}
	
							else
	
							{
	
								$minutes = $elapsed_minutes;
	
							}
	
							$Appointment->When = $session_day_array[$d]['session_date'] . " " . $hours . ":" . $minutes . ":00.000";
							$Appointment->Length = $l;
							
							$apptLength = $Appointment->Length / 60;
							$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
							
							$appointments[] = $Appointment;
		
						}
	
					}		
					
					$s += $l;
				}
	
			}

	
			$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_date)";
	
			$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);
	
			$booked_appointments = array();
	
				while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {
	
					$Booked = new stdClass();
	
					$Booked->ApptID = $booked_appointments_row->ApptID;
	
					$Booked->PractitionerID = $booked_appointments_row->PractitionerID;
	
					$Booked->Descrip = $booked_appointments_row->Descrip;
	
					$Booked->When = $booked_appointments_row->When;
	
					$booked_appointments[] = $Booked;
	
				}
	
	
			for ($a=0;$a<count($appointments);$a++) {
	
				for ($i=0;$i<count($booked_appointments);$i++) {
	
						if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) {
							
							if (in_array(trim($booked_appointments[$i]->Descrip), $patient_array)) {
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;
	
							} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') {
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$apptLength = $appointments[$a]->Length / 60;
								$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';
	
							} else {
	
								$appointments[$a]->Descrip = 'Unavailable';

	
							}
							
						} 
	
					}
	
				}
			}
	
			
	
	if ($user_type == "patient") {
		
		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_date) and (LOWER([Descrip]) = 'internet' or [Descrip] in ($patient_description)) and [PractitionerID] = $doctor_id";		
	
	} else {
	
		$patient_description .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_date) and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 'internetwc' or [Descrip] LIKE '$patient_description') and [PractitionerID] = $doctor_id";
	
	}
	
			$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);
	
			while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {
	
				$Appointment = new StdClass();
				$Appointment->ApptID = (int) $not_all_appts_row->ApptID;
				$Appointment->PractitionerID = (int) $not_all_appts_row->PractitionerID;
				if (strtolower(trim($not_all_appts_row->Descrip)) == 'internet') {
					$apptLength = $appointment_length / 60;
					$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
				} else {
					$Appointment->Descrip = trim($not_all_appts_row->Descrip);
				}
				$Appointment->When = trim($not_all_appts_row->When);
				$booked_date_array = explode(' ', $Appointment->When);
	
					if (!in_array($booked_date_array[0], $days_away_array)) {
						
						
							
							$appointments[] = $Appointment;
					
					}
				}	
		
			
	echo json_encode($appointments);
}

if(isset($_POST['getAllDayAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_string = $item['doctor_string'];
		$doctor_book_string = $item['doctor_book_string'];
		$doctor_length_string = $item['doctor_length_string'];
		$all_appts_doctor_string = $item['all_appts_doctor_string'];
		$all_appts_book_string = $item['all_appts_book_string'];
		$all_appts_length_string = $item['all_appts_length_string'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$user_type = $item['user_type'];
		
	}

$doctor_id_array = explode (',',$all_appts_doctor_string);
$doctor_book_array = explode (',',$doctor_book_string);
$doctor_length_array = explode (',',$all_appts_length_string);

$patient_array = explode (',',$patient_string);

for ($i=0;$i<count($doctor_id_array);$i++) {

	$doctor_id_array[$i] = (int) $doctor_id_array[$i];

}

for ($i=0;$i<count($doctor_book_array);$i++) {

	$doctor_book_array[$i] = (int) $doctor_book_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$doctor_length_array[$i] = (int) $doctor_length_array[$i];

}

$all_doctors = '';
$all_book_string = '';
$all_length_doctors = '';

if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $doctor_string .','.$all_appts_doctor_string;
	$all_book_string = $doctor_book_string .','.$all_appts_book_string;
	$all_length_doctors = $doctor_length_string .','.$all_appts_length_string;
	
} else if (strlen($doctor_string) == 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $all_appts_doctor_string;
	$all_book_string = $all_appts_book_string;
	$all_length_doctors = $all_appts_length_string;
	
} else if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) == 0) {
	
	$all_doctors = $doctor_string;
	$all_book_string = $doctor_book_string;
	$all_length_doctors = $doctor_length_string;
}

$all_doctors_id_array = explode (',',$all_doctors);
$all_book_doctors_array = explode (',',$all_book_string);
$all_length_doctors_array = explode (',',$all_length_doctors);

for ($i=0;$i<count($all_doctors_id_array);$i++) {

	$all_doctors_id_array[$i] = (int) $all_doctors_id_array[$i];

}

for ($i=0;$i<count($all_book_doctors_array);$i++) {

	$all_book_doctors_array[$i] = (int) $all_book_doctors_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$all_length_doctors_array[$i] = (int) $all_length_doctors_array[$i];

}

$session_day = intval(date('N', strtotime($appt_date)));

$session_day ++;

if ($session_day > 7) {

	$session_day -= 7;

}

$appointments = array();
$days_away_array = array();
$sessions_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
  ,[PractitionerID]
  ,[ApptBookID]
  ,[Date] as theDate
  ,$session_day as DayOfWk
  ,[1Start] as start1
  ,[1End] as end1
  ,[2Start] as start2
  ,[2End] as end2
  ,[3Start] as start3
  ,[3End] as end3
  ,[4Start] as start4
  ,[4End] as end4
  ,[5Start] as start5
  ,[5End] as end5
  ,[6Start] as start6
  ,[6End] as end6
FROM [HCN].[dbo].[TEMP_SESS]
WHERE [PractitionerID] IN ($all_doctors)
AND [ApptBookID] IN ($all_book_string)
AND CONVERT(CHAR(10),[Date],120) = '$appt_date'
ORDER BY PractitionerID, DayOfWk";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {

	$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
	$theDate = $session_date_array[0];

	if ($theDate == $appt_date && strlen($get_temp_sessions_row->start1) == 0) {

		$days_away_array[] = (int) $get_temp_sessions_row->PractitionerID;

	} else {
		
		if (strlen($all_appts_doctor_string) > 0) {

			if ($theDate == $appt_date && strlen($get_temp_sessions_row->start1) > 0) {
	
				$startTime = getSeconds($get_temp_sessions_row->start1);
				$endTime = getSeconds($get_temp_sessions_row->end1);
				$dr_id = (int) $get_temp_sessions_row->PractitionerID;
				
				for($l=0;$l<count($doctor_id_array);$l++) {
					
					if ($doctor_id_array[$l] == $dr_id) {
						
						$appt_length = $doctor_length_array[$l];
						
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
						$Session = new StdClass();
						$Session->UserID = $dr_id;
						$Session->theDate = $theDate;
						$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
						$Session->StartTime = $startTime;
						$Session->EndTime = $endTime;
						$Session->Length = $appt_length;
						$sessions_array[] = $Session;
		
						}
							
						$startTime = getSeconds($get_temp_sessions_row->start2);
						$endTime = getSeconds($get_temp_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_temp_sessions_row->start3);
						$endTime = getSeconds($get_temp_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start4);
						$endTime = getSeconds($get_temp_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start5);
						$endTime = getSeconds($get_temp_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start6);
						$endTime = getSeconds($get_temp_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
					}
				}
		
			}
		
		}
	}
}

		if (strlen($all_appts_doctor_string) > 0) {

$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] IN ($all_appts_doctor_string)
	  AND [ApptBookID] IN ($all_appts_book_string)
	  AND [DayOfWk] = $session_day";

$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);


	while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {

			if ($get_sessions_row->DayOfWk == $session_day && !in_array($get_sessions_row->PractitionerID, $days_away_array)) {
	
				$startTime = getSeconds($get_sessions_row->start1);
				$endTime = getSeconds($get_sessions_row->end1);
				$dr_id = (int) $get_sessions_row->PractitionerID;
				
				for($l=0;$l<count($doctor_id_array);$l++) {
					
					if ($doctor_id_array[$l] == $dr_id) {
						
						$appt_length = $doctor_length_array[$l];
						
					}
				}
				
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
					
				$startTime = getSeconds($get_sessions_row->start2);
				$endTime = getSeconds($get_sessions_row->end2);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
	
				$startTime = getSeconds($get_sessions_row->start3);
				$endTime = getSeconds($get_sessions_row->end3);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start4);
				$endTime = getSeconds($get_sessions_row->end4);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start5);
				$endTime = getSeconds($get_sessions_row->end5);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start6);
				$endTime = getSeconds($get_sessions_row->end6);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
	
			}

		}

	}	
	
	if (strlen($all_appts_doctor_string) > 0) {

		for ($i=0;$i<count($sessions_array);$i++) {

			$dr_id = $sessions_array[$i]->UserID;

			$day = $sessions_array[$i]->DayOfWeek;
			
			if (!in_array($dr_id, $days_away_array)) {

				if ($day == $session_day) {
	
					$s = $sessions_array[$i]->StartTime;
	
					$e =  $sessions_array[$i]->EndTime;
	
					$l = $sessions_array[$i]->Length;
			
				while ($s < $e) {
	
					$Appointment = new stdClass();
	
					$Appointment->ApptID = -1;
	
					$Appointment->PractitionerID = $dr_id;
	
					$Appointment->PatientID = 0;
	
					$elapsed_hours = floor ($s / 3600);
	
					$remaining = $s - ($elapsed_hours * 3600);
	
					$elapsed_minutes = floor ($remaining / 60);
	
					$remaining = $remaining - ($elapsed_minutes * 60);
	
					if ($elapsed_hours < 10)
	
					{
	
						$hours = "0" . $elapsed_hours;
	
					}
	
					else
	
					{
	
						$hours = $elapsed_hours;
	
					}
	
					if ($elapsed_minutes < 10)
	
					{
	
						$minutes = "0" . $elapsed_minutes;
	
					}
	
					else
	
					{
	
						$minutes = $elapsed_minutes;
	
					}
	
					$Appointment->When = $appt_date . " " . $hours . ":" . $minutes . ":00.000";
					$Appointment->Length = $l;
	
					if (in_array($dr_id, $days_away_array)) {
	
						$Appointment->Descrip = 'Unavailable';
	
					} else {
						
						$apptLength = $Appointment->Length / 60;
						$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
						
					}
	
					$appointments[] = $Appointment;
					
					$s += $l;
	
					}		
	
				}
			
			}

		}
		
$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and [PractitionerID] IN ($all_appts_doctor_string)";


		$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);

		$booked_appointments = array();

			while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {

				$Booked = new stdClass();

				$Booked->ApptID = $booked_appointments_row->ApptID;

				$Booked->PractitionerID = $booked_appointments_row->PractitionerID;

				$Booked->Descrip = $booked_appointments_row->Descrip;

				$Booked->When = $booked_appointments_row->When;

				$booked_appointments[] = $Booked;

			}


		for ($a=0;$a<count($appointments);$a++) {

			for ($i=0;$i<count($booked_appointments);$i++) {

					if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) {
						
						if (in_array(trim($booked_appointments[$i]->Descrip), $patient_array)) {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;

						} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$apptLength = $appointments[$a]->Length / 60;
							$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';

						} else {

							$appointments[$a]->Descrip = 'Unavailable';


						}
						
					} 

				}

			}
		
	}
	
	if ($user_type == "patient") {
		
		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and (LOWER([Descrip]) = 'internet' or [Descrip] in ($patient_description)) and [PractitionerID] in ($all_doctors)";
		
	} else {
	
		$patient_description .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 'internetwc' or [Descrip] LIKE '$patient_description') and [PractitionerID] in ($all_doctors)";
	
	}

		$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);

		while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $not_all_appts_row->ApptID;

			$dr_id = (int) $not_all_appts_row->PractitionerID;
			$appt_book_id = (int) $not_all_appts_row->ApptBookID;
			
			for($l=0;$l<count($all_doctors_id_array);$l++) {
					
				if ($all_doctors_id_array[$l] == $dr_id) {
					
					$dr_book_id = $all_book_doctors_array[$l];
					$appt_length = $all_length_doctors_array[$l];
					
				}
				
			}

			$Appointment->PractitionerID = $dr_id;
			if (trim($not_all_appts_row->Descrip) == 'Internet' || trim($not_all_appts_row->Descrip) == 'Internetwc') {
				$apptLength = $appt_length / 60;
				$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
			} else {
				$Appointment->Descrip = trim($not_all_appts_row->Descrip);
			}
			
			$Appointment->When = trim($not_all_appts_row->When);
			$Appointment->Length = $appt_length;
			
				if (!in_array($Appointment->PractitionerID, $days_away_array) && $appt_book_id == $dr_book_id) {
					
					$appointments[] = $Appointment;
		
				}

			}
	
	$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
		$orderby = "PractitionerID"; //change this to whatever key you want from the array
		
		if(count($appointments) > 0) {
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
		}
	
	
		
	echo json_encode($appointments);

}

if(isset($_POST['getDayAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_day = $item['appt_day'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$appointment_description = $item['appointment_description'];
		$user_type = $item['user_type'];
	}

	if ($user_type == "patient") {
	
 		$sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_day' and (LOWER([Descrip]) = 'internet' or [Descrip] in ($patient_description))";
		
	} else {
	
		$patient_description .= '%';
		$sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_day' and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 'internetwc' or [Descrip] like '$patient_description')";
	}
	 
	$rs = odbc_exec($Connect,$sql) or die($sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $row->ApptID;
			$Appointment->PractitionerID = (int) $row->PractitionerID;
			if (trim($row->Descrip) == 'Internet') {
				$Appointment->Descrip =  'Available';
			} else {
				$Appointment->Descrip = trim($row->Descrip);
			}
			$Appointment->When = trim($row->When);
			$p[] = $Appointment;
		}

	echo json_encode($p);
	
}

if(isset($_POST['getDoctorApptID']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$doctor_id = $item['doctor_id'];
	}
	
 	$sql = "SELECT     PractitionerID, Descrip
FROM         APPT
WHERE     (Descrip = '$doctor_id')";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$DoctorApptIDObject = new StdClass();;
			$DoctorApptIDObject->DoctorApptID = (int) $row->PractitionerID;
			$DoctorApptIDObject->DoctorID = (int) $doctor_id;
			$p[] = $DoctorApptIDObject;
		}

	echo json_encode($p);
	
}

if (isset($_POST['setDoctorAsOnline']))

{

	$jsonString = urldecode($_POST['jsonSendData']);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_practitioner_id = $item['appt_practitioner_id'];
		$doctor_id = $item['doctor_id'];
	}

	$sql = "insert into web_doctors (doctor_id, appt_practitioner_id) values ($doctor_id, $appt_practitioner_id)";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
	$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The selected doctor is now available to be used online."; 
				$StringResult->heading = "Doctor Available for Online Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The selected doctor is not available to be used online."; 
				$StringResult->heading = "Doctor Available for Online Failed";
				$StringResult->extra_data = 0;
			
			} 
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
}

if (isset($_POST['getApptSetUp']))

{

 	$sql = "SELECT TimeSlotSize, StartTime, EndTime FROM APPTBOOKSET";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$ApptSetUp = new StdClass();
			$ApptSetUp->TimeSlotSize = (int) $row->TimeSlotSize;
			$ApptSetUp->StartTime = trim($row->StartTime);
			$ApptSetUp->EndTime = trim($row->EndTime);
			$p[] = $ApptSetUp;
		}

	echo json_encode($p);
	
}

if(isset($_POST['setDoctorOnlineStatus']))
{

	$jsonString = urldecode($_POST['jsonSendData']);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_practitioner_id = $item['appt_practitioner_id'];
		$online_status = $item['online_status'];
	}

	$sql = "UPDATE    web_doctors
SET              online_status = '$online_status'
WHERE     (appt_practitioner_id = '$appt_practitioner_id')";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
	$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The selected doctor's online status has been changed."; 
				$StringResult->heading = "Doctor Online Status Change Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The selected doctor's online status could not be changed."; 
				$StringResult->heading = "Doctor Online Status Change Failed";
				$StringResult->extra_data = 0;
			
			}
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
}

if(isset($_POST['getPracSoftDoctors']))
{
	

 	$sql = "SELECT     DOCTOR.FullName, DOCTOR.DoctorID, DOCTOR.DoctorCode, DOCTOR.Location, CM_RESOURCE.RESOURCE_ID, CM_PRACTITIONER.PROVIDER_NO
FROM         CM_RESOURCE LEFT OUTER JOIN
                      DOCTOR ON DOCTOR.RESOURCE_ID = CM_RESOURCE.RESOURCE_ID LEFT OUTER JOIN
                      CM_PRACTITIONER ON CM_RESOURCE.RESOURCE_ID = CM_PRACTITIONER.RESOURCE_ID
WHERE     (CM_RESOURCE.RESOURCE_CODE = 'P') AND (DOCTOR.Deleted <> 'Y' OR
                      DOCTOR.Deleted IS NULL) AND (DOCTOR.DoctorID > 0)
ORDER BY DOCTOR.FullName, DOCTOR.Location";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->FullName);
			$doctor->data = (int) $row->DoctorID;
			$doctor->DoctorID = (int) $row->DoctorID;
			$doctor->DoctorCode = trim($row->DoctorCode);
			$doctor->FullName = trim($row->FullName);
			$doctor->location = trim($row->Location);
			$doctor->online_status = "";
			$p[] = $doctor;
		}

	echo json_encode($p);
	
}

if(isset($_POST['getDoctorsAdmin']))
{
	

 	$sql = "SELECT     PRAC.PractitionerID, PRAC.Practitioner, PRAC.Name, PRAC.ApptBookID
FROM         PRAC INNER JOIN
                      APPTBOOKSET ON PRAC.ApptBookID = APPTBOOKSET.ApptBookID
WHERE     (PRAC.Deleted = 'N') AND (APPTBOOKSET.Deleted = 'N')";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->Name);
			$doctor->data = (int) $row->PractitionerID;
			$doctor->doctor_id = (int) $row->PractitionerID;
			$doctor->doctor_code = trim($row->Practitioner);
			$doctor->doctor_name = trim($row->Name);
			$doctor->appointment_book_id = (int) $row->ApptBookID;
			$p[] = $doctor;
		}

	echo json_encode($p);
	
}

if(isset($_POST['getDoctors']))
{
	
	$include_surgery_id = $_POST['include_surgery_id'];
	
	if ($include_surgery_id == 'yes') {
		
		$sql = "SELECT web_doctors.appt_practitioner_id as DoctorID, web_doctors.online_status, web_doctors.surgery_id, DOCTOR.DoctorCode, DOCTOR.FullName FROM DOCTOR INNER JOIN web_doctors ON DOCTOR.DoctorID = web_doctors.doctor_id where web_doctors.online_status = 'yes'";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->FullName);
			$doctor->data = (int) $row->DoctorID;
			$doctor->DoctorID = (int) $row->DoctorID;
			$doctor->surgery_id = (int) $row->surgery_id;
			$doctor->DoctorCode = trim($row->DoctorCode);
			$doctor->FullName = trim($row->FullName);
			$doctor->online_status = trim($row->online_status);
			$p[] = $doctor;
		}
		
	} else {
		
		$sql = "SELECT web_doctors.appt_practitioner_id as DoctorID, web_doctors.online_status, DOCTOR.DoctorCode, DOCTOR.FullName FROM DOCTOR INNER JOIN web_doctors ON DOCTOR.DoctorID = web_doctors.doctor_id where web_doctors.online_status = 'yes'";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->FullName);
			$doctor->data = (int) $row->DoctorID;
			$doctor->DoctorID = (int) $row->DoctorID;
			$doctor->DoctorCode = trim($row->DoctorCode);
			$doctor->FullName = trim($row->FullName);
			$doctor->online_status = trim($row->online_status);
			$p[] = $doctor;
		}
		
	}
	
	echo json_encode($p);
	
}

if (isset($_POST['getUsers']))
{

		$which_users = $_POST['which_users'];
		
		if ($which_users == "all") {
		
			$sql = "SELECT * from web_users where user_type <> 'admin' order by user_type, company_name, last_name, first_name";
			
		} else if ($which_users == "active") {
		
			$sql = "SELECT * from web_users where user_type <> 'admin' and user_status = 'active' order by user_type, company_name, last_name, first_name";
		
		} else if ($which_users == "inactive") {
		
			$sql = "SELECT * from web_users where user_type <> 'admin' and user_status = 'inactive' order by user_type, company_name, last_name, first_name";
		
		}
		
		$rs = odbc_exec($Connect,$sql);
		 
		$p = array();
		
		while ($row = odbc_fetch_object($rs)) 
		
		{
			$User = new StdClass();
			$User->user_id = (int) $row->user_id;
			$User->company_name = trim($row->company_name);
			$User->first_name = trim($row->first_name);
			$User->middle_name = trim($row->middle_name);
			$User->last_name = trim($row->last_name);
			$User->dob = trim($row->dob);
			$User->username = trim($row->username);
			$User->password = trim($row->password);
			$User->family_members = trim($row->family_members);
			$User->number_street = trim($row->number_street);
			$User->town_suburb = trim($row->town_suburb);
			$User->state = trim($row->state);
			$User->postcode = trim($row->postcode);
			$User->phone = trim($row->phone);
			$User->mobile = trim($row->mobile);
			$User->email = trim($row->email);
			$User->account_id = trim($row->account_id);
			$User->user_type = trim($row->user_type);
			$User->user_status = trim($row->user_status);
            $p[] = $User;

		}
		
		echo json_encode($p);
	
	}
	
if (isset($_POST['login']))
{
		
		$username = $_POST['username'];
		$password = $_POST['password'];

		$sql = "SELECT * from web_users where username = '".$username."' and password = '".$password."' and user_status = 'active'";
		
		$rs = odbc_exec($Connect,$sql);
		 
		$p = array();
		
		while ($row = odbc_fetch_object($rs)) 
		
		{
			$User = new StdClass();
			$User->user_id = (int) $row->user_id;
			$User->company_name = trim($row->company_name);
			$User->first_name = trim($row->first_name);
			$User->middle_name = trim($row->middle_name);
			$User->last_name = trim($row->last_name);
			$User->dob = trim($row->dob);
			$User->username = trim($row->username);
			$User->password = trim($row->password);
			$User->family_members = trim($row->family_members);
			$User->number_street = trim($row->number_street);
			$User->town_suburb = trim($row->town_suburb);
			$User->state = trim($row->state);
			$User->postcode = trim($row->postcode);
			$User->phone = trim($row->phone);
			$User->mobile = trim($row->mobile);
			$User->email = trim($row->email);
			$User->account_id = trim($row->account_id);
			$User->user_type = trim($row->user_type);
            $p[] = $User;

		}
		
		echo json_encode($p);
	
	}
	
	if (isset($_POST['addUser']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$company_name = str_replace("'", "''", $item['company_name']);
		$first_name = str_replace("'", "''", $item['first_name']);
		$middle_name = str_replace("'", "''", $item['middle_name']);
		$last_name = str_replace("'", "''", $item['last_name']);
		$dob = str_replace("'", "''", $item['dob']);
		$username = str_replace("'", "''", $item['username']);
		$password = str_replace("'", "''", $item['password']);
		$family_members = str_replace("'", "''", $item['family_members']);
		$number_street = str_replace("'", "''", $item['number_street']);
		$town_suburb = str_replace("'", "''", $item['town_suburb']);
		$state = str_replace("'", "''", $item['state']);
		$postcode = str_replace("'", "''", $item['postcode']);
		$phone = str_replace("'", "''", $item['phone']);
		$mobile = str_replace("'", "''", $item['mobile']);
		$email = str_replace("'", "''", $item['email']);
		$user_type = str_replace("'", "''", $item['user_type']);
		
		}
		
		$sql = "SELECT * from web_users where username = '$username' or email = '$email'";
		
		$sql2 = "insert into web_users (company_name, first_name, middle_name, last_name, dob, username, password, family_members, number_street, town_suburb, state, postcode, phone, mobile, email, user_type, user_status) values ('$company_name', '$first_name', '$middle_name', '$last_name', '$dob', '$username', '$password', '$family_members', '$number_street', '$town_suburb', '$state', '$postcode', '$phone', '$mobile', '$email', '$user_type', 'active') ";
		
		$rs = odbc_exec($Connect,$sql);
		
		$p = array();
		
		$StringResult = new StdClass();
		
		if (odbc_num_rows($rs) > 0) {
	
			$StringResult->result_string = "This username and/or email is in use."; 
			$StringResult->heading = "Username and/or Email Unavailable";
			$StringResult->extra_data = 0;
		
		} else {
		
			$rs2 = odbc_exec($Connect,$sql2);
			
			if ($rs2) {
				
				$StringResult->result_string = "Your ID has been registered."; 
				$StringResult->heading = "New ID Successful";
				$StringResult->extra_data = 0;
				
				if ($user_type == "employer") {
				
					$mail             = new PHPMailer();

					$body             = $first_name." ".$last_name." of ".$company_name.",
				
Has registered an employer account.

".$first_name." ".$last_name."'s email address is:

".$email."
				
";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port; // set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;        // SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Employer Registration";
		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				$mail->Body = $body;
				$address = $config_clinic_email;
				$mail->AddAddress($address);
				$mail->Send();
				}
			
			} else {
	
				
				$StringResult->result_string = "Your ID could not be registered."; 
				$StringResult->heading = "New ID Failed";
				$StringResult->extra_data = 0;
			
			}
			
		}
		
		$p[] = $StringResult;

		echo json_encode($p);
	
	}
	
	if (isset($_POST['setPassword']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$user_id = $item['user_id'];
		$password = str_replace("'", "''", $item['password']);

		}
		
		$sql = "update web_users set password = '$password' where user_id = $user_id";
		
		$rs = odbc_exec($Connect,$sql);
		
		$p = array();
		
		$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The new password has been set."; 
				$StringResult->heading = "Set Password Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The new password could not be set."; 
				$StringResult->heading = "Set Password Failed";
				$StringResult->extra_data = 0;
			
			}
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
	}
	
	if (isset($_POST['editUser']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$user_id = $item['user_id'];
		$company_name = str_replace("'", "''", $item['company_name']);
		$first_name = str_replace("'", "''", $item['first_name']);
		$middle_name = str_replace("'", "''", $item['middle_name']);
		$last_name = str_replace("'", "''", $item['last_name']);
		$dob = str_replace("'", "''", $item['dob']);
		$family_members = str_replace("'", "''", $item['family_members']);
		$number_street = str_replace("'", "''", $item['number_street']);
		$town_suburb = str_replace("'", "''", $item['town_suburb']);
		$state = str_replace("'", "''", $item['state']);
		$postcode = str_replace("'", "''", $item['postcode']);
		$phone = str_replace("'", "''", $item['phone']);
		$mobile = str_replace("'", "''", $item['mobile']);
		$email = str_replace("'", "''", $item['email']);
		
		}
		
		$sql = "update web_users set company_name = '$company_name', first_name = '$first_name', middle_name = '$middle_name', last_name = '$last_name', dob = '$dob', family_members = '$family_members', number_street = '$number_street', town_suburb = '$town_suburb', state = '$state', postcode = '$postcode', phone = '$phone', mobile = '$mobile', email = '$email' where user_id = $user_id";
		
		$rs = odbc_exec($Connect,$sql);
		
		$p = array();
		
		$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The account has been edited."; 
				$StringResult->heading = "Edit Account Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The account could not be edited."; 
				$StringResult->heading = "Edit Account Failed";
				$StringResult->extra_data = 0;
			
			}
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
	}
	

	if (isset($_POST['deleteUser']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
			$user_id = $item['user_id'];
		
		}
		
		$sql = "delete from web_users where user_id = $user_id";
		
		$rs = odbc_exec($Connect,$sql);
		
		$p = array();
		
		$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The selected user account has been deleted."; 
				$StringResult->heading = "User Deleted Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The selected user account could not be deleted."; 
				$StringResult->heading = "User Deleted Failed";
				$StringResult->extra_data = 0;
			
			}
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
	}
	
	if (isset($_POST['setUserStatus']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
			$user_id = $item['user_id'];
			$user_status = $item['user_status'];

		}
		
		$sql = "update web_users set user_status = '$user_status' where user_id = $user_id";
		
		$rs = odbc_exec($Connect,$sql);
		
		$p = array();
		
		$StringResult = new StdClass();

			
			if ($rs) {
				
				$StringResult->result_string = "The selected user's status has been set as $user_status."; 
				$StringResult->heading = "Set User Status Successful";
				$StringResult->extra_data = 0;
			
			} else {
	
				
				$StringResult->result_string = "The selected user's status has not been set as $user_status."; 
				$StringResult->heading = "Set User Status Failed";
				$StringResult->extra_data = 0;
			
			}
			
		 $p[] = $StringResult;

		echo json_encode($p);
	
	}
	
	
	if (isset($_POST['retrievePassword']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$email = $item['email'];

		}
		
		$p = array();
		
		$StringResult = new StdClass();
		
		$sql = "select first_name, last_name, username, password, account_id from web_users where email = '$email'";
		
		$rs = odbc_exec($Connect,$sql);
		
		if (odbc_num_rows($rs) > 0) {
		
		
				
				while ($row = odbc_fetch_object($rs)) 
		
				{

					$first_name = $row->first_name;
					$last_name = $row->last_name;
					$username = $row->username;
					$password = $row->password;
					$account_id = $row->account_id;

				}
	
				/* $StringResult->result_string = "Your account has been activated.\n\nYour account ID is $activate_string\n\nPlease use your account id when you log in."; 
				$StringResult->heading = "Account Activation Successful";
				$StringResult->extra_data = 0; */
				
		
				
				$mail             = new PHPMailer();

				$body             = "Dear ".$first_name." ".$last_name.",
				
Your " . $config_clinic_name . " Online Appointment Account details are:

Your username is $username

Your password is $password

Click on the link below to activate your account.

You must log in with your username and password.

" . $config_docappointment_address . "/index.php?account_id=$account_id
				
				
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
				
";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port;       // set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;        // SMTP account password
				$mail->SetFrom($config_smtp_from);
				$mail->AddReplyTo($config_smtp_replyto);
				$mail->Subject    = $config_clinic_name . " Online Appointment Account";

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

				$mail->Body = $body;

				$address = $email;
				
				$mail->AddAddress($address);

					if(!$mail->Send()) {
				
  						$StringResult->result_string = "Your account details could not be sent.\r\rPlease try again."; 
						$StringResult->heading = "Password Retrieval Failed";
						$StringResult->extra_data = 0;
					
					} else {
				
  						$StringResult->result_string = "Your account details have been sent."; 
						$StringResult->heading = "Password Retrieval Succeeded";
						$StringResult->extra_data = 0;
					
					}
		
		} else {
		
			$StringResult->result_string = "There is no one in the database with this email address.\r\rPlease try again."; 
			$StringResult->heading = "User Not Found";
			$StringResult->extra_data = 0;
		
		}
		
		$p[] = $StringResult;

		echo json_encode($p);

	}
	
	
	if (isset($_POST['activateUser']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$activate_email = $item['activate_email'];
		$activate_string = $item['activate_string'];

		}
		
		
		$sql = "select first_name, last_name, username, password from web_users where email = '$activate_email'";
		
		$rs = odbc_exec($Connect,$sql);
		
		if (odbc_num_rows($rs) > 0) {
		
		
				
				while ($row = odbc_fetch_object($rs)) 
		
				{

					$first_name = $row->first_name;
					$last_name = $row->last_name;
					$username = $row->username;
					$password = $row->password;

				}
				
		
		$sql2 = "update web_users set account_id = '$activate_string', user_status = 'active' where email = '$activate_email'";
		
		$rs2 = odbc_exec($Connect,$sql2);
		
		$p = array();
		
		$StringResult = new StdClass();
		
				/* $StringResult->result_string = "Your account has been activated.\n\nYour account ID is $activate_string\n\nPlease use your account id when you log in."; 
				$StringResult->heading = "Account Activation Successful";
				$StringResult->extra_data = 0; */
				
		
				
				$mail             = new PHPMailer();

				$body             = "Dear ".$first_name." ".$last_name.",
				
Thank you for signing up for a " . $config_clinic_name . " Online Appointment Account.

Your username is $username

Your password is $password

Your account_id is $activate_string

Click on the link below to activate your account.

You must log in with your username and password.

" . $config_docappointment_address  . " /index.php?account_id=$activate_string
				
				
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
				
";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port;                    // set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;        // SMTP account password

				$mail->SetFrom($config_smtp_from);

				$mail->AddReplyTo($config_smtp_replyto);

				$mail->Subject    = $config_clinic_name . " Online Appointment Account";

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

				$mail->Body = $body;

				$address = $activate_email;
				
				$mail->AddAddress($address);

					if(!$mail->Send()) {
				
  						$StringResult->result_string = "Your account has not been activated."; 
						$StringResult->heading = "Account Activation Failed";
						$StringResult->extra_data = 0;
					
					} else {
				
  						$StringResult->result_string = "Your account has been activated.\r\rPlease check you email for your confirmation message.\r\rYou must folllow the link in the email to activate your account.\r\rIf you can't see the email check your spam or junk mail box."; 
						$StringResult->heading = "Account Activation Succeeded";
						$StringResult->extra_data = 0;
					
					}
		
			}
		
			
		$p[] = $StringResult;

		echo json_encode($p);

	}
	
	if (isset($_POST['approveEmployerAccount']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
  		$jsonString = str_replace("\\", "", $jsonString);
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$user_id = (int) $item['user_id'];
		$activate_email = $item['activate_email'];
		$activate_string = $item['activate_string'];

		}
		
		
		$sql = "select user_id, company_name, first_name, last_name, username, password from web_users where user_id = $user_id";
		
		$rs = odbc_exec($Connect,$sql);
		
		if (odbc_num_rows($rs) > 0) {
		
		
				
				while ($row = odbc_fetch_object($rs)) 
		
				{
					$company_name = $row->company_name;
					$first_name = $row->first_name;
					$last_name = $row->last_name;
					$username = $row->username;
					$password = $row->password;

				}
				
		
		$sql2 = "update web_users set account_id = '$activate_string', user_status = 'active' where user_id = $user_id";
		
		$rs2 = odbc_exec($Connect,$sql2);
		
		$p = array();
		
		$StringResult = new StdClass();
		
				/* $StringResult->result_string = "Your account has been activated.\n\nYour account ID is $activate_string\n\nPlease use your account id when you log in."; 
				$StringResult->heading = "Account Activation Successful";
				$StringResult->extra_data = 0; */
				
		
				
				$mail             = new PHPMailer();

				$body             = "Dear ".$first_name." ".$last_name." of ".$company_name.",
				
Your request for a " . $config_clinic_name . " Online Appointment Employer Account has been approved.

Your username is $username

Your password is $password

Your account_id is $activate_string

Click on the link below to activate your account.

You must log in with your username and password.

" . $config_docappointment_address . "/index.php?account_id=$activate_string
				
				
Many thanks,

The " . $config_clinic_name . " Online Appointment Team.		
				
";

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port; // set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;        // SMTP account password

				$mail->SetFrom($config_smtp_from);

				$mail->AddReplyTo($config_smtp_replyto);

				$mail->Subject    = $config_clinic_name . " Online Appointment Account";

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

				$mail->Body = $body;

				$address = $activate_email;
				
				$mail->AddAddress($address);

					if(!$mail->Send()) {
				
  						$StringResult->result_string = "The selected employer account has not been approved."; 
						$StringResult->heading = "Employer Account Approval Failed";
						$StringResult->extra_data = 0;
					
					} else {
				
  						$StringResult->result_string = "The selected employer account has been approved."; 
						$StringResult->heading = "Employer Account Approval Succeeded";
						$StringResult->extra_data = 0;
					
					}
		
			}
		
			
		$p[] = $StringResult;

		echo json_encode($p);

	}

if (isset($_POST['sendEmail']))
{
		
		$jsonString = urldecode($_POST['jsonSendData']);
		
  		$data = json_decode($jsonString, true);
  
		foreach ($data as $item) {
		
		$email_subject = $item['email_subject'];
		$email_message = $item['email_message'];
		$email_address = $item['email_address'];

		}
		
		$p = array();
		
		$StringResult = new StdClass();
		
				/* $StringResult->result_string = "Your account has been activated.\n\nYour account ID is $activate_string\n\nPlease use your account id when you log in."; 
				$StringResult->heading = "Account Activation Successful";
				$StringResult->extra_data = 0; */
				
		
				
				$mail             = new PHPMailer();

				$body             = $email_message;

				$mail->IsSMTP(); // telling the class to use SMTP
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
				$mail->SMTPAuth   = $config_smtp_auth;                  // enable SMTP authentication
				$mail->Host       = $config_smtp_host; // sets the SMTP server
				$mail->Port       = $config_smtp_port; // set the SMTP port for the GMAIL server
				$mail->Username   = $config_smtp_username; // SMTP account username
				$mail->Password   = $config_smtp_password;        // SMTP account password

				$mail->SetFrom($config_smtp_from);

				$mail->AddReplyTo($config_smtp_replyto);

				$mail->Subject    = $email_subject;

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

				$mail->Body = $body;

				$address = $email_address;
				
				$mail->AddAddress($address);

					if($mail->Send()) {
				
  						$StringResult->result_string = "Your email has been sent successfully."; 
						$StringResult->heading = "Email Sent";
						$StringResult->extra_data = 0;
					
					} else {
				
  						$StringResult->result_string = "Error! The email has not been sent."; 
						$StringResult->heading = "Email Not Sent";
						$StringResult->extra_data = 0;
					
					}
		
			
		$p[] = $StringResult;

		echo json_encode($p);

	}


?>