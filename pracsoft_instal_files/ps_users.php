<?php

require_once "configbp.php";

if (isset($_POST['getUsers']))
{

		$sql = "SELECT * from web_users where user_type <> 'admin' and user_status = 'active' order by user_type, company_name, last_name, first_name";
		
		$rs = odbc_exec($Connect,$sql);
		 
		$p = array();
		
		while ($row = odbc_fetch_object($rs)) 
		
		{
			$User = new StdClass();
			$User->company_name = trim($row->company_name);
			$User->first_name = trim($row->first_name);
			$User->last_name = trim($row->last_name);
			$User->dob = trim($row->dob);
			$User->username = trim($row->username);
			$User->password = trim($row->password);
			$User->family_members = trim($row->family_members);
			$User->mobile = trim($row->mobile);
			$User->email = trim($row->email);
			$User->user_type = trim($row->user_type);
			$User->user_status = trim($row->user_status);
            $p[] = $User;

		}
		
		echo json_encode($p);
	
	}



?>