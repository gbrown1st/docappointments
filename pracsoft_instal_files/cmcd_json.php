<?php

require_once "config.php";

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

$code_version = '150426';

function getSeconds($startTime) {
	
	if (strlen($startTime) == 0) {
		
		return 60;
		
	} else {
		
		$startTime = explode (' ', $startTime);
		$startTime = explode ('.', $startTime[1]);
		$hms = $startTime[0];
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60) + 60;
		$seconds += (intval($s));
		return $seconds;
	}
	
}

if (isset($_GET['showVersion']))

{
	
	echo $code_version;
	
}

if(isset($_POST['getExistingPatientDetails']))
{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$patient_id = (int) $item['patient_id'];
			
		}
  		
	$get_patient_sql = "SELECT CM_PATIENT.PATIENT_ID, CM_PATIENT.TITLE, CM_PATIENT.FIRST_NAME, CM_PATIENT.MIDDLE_NAME, 
CM_PATIENT.SURNAME, CM_PATIENT.KNOWN_AS,  
CM_PATIENT.GENDER_CODE, CM_PATIENT.ATSI, CM_PATIENT.RECEIVE_SMS, CM_PATIENT.DOB, CM_PATIENT.PHONE_HOME, CM_PATIENT.PHONE_WORK, CM_PATIENT.PHONE_MOBILE,  
CM_PATIENT.STREET_LINE_1, CM_PATIENT.STREET_LINE_2, CM_PATIENT.CITY, CM_PATIENT.POSTCODE, CM_PATIENT.P_STREET_LINE_1, 
CM_PATIENT.P_CITY,  
CM_PATIENT.P_POSTCODE, CM_PATIENT.PENSION_NO, CM_PATIENT.PENSION_CODE, CM_PATIENT.PENSION_EXPIRY_DATE, CM_PATIENT.DVA_NO, 
 
CM_PATIENT.MEDICARE_NO, CM_PATIENT.MEDICARE_INDEX, CM_PATIENT.MEDICARE_EXPIRY_DATE, CM_PATIENT_EMAIL.EMAIL_ADDRESS 
FROM         CM_PATIENT LEFT OUTER JOIN
                      CM_PATIENT_EMAIL ON CM_PATIENT.PATIENT_ID = CM_PATIENT_EMAIL.PATIENT_ID
WHERE     (CM_PATIENT.PATIENT_ID = $patient_id)";

  $get_patient_rs = odbc_exec($Connect,$get_patient_sql);
  $p = array();

			while ($row = odbc_fetch_object($get_patient_rs)) 

			{
				$Appointment = new StdClass();
				$Appointment->INTERNALID = (int) $row->PATIENT_ID;
				$Appointment->TITLECODE = $row->TITLE;
				$Appointment->FIRSTNAME = $row->FIRST_NAME;
				$Appointment->MIDDLENAME = $row->MIDDLE_NAME;
				$Appointment->SURNAME = $row->SURNAME;
				$Appointment->PREFERREDNAME = $row->KNOWN_AS;
				$Appointment->ADDRESS1 = $row->STREET_LINE_1;
				$Appointment->ADDRESS2 = $row->STREET_LINE_2;
				$Appointment->CITY = $row->CITY;
				$Appointment->POSTCODE = $row->POSTCODE;
				$Appointment->POSTALADDRESS = $row->P_STREET_LINE_1;
				$Appointment->POSTALCITY = $row->P_CITY;
				$Appointment->POSTALPOSTCODE = $row->P_POSTCODE;
				$Appointment->DOB = $row->DOB;
				$Appointment->SEXCODE = $row->GENDER_CODE;
				$Appointment->ATSI = $row->ATSI;
				$Appointment->SMS = $row->RECEIVE_SMS;
				$Appointment->HOMEPHONE = $row->PHONE_HOME;
				$Appointment->WORKPHONE = $row->PHONE_WORK;
				$Appointment->MOBILEPHONE = $row->PHONE_MOBILE;
				$Appointment->MEDICARENO = $row->MEDICARE_NO;
				$Appointment->MEDICARELINENO = $row->MEDICARE_INDEX;
				$Appointment->MEDICAREEXPIRY = $row->MEDICARE_EXPIRY_DATE;
				$Appointment->PENSIONCODE = $row->PENSION_CODE;
				$Appointment->PENSIONNO = $row->PENSION_NO;
				$Appointment->PENSIONEXPIRY = $row->PENSION_EXPIRY_DATE;
				$Appointment->DVACODE = $row->DVACODE;
				$Appointment->DVANO = $row->DVA_NO;
				$Appointment->EMAIL = $row->EMAIL_ADDRESS;
				
				$p[] = $Appointment;
			}

	echo json_encode($p);
}

if (isset($_POST['addPatient']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$title_code = $item['title_code'];
			$first_name = str_replace("'", "''", $item['first_name']);
			$middle_name = str_replace("'", "''", $item['middle_name']);
			$last_name = str_replace("'", "''", $item['last_name']);
			$preferred_name = str_replace("'", "''", $item['preferred_name']);
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$medicare_number = $item['medicare_number'];
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$atsic_code = $item['atsic_code'];
			$sms_code = $item['sms_code'];
	
		}

		$p = array();
		$StringResult = new stdClass();
		
		$addPatientSQL = "DECLARE	@return_value int

EXEC	@return_value = [HCN].[dbo].[usp_pm_InsertPatient] 
   @P_EXTERNAL_ID = NULL
  ,@P_PAYER_ID = NULL
  ,@P_ORIGIN_CODE = NULL
  ,@P_TITLE = '$title_code'
  ,@P_SALUTATION = NULL
  ,@P_FIRST_NAME = '$first_name'
  ,@P_MIDDLE_NAME = '$middle_name'
  ,@P_SURNAME = '$last_name'
  ,@P_KNOWN_AS = '$preferred_name'
  ,@P_ATSI = '$atsic_code'
  ,@P_GENDER_CODE = '$gender_code'
  ,@P_SEXUALITY_CODE = NULL
  ,@P_MARITAL_STATUS_CODE = NULL
  ,@P_STATUS_CODE = ''
  ,@P_DOB = '$dob'
  ,@P_DECEASED_DATE = NULL
  ,@P_PHONE_HOME = '$home_phone'
  ,@P_PHONE_WORK = '$work_phone'
  ,@P_PHONE_MOBILE = '$mobile_phone'
  ,@P_FAX = NULL
  ,@P_STREET_LINE_1 = '$address_1'
  ,@P_STREET_LINE_2 = '$address_2'
  ,@P_STREET_LINE_3 = NULL
  ,@P_CITY = '$city'
  ,@P_POSTCODE = '$postcode'
  ,@P_P_STREET_LINE_1 = '$postal_address'
  ,@P_P_STREET_LINE_2 = NULL
  ,@P_P_STREET_LINE_3 = NULL
  ,@P_P_CITY = '$title_code'
  ,@P_P_POSTCODE = '$title_code'
  ,@P_PREFERRED_DOCTOR = NULL
  ,@P_CHART_NO = NULL
  ,@P_PENSION_NO = '$pension_number'
  ,@P_PENSION_CODE = '$pension_code'
  ,@P_PENSION_EXPIRY_DATE = '$pension_expiry'
  ,@P_DVA_NO = '$dva_number'
  ,@P_SAFETY_NET_NO = NULL
  ,@P_REFERRING_DOCTOR = NULL
  ,@P_REFERRING_DATE = NULL
  ,@P_REFERRING_CODE = NULL
  ,@P_INSURANCE_COMPANY_ID = NULL
  ,@P_INSURANCE_NO = NULL
  ,@P_INSURANCE_TYPE = NULL
  ,@P_MEDICARE_NO = '$medicare_number'
  ,@P_MEDICARE_INDEX = '$medicare_line_number'
  ,@P_MEDICARE_EXPIRY_DATE = '$medicare_expiry'
  ,@P_EMAIL_ADDRESS = '$email'
  ,@P_STAMP_CREATED_USER_ID = 0
  ,@P_STAMP_CREATED_DATETIME = ''
  ,@P_STAMP_USER_ID = 0
  ,@P_STAMP_ACTION_CODE = 'A'
  ,@P_STAMP_DATETIME = ''

  select 'new_patient_id' = @return_value";

			$add_user_rs = odbc_exec($Connect,$addPatientSQL);
		
			if ($add_user_rs) {
				
				while ($add_user_row = odbc_fetch_object($add_user_rs)) 

					{
						$new_patient_id = (int) $row->new_patient_id;
					}
		
					$StringResult->result_string = "Your details have been saved."; 
					$StringResult->heading = "Patient Details Saved";
					$StringResult->extra_data = $new_patient_id;
				
			} else {

					$StringResult->result_string = "Your details could not be saved."; 
					$StringResult->heading = "Patient Details Failed";
					$StringResult->extra_data = "Line 223";

			}


		$p[] = $StringResult;

		echo json_encode($p);

	}
	
	if (isset($_POST['updateExistingPatient']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$patient_id = (int) $item['patient_id'];
			$title_code = $item['title_code'];
			$first_name = str_replace("'", "''", $item['first_name']);
			$middle_name = str_replace("'", "''", $item['middle_name']);
			$last_name = str_replace("'", "''", $item['last_name']);
			$preferred_name = str_replace("'", "''", $item['preferred_name']);
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$atsic_code = $item['atsic_code'];
			$medicare_number = $item['medicare_number'];
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$atsic_code = $item['atsic_code'];
			$sms_code = $item['sms_code'];
	
		}

		$p = array();
		$StringResult = new stdClass();
		
		$updatePatientSQL = "UPDATE CM_PATIENT SET
		TITLE = '$title_code', 
		FIRST_NAME = '$first_name',  
		MIDDLE_NAME = '$middle_name', 
		SURNAME = '$last_name',  
		KNOWN_AS = '$preferred_name',  
		GENDER_CODE = '$gender_code',
		DOB = '$dob',  
		PHONE_HOME = '$home_phone',  
		PHONE_WORK = '$work_phone',  
		PHONE_MOBILE = '$mobile_phone',  
		STREET_LINE_1 = '$address_1',  
		STREET_LINE_2 = '$address_2',  
		CITY = '$city',  
		POSTCODE = '$postcode',  
		P_STREET_LINE_1 = '$postal_address',  
		P_CITY = '$postal_city',  
		P_POSTCODE = '$postal_postcode',  
		PENSION_NO = '$pension_number',  
		PENSION_CODE = '$pension_code',  
		PENSION_EXPIRY_DATE = '$pension_expiry',  
		DVA_NO = '$dva_number',  
		MEDICARE_NO = '$medicare_number',  
		MEDICARE_INDEX = '$medicare_line_number',  
		MEDICARE_EXPIRY_DATE = '$medicare_expiry',
		ATSI = '$atsic_code', 
		RECEIVE_SMS = '$sms_code'
		
		WHERE PATIENT_ID = $patient_id";

			$update_user_rs = odbc_exec($Connect,$updatePatientSQL);
		
			if ($update_user_rs) {
				
				$getEmailSQL = "SELECT PATIENT_EMAIL_ID FROM CM_PATIENT_EMAIL
		WHERE PATIENT_ID = $patient_id
		AND EMAIL_TYPE = 'Home'
		AND STAMP_ACTION_CODE != 'D'
		ORDER BY PATIENT_EMAIL_ID";
		
				$getEmailSQL_rs = odbc_exec($Connect,$getEmailSQL);
				
				if( odbc_num_rows( $getEmailSQL_rs ) > 0) {
					
					
					while ($getEmailSQL_row = odbc_fetch_object($getEmailSQL_rs)) 

					{
						$patient_email_id = (int) $getEmailSQL_row->PATIENT_EMAIL_ID;
					}
					
					
					$updatePatientEmailSQL = "UPDATE CM_PATIENT_EMAIL
		SET EMAIL_ADDRESS = '$email',
			EMAIL_TYPE = 'Home',
			STAMP_ACTION_CODE = 'U',
			STAMP_USER_ID = 0,
			STAMP_DATETIME = GETDATE()
		WHERE PATIENT_EMAIL_ID = $patient_email_id";
					
					$update_user_email_rs = odbc_exec($Connect,$updatePatientEmailSQL);
					
					if ($update_user_email_rs) {
						
						$StringResult->result_string = "Your details have been updated."; 
						$StringResult->heading = "Update Patient Details Succeeded";
						$StringResult->extra_data = 0;
						
					} else {
	
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Update Patient Details Failed";
						$StringResult->extra_data = $updatePatientEmailSQL;
	
					}
					
				} else {
					
					$addPatientEmailSQL = "INSERT INTO [HCN].[dbo].[CM_PATIENT_EMAIL]
           	([PATIENT_ID]
	           ,[EMAIL_TYPE]
	           ,[EMAIL_ADDRESS]
	           ,[STAMP_USER_ID])
	     VALUES
	            ($patient_id
	            ,'Home'
	           ,''$email'
	           ,0)";
					
					$add_user_email_rs = odbc_exec($Connect,$addPatientEmailSQL);
					
					if ($add_user_email_rs) {
						
						$StringResult->result_string = "Your details have been updated."; 
						$StringResult->heading = "Update Patient Details Succeeded";
						$StringResult->extra_data = 0;
						
					} else {
	
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Update Patient Details Failed";
						$StringResult->extra_data = 0;
	
					}
					
				}
				
				
			} else {

					$StringResult->result_string = "Your details could not be saved."; 
					$StringResult->heading = "Update Patient Details Failed";
					$StringResult->extra_data = $updatePatientSQL;

			}

		$p[] = $StringResult;

		echo json_encode($p);

	}
	
if(isset($_POST['getApptDoctors']))

{

	$fromDate = strtotime('now');

	$today = date("Y-m-d",$fromDate);

	$sql = "SELECT DISTINCT PRAC.PractitionerID, PRAC.Practitioner, PRAC.Location, PRAC.Name

FROM         APPT INNER JOIN

                      PRAC ON APPT.PractitionerID = PRAC.PractitionerID

WHERE     (APPT.[When] >= DATEADD(HOUR, - 2, GETDATE())) AND (APPT.[When] <= DATEADD(HOUR, 2, GETDATE()))

ORDER BY PRAC.Location, PRAC.Name";

	$rs = odbc_exec($Connect,$sql);

	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerID = (int) $row->PractitionerID;
			$Appointment->DoctorID = (int) $row->PractitionerID;
			$Appointment->DoctorCode = $row->Practitioner;
			$Appointment->FullName = $row->Name;
			$Appointment->Location = $row->Location;
			$p[] = $Appointment;

		}

	echo json_encode($p);

} 

if(isset($_POST['addToWaitingRoom']))

{

	$first_name = strtoupper(trim($_POST['first_name']));

	$last_name = strtoupper(trim($_POST['last_name']));
	
	$dob = $_POST['dob'];
	
	$gender = strtoupper(trim($_POST['gender']));
	
	$gender_code = $gender[0];

	$doctor_id = $_POST['doctor_id'];

	$doctor_code = $_POST['doctor_code'];
	
	$p = array();

	$StringResult = new StdClass();
	
	$patient_sql = "SELECT     *

FROM         CM_PATIENT

WHERE     (UPPER(FIRST_NAME) = '$first_name') 

AND (UPPER(SURNAME) = '$last_name') 

AND (DOB = CONVERT(DATETIME, '$dob 00:00:00', 102))

AND GENDER_CODE = '$gender_code'";

	$patient_rs = odbc_exec($Connect,$patient_sql);

 	if( odbc_num_rows( $patient_rs ) == 1) {

		while ($patient_row = odbc_fetch_object($patient_rs)) {

			$PatientID = $patient_row->PATIENT_ID;
			$Ur_No = $patient_row->UR_NO;
			$Chart_No = '['.$patient_row->CHART_NO.']';
			$Address = $patient_row->STREET_LINE_1;
			$City = $patient_row->CITY;
			$Postcode = $patient_row->POSTCODE;
			$Phone = $patient_row->PHONE_HOME;
			$Mobile = $patient_row->PHONE_MOBILE;
			$last_updated = strtotime($patient_row->STAMP_DATETIME);

		}
		
		$last_six_months = strtotime("-6 months");

		$sql = "SELECT     ApptID, PractitionerID, PatientID, Descrip, [When]

FROM         APPT

WHERE     

	APPT.[Descrip] like '%$first_name%'

	AND APPT.[Descrip] like '%$last_name%'

	AND APPT.[When] >= DATEADD(HOUR,-2,getDate()) 

	AND APPT.[When] <= DATEADD(HOUR, 2 ,getDate())

	AND APPT.PractitionerID = $doctor_id";

	$rs = odbc_exec($Connect,$sql);

		if( odbc_num_rows( $rs ) > 0) {
		
			while ($row = odbc_fetch_object($rs)) {
				
					$ApptPatientID = (int) $row->PatientID;
					$ApptID = (int) $row->ApptID;
					$ApptTime = $row->When;
					$Descrip = $row->Descrip;
						
			}
		
			$display_order_sql = "Select IDENT_CURRENT('WaitRoom') + 1 as DISPLAY_ORDER";
		
			$display_order_rs = odbc_exec($Connect,$display_order_sql);
		
				while ($display_order_row = odbc_fetch_object($display_order_rs)) {
		
					$display_order = (int) $display_order_row->DISPLAY_ORDER;
		
				}
		
			if ($ApptPatientID == 0) {
		
						$pos = strrpos($Descrip, 'DOB:');
		
						if ($pos === false) {
		
							$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
							$StringResult->heading = "No Appointment Found";
							$StringResult->sql = $patient_sql;
		
						} else {
		
					$array_1 = explode(' [DOB: ', $Descrip);
					$array_2 = explode('] [Ph: ', $array_1[1]);
					
					$dob_array =  explode('/', $array_2[0]);
					
					$dob = $dob_array[2].'-'.$dob_array[1].'-'.$dob_array[0];
					$descrip_dob = $array_2[0];
					
								$new_descrip = "$last_name, $first_name $Chart_No, $Phone, $Mobile, $Address, $City, $Postcode $descrip_dob"; 
								$sql_wait = "insert into WAITROOM (PatientNo, 
DISPLAY_ORDER,[Type],ApptIDList,DrCode,DaysOverDue,ArriveTime,ApptTime) values ($PatientID, $display_order, 0, $ApptID, 
'$doctor_code', -9999, getDate(), '$ApptTime')";  
		
								$rs_wait = odbc_exec($Connect,$sql_wait);
		
								if ($rs_wait) {
		
									$sql_wait_appt = "update APPT set Flag = 2, TimeInWAITROOM = getDate(), PatientID = 
$PatientID, Ur_No = '$Ur_No', Descrip = '$new_descrip' where ApptID = $ApptID";  
									$rs_wait_appt = odbc_exec($Connect,$sql_wait_appt);
		
										if ($rs_wait_appt) {
		
											if ($last_six_months > $last_updated) {
						
												$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
											
											} else {
												
												$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nPlease take a seat."; 
												
											}
					 
											$StringResult->heading = "Your Arrival Has Been Noted"; 
											
		
										} else {
		
											if ($last_six_months > $last_updated) {
						
												$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
											
											} else {
												
												$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
												
											}
											
											$StringResult->heading = "No Appointment Found";
											$StringResult->sql = $patient_sql;
		
										}
		
								} else {
		
									$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
									$StringResult->heading = "No Appointment Found";
									$StringResult->sql = $patient_sql;
		
								}
		
						} 
		
			} else if ($ApptPatientID == $PatientID) {
		
				$sql_waitroom_check = "select * from WAITROOM where PatientNo = $PatientID";
				$rs_waitroom_check = odbc_exec($Connect,$sql_waitroom_check);
		
				if( odbc_num_rows( $rs_waitroom_check ) == 1) {
		
					if ($last_six_months > $last_updated) {
						
						$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
					
					} else {
						
						$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nPlease take a seat."; 
						
					}
		
					$StringResult->heading = "Your Arrival Has Been Noted"; 
		
				} else {
		
					$sql_wait = "insert into WAITROOM (PatientNo, 
DISPLAY_ORDER,[Type],ApptIDList,DrCode,DaysOverDue,ArriveTime,ApptTime) values ($PatientID, $display_order, 0, $ApptID, 
'$doctor_code', -9999, getDate(), '$ApptTime')";  
		
					$rs_wait = odbc_exec($Connect,$sql_wait);
		
								if ($rs_wait) {
		
									$sql_wait_appt = "update APPT set Flag = 2, TimeInWAITROOM = getDate() where ApptID = 
$ApptID";  
									$rs_wait_appt = odbc_exec($Connect,$sql_wait_appt);
		
										if ($rs_wait_appt) {
		
											if ($last_six_months > $last_updated) {
						
												$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
											
											} else {
												
												$StringResult->result_string = "The front desk has been advised of your arrival.\n\nPlease take a seat.";
												
											}
											  
											$StringResult->heading = "Your Arrival Has Been Noted"; 
		
										} else {
		
											$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
											$StringResult->heading = "No Appointment Found";
											$StringResult->sql = $patient_sql;
		
										}
		
								} else {
		
									$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
									$StringResult->heading = "No Appointment Found";
									$StringResult->sql = $patient_sql;
		
		
								}
		
				}
		
			}
		
		} else {
		
		
				$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
				$StringResult->heading = "No Appointment Found";
				$StringResult->sql = $patient_sql;
		
		}
						
	} else {
		
		$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		$StringResult->sql = $patient_sql;
		
	}

	
	$p[] = $StringResult;
	echo json_encode($p);

} 



if(isset($_POST['makePassword']))
{

  $length = 6;
  // start with a blank password
  $password = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
    
  // set up a counter
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($password, $char)) { 
      $password .= $char;
      $i++;
    }

  }
					
  // done!
	echo json_encode($password);

}

if(isset($_POST['makeNewAppointmentRequest']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$appt_date = $item['appt_date'];
		$delete_from_day = $item['delete_from_day'];
		$patient_description = trim($item['patient_description']);
		$doctor_id = $item['doctor_id'];
		$doctor_code = $item['doctor_code'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$appointment_book_id = $item['appointment_book_id'];
		$multiple_appointments = $item['multiple_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
	
	}

$existing_appt_warning = "";

	if ($multiple_appointments == "no") {
		
		$existing_appt_sql = "SELECT APPT.ApptID, APPT.PractitionerID, APPT.Descrip, PRAC.Name, APPT.[When]
FROM APPT INNER JOIN PRAC ON APPT.PractitionerID = PRAC.PractitionerID
WHERE ([Descrip] = '$patient_description' 
and CONVERT(CHAR(10),[When],120) >= '$delete_from_day')"; 
		$existing_appt_rs = odbc_exec($Connect,$existing_appt_sql);
		if( odbc_num_rows( $existing_appt_rs ) < 3) {
			while ($existing_appt_row = odbc_fetch_object($existing_appt_rs)) {
				$existing_appt_date_time = strtotime($existing_appt_row->When);
				$existing_appt = date("l jS \of F Y H:i a", $existing_appt_date_time);
				$existing_appt_descrip = $existing_appt_row->Descrip;
				$existing_appt_id = $existing_appt_row->ApptID;
				$dr_name = $existing_appt_row->Name;
				$existing_appt_warning = "WARNING!

The existing appointment for $existing_appt_descrip on $existing_appt with $dr_name was cancelled while making your new appointment.

Please make sure you cancel any existing appointments before making a new one. You can only have one online appointment per family member at a time.

An email notification has been sent alerting you to the cancellation.";

$delete_existing_appt_sql = "DELETE FROM APPT
WHERE ApptID = $existing_appt_id"; 
		$delete_existing_appt_rs = odbc_exec($Connect,$delete_existing_appt_sql);

			}
		}
	}

	$p = array();
		
	$StringResult = new StdClass();

	if ((int) $appt_id > 0) {

		$sql2 = "update APPT set [Descrip] = '$patient_description' WHERE ApptID = $appt_id";

	} else {

		$sql2 = "
	INSERT INTO [HCN].[dbo].[APPT]
           ([PractitionerID]
           ,[PatientID]
           ,[Ur_No]
           ,[Type]
           ,[Descrip]
           ,[When]
           ,[Flag]
           ,[User]
           ,[ApptBookID])
     VALUES
           ($doctor_id
           ,0
           ,''
           ,0
           ,'$patient_description'
           ,'$appt_date'
           ,16
           ,'$doctor_code'
           ,$appointment_book_id)";

	}
	
	  
	$rs2 = odbc_exec($Connect,$sql2);

		if ($rs2) {
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = $existing_appt_warning;
					
			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = 0;
			
			}

	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['cancelExistingAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$delete_from_day = $item['delete_from_day'];
		$appointment_description = $item['appointment_description'];
		$user_string = $item['user_string'];
		$user_type = $item['user_type'];
		$patient_description = $item['patient_description'];
		$doctor_id = $item['doctor_id'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
		
	}

	if ($show_all_appointments == 'yes') {

		$sql = "delete from APPT WHERE [ApptID] = $appt_id";

	} else {
		
		$sql = "update APPT set [Descrip] = 'Internet' WHERE ApptID = $appt_id";

	}
	

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
		
	$StringResult = new StdClass();
	
	if ($rs) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = 0;
				
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;
			
		}
	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['getAppointmentBooksRequest']))
{

	$sql = "SELECT     [ApptBookID], [TimeSlotSize], [StartTime], [EndTime], [Description]
FROM         APPTBOOKSET
WHERE     ([Deleted] = 'N')";

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$AppointmentBook = new StdClass();
			$AppointmentBook->ApptBookID = (int) $row->ApptBookID;
			$AppointmentBook->TimeSlotSize = (int) $row->TimeSlotSize;
			$AppointmentBook->Description = trim($row->Description);
			$AppointmentBook->StartTime = trim($row->StartTime);
			$AppointmentBook->EndTime = trim($row->EndTime);
			$p[] = $AppointmentBook;
		}

	echo json_encode($p);

}

if(isset($_POST['checkAppointmentAvailability']))

{

	$jsonString = urldecode($_POST['jsonSendData']);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_date = $item['appt_date'];
		$doctor_id = $item['doctor_id'];

	}

$booked_sql = "SELECT ApptID, Descrip
FROM         APPT
WHERE     ([When] = '$appt_date') AND (PractitionerID = $doctor_id)";

			$booked_array = array();

			$booked_rs = odbc_exec($Connect,$booked_sql);

			if (odbc_num_rows($booked_rs) == 0) {

				$Booked = new stdClass();

				$Booked->is_available = 0;

				$booked_array[] = $Booked;

			} else {

				while ($booked_row = odbc_fetch_object($booked_rs)) {

					if (trim($booked_row->Descrip) == 'Internet' || trim($booked_row->Descrip) == 'Internetwc') {

						$Booked = new stdClass();

						$Booked->is_available = 0;

						$booked_array[] = $Booked;

					} else {

						$Booked = new stdClass();

						$Booked->is_available = 1;

						$booked_array[] = $Booked;

					}

				}

			}


	echo json_encode($booked_array);

}

if(isset($_POST['getAllWeekAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_id = (int) $item['doctor_id'];
		$appointment_book_id = (int) $item['appointment_book_id'];
		$appointment_length = (int) $item['appointment_length'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$show_all_appointments = $item['show_all_appointments'];
		$user_type = $item['user_type'];
		
	}

$patient_array = explode (',',$patient_string);

$sql_patients = implode("','", $patient_array);
$sql_patients = "'".$sql_patients."'";

for ($i=0;$i<count($patient_array);$i++) {

	$patient_array[$i] = trim($patient_array[$i]);

}

$appt_date_cleared = str_replace("'","",$appt_date);

$days_array = explode (',',$appt_date_cleared);

$session_day_array = array();
$session_day_number_array = array();

for ($i=0;$i<count($days_array);$i++) {

	$session_day = intval(date('N', strtotime($days_array[$i])));

	$session_day ++;

	if ($session_day > 7) {

		$session_day -= 7;

	}

	$session_day_array[] = array("session_day" => $session_day, "session_date" => $days_array[$i]);
	$session_day_number_array[] = $session_day;
	
}
	
$appointments = array();
$days_away_array = array();
$sessions_array = array();
$temp_sessions_days_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
      ,[PractitionerID]
  	  ,[ApptBookID]
      ,[Date] as theDate
      ,0 as DayOfWk
      ,[1Start] as start1
      ,[1End] as end1
      ,[2Start] as start2
      ,[2End] as end2
      ,[3Start] as start3
      ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
  FROM [HCN].[dbo].[TEMP_SESS]
  WHERE [PractitionerID] = $doctor_id
  AND CONVERT(CHAR(10),[Date],120) in ($appt_date)
  ORDER BY Date";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

	if (odbc_num_rows($get_temp_sessions_rs) > 0) {

		while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {
			
			$session_appt_book_id = (int) $get_temp_sessions_row->ApptBookID;
			
			$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
			$theDate = $session_date_array[0];
			
			$temp_session_day = intval(date('N', strtotime($theDate)));

			$temp_session_day ++;
			
			if ($temp_session_day > 7) {
			
				$temp_session_day -= 7;
			
			}
					
				if (in_array($theDate, $days_array) && $get_temp_sessions_row->start1 === NULL) {
			
					$days_away_array[] = $theDate;
			
				} else {
					
					if ($show_all_appointments == 'yes') {
		
						if (in_array($theDate, $days_array) && $get_temp_sessions_row->start1 !== NULL) {
							
							$temp_sessions_days_array[] = $temp_session_day;
				
							$startTime = getSeconds($get_temp_sessions_row->start1);
							$endTime = getSeconds($get_temp_sessions_row->end1);
							
							if ($session_appt_book_id == $appointment_book_id) {
							
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
								
							$startTime = getSeconds($get_temp_sessions_row->start2);
							$endTime = getSeconds($get_temp_sessions_row->end2);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
				
							$startTime = getSeconds($get_temp_sessions_row->start3);
							$endTime = getSeconds($get_temp_sessions_row->end3);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start4);
							$endTime = getSeconds($get_temp_sessions_row->end4);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start5);
							$endTime = getSeconds($get_temp_sessions_row->end5);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start6);
							$endTime = getSeconds($get_temp_sessions_row->end6);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							}
				
						}
						
					}
				
				}
			
			}

		}
		
if ($show_all_appointments == 'yes') {

	$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
      ,[ApptBookID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] = $doctor_id
	  AND [1Start] IS NOT NULL
	  ORDER BY [ApptBookID], [DayOfWk]";

		$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);
	
		while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {
			
					$session_appt_book_id = (int) $get_sessions_row->ApptBookID;
					$startTime = getSeconds($get_sessions_row->start1);
					$endTime = getSeconds($get_sessions_row->end1);
			
				if (in_array($get_sessions_row->DayOfWk, $session_day_number_array) && 
!in_array($get_sessions_row->DayOfWk, $temp_sessions_days_array) && $session_appt_book_id == $appointment_book_id) { 
				
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
							
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
							
						}
							
						$startTime = getSeconds($get_sessions_row->start2);
						$endTime = getSeconds($get_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_sessions_row->start3);
						$endTime = getSeconds($get_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start4);
						$endTime = getSeconds($get_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start5);
						$endTime = getSeconds($get_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start6);
						$endTime = getSeconds($get_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
					}
		
				}

		
		for ($i=0;$i<count($sessions_array);$i++) {
			
			$session_type = $sessions_array[$i]->SessionType;
			
			$dr_id = $sessions_array[$i]->UserID;
			
			$appt_book_id = $sessions_array[$i]->ApptBookID;
			
			$dr_appt_book_id = $sessions_array[$i]->DoctorApptBookID;
				
			$day = (int) $sessions_array[$i]->DayOfWeek;

			$s = $sessions_array[$i]->StartTime;

			$e =  $sessions_array[$i]->EndTime;

			$l = $sessions_array[$i]->Length;

			while ($s < $e) {

				for ($d=0;$d<count($session_day_array);$d++) {
	
						if ($day == (int) $session_day_array[$d]['session_day'] && 
!in_array($session_day_array[$d]['session_date'], $days_away_array)) { 
	
							$Appointment = new stdClass();
							
							$Appointment->SessionType = $session_type;
	
							$Appointment->ApptID = -1;
	
							$Appointment->PractitionerID = $dr_id;
							
							$Appointment->ApptBookID = $appt_book_id;
							
							$Appointment->DoctorApptBookID = $dr_appt_book_id;
	
							$Appointment->PatientID = 0;
	
							$elapsed_hours = floor ($s / 3600);
	
							$remaining = $s - ($elapsed_hours * 3600);
	
							$elapsed_minutes = floor ($remaining / 60);
	
							$remaining = $remaining - ($elapsed_minutes * 60);
	
							if ($elapsed_hours < 10)
	
							{
	
								$hours = "0" . $elapsed_hours;
	
							}
	
							else
	
							{
	
								$hours = $elapsed_hours;
	
							}
	
							if ($elapsed_minutes < 10)
	
							{
	
								$minutes = "0" . $elapsed_minutes;
	
							}
	
							else
	
							{
	
								$minutes = $elapsed_minutes;
	
							}
	
							$Appointment->When = $session_day_array[$d]['session_date'] . " " . $hours . ":" . $minutes . 
":00.000"; 
							$Appointment->Length = $l;
							
							$apptLength = $Appointment->Length / 60;
							$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
							
							$appointments[] = $Appointment;
		
						}
	
					}		
					
					$s += $l;
				}
	
			}

	
			$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE 
CONVERT(CHAR(10),[When],120) in ($appt_date)"; 
	
			$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);
	
			$booked_appointments = array();
	
				while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {
	
					$Booked = new stdClass();
	
					$Booked->ApptID = $booked_appointments_row->ApptID;
	
					$Booked->PractitionerID = $booked_appointments_row->PractitionerID;
	
					$Booked->Descrip = $booked_appointments_row->Descrip;
	
					$Booked->When = $booked_appointments_row->When;
	
					$booked_appointments[] = $Booked;
	
				}
	
	
			for ($a=0;$a<count($appointments);$a++) {
	
				for ($i=0;$i<count($booked_appointments);$i++) {
	
						if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && 
trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) { 
							
							if (in_array(trim($booked_appointments[$i]->Descrip), $patient_array)) {
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;
	
							} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || 
strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') { 
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$apptLength = $appointments[$a]->Length / 60;
								$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';
	
							} else {
	
								$appointments[$a]->Descrip = 'Unavailable';

	
							}
							
						} 
	
					}
	
				}
			}
	
			
	
		$patient_string .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE 
CONVERT(CHAR(10),[When],120) in ($appt_date) and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) =  
'internetwc' or [Descrip] IN ($sql_patients)) and [PractitionerID] = $doctor_id"; 

			$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);
	
			while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {
	
				$Appointment = new StdClass();
				$Appointment->ApptID = (int) $not_all_appts_row->ApptID;
				$Appointment->PractitionerID = (int) $not_all_appts_row->PractitionerID;
				if (strtolower(trim($not_all_appts_row->Descrip)) == 'internet') {
					$apptLength = $appointment_length / 60;
					$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
				} else {
					$Appointment->Descrip = trim($not_all_appts_row->Descrip);
				}
				$Appointment->When = trim($not_all_appts_row->When);
				$booked_date_array = explode(' ', $Appointment->When);
	
					if (!in_array($booked_date_array[0], $days_away_array)) {
						
						
							
							$appointments[] = $Appointment;
					
					}
				}	
		
			
	echo json_encode($appointments);
}

if(isset($_POST['getAllDayAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_string = $item['doctor_string'];
		$doctor_book_string = $item['doctor_book_string'];
		$doctor_length_string = $item['doctor_length_string'];
		$all_appts_doctor_string = $item['all_appts_doctor_string'];
		$all_appts_book_string = $item['all_appts_book_string'];
		$all_appts_length_string = $item['all_appts_length_string'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$user_type = $item['user_type'];
		
	}

$doctor_id_array = explode (',',$all_appts_doctor_string);
$doctor_book_array = explode (',',$all_appts_book_string);
$doctor_length_array = explode (',',$all_appts_length_string);

$patient_array = explode (',',$patient_string);

$sql_patients = implode("','", $patient_array);
$sql_patients = "'".$sql_patients."'";

for ($i=0;$i<count($doctor_id_array);$i++) {

	$doctor_id_array[$i] = (int) $doctor_id_array[$i];

}

for ($i=0;$i<count($doctor_book_array);$i++) {

	$doctor_book_array[$i] = (int) $doctor_book_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$doctor_length_array[$i] = (int) $doctor_length_array[$i];

}

$all_doctors = '';
$all_book_string = '';
$all_length_doctors = '';

if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $doctor_string .','.$all_appts_doctor_string;
	$all_book_string = $doctor_book_string .','.$all_appts_book_string;
	$all_length_doctors = $doctor_length_string .','.$all_appts_length_string;
	
} else if (strlen($doctor_string) == 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $all_appts_doctor_string;
	$all_book_string = $all_appts_book_string;
	$all_length_doctors = $all_appts_length_string;
	
} else if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) == 0) {
	
	$all_doctors = $doctor_string;
	$all_book_string = $doctor_book_string;
	$all_length_doctors = $doctor_length_string;
}

$all_doctors_id_array = explode (',',$all_doctors);
$all_book_doctors_array = explode (',',$all_book_string);
$all_length_doctors_array = explode (',',$all_length_doctors);

for ($i=0;$i<count($all_doctors_id_array);$i++) {

	$all_doctors_id_array[$i] = (int) $all_doctors_id_array[$i];

}

for ($i=0;$i<count($all_book_doctors_array);$i++) {

	$all_book_doctors_array[$i] = (int) $all_book_doctors_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$all_length_doctors_array[$i] = (int) $all_length_doctors_array[$i];

}

$session_day = intval(date('N', strtotime($appt_date)));

$session_day ++;

if ($session_day > 7) {

	$session_day -= 7;

}

$appointments = array();
$days_away_array = array();

$doctor_temp_sessions_array = array();
$sessions_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
  ,[PractitionerID]
  ,[ApptBookID]
  ,[Date] as theDate
  ,$session_day as DayOfWk
  ,[1Start] as start1
  ,[1End] as end1
  ,[2Start] as start2
  ,[2End] as end2
  ,[3Start] as start3
  ,[3End] as end3
  ,[4Start] as start4
  ,[4End] as end4
  ,[5Start] as start5
  ,[5End] as end5
  ,[6Start] as start6
  ,[6End] as end6
FROM [HCN].[dbo].[TEMP_SESS]
WHERE [PractitionerID] IN ($all_appts_doctor_string)
AND CONVERT(CHAR(10),[Date],120) = '$appt_date'
ORDER BY [ApptBookID], [PractitionerID]";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {

	$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
	$theDate = $session_date_array[0];
	
	$session_appt_book_id = (int) $get_temp_sessions_row->ApptBookID;

	if ($theDate == $appt_date && $get_temp_sessions_row->start1 === NULL) {

		$days_away_array[] = (int) $get_temp_sessions_row->PractitionerID;

	} else {
		
		if (strlen($all_appts_doctor_string) > 0) {

			if ($theDate == $appt_date && $get_temp_sessions_row->start1 !== NULL) {
	
				$startTime = getSeconds($get_temp_sessions_row->start1);
				$endTime = getSeconds($get_temp_sessions_row->end1);
				$dr_id = (int) $get_temp_sessions_row->PractitionerID;
				
				$doctor_temp_sessions_array[] = $dr_id;
				
				for($l=0;$l<count($all_doctors_id_array);$l++) {
					
					if ($all_doctors_id_array[$l] == $dr_id) {
						
						$appt_length = $all_length_doctors_array[$l];
						$appt_book_id = $all_book_doctors_array[$l];
						
						if ($session_appt_book_id == $appt_book_id) {
						
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
		
						}
							
						$startTime = getSeconds($get_temp_sessions_row->start2);
						$endTime = getSeconds($get_temp_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_temp_sessions_row->start3);
						$endTime = getSeconds($get_temp_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start4);
						$endTime = getSeconds($get_temp_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start5);
						$endTime = getSeconds($get_temp_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start6);
						$endTime = getSeconds($get_temp_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						}
						
					}
				}
		
			}
		
		}
	}
}

		if (strlen($all_appts_doctor_string) > 0) {

$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
	  ,[ApptBookID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] IN ($all_appts_doctor_string)
	  AND [1Start] IS NOT NULL
	  ORDER BY [DayOfWk], [ApptBookID], [PractitionerID]";

$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);


	while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {

			if ($get_sessions_row->DayOfWk == $session_day && !in_array($get_sessions_row->PractitionerID, 
$days_away_array) && !in_array($get_sessions_row->PractitionerID, $doctor_temp_sessions_array)) { 
	
				$startTime = getSeconds($get_sessions_row->start1);
				$endTime = getSeconds($get_sessions_row->end1);
				$dr_id = (int) $get_sessions_row->PractitionerID;
				$session_appt_book_id = (int) $get_sessions_row->ApptBookID;
				
				for($l=0;$l<count($doctor_id_array);$l++) {
					
					if ($doctor_id_array[$l] == $dr_id) {
						
						$appt_length = $doctor_length_array[$l];
						$appt_book_id = $doctor_book_array[$l];
						
					}
				}
				
				if ($session_appt_book_id == $appt_book_id) {
					
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
					
	
				}
					
				$startTime = getSeconds($get_sessions_row->start2);
				$endTime = getSeconds($get_sessions_row->end2);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
	
				$startTime = getSeconds($get_sessions_row->start3);
				$endTime = getSeconds($get_sessions_row->end3);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start4);
				$endTime = getSeconds($get_sessions_row->end4);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start5);
				$endTime = getSeconds($get_sessions_row->end5);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start6);
				$endTime = getSeconds($get_sessions_row->end6);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				}
			}

		}

	}	

	
	if (strlen($all_appts_doctor_string) > 0) {

		for ($i=0;$i<count($sessions_array);$i++) {

			$session_type = $sessions_array[$i]->SessionType;
			
			$dr_id = $sessions_array[$i]->UserID;
			
			$appt_book_id = $sessions_array[$i]->ApptBookID;
			
			$dr_appt_book_id = $sessions_array[$i]->DoctorApptBookID;

			$day = $sessions_array[$i]->DayOfWeek;
			
			if (!in_array($dr_id, $days_away_array)) {

				if ($day == $session_day) {
	
					$s = $sessions_array[$i]->StartTime;
	
					$e =  $sessions_array[$i]->EndTime;
	
					$l = $sessions_array[$i]->Length;
			
				while ($s < $e) {
	
					$Appointment = new stdClass();
					
					$Appointment->SessionType = $session_type;
	
					$Appointment->ApptID = -1;
	
					$Appointment->PractitionerID = $dr_id;
					
					$Appointment->ApptBookID = $appt_book_id;
					
					$Appointment->DoctorApptBookID = $dr_appt_book_id;
	
					$Appointment->PatientID = 0;
	
					$elapsed_hours = floor ($s / 3600);
	
					$remaining = $s - ($elapsed_hours * 3600);
	
					$elapsed_minutes = floor ($remaining / 60);
	
					$remaining = $remaining - ($elapsed_minutes * 60);
	
					if ($elapsed_hours < 10)
	
					{
	
						$hours = "0" . $elapsed_hours;
	
					}
	
					else
	
					{
	
						$hours = $elapsed_hours;
	
					}
	
					if ($elapsed_minutes < 10)
	
					{
	
						$minutes = "0" . $elapsed_minutes;
	
					}
	
					else
	
					{
	
						$minutes = $elapsed_minutes;
	
					}
	
					$Appointment->When = $appt_date . " " . $hours . ":" . $minutes . ":00.000";
					$Appointment->Length = $l;
	
					if (in_array($dr_id, $days_away_array)) {
	
						$Appointment->Descrip = 'Unavailable';
	
					} else {
						
						$apptLength = $Appointment->Length / 60;
						$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
						
					}
	
					$appointments[] = $Appointment;
					
					$s += $l;
	
					}		
	
				}
			
			}

		}
		
$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE 
CONVERT(CHAR(10),[When],120) = '$appt_date' and [PractitionerID] IN ($all_appts_doctor_string)"; 


		$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);

		$booked_appointments = array();

			while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {

				$Booked = new stdClass();

				$Booked->ApptID = $booked_appointments_row->ApptID;

				$Booked->PractitionerID = $booked_appointments_row->PractitionerID;

				$Booked->Descrip = $booked_appointments_row->Descrip;

				$Booked->When = $booked_appointments_row->When;

				$booked_appointments[] = $Booked;

			}


		for ($a=0;$a<count($appointments);$a++) {

			for ($i=0;$i<count($booked_appointments);$i++) {

					if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && 
trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) { 
						
						$test_descrip = $booked_appointments[$i]->Descrip;
						$test_descrip_array = explode(' - ', $test_descrip);
						
						if ($test_descrip_array[0] == $patient_string) {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;

						} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || 
strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') { 

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$apptLength = $appointments[$a]->Length / 60;
							$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';

						} else {

							$appointments[$a]->Descrip = 'Unavailable';


						}
						
					} 

				}

			}
		
	}
	

		$patient_string .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE 
CONVERT(CHAR(10),[When],120) = '$appt_date' and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) =  
'internetwc' or [Descrip] IN ($sql_patients)) and [PractitionerID] in ($all_doctors)"; 


		$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);

		while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $not_all_appts_row->ApptID;

			$dr_id = (int) $not_all_appts_row->PractitionerID;
			$appt_book_id = (int) $not_all_appts_row->ApptBookID;
			
			for($l=0;$l<count($all_doctors_id_array);$l++) {
					
				if ($all_doctors_id_array[$l] == $dr_id) {
					
					$dr_book_id = $all_book_doctors_array[$l];
					$appt_length = $all_length_doctors_array[$l];
					
				}
				
			}

			$Appointment->PractitionerID = $dr_id;
			if (trim($not_all_appts_row->Descrip) == 'Internet' || trim($not_all_appts_row->Descrip) == 'Internetwc') {
				$apptLength = $appt_length / 60;
				$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
			} else {
				$Appointment->Descrip = trim($not_all_appts_row->Descrip);
			}
			
			$Appointment->When = trim($not_all_appts_row->When);
			$Appointment->Length = $appt_length;
			
				if (!in_array($Appointment->PractitionerID, $days_away_array) && $appt_book_id == $dr_book_id) {
					
					$appointments[] = $Appointment;
		
				}

			}
	
	$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
		$orderby = "PractitionerID"; //change this to whatever key you want from the array
		
		if(count($appointments) > 0) {
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
		}
	
	
		
	echo json_encode($appointments);

}

if(isset($_POST['getDoctorsAdmin']))
{
	

 	$sql = "SELECT     PRAC.PractitionerID, PRAC.Practitioner, PRAC.Name, PRAC.ApptBookID
FROM         PRAC INNER JOIN
                      APPTBOOKSET ON PRAC.ApptBookID = APPTBOOKSET.ApptBookID
WHERE     (PRAC.Deleted = 'N') AND (APPTBOOKSET.Deleted = 'N')";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->Name);
			$doctor->data = (int) $row->PractitionerID;
			$doctor->doctor_id = (int) $row->PractitionerID;
			$doctor->doctor_code = trim($row->Practitioner);
			$doctor->doctor_name = trim($row->Name);
			$doctor->appointment_book_id = (int) $row->ApptBookID;
			$p[] = $doctor;
		}

	echo json_encode($p);
	
}

?>
