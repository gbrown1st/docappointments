<?php
$code_version = '160805';
require_once "config.php";

if (isset($_GET['showVersion']))
{
	echo $code_version;
}

if(isset($_POST['getExistingPatientDetails']))
{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$patient_id = (int) $item['patient_id'];
			
		}
  		
	$get_patient_sql = "SELECT CM_PATIENT.PATIENT_ID, CM_PATIENT.TITLE, CM_PATIENT.FIRST_NAME, CM_PATIENT.MIDDLE_NAME, 
CM_PATIENT.SURNAME, CM_PATIENT.KNOWN_AS,  
CM_PATIENT.GENDER_CODE, CM_PATIENT.ATSI, CM_PATIENT.RECEIVE_SMS, CM_PATIENT.DOB, CM_PATIENT.PHONE_HOME, CM_PATIENT.PHONE_WORK, CM_PATIENT.PHONE_MOBILE,  
CM_PATIENT.STREET_LINE_1, CM_PATIENT.STREET_LINE_2, CM_PATIENT.CITY, CM_PATIENT.POSTCODE, CM_PATIENT.P_STREET_LINE_1, 
CM_PATIENT.P_CITY,  
CM_PATIENT.P_POSTCODE, CM_PATIENT.PENSION_NO, CM_PATIENT.PENSION_CODE, CM_PATIENT.PENSION_EXPIRY_DATE, CM_PATIENT.DVA_NO, 
 
CM_PATIENT.MEDICARE_NO, CM_PATIENT.MEDICARE_INDEX, CM_PATIENT.MEDICARE_EXPIRY_DATE, CM_PATIENT_EMAIL.EMAIL_ADDRESS 
FROM         CM_PATIENT LEFT OUTER JOIN
                      CM_PATIENT_EMAIL ON CM_PATIENT.PATIENT_ID = CM_PATIENT_EMAIL.PATIENT_ID
WHERE     (CM_PATIENT.PATIENT_ID = $patient_id)";

  $get_patient_rs = odbc_exec($Connect,$get_patient_sql);
  $p = array();

			while ($row = odbc_fetch_object($get_patient_rs)) 

			{
				$Appointment = new StdClass();
				$Appointment->INTERNALID = (int) $row->PATIENT_ID;
				$Appointment->TITLECODE = $row->TITLE;
				$Appointment->FIRSTNAME = $row->FIRST_NAME;
				$Appointment->MIDDLENAME = $row->MIDDLE_NAME;
				$Appointment->SURNAME = $row->SURNAME;
				$Appointment->PREFERREDNAME = $row->KNOWN_AS;
				$Appointment->ADDRESS1 = $row->STREET_LINE_1;
				$Appointment->ADDRESS2 = $row->STREET_LINE_2;
				$Appointment->CITY = $row->CITY;
				$Appointment->POSTCODE = $row->POSTCODE;
				$Appointment->POSTALADDRESS = $row->P_STREET_LINE_1;
				$Appointment->POSTALCITY = $row->P_CITY;
				$Appointment->POSTALPOSTCODE = $row->P_POSTCODE;
				$Appointment->DOB = $row->DOB;
				$Appointment->SEXCODE = $row->GENDER_CODE;
				$Appointment->ATSI = $row->ATSI;
				$Appointment->SMS = $row->RECEIVE_SMS;
				$Appointment->HOMEPHONE = $row->PHONE_HOME;
				$Appointment->WORKPHONE = $row->PHONE_WORK;
				$Appointment->MOBILEPHONE = $row->PHONE_MOBILE;
				$Appointment->MEDICARENO = $row->MEDICARE_NO;
				$Appointment->MEDICARELINENO = $row->MEDICARE_INDEX;
				$Appointment->MEDICAREEXPIRY = $row->MEDICARE_EXPIRY_DATE;
				$Appointment->PENSIONCODE = $row->PENSION_CODE;
				$Appointment->PENSIONNO = $row->PENSION_NO;
				$Appointment->PENSIONEXPIRY = $row->PENSION_EXPIRY_DATE;
				$Appointment->DVACODE = $row->DVACODE;
				$Appointment->DVANO = $row->DVA_NO;
				$Appointment->EMAIL = $row->EMAIL_ADDRESS;
				
				$p[] = $Appointment;
			}

	echo json_encode($p);
}

if (isset($_POST['addPatient']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$title_code = $item['title_code'];
			$first_name = str_replace("'", "''", $item['first_name']);
			$middle_name = str_replace("'", "''", $item['middle_name']);
			$last_name = str_replace("'", "''", $item['last_name']);
			$preferred_name = str_replace("'", "''", $item['preferred_name']);
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			
			$card = $item['card'];
			$allergies = $item['allergies'];
			$medications = $item['medications'];
			$medical_conditions = $item['medical_conditions'];
			$family_history = $item['family_history'];
			$non_smoker = $item['$non_smoker'];
			$smoker = $item['smoker'];
			$smokes_per_day = $item['smokes_per_day'];
			$ex_smoker = $item['ex_smoker'];
			$hear = $item['hear'];
			$hear_other = $item['hear_other'];
			
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$medicare_number = $item['medicare_number'];
			if ($medicare_number == '0000000000') {
				$medicare_number = '';
				$concession = 0;
			} else {
				$concession = 6;
			}
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$atsic_code = $item['atsic_code'];
			$sms_code = $item['sms_code'];
			
			$nok_title = $item['nok_title'];
			$nok_title_code = $item['nok_title_code'];
			$nok_first_name = $item['nok_first_name'];
			$nok_last_name = $item['nok_last_name'];
			$nok_address = $item['nok_address'];
			$nok_city = $item['nok_city'];
			$nok_postcode = $item['nok_postcode'];
			$nok_phone = $item['nok_phone'];
			$nok_alternate_phone = $item['nok_alternate_phone'];
			$nok_relationship = $item['nok_relationship'];
			$ec_title = $item['ec_title'];
			$ec_title_code = $item['ec_title_code'];
			$ec_first_name = $item['ec_first_name'];
			$ec_last_name = $item['ec_last_name'];
			$ec_address = $item['ec_address'];
			$ec_city = $item['ec_city'];
			$ec_postcode = $item['ec_postcode'];
			$ec_phone = $item['ec_phone'];
			$ec_alternate_phone = $item['ec_alternate_phone'];
			$ec_relationship = $item['ec_relationship'];
	
		}
		
		$nok_message_body = "NOK Title: $nok_title 
";
		$nok_message_body .= "NOK First name: $nok_first_name 
";
		$nok_message_body .= "NOK Last name: $nok_last_name 
";
		$nok_message_body .= "NOK Address: $nok_address 
";
		$nok_message_body .= "NOK City: $nok_city 
";
		$nok_message_body .= "NOK Postcode: $nok_postcode 
";
		$nok_message_body .= "NOK Contact phone: $nok_phone 
";
		$nok_message_body .= "NOK Alternate contact: $nok_alternate_phone 
";
		$nok_message_body .= "NOK Relationship: $nok_relationship 
";
		
		$ec_message_body = "EC Title: $ec_title 
";
		$ec_message_body .= "EC First name: $ec_first_name 
";
		$ec_message_body .= "EC Last name: $ec_last_name 
";
		$ec_message_body .= "EC Address: $ec_address 
";
		$ec_message_body .= "EC City: $ec_city 
";
		$ec_message_body .= "EC Postcode: $ec_postcode 
";
		$ec_message_body .= "EC Contact phone: $ec_phone 
";
		$ec_message_body .= "EC Alternate contact: $ec_alternate_phone 
";
		$ec_message_body .= "EC Relationship: $ec_relationship 
";
		
		$smoker_data = '';
		
		if ($non_smoker == 'Y') {
			$smoker_data = 'Non smoker';
		} else if ($smoker == 'Y') {
			$smoker_data = 'Smoker - '.$smokes_per_day. ' smokes per day';
		} else if ($ex_smoker == 'Y') {
			$smoker_data = 'Ex smoker';
		}
		
		$warnings = 'PATIENT MEDICAL INFORMATION
		
ALLERGIES: '.$allergies.'

MEDICAL CONDITIONS: '.$medical_conditions.'

FAMILY HISTORY: '.$family_history.'

CURRENT/REGULAR MEDICATIONS: '.$medications.'

SMOKING: '.$smoker_data.'

HEARD ABOUT FROM: '.$hear. ' '.$hear_other;


		$general_notes = 'NEXT OF KIN:

'.$nok_message_body.' 

EMMERGENCY CONTACT:

'.$ec_message_body.'

HEALTH CARE CARD: '.$card.'

HEARD ABOUT FROM: '.$hear. ' '.$hear_other;
		
		$p = array();
		$StringResult = new stdClass();
		
		$addPatientSQL = "DECLARE	@return_value int
		
EXEC	@return_value = [HCN].[dbo].[usp_pm_InsertPatientApp]
   @P_EXTERNAL_ID = NULL
  ,@P_PAYER_ID = NULL
  ,@P_ORIGIN_CODE = NULL
  ,@P_TITLE = '$title_code'
  ,@P_SALUTATION = NULL
  ,@P_FIRST_NAME = '$first_name'
  ,@P_MIDDLE_NAME = '$middle_name'
  ,@P_SURNAME = '$last_name'
  ,@P_KNOWN_AS = '$preferred_name'
  ,@P_ATSI = '$atsic_code'
  ,@P_GENDER_CODE = '$gender_code'
  ,@P_SEXUALITY_CODE = NULL
  ,@P_MARITAL_STATUS_CODE = NULL
  ,@P_STATUS_CODE = 'A'
  ,@P_DOB = '$dob'
  ,@P_DECEASED_DATE = NULL
  ,@P_PHONE_HOME = '$home_phone'
  ,@P_PHONE_WORK = '$work_phone'
  ,@P_PHONE_MOBILE = '$mobile_phone'
  ,@P_FAX = NULL
  ,@P_STREET_LINE_1 = '$address_1'
  ,@P_STREET_LINE_2 = '$address_2'
  ,@P_STREET_LINE_3 = NULL
  ,@P_CITY = '$city'
  ,@P_POSTCODE = '$postcode'
  ,@P_P_STREET_LINE_1 = '$postal_address'
  ,@P_P_STREET_LINE_2 = NULL
  ,@P_P_STREET_LINE_3 = NULL
  ,@P_P_CITY = '$postal_city'
  ,@P_P_POSTCODE = '$postal_postcode'
  ,@P_FAMILY_HISTORY = '$family_history'
  ,@P_ALLERGIES = '$allergies'
  ,@P_SMOKER = '$smoker'
  ,@P_SMOKES_PER_DAY = '$smokes_per_day'
  ,@P_SMOKING_EX = '$ex_smoker'
  ,@P_WARNINGS = '$warnings'
  ,@P_NOTES = '$general_notes'
  ,@P_PREFERRED_DOCTOR = NULL
  ,@P_CHART_NO = NULL
  ,@P_PENSION_NO = '$pension_number'
  ,@P_PENSION_CODE = '$pension_code'
  ,@P_PENSION_EXPIRY_DATE = '$pension_expiry'
  ,@P_DVA_NO = '$dva_number'
  ,@P_SAFETY_NET_NO = NULL
  ,@P_REFERRING_DOCTOR = NULL
  ,@P_REFERRING_DATE = NULL
  ,@P_REFERRING_CODE = NULL
  ,@P_INSURANCE_COMPANY_ID = NULL
  ,@P_INSURANCE_NO = NULL
  ,@P_INSURANCE_TYPE = NULL
  ,@P_MEDICARE_NO = '$medicare_number'
  ,@P_MEDICARE_INDEX = '$medicare_line_number'
  ,@P_MEDICARE_EXPIRY_DATE = '$medicare_expiry'
  ,@P_EMAIL_ADDRESS = '$email'
  ,@P_STAMP_CREATED_USER_ID = 0
  ,@P_STAMP_CREATED_DATETIME = ''
  ,@P_STAMP_USER_ID = 0
  ,@P_STAMP_ACTION_CODE = 'A'
  ,@P_STAMP_DATETIME = ''
  ,@P_TRANSGENDER = 0
  ,@P_MEDICARE_GENDER_CODE = '$gender_code'
  ,@P_CONCESSION = $concession 

  select 'new_patient_id' = @return_value";

			$add_user_rs = odbc_exec($Connect,$addPatientSQL);
			$result = new stdClass();
		
			while ($add_user_row = odbc_fetch_object($add_user_rs)) {
				$result = $add_user_row;
			}
			foreach ($result as $key => $value) {
           		$patient_id = (int) $value;
		    }
		    
		    if ($patient_id > 0) {
			
				$StringResult->result_string = "Your details have been saved."; 
				$StringResult->heading = "Patient Details Saved";
				$StringResult->extra_data = $patient_id.' - '.$non_smoker;
				$StringResult->sql = $addPatientSQL;
			
			} else {
				
				$StringResult->result_string = "Your details could not be saved."; 
				$StringResult->heading = "Patient Details Failed";
				$StringResult->extra_data = $patient_id;
				$StringResult->sql = $addPatientSQL;
			
			}
			
 		$p[] = $StringResult;

		echo json_encode($p);

	}
	
	if (isset($_POST['addPatientMedicalDetails']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {
			$patient_id = (int) $item['patient_id'];
			$card = $item['card'];
			$allergies = $item['allergies'];
			$medications = $item['medications'];
			$medical_conditions = $item['medical_conditions'];
			$family_history = $item['family_history'];
			$smoker = $item['smoker'];
			$smokes_per_day = $item['smokes_per_day'];
			$ex_smoker = $item['ex_smoker'];
			$hear = $item['hear'];
			$hear_other = $item['hear_other'];
		}
		
		$warnings = 'MEDICAL CONDITIONS: '.$medical_conditions.' CURRENT/REGULAR MEDICATIONS: '.$medications;
		$general_notes = 'HEALTH CARE CARD: '.$card.' HEARD ABOUT FROM: '.$hear. ' '.$hear_other;

		$p = array();
		$StringResult = new stdClass();
		
		$addPatientSQL = "INSERT INTO [HCN].[dbo].[MD_PATIENT_CLINICAL]
           ([PATIENT_ID]
           ,[FAMILY_HISTORY]
           ,[WARNINGS] 
           ,[ALLERGIES]
           ,[SMOKER]
           ,[SMOKES_PER_DAY]
           ,[SMOKING_EX]
           ,[STAMP_CREATED_DATETIME]
           ,[STAMP_USER_ID]
           ,[STAMP_ACTION_CODE]
           ,[STAMP_DATETIME]
           )
     VALUES
           ($patient_id
           ,'$family_history'
           ,'$warnings'
           ,'$allergies'
           ,'$smoker'
           ,'$smokes_per_day'
           ,'$ex_smoker'
           ,''
           ,0
           ,'A'
           ,''
           )";

			$add_user_rs = odbc_exec($Connect,$addPatientSQL);
			
		    if ($add_user_rs) {
		        
		       		 $addPatientNotesSQL = "UPDATE [HCN].[dbo].[PS_PATIENT_BILLING]
		   SET [GENERAL_NOTES] = '$general_notes'
		 WHERE  [PATIENT_ID] = $patient_id";
		
					$add_notes_rs = odbc_exec($Connect,$addPatientNotesSQL);
					
				    if ($add_notes_rs) {
					
						$StringResult->result_string = "Your details have been saved."; 
						$StringResult->heading = "Patient Details Saved";
						$StringResult->extra_data = $patient_id;
						$StringResult->sql = $addPatientSQL;
					
					} else {
						
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Patient Details Failed";
						$StringResult->extra_data = $patient_id;
						$StringResult->sql = $addPatientSQL;
					
					}
		    } else {
						
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Patient Details Failed";
						$StringResult->extra_data = $patient_id;
						$StringResult->sql = $addPatientSQL;
					
					}
		    
		    
 		$p[] = $StringResult;

		echo json_encode($p);

	}

	
	if (isset($_POST['updateExistingPatient']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$patient_id = (int) $item['patient_id'];
			$title_code = $item['title_code'];
			$first_name = str_replace("'", "''", $item['first_name']);
			$middle_name = str_replace("'", "''", $item['middle_name']);
			$last_name = str_replace("'", "''", $item['last_name']);
			$preferred_name = str_replace("'", "''", $item['preferred_name']);
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$atsic_code = $item['atsic_code'];
			$medicare_number = $item['medicare_number'];
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$atsic_code = $item['atsic_code'];
			$sms_code = $item['sms_code'];
	
		}

		$p = array();
		$StringResult = new stdClass();
		
		$updatePatientSQL = "UPDATE CM_PATIENT SET
		TITLE = '$title_code', 
		FIRST_NAME = '$first_name',  
		MIDDLE_NAME = '$middle_name', 
		SURNAME = '$last_name',  
		KNOWN_AS = '$preferred_name',  
		GENDER_CODE = '$gender_code',
		DOB = '$dob',  
		PHONE_HOME = '$home_phone',  
		PHONE_WORK = '$work_phone',  
		PHONE_MOBILE = '$mobile_phone',  
		STREET_LINE_1 = '$address_1',  
		STREET_LINE_2 = '$address_2',  
		CITY = '$city',  
		POSTCODE = '$postcode',  
		P_STREET_LINE_1 = '$postal_address',  
		P_CITY = '$postal_city',  
		P_POSTCODE = '$postal_postcode',  
		PENSION_NO = '$pension_number',  
		PENSION_CODE = '$pension_code',  
		PENSION_EXPIRY_DATE = '$pension_expiry',  
		DVA_NO = '$dva_number',  
		MEDICARE_NO = '$medicare_number',  
		MEDICARE_INDEX = '$medicare_line_number',  
		MEDICARE_EXPIRY_DATE = '$medicare_expiry',
		ATSI = '$atsic_code', 
		RECEIVE_SMS = '$sms_code'
		
		WHERE PATIENT_ID = $patient_id";

			$update_user_rs = odbc_exec($Connect,$updatePatientSQL);
		
			if ($update_user_rs) {
				
				$getEmailSQL = "SELECT PATIENT_EMAIL_ID FROM CM_PATIENT_EMAIL
		WHERE PATIENT_ID = $patient_id
		AND EMAIL_TYPE = 'Home'
		AND STAMP_ACTION_CODE != 'D'
		ORDER BY PATIENT_EMAIL_ID";
		
				$getEmailSQL_rs = odbc_exec($Connect,$getEmailSQL);
				
				if( odbc_num_rows( $getEmailSQL_rs ) > 0) {
					
					
					while ($getEmailSQL_row = odbc_fetch_object($getEmailSQL_rs)) 

					{
						$patient_email_id = (int) $getEmailSQL_row->PATIENT_EMAIL_ID;
					}
					
					
					$updatePatientEmailSQL = "UPDATE CM_PATIENT_EMAIL
		SET EMAIL_ADDRESS = '$email',
			EMAIL_TYPE = 'Home',
			STAMP_ACTION_CODE = 'U',
			STAMP_USER_ID = 0,
			STAMP_DATETIME = GETDATE()
		WHERE PATIENT_EMAIL_ID = $patient_email_id";
					
					$update_user_email_rs = odbc_exec($Connect,$updatePatientEmailSQL);
					
					if ($update_user_email_rs) {
						
						$StringResult->result_string = "Your details have been updated."; 
						$StringResult->heading = "Update Patient Details Succeeded";
						
					} else {
	
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Update Patient Details Failed";
	
					}
					
				} else {
					
					$addPatientEmailSQL = "INSERT INTO [HCN].[dbo].[CM_PATIENT_EMAIL]
           	([PATIENT_ID]
	           ,[EMAIL_TYPE]
	           ,[EMAIL_ADDRESS]
	           ,[STAMP_USER_ID])
	     VALUES
	            ($patient_id
	            ,'Home'
	           ,'$email'
	           ,0)";
					
					$add_user_email_rs = odbc_exec($Connect,$addPatientEmailSQL);
					
					if ($add_user_email_rs) {
						
						$StringResult->result_string = "Your details have been updated."; 
						$StringResult->heading = "Update Patient Details Succeeded";
						
					} else {
	
						$StringResult->result_string = "Your details could not be saved."; 
						$StringResult->heading = "Update Patient Details Failed";
	
					}
					
				}
				
				
			} else {

					$StringResult->result_string = "Your details could not be saved."; 
					$StringResult->heading = "Update Patient Details Failed";

			}

		$p[] = $StringResult;

		echo json_encode($p);

	}
	
if(isset($_POST['getApptDoctors']))

{

	$fromDate = strtotime('now');

	$today = date("Y-m-d",$fromDate);

	$sql = "SELECT DISTINCT PRAC.PractitionerID, PRAC.Practitioner, PRAC.Location, PRAC.Name

FROM         APPT INNER JOIN

                      PRAC ON APPT.PractitionerID = PRAC.PractitionerID

WHERE     (APPT.[When] >= DATEADD(HOUR, - 2, GETDATE())) AND (APPT.[When] <= DATEADD(HOUR, 2, GETDATE()))

ORDER BY PRAC.Location, PRAC.Name";

	$rs = odbc_exec($Connect,$sql);

	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerID = (int) $row->PractitionerID;
			$Appointment->DoctorID = (int) $row->PractitionerID;
			$Appointment->DoctorCode = $row->Practitioner;
			$Appointment->FullName = $row->Name;
			$Appointment->Location = $row->Location;
			$p[] = $Appointment;

		}

	echo json_encode($p);

} 

if(isset($_POST['addToWaitingRoom']))

{

	$first_name = strtoupper(trim($_POST['first_name']));

	$last_name = strtoupper(trim($_POST['last_name']));
	
	$dob = $_POST['dob'];
	
	$gender = strtoupper(trim($_POST['gender']));
	
	$gender_code = $gender[0];

	$doctor_id = $_POST['doctor_id'];

	$doctor_code = $_POST['doctor_code'];
	
	$p = array();

	$StringResult = new StdClass();
	
	$patient_sql = "SELECT     *

FROM         CM_PATIENT

WHERE     (UPPER(FIRST_NAME) = '$first_name') 

AND (UPPER(SURNAME) = '$last_name') 

AND CONVERT(char(10), [DOB],126) = '$dob'

AND GENDER_CODE = '$gender_code'";

	$patient_rs = odbc_exec($Connect,$patient_sql);

 	if( odbc_num_rows( $patient_rs ) == 1) {

		while ($patient_row = odbc_fetch_object($patient_rs)) {

			$PatientID = $patient_row->PATIENT_ID;
			$Ur_No = $patient_row->UR_NO;
			$Chart_No = '['.$patient_row->CHART_NO.']';
			$Address = $patient_row->STREET_LINE_1;
			$City = $patient_row->CITY;
			$Postcode = $patient_row->POSTCODE;
			$Phone = $patient_row->PHONE_HOME;
			$Mobile = $patient_row->PHONE_MOBILE;
			$last_updated = strtotime($patient_row->STAMP_DATETIME);

		}
		
		$last_six_months = strtotime("-6 months");

		$sql = "SELECT     ApptID, PractitionerID, PatientID, Descrip, [When]


FROM         APPT

WHERE     

	APPT.[PatientID] = $PatientID

	AND APPT.[When] >= DATEADD(HOUR,-2,getDate()) 

	AND APPT.[When] <= DATEADD(HOUR, 2 ,getDate())

	AND APPT.PractitionerID = $doctor_id";

	$rs = odbc_exec($Connect,$sql);

		if( odbc_num_rows( $rs ) > 0) {
		
			while ($row = odbc_fetch_object($rs)) {
				
					$ApptPatientID = (int) $row->PatientID;
					$ApptID = (int) $row->ApptID;
					$ApptTime = $row->When;
					$Descrip = $row->Descrip;
						
			}
		
			$display_order_sql = "Select IDENT_CURRENT('WaitRoom') + 1 as DISPLAY_ORDER";
		
			$display_order_rs = odbc_exec($Connect,$display_order_sql);
		
				while ($display_order_row = odbc_fetch_object($display_order_rs)) {
		
					$display_order = (int) $display_order_row->DISPLAY_ORDER;
		
				}
		
			 if ($ApptPatientID == $PatientID) {
		
				$sql_waitroom_check = "select * from WAITROOM where PatientNo = $PatientID";
				$rs_waitroom_check = odbc_exec($Connect,$sql_waitroom_check);
		
				if( odbc_num_rows( $rs_waitroom_check ) == 1) {
		
					if ($last_six_months > $last_updated) {
						
						$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
					
					} else {
						
						$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nPlease take a seat."; 
						
					}
		
					$StringResult->heading = "Your Arrival Has Been Noted"; 
		
				} else {
		
					$sql_wait = "insert into WAITROOM (PatientNo, 
DISPLAY_ORDER,[Type],ApptIDList,DrCode,DaysOverDue,ArriveTime,ApptTime) values ($PatientID, $display_order, 0, $ApptID, 
'$doctor_code', -9999, getDate(), '$ApptTime')";  
		
					$rs_wait = odbc_exec($Connect,$sql_wait);
		
								if ($rs_wait) {
		
									$sql_wait_appt = "update APPT set Flag = 2, TimeInWAITROOM = getDate() where ApptID = 
$ApptID";  
									$rs_wait_appt = odbc_exec($Connect,$sql_wait_appt);
		
										if ($rs_wait_appt) {
		
											if ($last_six_months > $last_updated) {
						
												$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
											
											} else {
												
												$StringResult->result_string = "The front desk has been advised of your arrival.\n\nPlease take a seat.";
												
											}
											  
											$StringResult->heading = "Your Arrival Has Been Noted"; 
		
										} else {
		
											$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
										}
		
								} else {
		
									$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
		
								}
		
				}
		
			}
		
		} else {
		
		
				$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
		}
						
	} else {
		
		$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
	}

	
	$p[] = $StringResult;
	echo json_encode($p);

} 

?>