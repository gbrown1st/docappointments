<?php

require_once "config.php";

$code_version = '140217';

function getSeconds($startTime) {
	
	if (strlen($startTime) == 0) {
		
		return 60;
		
	} else {
		
		$startTime = explode (' ', $startTime);
		$startTime = explode ('.', $startTime[1]);
		$hms = $startTime[0];
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60) + 60;
		$seconds += (intval($s));
		return $seconds;
	}
	
}

if (isset($_GET['showVersion']))

{
	
	echo $code_version;
	
}

if(isset($_POST['getApptDoctors']))

{

	$fromDate = strtotime('now');

	$today = date("Y-m-d",$fromDate);

	$sql = "SELECT DISTINCT PRAC.PractitionerID, PRAC.Practitioner, PRAC.Location, PRAC.Name

FROM         APPT INNER JOIN

                      PRAC ON APPT.PractitionerID = PRAC.PractitionerID

WHERE     (APPT.[When] >= DATEADD(HOUR, - 2, GETDATE())) AND (APPT.[When] <= DATEADD(HOUR, 2, GETDATE()))

ORDER BY PRAC.Location, PRAC.Name";

	$rs = odbc_exec($Connect,$sql);

	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerID = (int) $row->PractitionerID;
			$Appointment->DoctorID = (int) $row->PractitionerID;
			$Appointment->DoctorCode = $row->Practitioner;
			$Appointment->FullName = $row->Name;
			$Appointment->Location = $row->Location;
			$p[] = $Appointment;

		}

	echo json_encode($p);

} 

if(isset($_POST['addToWaitingRoom']))

{

	$first_name = strtoupper(trim($_POST['first_name']));

	$last_name = strtoupper(trim($_POST['last_name']));
	
	$dob = $_POST['dob'];
	
	$gender = strtoupper(trim($_POST['gender']));
	
	$gender_code = $gender[0];

	$doctor_id = $_POST['doctor_id'];

	$doctor_code = $_POST['doctor_code'];
	
	$p = array();

	$StringResult = new StdClass();
	
	$patient_sql = "

					SELECT     *

FROM         CM_PATIENT

WHERE     (UPPER(FIRST_NAME) = '$first_name') 

AND (UPPER(SURNAME) = '$last_name') 

AND (DOB = CONVERT(DATETIME, '$dob 00:00:00', 102))

AND GENDER_CODE = '$gender_code'";

	$patient_rs = odbc_exec($Connect,$patient_sql);

 	if( odbc_num_rows( $patient_rs ) == 1) {

		while ($patient_row = odbc_fetch_object($patient_rs)) {

			$PatientID = $patient_row->PATIENT_ID;
			$Ur_No = $patient_row->UR_NO;
			$Chart_No = '['.$patient_row->CHART_NO.']';
			$Address = $patient_row->STREET_LINE_1;
			$City = $patient_row->CITY;
			$Postcode = $patient_row->POSTCODE;
			$Phone = $patient_row->PHONE_HOME;
			$Mobile = $patient_row->PHONE_MOBILE;
			
		}
		
		$sql = "SELECT     ApptID, PractitionerID, PatientID, Descrip, [When]

FROM         APPT

WHERE     

	APPT.[Descrip] like '%$first_name%'

	AND APPT.[Descrip] like '%$last_name%'

	AND APPT.[When] >= DATEADD(HOUR,-2,getDate()) 

	AND APPT.[When] <= DATEADD(HOUR, 2 ,getDate())

	AND APPT.PractitionerID = $doctor_id";

	$rs = odbc_exec($Connect,$sql);

		if( odbc_num_rows( $rs ) > 0) {
		
			while ($row = odbc_fetch_object($rs)) {
				
					$ApptPatientID = (int) $row->PatientID;
					$ApptID = (int) $row->ApptID;
					$ApptTime = $row->When;
					$Descrip = $row->Descrip;
						
			}
		
			$display_order_sql = "Select IDENT_CURRENT('WaitRoom') + 1 as DISPLAY_ORDER";
		
			$display_order_rs = odbc_exec($Connect,$display_order_sql);
		
				while ($display_order_row = odbc_fetch_object($display_order_rs)) {
		
					$display_order = (int) $display_order_row->DISPLAY_ORDER;
		
				}
		
			if ($ApptPatientID == 0) {
		
						$pos = strrpos($Descrip, 'DOB:');
		
						if ($pos === false) {
		
							$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
							$StringResult->heading = "No Appointment Found";
							$StringResult->sql = $patient_sql;
		
						} else {
		
								$new_descrip = "$last_name, $first_name $Chart_No, $Phone, $Mobile, $Address, $City, $Postcode $descrip_dob";
								$sql_wait = "insert into WAITROOM (PatientNo, DISPLAY_ORDER,[Type],ApptIDList,DrCode,DaysOverDue,ArriveTime,ApptTime) values ($PatientID, $display_order, 0, $ApptID, '$doctor_code', -9999, getDate(), '$ApptTime')"; 
		
								$rs_wait = odbc_exec($Connect,$sql_wait);
		
								if ($rs_wait) {
		
									$sql_wait_appt = "update APPT set Flag = 2, TimeInWAITROOM = getDate(), PatientID = $PatientID, Ur_No = '$Ur_No', Descrip = '$new_descrip' where ApptID = $ApptID"; 
									$rs_wait_appt = odbc_exec($Connect,$sql_wait_appt);
		
										if ($rs_wait_appt) {
		
											$StringResult->result_string = "The front desk has been advised of your arrival.\n\nPlease take a seat."; 
											$StringResult->heading = "Your Arrival Has Been Noted"; 
		
										} else {
		
											$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
											$StringResult->heading = "No Appointment Found";
											$StringResult->sql = $patient_sql;
		
										}
		
								} else {
		
									$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
									$StringResult->heading = "No Appointment Found";
									$StringResult->sql = $patient_sql;
		
								}
		
						} 
		
			} else if ($ApptPatientID == $PatientID) {
		
				$sql_waitroom_check = "select * from WAITROOM where PatientNo = $PatientID";
				$rs_waitroom_check = odbc_exec($Connect,$sql_waitroom_check);
		
				if( odbc_num_rows( $rs_waitroom_check ) == 1) {
		
					$StringResult->result_string = "The front desk has already been advised of your arrival.\n\nPlease take a seat."; 
		
					$StringResult->heading = "Your Arrival Has Been Noted"; 
		
				} else {
		
					$sql_wait = "insert into WAITROOM (PatientNo, DISPLAY_ORDER,[Type],ApptIDList,DrCode,DaysOverDue,ArriveTime,ApptTime) values ($PatientID, $display_order, 0, $ApptID, '$doctor_code', -9999, getDate(), '$ApptTime')"; 
		
					$rs_wait = odbc_exec($Connect,$sql_wait);
		
								if ($rs_wait) {
		
									$sql_wait_appt = "update APPT set Flag = 2, TimeInWAITROOM = getDate() where ApptID = $ApptID"; 
									$rs_wait_appt = odbc_exec($Connect,$sql_wait_appt);
		
										if ($rs_wait_appt) {
		
											$StringResult->result_string = "The front desk has been advised of your arrival.\n\nPlease take a seat."; 
											$StringResult->heading = "Your Arrival Has Been Noted"; 
		
										} else {
		
											$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
											$StringResult->heading = "No Appointment Found";
											$StringResult->sql = $patient_sql;
		
										}
		
								} else {
		
									$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
									$StringResult->heading = "No Appointment Found";
									$StringResult->sql = $patient_sql;
		
		
								}
		
				}
		
			}
		
		} else {
		
		
				$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
				$StringResult->heading = "No Appointment Found";
				$StringResult->sql = $patient_sql;
		
		}
						
	} else {
		
		$StringResult->result_string = "We can\'t find an appointment for you with this doctor.\n\nPlease see the front desk."; 
		$StringResult->heading = "No Appointment Found";
		$StringResult->sql = $patient_sql;
		
	}

	
	$p[] = $StringResult;
	echo json_encode($p);

} 



if(isset($_POST['makePassword']))
{

  $length = 6;
  // start with a blank password
  $password = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
    
  // set up a counter
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($password, $char)) { 
      $password .= $char;
      $i++;
    }

  }
					
  // done!
	echo json_encode($password);

}

if(isset($_POST['makeNewAppointmentRequest']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$appt_date = $item['appt_date'];
		$delete_from_day = $item['delete_from_day'];
		$patient_description = trim($item['patient_description']);
		$doctor_id = $item['doctor_id'];
		$doctor_code = $item['doctor_code'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$appointment_book_id = $item['appointment_book_id'];
		$multiple_appointments = $item['multiple_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
	
	}

	if ($multiple_appointments == "no") {
		
		if ($show_all_appointments == 'no') {

			$sql = "update APPT set [Descrip] = 'Internet' WHERE ([Descrip] = '$patient_description' and CONVERT(CHAR(10),[When],120) >= '$delete_from_day')";
			
		} else {
			
			$sql = "delete from APPT WHERE ([Descrip] = '$patient_description' and CONVERT(CHAR(10),[When],120) >= '$delete_from_day')";
			
		}
		
		$rs = odbc_exec($Connect,$sql);
		
	}

	$p = array();
		
	$StringResult = new StdClass();

	if ((int) $appt_id > 0) {

		$sql2 = "update APPT set [Descrip] = '$patient_description' WHERE ApptID = $appt_id";

	} else {

		$sql2 = "
	INSERT INTO [HCN].[dbo].[APPT]
           ([PractitionerID]
           ,[PatientID]
           ,[Ur_No]
           ,[Type]
           ,[Descrip]
           ,[When]
           ,[Flag]
           ,[User]
           ,[ApptBookID])
     VALUES
           ($doctor_id
           ,0
           ,''
           ,0
           ,'$patient_description'
           ,'$appt_date'
           ,16
           ,'$doctor_code'
           ,$appointment_book_id)";

	}
	
	  
	$rs2 = odbc_exec($Connect,$sql2);

		if ($rs2) {
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = 0;
					
			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = 0;
			
			}

	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['cancelExistingAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$delete_from_day = $item['delete_from_day'];
		$appointment_description = $item['appointment_description'];
		$user_string = $item['user_string'];
		$user_type = $item['user_type'];
		$patient_description = $item['patient_description'];
		$doctor_id = $item['doctor_id'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
		
	}

	if ($show_all_appointments == 'yes') {

		$sql = "delete from APPT WHERE [ApptID] = $appt_id";

	} else {
		
		$sql = "update APPT set [Descrip] = 'Internet' WHERE ApptID = $appt_id";

	}
	

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
		
	$StringResult = new StdClass();
	
	if ($rs) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = 0;
				
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;
			
		}
	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['getAppointmentBooksRequest']))
{

	$sql = "SELECT     [ApptBookID], [TimeSlotSize], [StartTime], [EndTime], [Description]
FROM         APPTBOOKSET
WHERE     ([Deleted] = 'N')";

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();

	while ($row = odbc_fetch_object($rs)) {

			$AppointmentBook = new StdClass();
			$AppointmentBook->ApptBookID = (int) $row->ApptBookID;
			$AppointmentBook->TimeSlotSize = (int) $row->TimeSlotSize;
			$AppointmentBook->Description = trim($row->Description);
			$AppointmentBook->StartTime = trim($row->StartTime);
			$AppointmentBook->EndTime = trim($row->EndTime);
			$p[] = $AppointmentBook;
		}

	echo json_encode($p);

}

if(isset($_POST['checkAppointmentAvailability']))

{

	$jsonString = urldecode($_POST['jsonSendData']);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_date = $item['appt_date'];
		$doctor_id = $item['doctor_id'];

	}

$booked_sql = "SELECT ApptID, Descrip
FROM         APPT
WHERE     ([When] = '$appt_date') AND (PractitionerID = $doctor_id)";

			$booked_array = array();

			$booked_rs = odbc_exec($Connect,$booked_sql);

			if (odbc_num_rows($booked_rs) == 0) {

				$Booked = new stdClass();

				$Booked->is_available = 0;

				$booked_array[] = $Booked;

			} else {

				while ($booked_row = odbc_fetch_object($booked_rs)) {

					if (trim($booked_row->Descrip) == 'Internet' || trim($booked_row->Descrip) == 'Internetwc') {

						$Booked = new stdClass();

						$Booked->is_available = 0;

						$booked_array[] = $Booked;

					} else {

						$Booked = new stdClass();

						$Booked->is_available = 1;

						$booked_array[] = $Booked;

					}

				}

			}


	echo json_encode($booked_array);

}

if(isset($_POST['getAllWeekAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_id = (int) $item['doctor_id'];
		$appointment_book_id = (int) $item['appointment_book_id'];
		$appointment_length = (int) $item['appointment_length'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$show_all_appointments = $item['show_all_appointments'];
		$user_type = $item['user_type'];
		
	}

$patient_array = explode (',',$patient_string);

$sql_patients = implode("','", $patient_array);
$sql_patients = "'".$sql_patients."'";

for ($i=0;$i<count($patient_array);$i++) {

	$patient_array[$i] = trim($patient_array[$i]);

}

$appt_date_cleared = str_replace("'","",$appt_date);

$days_array = explode (',',$appt_date_cleared);

$session_day_array = array();
$session_day_number_array = array();

for ($i=0;$i<count($days_array);$i++) {

	$session_day = intval(date('N', strtotime($days_array[$i])));

	$session_day ++;

	if ($session_day > 7) {

		$session_day -= 7;

	}

	$session_day_array[] = array("session_day" => $session_day, "session_date" => $days_array[$i]);
	$session_day_number_array[] = $session_day;
	
}
	
$appointments = array();
$days_away_array = array();
$sessions_array = array();
$temp_sessions_days_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
      ,[PractitionerID]
  	  ,[ApptBookID]
      ,[Date] as theDate
      ,0 as DayOfWk
      ,[1Start] as start1
      ,[1End] as end1
      ,[2Start] as start2
      ,[2End] as end2
      ,[3Start] as start3
      ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
  FROM [HCN].[dbo].[TEMP_SESS]
  WHERE [PractitionerID] = $doctor_id
  AND CONVERT(CHAR(10),[Date],120) in ($appt_date)
  ORDER BY Date";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

	if (odbc_num_rows($get_temp_sessions_rs) > 0) {

		while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {
			
			$session_appt_book_id = (int) $get_temp_sessions_row->ApptBookID;
			
			$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
			$theDate = $session_date_array[0];
			
			$temp_session_day = intval(date('N', strtotime($theDate)));

			$temp_session_day ++;
			
			if ($temp_session_day > 7) {
			
				$temp_session_day -= 7;
			
			}
					
				if (in_array($theDate, $days_array) && $get_temp_sessions_row->start1 === NULL) {
			
					$days_away_array[] = $theDate;
			
				} else {
					
					if ($show_all_appointments == 'yes') {
		
						if (in_array($theDate, $days_array) && $get_temp_sessions_row->start1 !== NULL) {
							
							$temp_sessions_days_array[] = $temp_session_day;
				
							$startTime = getSeconds($get_temp_sessions_row->start1);
							$endTime = getSeconds($get_temp_sessions_row->end1);
							
							if ($session_appt_book_id == $appointment_book_id) {
							
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
								
							$startTime = getSeconds($get_temp_sessions_row->start2);
							$endTime = getSeconds($get_temp_sessions_row->end2);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
				
							$startTime = getSeconds($get_temp_sessions_row->start3);
							$endTime = getSeconds($get_temp_sessions_row->end3);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start4);
							$endTime = getSeconds($get_temp_sessions_row->end4);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start5);
							$endTime = getSeconds($get_temp_sessions_row->end5);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							$startTime = getSeconds($get_temp_sessions_row->start6);
							$endTime = getSeconds($get_temp_sessions_row->end6);
				
							if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
				
								$Session = new StdClass();
								$Session->SessionType = 'Temporary';
								$Session->UserID = (int) $get_temp_sessions_row->PractitionerID;
								$Session->ApptBookID = $session_appt_book_id;
								$Session->DoctorApptBookID = $appointment_book_id;
								$Session->theDate = $theDate;
								$Session->DayOfWeek = $temp_session_day;
								$Session->StartTime = $startTime;
								$Session->EndTime = $endTime;
								$Session->Length = $appointment_length;
								$sessions_array[] = $Session;
				
							}
							
							}
				
						}
						
					}
				
				}
			
			}

		}
		
if ($show_all_appointments == 'yes') {

	$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
      ,[ApptBookID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] = $doctor_id
	  AND [1Start] IS NOT NULL
	  ORDER BY [ApptBookID], [DayOfWk]";

		$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);
	
		while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {
			
					$session_appt_book_id = (int) $get_sessions_row->ApptBookID;
					$startTime = getSeconds($get_sessions_row->start1);
					$endTime = getSeconds($get_sessions_row->end1);
			
				if (in_array($get_sessions_row->DayOfWk, $session_day_number_array) && !in_array($get_sessions_row->DayOfWk, $temp_sessions_days_array) && $session_appt_book_id == $appointment_book_id) {
				
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
							
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
							
						}
							
						$startTime = getSeconds($get_sessions_row->start2);
						$endTime = getSeconds($get_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_sessions_row->start3);
						$endTime = getSeconds($get_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start4);
						$endTime = getSeconds($get_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start5);
						$endTime = getSeconds($get_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_sessions_row->start6);
						$endTime = getSeconds($get_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Standard';
							$Session->UserID = (int) $get_sessions_row->PractitionerID;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appointment_book_id;
							$Session->theDate = '';
							$Session->DayOfWeek = $get_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appointment_length;
							$sessions_array[] = $Session;
			
						}
						
					}
		
				}

		
		for ($i=0;$i<count($sessions_array);$i++) {
			
			$session_type = $sessions_array[$i]->SessionType;
			
			$dr_id = $sessions_array[$i]->UserID;
			
			$appt_book_id = $sessions_array[$i]->ApptBookID;
			
			$dr_appt_book_id = $sessions_array[$i]->DoctorApptBookID;
				
			$day = (int) $sessions_array[$i]->DayOfWeek;

			$s = $sessions_array[$i]->StartTime;

			$e =  $sessions_array[$i]->EndTime;

			$l = $sessions_array[$i]->Length;

			while ($s < $e) {

				for ($d=0;$d<count($session_day_array);$d++) {
	
						if ($day == (int) $session_day_array[$d]['session_day'] && !in_array($session_day_array[$d]['session_date'], $days_away_array)) {
	
							$Appointment = new stdClass();
							
							$Appointment->SessionType = $session_type;
	
							$Appointment->ApptID = -1;
	
							$Appointment->PractitionerID = $dr_id;
							
							$Appointment->ApptBookID = $appt_book_id;
							
							$Appointment->DoctorApptBookID = $dr_appt_book_id;
	
							$Appointment->PatientID = 0;
	
							$elapsed_hours = floor ($s / 3600);
	
							$remaining = $s - ($elapsed_hours * 3600);
	
							$elapsed_minutes = floor ($remaining / 60);
	
							$remaining = $remaining - ($elapsed_minutes * 60);
	
							if ($elapsed_hours < 10)
	
							{
	
								$hours = "0" . $elapsed_hours;
	
							}
	
							else
	
							{
	
								$hours = $elapsed_hours;
	
							}
	
							if ($elapsed_minutes < 10)
	
							{
	
								$minutes = "0" . $elapsed_minutes;
	
							}
	
							else
	
							{
	
								$minutes = $elapsed_minutes;
	
							}
	
							$Appointment->When = $session_day_array[$d]['session_date'] . " " . $hours . ":" . $minutes . ":00.000";
							$Appointment->Length = $l;
							
							$apptLength = $Appointment->Length / 60;
							$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
							
							$appointments[] = $Appointment;
		
						}
	
					}		
					
					$s += $l;
				}
	
			}

	
			$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_date)";
	
			$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);
	
			$booked_appointments = array();
	
				while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {
	
					$Booked = new stdClass();
	
					$Booked->ApptID = $booked_appointments_row->ApptID;
	
					$Booked->PractitionerID = $booked_appointments_row->PractitionerID;
	
					$Booked->Descrip = $booked_appointments_row->Descrip;
	
					$Booked->When = $booked_appointments_row->When;
	
					$booked_appointments[] = $Booked;
	
				}
	
	
			for ($a=0;$a<count($appointments);$a++) {
	
				for ($i=0;$i<count($booked_appointments);$i++) {
	
						if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) {
							
							if (in_array(trim($booked_appointments[$i]->Descrip), $patient_array)) {
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;
	
							} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') {
	
								$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
								$apptLength = $appointments[$a]->Length / 60;
								$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';
	
							} else {
	
								$appointments[$a]->Descrip = 'Unavailable';

	
							}
							
						} 
	
					}
	
				}
			}
	
			
	
		$patient_string .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) in ($appt_date) and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 
'internetwc' or [Descrip] IN ($sql_patients)) and [PractitionerID] = $doctor_id"; 

			$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);
	
			while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {
	
				$Appointment = new StdClass();
				$Appointment->ApptID = (int) $not_all_appts_row->ApptID;
				$Appointment->PractitionerID = (int) $not_all_appts_row->PractitionerID;
				if (strtolower(trim($not_all_appts_row->Descrip)) == 'internet') {
					$apptLength = $appointment_length / 60;
					$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
				} else {
					$Appointment->Descrip = trim($not_all_appts_row->Descrip);
				}
				$Appointment->When = trim($not_all_appts_row->When);
				$booked_date_array = explode(' ', $Appointment->When);
	
					if (!in_array($booked_date_array[0], $days_away_array)) {
						
						
							
							$appointments[] = $Appointment;
					
					}
				}	
		
			
	echo json_encode($appointments);
}

if(isset($_POST['getAllDayAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_string = $item['doctor_string'];
		$doctor_book_string = $item['doctor_book_string'];
		$doctor_length_string = $item['doctor_length_string'];
		$all_appts_doctor_string = $item['all_appts_doctor_string'];
		$all_appts_book_string = $item['all_appts_book_string'];
		$all_appts_length_string = $item['all_appts_length_string'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$user_type = $item['user_type'];
		
	}

$doctor_id_array = explode (',',$all_appts_doctor_string);
$doctor_book_array = explode (',',$all_appts_book_string);
$doctor_length_array = explode (',',$all_appts_length_string);

$patient_array = explode (',',$patient_string);

$sql_patients = implode("','", $patient_array);
$sql_patients = "'".$sql_patients."'";

for ($i=0;$i<count($doctor_id_array);$i++) {

	$doctor_id_array[$i] = (int) $doctor_id_array[$i];

}

for ($i=0;$i<count($doctor_book_array);$i++) {

	$doctor_book_array[$i] = (int) $doctor_book_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$doctor_length_array[$i] = (int) $doctor_length_array[$i];

}

$all_doctors = '';
$all_book_string = '';
$all_length_doctors = '';

if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $doctor_string .','.$all_appts_doctor_string;
	$all_book_string = $doctor_book_string .','.$all_appts_book_string;
	$all_length_doctors = $doctor_length_string .','.$all_appts_length_string;
	
} else if (strlen($doctor_string) == 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $all_appts_doctor_string;
	$all_book_string = $all_appts_book_string;
	$all_length_doctors = $all_appts_length_string;
	
} else if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) == 0) {
	
	$all_doctors = $doctor_string;
	$all_book_string = $doctor_book_string;
	$all_length_doctors = $doctor_length_string;
}

$all_doctors_id_array = explode (',',$all_doctors);
$all_book_doctors_array = explode (',',$all_book_string);
$all_length_doctors_array = explode (',',$all_length_doctors);

for ($i=0;$i<count($all_doctors_id_array);$i++) {

	$all_doctors_id_array[$i] = (int) $all_doctors_id_array[$i];

}

for ($i=0;$i<count($all_book_doctors_array);$i++) {

	$all_book_doctors_array[$i] = (int) $all_book_doctors_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$all_length_doctors_array[$i] = (int) $all_length_doctors_array[$i];

}

$session_day = intval(date('N', strtotime($appt_date)));

$session_day ++;

if ($session_day > 7) {

	$session_day -= 7;

}

$appointments = array();
$days_away_array = array();

$doctor_temp_sessions_array = array();
$sessions_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
  ,[PractitionerID]
  ,[ApptBookID]
  ,[Date] as theDate
  ,$session_day as DayOfWk
  ,[1Start] as start1
  ,[1End] as end1
  ,[2Start] as start2
  ,[2End] as end2
  ,[3Start] as start3
  ,[3End] as end3
  ,[4Start] as start4
  ,[4End] as end4
  ,[5Start] as start5
  ,[5End] as end5
  ,[6Start] as start6
  ,[6End] as end6
FROM [HCN].[dbo].[TEMP_SESS]
WHERE [PractitionerID] IN ($all_doctors)
AND CONVERT(CHAR(10),[Date],120) = '$appt_date'
ORDER BY [ApptBookID], [PractitionerID]";

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {

	$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
	$theDate = $session_date_array[0];
	
	$session_appt_book_id = (int) $get_temp_sessions_row->ApptBookID;

	if ($theDate == $appt_date && $get_temp_sessions_row->start1 === NULL) {

		$days_away_array[] = (int) $get_temp_sessions_row->PractitionerID;

	} else {
		
		if (strlen($all_appts_doctor_string) > 0) {

			if ($theDate == $appt_date && $get_temp_sessions_row->start1 !== NULL) {
	
				$startTime = getSeconds($get_temp_sessions_row->start1);
				$endTime = getSeconds($get_temp_sessions_row->end1);
				$dr_id = (int) $get_temp_sessions_row->PractitionerID;
				
				$doctor_temp_sessions_array[] = $dr_id;
				
				for($l=0;$l<count($all_doctors_id_array);$l++) {
					
					if ($all_doctors_id_array[$l] == $dr_id) {
						
						$appt_length = $all_length_doctors_array[$l];
						$appt_book_id = $all_book_doctors_array[$l];
						
						if ($session_appt_book_id == $appt_book_id) {
						
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
		
						}
							
						$startTime = getSeconds($get_temp_sessions_row->start2);
						$endTime = getSeconds($get_temp_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_temp_sessions_row->start3);
						$endTime = getSeconds($get_temp_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start4);
						$endTime = getSeconds($get_temp_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start5);
						$endTime = getSeconds($get_temp_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start6);
						$endTime = getSeconds($get_temp_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						}
						
					}
				}
		
			}
		
		}
	}
}

		if (strlen($all_appts_doctor_string) > 0) {

$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
	  ,[ApptBookID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] IN ($all_appts_doctor_string)
	  AND [1Start] IS NOT NULL
	  ORDER BY [DayOfWk], [ApptBookID], [PractitionerID]";

$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);


	while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {

			if ($get_sessions_row->DayOfWk == $session_day && !in_array($get_sessions_row->PractitionerID, $days_away_array) && !in_array($get_sessions_row->PractitionerID, $doctor_temp_sessions_array)) {
	
				$startTime = getSeconds($get_sessions_row->start1);
				$endTime = getSeconds($get_sessions_row->end1);
				$dr_id = (int) $get_sessions_row->PractitionerID;
				$session_appt_book_id = (int) $get_sessions_row->ApptBookID;
				
				for($l=0;$l<count($doctor_id_array);$l++) {
					
					if ($doctor_id_array[$l] == $dr_id) {
						
						$appt_length = $doctor_length_array[$l];
						$appt_book_id = $doctor_book_array[$l];
						
					}
				}
				
				if ($session_appt_book_id == $appt_book_id) {
					
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
					
	
				}
					
				$startTime = getSeconds($get_sessions_row->start2);
				$endTime = getSeconds($get_sessions_row->end2);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
	
				$startTime = getSeconds($get_sessions_row->start3);
				$endTime = getSeconds($get_sessions_row->end3);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start4);
				$endTime = getSeconds($get_sessions_row->end4);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start5);
				$endTime = getSeconds($get_sessions_row->end5);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start6);
				$endTime = getSeconds($get_sessions_row->end6);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				}
			}

		}

	}	

	
	if (strlen($all_appts_doctor_string) > 0) {

		for ($i=0;$i<count($sessions_array);$i++) {

			$session_type = $sessions_array[$i]->SessionType;
			
			$dr_id = $sessions_array[$i]->UserID;
			
			$appt_book_id = $sessions_array[$i]->ApptBookID;
			
			$dr_appt_book_id = $sessions_array[$i]->DoctorApptBookID;

			$day = $sessions_array[$i]->DayOfWeek;
			
			if (!in_array($dr_id, $days_away_array)) {

				if ($day == $session_day) {
	
					$s = $sessions_array[$i]->StartTime;
	
					$e =  $sessions_array[$i]->EndTime;
	
					$l = $sessions_array[$i]->Length;
			
				while ($s < $e) {
	
					$Appointment = new stdClass();
					
					$Appointment->SessionType = $session_type;
	
					$Appointment->ApptID = -1;
	
					$Appointment->PractitionerID = $dr_id;
					
					$Appointment->ApptBookID = $appt_book_id;
					
					$Appointment->DoctorApptBookID = $dr_appt_book_id;
	
					$Appointment->PatientID = 0;
	
					$elapsed_hours = floor ($s / 3600);
	
					$remaining = $s - ($elapsed_hours * 3600);
	
					$elapsed_minutes = floor ($remaining / 60);
	
					$remaining = $remaining - ($elapsed_minutes * 60);
	
					if ($elapsed_hours < 10)
	
					{
	
						$hours = "0" . $elapsed_hours;
	
					}
	
					else
	
					{
	
						$hours = $elapsed_hours;
	
					}
	
					if ($elapsed_minutes < 10)
	
					{
	
						$minutes = "0" . $elapsed_minutes;
	
					}
	
					else
	
					{
	
						$minutes = $elapsed_minutes;
	
					}
	
					$Appointment->When = $appt_date . " " . $hours . ":" . $minutes . ":00.000";
					$Appointment->Length = $l;
	
					if (in_array($dr_id, $days_away_array)) {
	
						$Appointment->Descrip = 'Unavailable';
	
					} else {
						
						$apptLength = $Appointment->Length / 60;
						$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
						
					}
	
					$appointments[] = $Appointment;
					
					$s += $l;
	
					}		
	
				}
			
			}

		}
		
$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and [PractitionerID] IN ($all_appts_doctor_string)";


		$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);

		$booked_appointments = array();

			while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {

				$Booked = new stdClass();

				$Booked->ApptID = $booked_appointments_row->ApptID;

				$Booked->PractitionerID = $booked_appointments_row->PractitionerID;

				$Booked->Descrip = $booked_appointments_row->Descrip;

				$Booked->When = $booked_appointments_row->When;

				$booked_appointments[] = $Booked;

			}


		for ($a=0;$a<count($appointments);$a++) {

			for ($i=0;$i<count($booked_appointments);$i++) {

					if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) {
						
						$test_descrip = $booked_appointments[$i]->Descrip;
						$test_descrip_array = explode(' - ', $test_descrip);
						
						if ($test_descrip_array[0] == $patient_string) {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;

						} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$apptLength = $appointments[$a]->Length / 60;
							$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';

						} else {

							$appointments[$a]->Descrip = 'Unavailable';


						}
						
					} 

				}

			}
		
	}
	

		$patient_string .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 
'internetwc' or [Descrip] IN ($sql_patients)) and [PractitionerID] in ($all_doctors)"; 


		$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);

		while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $not_all_appts_row->ApptID;

			$dr_id = (int) $not_all_appts_row->PractitionerID;
			$appt_book_id = (int) $not_all_appts_row->ApptBookID;
			
			for($l=0;$l<count($all_doctors_id_array);$l++) {
					
				if ($all_doctors_id_array[$l] == $dr_id) {
					
					$dr_book_id = $all_book_doctors_array[$l];
					$appt_length = $all_length_doctors_array[$l];
					
				}
				
			}

			$Appointment->PractitionerID = $dr_id;
			if (trim($not_all_appts_row->Descrip) == 'Internet' || trim($not_all_appts_row->Descrip) == 'Internetwc') {
				$apptLength = $appt_length / 60;
				$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
			} else {
				$Appointment->Descrip = trim($not_all_appts_row->Descrip);
			}
			
			$Appointment->When = trim($not_all_appts_row->When);
			$Appointment->Length = $appt_length;
			
				if (!in_array($Appointment->PractitionerID, $days_away_array) && $appt_book_id == $dr_book_id) {
					
					$appointments[] = $Appointment;
		
				}

			}
	
	$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
		$orderby = "PractitionerID"; //change this to whatever key you want from the array
		
		if(count($appointments) > 0) {
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
		}
	
	
		
	echo json_encode($appointments);

}

if(isset($_POST['getDoctorsAdmin']))
{
	

 	$sql = "SELECT     PRAC.PractitionerID, PRAC.Practitioner, PRAC.Name, PRAC.ApptBookID
FROM         PRAC INNER JOIN
                      APPTBOOKSET ON PRAC.ApptBookID = APPTBOOKSET.ApptBookID
WHERE     (PRAC.Deleted = 'N') AND (APPTBOOKSET.Deleted = 'N')";
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->Name);
			$doctor->data = (int) $row->PractitionerID;
			$doctor->doctor_id = (int) $row->PractitionerID;
			$doctor->doctor_code = trim($row->Practitioner);
			$doctor->doctor_name = trim($row->Name);
			$doctor->appointment_book_id = (int) $row->ApptBookID;
			$p[] = $doctor;
		}

	echo json_encode($p);
	
}

?>