<?php

require_once "config.php";

$code_version = '131125a';

function getSeconds($startTime) {
	
	if (strlen($startTime) == 0) {
		
		return 60;
		
	} else {
		
		$startTime = explode (' ', $startTime);
		$startTime = explode ('.', $startTime[1]);
		$hms = $startTime[0];
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60) + 60;
		$seconds += (intval($s));
		return $seconds;
	}
	
}
		$appt_date = $_GET['appt_day'];
		$doctor_string = $_GET['doctor_string'];
		$doctor_book_string = $_GET['doctor_book_string'];
		$doctor_length_string = $_GET['doctor_length_string'];
		$all_appts_doctor_string = $_GET['all_appts_doctor_string'];
		$all_appts_book_string = $_GET['all_appts_book_string'];
		$all_appts_length_string = $_GET['all_appts_length_string'];
		$patient_string = $_GET['patient_string'];
		$patient_description = str_replace("*", "'", $_GET['patient_description']);
		$user_type = "patient";
	

$doctor_id_array = explode (',',$all_appts_doctor_string);
$doctor_book_array = explode (',',$all_appts_book_string);
$doctor_length_array = explode (',',$all_appts_length_string);

$patient_array = explode (',',$patient_string);

echo '

patient_array ->

';
print_r($patient_array);

$sql_patients = implode("','", $patient_array);
$sql_patients = "'".$sql_patients."'";

for ($i=0;$i<count($doctor_id_array);$i++) {

	$doctor_id_array[$i] = (int) $doctor_id_array[$i];

}

for ($i=0;$i<count($doctor_book_array);$i++) {

	$doctor_book_array[$i] = (int) $doctor_book_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$doctor_length_array[$i] = (int) $doctor_length_array[$i];

}

echo '

doctor_id_array ->

';

print_r($doctor_id_array);

echo '

doctor_book_array ->

';

print_r($doctor_book_array);

echo '

doctor_length_array ->

';

print_r($doctor_length_array);

$all_doctors = '';
$all_book_string = '';
$all_length_doctors = '';

if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $doctor_string .','.$all_appts_doctor_string;
	$all_book_string = $doctor_book_string .','.$all_appts_book_string;
	$all_length_doctors = $doctor_length_string .','.$all_appts_length_string;
	
} else if (strlen($doctor_string) == 0 && strlen($all_appts_doctor_string) > 0) {
	
	$all_doctors = $all_appts_doctor_string;
	$all_book_string = $all_appts_book_string;
	$all_length_doctors = $all_appts_length_string;
	
} else if (strlen($doctor_string) > 0 && strlen($all_appts_doctor_string) == 0) {
	
	$all_doctors = $doctor_string;
	$all_book_string = $doctor_book_string;
	$all_length_doctors = $doctor_length_string;
}

$all_doctors_id_array = explode (',',$all_doctors);
$all_book_doctors_array = explode (',',$all_book_string);
$all_length_doctors_array = explode (',',$all_length_doctors);

for ($i=0;$i<count($all_doctors_id_array);$i++) {

	$all_doctors_id_array[$i] = (int) $all_doctors_id_array[$i];

}

for ($i=0;$i<count($all_book_doctors_array);$i++) {

	$all_book_doctors_array[$i] = (int) $all_book_doctors_array[$i];

}

for ($i=0;$i<count($doctor_length_array);$i++) {

	$all_length_doctors_array[$i] = (int) $all_length_doctors_array[$i];

}

echo '

all_doctors_id_array ->

';

print_r($all_doctors_id_array);

echo '

all_book_doctors_array ->

';

print_r($all_book_doctors_array);

echo '

all_length_doctors_array ->

';

print_r($all_length_doctors_array);

$session_day = intval(date('N', strtotime($appt_date)));

$session_day ++;

if ($session_day > 7) {

	$session_day -= 7;

}

$appointments = array();
$days_away_array = array();

$doctor_temp_sessions_array = array();
$sessions_array = array();

$get_temp_sessions_sql = "SELECT [Temp_SessID] as SessionID
  ,[PractitionerID]
  ,[ApptBookID]
  ,[Date] as theDate
  ,$session_day as DayOfWk
  ,[1Start] as start1
  ,[1End] as end1
  ,[2Start] as start2
  ,[2End] as end2
  ,[3Start] as start3
  ,[3End] as end3
  ,[4Start] as start4
  ,[4End] as end4
  ,[5Start] as start5
  ,[5End] as end5
  ,[6Start] as start6
  ,[6End] as end6
FROM [HCN].[dbo].[TEMP_SESS]
WHERE [PractitionerID] IN ($all_doctors)
AND CONVERT(CHAR(10),[Date],120) = '$appt_date'
ORDER BY [ApptBookID], [PractitionerID]";

echo '

get_temp_sessions_sql ->

';

print_r($get_temp_sessions_sql);

$get_temp_sessions_rs = odbc_exec($Connect,$get_temp_sessions_sql);

while ($get_temp_sessions_row = odbc_fetch_object($get_temp_sessions_rs)) {
	
	echo '
	
get_temp_sessions_row ->

';

print_r($get_temp_sessions_row);

	$session_date_array = explode(' ', $get_temp_sessions_row->theDate);
	$theDate = $session_date_array[0];
	
	$session_appt_book_id = (int) $get_temp_sessions_row->ApptBookID;

	if ($theDate == $appt_date && $get_temp_sessions_row->start1 === NULL) {

		$days_away_array[] = (int) $get_temp_sessions_row->PractitionerID;

	} else {
		
		if (strlen($all_appts_doctor_string) > 0) {

			if ($theDate == $appt_date && $get_temp_sessions_row->start1 !== NULL) {
	
				$startTime = getSeconds($get_temp_sessions_row->start1);
				$endTime = getSeconds($get_temp_sessions_row->end1);
				$dr_id = (int) $get_temp_sessions_row->PractitionerID;
				
				$doctor_temp_sessions_array[] = $dr_id;
				
				for($l=0;$l<count($all_doctors_id_array);$l++) {
					
					if ($all_doctors_id_array[$l] == $dr_id) {
						
						$appt_length = $all_length_doctors_array[$l];
						$appt_book_id = $all_book_doctors_array[$l];
						
						if ($session_appt_book_id == $appt_book_id) {
						
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
		
						}
							
						$startTime = getSeconds($get_temp_sessions_row->start2);
						$endTime = getSeconds($get_temp_sessions_row->end2);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
			
						$startTime = getSeconds($get_temp_sessions_row->start3);
						$endTime = getSeconds($get_temp_sessions_row->end3);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start4);
						$endTime = getSeconds($get_temp_sessions_row->end4);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start5);
						$endTime = getSeconds($get_temp_sessions_row->end5);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						$startTime = getSeconds($get_temp_sessions_row->start6);
						$endTime = getSeconds($get_temp_sessions_row->end6);
			
						if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
			
							$Session = new StdClass();
							$Session->SessionType = 'Temporary';
							$Session->UserID = $dr_id;
							$Session->ApptBookID = $session_appt_book_id;
							$Session->DoctorApptBookID = $appt_book_id;
							$Session->theDate = $theDate;
							$Session->DayOfWeek = $get_temp_sessions_row->DayOfWk;
							$Session->StartTime = $startTime;
							$Session->EndTime = $endTime;
							$Session->Length = $appt_length;
							$sessions_array[] = $Session;
			
						}
						
						}
						
					}
				}
		
			}
		
		}
	}
}

	echo '
	
days_away_array ->

';

print_r($days_away_array);

	echo '
	
sessions_array (temp) ->

';

print_r($sessions_array);

		if (strlen($all_appts_doctor_string) > 0) {

$get_sessions_sql = "SELECT [SessionID] as SessionID
	  ,[PractitionerID]
	  ,[ApptBookID]
	  ,[DayOfWk] as DayOfWk
	  ,[1Start] as start1
	  ,[1End] as end1
	  ,[2Start] as start2
	  ,[2End] as end2
	  ,[3Start] as start3
	  ,[3End] as end3
	  ,[4Start] as start4
	  ,[4End] as end4
	  ,[5Start] as start5
	  ,[5End] as end5
	  ,[6Start] as start6
	  ,[6End] as end6
	  FROM [HCN].[dbo].[SESSION]
	  WHERE [PractitionerID] IN ($all_appts_doctor_string)
	  AND [1Start] IS NOT NULL
	  ORDER BY [DayOfWk], [ApptBookID], [PractitionerID]";
	  
	  	echo '
	
get_sessions_sql ->

';

print_r($get_sessions_sql);

$get_sessions_rs = odbc_exec($Connect,$get_sessions_sql);


	while ($get_sessions_row = odbc_fetch_object($get_sessions_rs)) {
		
			  	echo '
	
get_sessions_row ->

';

print_r($get_sessions_row);

			if ($get_sessions_row->DayOfWk == $session_day && !in_array($get_sessions_row->PractitionerID, $days_away_array) && !in_array($get_sessions_row->PractitionerID, $doctor_temp_sessions_array)) {
	
				$startTime = getSeconds($get_sessions_row->start1);
				$endTime = getSeconds($get_sessions_row->end1);
				$dr_id = (int) $get_sessions_row->PractitionerID;
				$session_appt_book_id = (int) $get_sessions_row->ApptBookID;
				
				for($l=0;$l<count($doctor_id_array);$l++) {
					
					if ($doctor_id_array[$l] == $dr_id) {
						
						$appt_length = $doctor_length_array[$l];
						$appt_book_id = $doctor_book_array[$l];
						
					}
				}
				
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
					
	
				}
					
				$startTime = getSeconds($get_sessions_row->start2);
				$endTime = getSeconds($get_sessions_row->end2);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
	
				$startTime = getSeconds($get_sessions_row->start3);
				$endTime = getSeconds($get_sessions_row->end3);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start4);
				$endTime = getSeconds($get_sessions_row->end4);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start5);
				$endTime = getSeconds($get_sessions_row->end5);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
				
				$startTime = getSeconds($get_sessions_row->start6);
				$endTime = getSeconds($get_sessions_row->end6);
	
				if ($startTime > 60 && $endTime > 60  && $endTime > $startTime) {
	
					$Session = new StdClass();
					$Session->SessionType = 'Standard';
					$Session->UserID = $dr_id;
					$Session->ApptBookID = $session_appt_book_id;
					$Session->DoctorApptBookID = $appt_book_id;
					$Session->theDate = $appt_date;
					$Session->DayOfWeek = $get_sessions_row->DayOfWk;
					$Session->StartTime = $startTime;
					$Session->EndTime = $endTime;
					$Session->Length = $appt_length;
					$sessions_array[] = $Session;
	
				}
			}

		}

	}	
	
	echo '
	
sessions_array (standard) ->

';

print_r($sessions_array);

	
	if (strlen($all_appts_doctor_string) > 0) {

		for ($i=0;$i<count($sessions_array);$i++) {

			$session_type = $sessions_array[$i]->SessionType;
			
			$dr_id = $sessions_array[$i]->UserID;
			
			$appt_book_id = $sessions_array[$i]->ApptBookID;
			
			$dr_appt_book_id = $sessions_array[$i]->DoctorApptBookID;

			$day = $sessions_array[$i]->DayOfWeek;
			
			if (!in_array($dr_id, $days_away_array)) {

				if ($day == $session_day) {
	
					$s = $sessions_array[$i]->StartTime;
	
					$e =  $sessions_array[$i]->EndTime;
	
					$l = $sessions_array[$i]->Length;
			
				while ($s < $e) {
	
					$Appointment = new stdClass();
					
					$Appointment->SessionType = $session_type;
	
					$Appointment->ApptID = -1;
	
					$Appointment->PractitionerID = $dr_id;
					
					$Appointment->ApptBookID = $appt_book_id;
					
					$Appointment->DoctorApptBookID = $dr_appt_book_id;
	
					$Appointment->PatientID = 0;
	
					$elapsed_hours = floor ($s / 3600);
	
					$remaining = $s - ($elapsed_hours * 3600);
	
					$elapsed_minutes = floor ($remaining / 60);
	
					$remaining = $remaining - ($elapsed_minutes * 60);
	
					if ($elapsed_hours < 10)
	
					{
	
						$hours = "0" . $elapsed_hours;
	
					}
	
					else
	
					{
	
						$hours = $elapsed_hours;
	
					}
	
					if ($elapsed_minutes < 10)
	
					{
	
						$minutes = "0" . $elapsed_minutes;
	
					}
	
					else
	
					{
	
						$minutes = $elapsed_minutes;
	
					}
	
					$Appointment->When = $appt_date . " " . $hours . ":" . $minutes . ":00.000";
					$Appointment->Length = $l;
	
					if (in_array($dr_id, $days_away_array)) {
	
						$Appointment->Descrip = 'Unavailable';
	
					} else {
						
						$apptLength = $Appointment->Length / 60;
						$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
						
					}
	
					$appointments[] = $Appointment;
					
					$s += $l;
	
					}		
	
				}
			
			}

		}
		
$booked_appointments_sql = "SELECT [ApptID], [PractitionerID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and [PractitionerID] IN ($all_appts_doctor_string)";

echo '
	
booked_appointments_sql ->

';

print_r($booked_appointments_sql);


		$booked_appointments_rs = odbc_exec($Connect,$booked_appointments_sql);

		$booked_appointments = array();

			while ($booked_appointments_row = odbc_fetch_object($booked_appointments_rs)) {
				
				echo '
	
booked_appointments_row ->

';

print_r($booked_appointments_row);

				$Booked = new stdClass();

				$Booked->ApptID = $booked_appointments_row->ApptID;

				$Booked->PractitionerID = $booked_appointments_row->PractitionerID;

				$Booked->Descrip = $booked_appointments_row->Descrip;

				$Booked->When = $booked_appointments_row->When;

				$booked_appointments[] = $Booked;

			}


		for ($a=0;$a<count($appointments);$a++) {

			for ($i=0;$i<count($booked_appointments);$i++) {

					if ($appointments[$a]->PractitionerID == $booked_appointments[$i]->PractitionerID && trim($appointments[$a]->When) == trim($booked_appointments[$i]->When)) {
						
						$test_descrip = $booked_appointments[$i]->Descrip;
						$test_descrip_array = explode(' - ', $test_descrip);
						
						if ($test_descrip_array[0] == $patient_string) {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$appointments[$a]->Descrip = $booked_appointments[$i]->Descrip;

						} else if (strtolower(trim($booked_appointments[$i]->Descrip)) == 'internet' || strtolower(trim($booked_appointments[$i]->Descrip)) == 'internetwc') {

							$appointments[$a]->ApptID = $booked_appointments[$i]->ApptID;
							$apptLength = $appointments[$a]->Length / 60;
							$appointments[$a]->Descrip = 'Available
('.$apptLength.' mins)';

						} else {

							$appointments[$a]->Descrip = 'Unavailable';


						}
						
					} 

				}

			}
		
	}
	

		$patient_string .= '%';

		$not_all_appts_sql = "SELECT [ApptID], [PractitionerID], [ApptBookID], [Descrip], [When] FROM APPT WHERE CONVERT(CHAR(10),[When],120) = '$appt_date' and (LOWER([Descrip]) = 'internet' or LOWER([Descrip]) = 
'internetwc' or [Descrip] IN ($sql_patients)) and [PractitionerID] in ($all_doctors)"; 

	echo '
	
not_all_appts_sql ->

';

print_r($not_all_appts_sql);

		$not_all_appts_rs = odbc_exec($Connect,$not_all_appts_sql);

		while ($not_all_appts_row = odbc_fetch_object($not_all_appts_rs)) {
			
			echo '
	
not_all_appts_row ->

';

print_r($not_all_appts_row);

			$Appointment = new StdClass();
			$Appointment->ApptID = (int) $not_all_appts_row->ApptID;

			$dr_id = (int) $not_all_appts_row->PractitionerID;
			$appt_book_id = (int) $not_all_appts_row->ApptBookID;
			
			for($l=0;$l<count($all_doctors_id_array);$l++) {
					
				if ($all_doctors_id_array[$l] == $dr_id) {
					
					$dr_book_id = $all_book_doctors_array[$l];
					$appt_length = $all_length_doctors_array[$l];
					
				}
				
			}

			$Appointment->PractitionerID = $dr_id;
			if (trim($not_all_appts_row->Descrip) == 'Internet' || trim($not_all_appts_row->Descrip) == 'Internetwc') {
				$apptLength = $appt_length / 60;
				$Appointment->Descrip = 'Available
('.$apptLength.' mins)';
			} else {
				$Appointment->Descrip = trim($not_all_appts_row->Descrip);
			}
			
			$Appointment->When = trim($not_all_appts_row->When);
			$Appointment->Length = $appt_length;
			
				if (!in_array($Appointment->PractitionerID, $days_away_array) && $appt_book_id == $dr_book_id) {
					
					$appointments[] = $Appointment;
		
				}

			}
	
	$sortArray = array();
	
			foreach($appointments as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
		$orderby = "PractitionerID"; //change this to whatever key you want from the array
		
		if(count($appointments) > 0) {
			array_multisort($sortArray[$orderby],SORT_ASC,$appointments); 
		}
	
	
		echo '
	
appointments ->

';

print_r($appointments);
	//print_r($appointments);




?>