<?php

require_once('configpractice2000.php');

$code_version = '131217';

function getSeconds($startTime) {
	
	if (strlen($startTime) == 0) {
		
		return 60;
		
	} else {
		
		$startTime = explode (' ', $startTime);
		$startTime = explode ('.', $startTime[1]);
		$hms = $startTime[0];
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60) + 60;
		$seconds += (intval($s));
		return $seconds;
	}
	
}

if (isset($_GET['showVersion']))

{
	
	echo $code_version;
	
}

if(isset($_POST['makePassword']))
{

  $length = 6;
  // start with a blank password
  $password = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
    
  // set up a counter
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($password, $char)) { 
      $password .= $char;
      $i++;
    }

  }
					
  // done!
	echo json_encode($password);

}

if(isset($_POST['makeAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$appt_date = $item['appt_date'];
		$delete_from_day = $item['delete_from_day'];
		$patient_description = trim($item['patient_description']);
		$doctor_id = $item['doctor_id'];
		$doctor_code = $item['doctor_code'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$appt_pos = $item['appt_pos'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$include_in_notifications = $item['include_in_notifications'];
	
	}

	$type_colour_sql = "Select ApptBookColors.ID, ApptBookColorsDetail.ID AS IDColorDetail
From ApptBookColors Right Join ApptBookColorsDetail ON ApptBookColors.ID = ApptBookColorsDetail.IDApptBookColors  
Where ApptBookColors.StartText = 'C'";

	$type_colour_rs = odbc_exec($Connect,$type_colour_sql);
	
	while ($type_colour_row = odbc_fetch_object($type_colour_rs)) {
			
		$cancel_id = (int) $type_colour_row->ID;
		$cancel_color = (int) $type_colour_row->IDColorDetail;
			
	}

	$appt_type_colour_sql = "Select top 1 ApptBookColors.ID as TypeID, ApptBookColorsDetail.ID AS ColorID
From ApptBookColors Right Join ApptBookColorsDetail ON ApptBookColors.ID = ApptBookColorsDetail.IDApptBookColors  
Where ApptBookColors.StartText = '/'";

	$appt_type_colour_rs = odbc_exec($Connect,$appt_type_colour_sql);
	
	while ($appt_type_colour_row = odbc_fetch_object($appt_type_colour_rs)) {
			
		$appt_type_id = (int) $appt_type_colour_row->TypeID;
		$appt_type_color = (int) $appt_type_colour_row->ColorID;
			
	} 
	
	$pos = strrpos($patient_description, '[DOB:');
	
	if ($pos !== false) {
		
			$sql = "select * from [Appointments] 
Where [Desc] = '$patient_description' 
and CONVERT(CHAR(10),[Time],120) >= '$delete_from_day'
and DeletedDate is null";
		
			$rs = odbc_exec($Connect, $sql);

			if( odbc_num_rows( $rs ) < 3) {
			
				 while ($row = odbc_fetch_object($rs)) {
					 
					 $cancel_existing_sql = "Update [Appointments] 
						Set DeletedDate = Getdate(), TypeID = $cancel_id, ColorID = $cancel_color 
						Where [Desc] = '$patient_description' 
						and CONVERT(CHAR(10),[Time],120) >= '$delete_from_day'";
						
					 $cancel_existing_rs = odbc_exec($Connect,$cancel_existing_sql);
					 
				 }
			 
			}
		
	}

	$p = array();
		
	$StringResult = new StdClass();

	if ((int) $appt_id > 0) {

		$sql2 = "Update [Appointments] 
Set [Desc] = '$patient_description', [Patient#] = null, [Patient Name] = ''
where ID = $appt_id";

	} else {

		$sql2 = "
	DECLARE @RC int
	DECLARE @Action int
	DECLARE @Time datetime
	DECLARE @Provider# nvarchar(50)
	DECLARE @Patient_Name nvarchar(255)
	DECLARE @Patient# int
	DECLARE @ID int
	DECLARE @Provider int
	DECLARE @TypeID int
	DECLARE @Note nvarchar(50)
	DECLARE @Desc nvarchar(255)
	DECLARE @Pos int
	DECLARE @Temp1 nvarchar(255)
	DECLARE @ColorID int
	DECLARE @Status int

-- TODO: Set parameter values here.

EXECUTE @RC = [PRACTICE].[dbo].[sp_Appointments] 
   @Action = 0,
		@Time = N'$appt_time',
		@Provider# = N'$doctor_code',
		@Patient_Name = N'',
		@Patient# = NULL,
		@ID = $doctor_id,
		@Provider = $doctor_id,
		@TypeID = $appt_type_id,
		@Note = NULL,
		@Desc =  N'$patient_description',
		@Pos = $appt_pos,
		@Temp1 = NULL,
		@ColorID = $appt_type_color,
		@Status = NULL";

	}
	
	  
	$rs2 = odbc_exec($Connect,$sql2);

		if ($rs2) {
				
					$StringResult->result_string = "Your appointment has been saved."; 
					$StringResult->heading = "Save Appointment Successful";
					$StringResult->extra_data = 0;
					
			} else {
				
					$StringResult->result_string = "Your appointment could not be saved."; 
					$StringResult->heading = "New Appointment Failed";
					$StringResult->extra_data = $sql2;
				
			}
	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['cancelAppointment']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_id = $item['appt_id'];
		$appt_day = $item['appt_day'];
		$delete_from_day = $item['delete_from_day'];
		$appointment_description = $item['appointment_description'];
		$user_string = $item['user_string'];
		$user_type = $item['user_type'];
		$patient_description = $item['patient_description'];
		$doctor_id = $item['doctor_id'];
		$doctor_name = $item['doctor_name'];
		$appt_time = $item['appt_time'];
		$patient_name = $item['patient_name'];
		$patient_email = $item['patient_email'];
		$show_all_appointments = $item['show_all_appointments'];
		$internet_id = $item['internet_id'];
		$include_in_notifications = $item['include_in_notifications'];
	
	}

	$type_colour_sql = "Select ApptBookColors.ID, ApptBookColorsDetail.ID AS IDColorDetail
From ApptBookColors Right Join ApptBookColorsDetail ON ApptBookColors.ID = ApptBookColorsDetail.IDApptBookColors  
Where ApptBookColors.StartText = 'C'";

	$type_colour_rs = odbc_exec($Connect,$type_colour_sql);
	
	while ($type_colour_row = odbc_fetch_object($type_colour_rs)) {
			
		$cancel_id = (int) $type_colour_row->ID;
		$cancel_color = (int) $type_colour_row->IDColorDetail;
			
	}

	$appt_type_colour_sql = "Select top 1 ApptBookColors.ID as TypeID, ApptBookColorsDetail.ID AS ColorID
From ApptBookColors Right Join ApptBookColorsDetail ON ApptBookColors.ID = ApptBookColorsDetail.IDApptBookColors  
Where ApptBookColors.StartText = '/'";

	$appt_type_colour_rs = odbc_exec($Connect,$appt_type_colour_sql);
	
	while ($appt_type_colour_row = odbc_fetch_object($appt_type_colour_rs)) {
			
		$appt_type_id = (int) $appt_type_colour_row->TypeID;
		$appt_type_color = (int) $appt_type_colour_row->ColorID;
			
	} 
	
	if ($show_all_appointments == 'yes') {

		$sql = "Update [Appointments] 
Set DeletedDate = Getdate(), TypeID = $cancel_id, ColorID = $cancel_color 
Where ID = $appt_id";

	} else {
		
		$sql = "update APPT set [Descrip] = 'Internet' WHERE ApptID = $appt_id";

	}
	

	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
		
	$StringResult = new StdClass();
	
	if ($rs) {
		  
			$StringResult->result_string = "Your appointment has been cancelled."; 
			$StringResult->heading = "Cancel Appointment Successful";
			$StringResult->extra_data = $sql;
			
		} else {
		
			$StringResult->result_string = "Your appointment could not be cancelled."; 
			$StringResult->heading = "Cancel Appointment Failed";
			$StringResult->extra_data = 0;
				
		}
	
		$p[] = $StringResult;
		echo json_encode($p);
	
}

if(isset($_POST['checkAppointmentAvailability']))

{

	$jsonString = urldecode($_POST['jsonSendData']);

  	$data = json_decode($jsonString, true);

	foreach ($data as $item) {

		$appt_date = $item['appt_date'];
		$doctor_id = $item['doctor_id'];
		$appt_pos = $item['appt_pos'];
		$internet_id = (int) $item['internet_id'];

	}

	$cancel_id_sql = "Select ID From ApptBookColors Where ApptBookColors.StartText = 'C'";
		
	$cancel_id_rs = odbc_exec($Connect, $cancel_id_sql);
		
	while ($cancel_id_row = odbc_fetch_object($cancel_id_rs)) {
			
		$cancel_id = (int) $cancel_id_row->ID;
			
	}
	
	$end_time_sql = "select top 1 [End Time] as end_time from [Appointment Data]";
		
	$end_time_rs = odbc_exec($Connect, $end_time_sql);
		
	while ($end_time_row = odbc_fetch_object($end_time_rs)) {
			
		$end_time_array = explode(' ', $end_time_row->end_time);
		$end_time = $end_time_array[1];
			
	}
	
		$appt_lengths_sql = "Select ApptInterval as appt_length, ID as doctor_id, [Provider#] as doctor_code From Doctors where ID = $doctor_id";
	
		$appt_lengths_rs = odbc_exec($Connect, $appt_lengths_sql);
		
		
		while ($appt_lengths_row = odbc_fetch_object($appt_lengths_rs)) {
			
			$appt_length = (int) $appt_lengths_row->appt_length;
			
		}
	
$booked_sql = "Select DisplayCol as descrip, [Datetime] as app_time, [Patient#] as patient_id, [Pos] as pos, TypeID as appt_type
From ApptBookView($doctor_id, '$appt_date', $appt_length, '$end_time', $cancel_id ) 
where [Pos] = $appt_pos";

			$booked_array = array();

			$booked_rs = odbc_exec($Connect,$booked_sql);

			
				while ($booked_row = odbc_fetch_object($booked_rs)) {
					
					$patient_id = $appts_row->patient_id;
					$appt_descrip = trim($booked_row->appt_descrip);
					$appt_type = $booked_row->appt_type;
					
					if (($appt_descrip == '' && $appt_type === NULL && $patient_id === NULL)) {

						$Booked = new stdClass();

						$Booked->is_available = 1;

						$booked_array[] = $Booked;

					} else {

						$Booked = new stdClass();

						$Booked->is_available = 0;

						$booked_array[] = $Booked;

					}

				}


	echo json_encode($booked_array);

}

if(isset($_POST['getAllDayAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_string = $item['doctor_string'];
		$all_appts_doctor_string = $item['all_appts_doctor_string'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$user_type = $item['user_type'];
		$internet_id = (int) $item['internet_id'];
		
	}
	
	$patient_array = explode (',',$patient_string);
	
	$session_day = intval(date('N', strtotime($appt_date)));
	
	$session_day ++;
	
	$session_day = (string) $session_day;

	$cancel_id_sql = "Select ID From ApptBookColors Where ApptBookColors.StartText = 'C'";
		
	$cancel_id_rs = odbc_exec($Connect, $cancel_id_sql);
		
	while ($cancel_id_row = odbc_fetch_object($cancel_id_rs)) {
			
		$cancel_id = (int) $cancel_id_row->ID;
			
	}
	
	$end_time_sql = "select top 1 [End Time] as end_time from [Appointment Data]";
		
	$end_time_rs = odbc_exec($Connect, $end_time_sql);
		
	while ($end_time_row = odbc_fetch_object($end_time_rs)) {
			
		$end_time_array = explode(' ', $end_time_row->end_time);
		$end_time = $end_time_array[1];
			
	}
	
	$appointments = array();
	
	if (strlen($all_appts_doctor_string) > 0) {
		
		$appt_lengths_sql = "Select ApptInterval as appt_length, ID as doctor_id, [Provider#] as doctor_code From Doctors where ID in ($all_appts_doctor_string)";
		
		$appt_lengths_rs = odbc_exec($Connect, $appt_lengths_sql);
		
		$appt_lengths = array();
		
		while ($appt_lengths_row = odbc_fetch_object($appt_lengths_rs)) {
			
			$ApptLength = new StdClass();
			$ApptLength->doctor_id = (int) $appt_lengths_row->doctor_id;
			$ApptLength->doctor_code = $appt_lengths_row->doctor_code;
			$ApptLength->appt_length = (int) $appt_lengths_row->appt_length;
			$ApptLength->doctor_days = '';
			$appt_lengths[] = $ApptLength;
			
		}
		
		$doctor_days_sql = "Select DocApptDays, FromDate, ToDate, Doctors.ID as doctor_id 
	From Doctors INNER JOIN DoctorApptDays ON Doctors.ID = DoctorApptDays.Provider 
	Where (ApptAvailable<>0) And (IsNumeric(Shorthand)=1) 
	And (DocApptDays<>'') And (DocApptDays Is Not Null) 
	And ('$appt_date' <= DoctorApptDays.Todate) 
	Order By DoctorApptDays.Fromdate, Len(DocApptDays) DESC";
	
		$doctor_days_rs = odbc_exec($Connect, $doctor_days_sql);
		
		while ($doctor_days_row = odbc_fetch_object($doctor_days_rs)) {
			
			for ($i=0;$i<count($appt_lengths);$i++) {
				
				if ($appt_lengths[$i]->doctor_id == (int) $doctor_days_row->doctor_id) {
					
					$appt_lengths[$i]->doctor_days = $doctor_days_row->DocApptDays;
					
				}
				
			}
				
		}
		
		for ($i=0;$i<count($appt_lengths);$i++) {
			
			$new_doc = $appt_lengths[$i]->doctor_id;
			$new_length = $appt_lengths[$i]->appt_length;
			$doctor_days = $appt_lengths[$i]->doctor_days;
			
			$appts_sql = "Select DisplayCol as appt_descrip, [Datetime] as appt_time, [Patient#] as patient_id, [Pos] as appt_pos, ID as appt_id, TypeID as appt_type
From ApptBookView($new_doc, '$appt_date', $new_length, '$end_time', $cancel_id) 
Order by [Pos]";
		
			$appts_rs = odbc_exec($Connect, $appts_sql);
			
			while ($appts_row = odbc_fetch_object($appts_rs)) {
				
					$pos = strpos($doctor_days, $session_day);
					$patient_id = $appts_row->patient_id;
					$appt_descrip = trim($appts_row->appt_descrip);
					$appt_pos = trim($appts_row->appt_pos);
					$appt_type = $appts_row->appt_type;
					
					if ($appts_row->appt_id === NULL) {
						
						$appt_id = -1;
						
					} else {
						
						$appt_id = $appts_row->appt_id;
						
					}

					if ($pos !== false) {
						
					    $Appointment = new stdClass();
	
						$Appointment->ApptID = $appt_id;
		
						$Appointment->PractitionerID = $new_doc;
		
						$Appointment->PatientID = 0;
						
						$appt_time_array = explode(' ', $appts_row->appt_time);
		
						$Appointment->When = $appt_date . " " . $appt_time_array[1];
						$Appointment->Length = $new_length;
						$Appointment->Pos = $appt_pos;
						
						if (strlen($patient_string) > 0) {
							
							
						}
						
						if (in_array($appt_descrip, $patient_array) && strlen($patient_string) > 0) {
								
								$Appointment->Descrip = $appt_descrip;
								$appointments[] = $Appointment;
								
						} else if (($appt_descrip == '' && $appt_type === NULL && $patient_id === NULL)) {
								
								$Appointment->Descrip = 'Available
('.$new_length.' mins)';
								
								$appointments[] = $Appointment;
								
						} 
						
					}
			}
			
		}
		
	}
	
	echo json_encode($appointments);

}

if(isset($_POST['getAllWeekAppointments']))
{
	
	$jsonString = urldecode($_POST['jsonSendData']);
  	$jsonString = str_replace("\\", "", $jsonString);
	
  	$data = json_decode($jsonString, true);
  
	foreach ($data as $item) {
		
		$appt_date = $item['appt_day'];
		$doctor_id = (int) $item['doctor_id'];
		$patient_string = $item['patient_string'];
		$patient_description = str_replace("*", "'", $item['patient_description']);
		$show_all_appointments = $item['show_all_appointments'];
		$user_type = $item['user_type'];
		$internet_id = (int) $item['internet_id'];
		
	}

$patient_array = explode (',',$patient_string);

for ($i=0;$i<count($patient_array);$i++) {

	$patient_array[$i] = trim($patient_array[$i]);

}

$appt_date_cleared = str_replace("'","",$appt_date);

$days_array = explode (',',$appt_date_cleared);

$session_day_array = array();
$session_day_number_array = array();

for ($i=0;$i<count($days_array);$i++) {

	$session_day = intval(date('N', strtotime($days_array[$i])));

	$session_day ++;
	
	if ($session_day > 7) {

		$session_day -= 7;

	}

	$session_day_array[] = array("session_day" => $session_day, "session_date" => $days_array[$i]);
	$session_day_number_array[] = $session_day;
	
}

	$cancel_id_sql = "Select ID From ApptBookColors Where ApptBookColors.StartText = 'C'";
		
	$cancel_id_rs = odbc_exec($Connect, $cancel_id_sql);
		
	while ($cancel_id_row = odbc_fetch_object($cancel_id_rs)) {
			
		$cancel_id = (int) $cancel_id_row->ID;
			
	}
	
	$end_time_sql = "select top 1 [End Time] as end_time from [Appointment Data]";
		
	$end_time_rs = odbc_exec($Connect, $end_time_sql);
		
	while ($end_time_row = odbc_fetch_object($end_time_rs)) {
			
		$end_time_array = explode(' ', $end_time_row->end_time);
		$end_time = $end_time_array[1];
			
	}

		$appt_lengths_sql = "Select ApptInterval as appt_length, ID as doctor_id, [Provider#] as doctor_code From Doctors where ID = $doctor_id";
	
		$appt_lengths_rs = odbc_exec($Connect, $appt_lengths_sql);
		
		$appt_lengths = array();
		
		while ($appt_lengths_row = odbc_fetch_object($appt_lengths_rs)) {
			
			$ApptLength = new StdClass();
			$ApptLength->doctor_id = (int) $appt_lengths_row->doctor_id;
			$ApptLength->doctor_code = $appt_lengths_row->doctor_code;
			$ApptLength->appt_length = (int) $appt_lengths_row->appt_length;
			$ApptLength->doctor_days = '';
			$appt_lengths[] = $ApptLength;
			
		}
	
	$last_day = count($days_array);
	$last_day --;
	$Todate = $days_array[$last_day];
		
		$doctor_days_sql = "Select DocApptDays, FromDate, ToDate, Doctors.ID as doctor_id 
	From Doctors INNER JOIN DoctorApptDays ON Doctors.ID = DoctorApptDays.Provider 
	Where (ApptAvailable<>0) And (IsNumeric(Shorthand)=1) 
	And (DocApptDays<>'') And (DocApptDays Is Not Null) 
	And ('$Todate' <= DoctorApptDays.Todate)
	And (Doctors.ID = $doctor_id) 
	Order By DoctorApptDays.Fromdate, Len(DocApptDays) DESC";
	
	$doctor_days_rs = odbc_exec($Connect, $doctor_days_sql);
		
		while ($doctor_days_row = odbc_fetch_object($doctor_days_rs)) {
			
			for ($i=0;$i<count($appt_lengths);$i++) {
				
				if ($appt_lengths[$i]->doctor_id == (int) $doctor_days_row->doctor_id) {
					
					$appt_lengths[$i]->doctor_days = $doctor_days_row->DocApptDays;
					
				}
				
			}
				
		}
	
	$new_doc = $appt_lengths[0]->doctor_id;
	$new_length = $appt_lengths[0]->appt_length;
	$doctor_days = $appt_lengths[0]->doctor_days;	
	
	$appointments = array();
		
	for ($i=0;$i<count($session_day_array);$i++) {
			
			$appt_date = $session_day_array[$i]['session_date'];
			$session_day = (string) $session_day_array[$i]['session_day'];
		
			$appts_sql = "Select DisplayCol as appt_descrip, [Datetime] as appt_time, [Patient#] as patient_id, [Pos] as appt_pos, ID as appt_id, TypeID as appt_type
From ApptBookView($new_doc, '$appt_date', $new_length, '$end_time', $cancel_id) 
Order by [Pos]";
		
			$appts_rs = odbc_exec($Connect, $appts_sql);
			
			while ($appts_row = odbc_fetch_object($appts_rs)) {
				
					$pos = strpos($doctor_days, $session_day);
					$patient_id = $appts_row->patient_id;
					$appt_descrip = trim($appts_row->appt_descrip);
					$appt_pos = (int) $appts_row->appt_pos;
					$appt_type = $appts_row->appt_type;

					if ($appts_row->appt_id === NULL) {
						
						$appt_id = -1;
						
					} else {
						
						$appt_id = $appts_row->appt_id;
						
					}

					if ($pos !== false) {
						
					    $Appointment = new stdClass();
	
						$Appointment->ApptID = $appt_id;
		
						$Appointment->PractitionerID = $new_doc;
		
						$Appointment->PatientID = 0;
						
						$appt_time_array = explode(' ', $appts_row->appt_time);
		
						$Appointment->When = $appt_date . " " . $appt_time_array[1];
						$Appointment->Length = $new_length;
						$Appointment->Pos = $appt_pos;
						
						if (in_array($appt_descrip, $patient_array) && strlen($patient_string) > 0) {
								
								$Appointment->Descrip = $appt_descrip;
								$appointments[] = $Appointment;
								
							} else if (($appt_descrip == '' && $appt_type === NULL && $patient_id === NULL)) {
								
								$Appointment->Descrip = 'Available
('.$new_length.' mins)';
								
								$appointments[] = $Appointment;
								
							} 
					}
			}
			
		}

echo json_encode($appointments);

}

if(isset($_POST['getInternetPatientID']))

{

	$sql = "select [Patient#] as internetID from Patients where UPPER(Surname) = 'INTERNET'";

   	$rs = odbc_exec($Connect, $sql);

 	$p = array();

	 while ($row = odbc_fetch_object($rs)) {

			$InternetID = new stdClass();
			$InternetID->internetID = (int) $row->internetID;
			$p[] = $InternetID;

		}

 echo json_encode($p);

}


if(isset($_POST['getInternetPatientIDEmployer']))

{

	$sql = "select [Patient#] as internetID from Patients where UPPER(Surname) = 'INTERNETWC'";

   	$rs = odbc_exec($Connect, $sql);

 	$p = array();

	 while ($row = odbc_fetch_object($rs)) {

			$InternetID = new stdClass();
			$InternetID->internetID = (int) $row->internetID;
			$p[] = $InternetID;

		}

 echo json_encode($p);

}


if(isset($_POST['getDoctors']))
{
	
		
		$sql = "SELECT     Surname as last_name, [First Name] as first_name, Title, ID as DoctorID, [Provider#] as DoctorCode 
FROM         Doctors
WHERE (ApptAvailable<>0) 
And (IsNumeric(Shorthand)=1) 
And DoctorGroup = 1 
Order By CONVERT(numeric, Shorthand)"; 
	  
	$rs = odbc_exec($Connect,$sql);
	
	$p = array();
	
		while ($row = odbc_fetch_object($rs)) {

			$doctor = new StdClass();
			$doctor->label = trim($row->Title).' '.trim($row->first_name).' '.trim($row->last_name);
			$doctor->data = (int) $row->DoctorID;
			$doctor->DoctorID = (int) $row->DoctorID;
			$doctor->DoctorApptID = (int) $row->DoctorID;
			$doctor->DoctorCode = $row->DoctorCode;
			$doctor->FullName = trim($row->Title).' '.trim($row->first_name).' '.trim($row->last_name);
			
			$p[] = $doctor;
		}
	
	echo json_encode($p);
	
}




?>
