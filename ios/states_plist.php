<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	surgeries.surgery_state
FROM
	surgeries
WHERE
	surgery_status = 'Active'
AND surgeries.surgery_state <> ''
ORDER BY
	surgeries.surgery_state ASC");


      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->surgery_state);

	$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

      while (mysqli_stmt_fetch($stmt)) {
		  
		  switch ($row->surgery_state) {
			 
			 case 'ACT': $long_state_name = 'Australian Capital Territory';
			 break; 
			 case 'NSW': $long_state_name = 'New South Wales';
			 break; 
			  case 'NT': $long_state_name = 'Northern Territory';
			 break; 
			  case 'QLD': $long_state_name = 'Queensland';
			 break;
			  case 'SA': $long_state_name = 'South Australia';
			 break;  
			  case 'TAS': $long_state_name = 'Tasmania';
			 break; 
			  case 'TEST': $long_state_name = 'Test State';
			 break; 
			  case 'VIC': $long_state_name = 'Victoria';
			 break; 
			 case 'WA': $long_state_name = 'Western Australia';
			 break; 
			  
		  }
		  
		  $plist_string .= '
	<dict>
		<key>surgery_state</key>
		<string><![CDATA['.$row->surgery_state.']]></string>
		<key>surgery_state_long</key>
		<string><![CDATA['.$long_state_name.']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
echo $plist_string;

?>



