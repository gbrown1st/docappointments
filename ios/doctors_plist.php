<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_GET['surgery_id'];

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
      }

      mysqli_stmt_free_result($stmt);

	  $doc_loc_stmt = mysqli_prepare($mysqli,
	  "SELECT
	doctor_locations.doctor_id,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone
FROM
	doctor_locations
INNER JOIN surgery_locations ON doctor_locations.location_id = surgery_locations.surgery_location_id
WHERE
	doctor_locations.surgery_id = ?");
	
	  mysqli_stmt_bind_param($doc_loc_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($doc_loc_stmt);

      mysqli_stmt_bind_result($doc_loc_stmt, $row->doctor_id, $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, $row->surgery_postcode, $row->surgery_phone);
	  
	  $doc_loc_array = array();

      while (mysqli_stmt_fetch($doc_loc_stmt)) {
		 
		  $doc_loc_array[] = array('doctor_id' => $row->doctor_id, 'surgery_address_1' => $row->surgery_address_1, 'surgery_address_2' => $row->surgery_address_2, 'surgery_suburb_town' => $row->surgery_suburb_town, 'surgery_state' => $row->surgery_state, 'surgery_postcode' => $row->surgery_postcode, 'surgery_phone' => $row->surgery_phone);
		  $surgery_name = $row->surgery_name;
      }

      mysqli_stmt_free_result($doc_loc_stmt);
	  mysqli_close($mysqli);
      
		
		$post_array = array('getDoctors' => 'true');

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		

		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA[First Available Appointments]]></string>
		<key>DoctorCode</key>
		<string><![CDATA[]]></string>
		<key>DoctorID</key>
		<string><![CDATA[-1]]></string>
		<key>DoctorApptID</key>
		<string><![CDATA[]]></string>
		<key>DoctorLocation</key>
		<string><![CDATA[The first available appointments in the next 2 days.]]></string>
	</dict>';
	
      for ($i=0;$i<count($response_array);$i++) {
		  
		  for ($n=0;$n<count($doc_loc_array);$n++) {
			  
			  if ($doc_loc_array[$n]['doctor_id'] == $response_array[$i]['DoctorID']) {
				  
				  $doc_location = $surgery_name;
				  
				  $doc_location .= '
'.$doc_loc_array[$n]['surgery_address_1'];
				  
				  if (strlen($doc_loc_array[$n]['surgery_address_2']) > 2) {
					  
					  $doc_location .= '
'.$doc_loc_array[$i]['surgery_address_2'];
					  
				  }
				  
				  $doc_location .= '
'.$doc_loc_array[$n]['surgery_suburb_town'].' '.$doc_loc_array[$n]['surgery_state'].' '.$doc_loc_array[$n]['surgery_postcode'].' Phone: '.$doc_loc_array[$n]['surgery_phone'];
			  
			  }
			  
		  }
		  
			
		  $plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.$response_array[$i]['FullName'].']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.$response_array[$i]['DoctorCode'].']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.$response_array[$i]['DoctorID'].']]></string>
		<key>DoctorApptID</key>
		<string><![CDATA['.$response_array[$i]['DoctorApptID'].']]></string>
		<key>DoctorLocation</key>
		<string><![CDATA['.$doc_location.']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

?>



