<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$company = $_POST['company'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

	
	if ($surgery_software == 'BestPractice') {
		 
		 if ($user_type == 'patient') {
			 
			 $user_type = 1;
			 
		 } else {
			 
			 $user_type = 2;
			 
		 }
		 
	 }
	 
     mysqli_stmt_free_result($stmt);
	 
	 if ($surgery_software == 'Zedmed') {
		 
		 $login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and user_password = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($login_stmt);
		 
		 mysqli_stmt_bind_result($login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($login_stmt);

		 if (mysqli_stmt_num_rows($login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			 
			$json_string = '{"result": [{"heading": "Log In Successful!", "message": "Proceed with getting doctors.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'"}]}';
			 
		 } else {
			 
			 $company_name = '';
			 
			  $add_user_stmt = mysqli_prepare($mysqli, "INSERT INTO users (surgery_id, user_type, company_name, first_name, last_name, dob, family_members, email, mobile, user_username, user_password, created_date) VALUES (?, ?,?,?,?,?,?,?,?,?,?,?)");
			  mysqli_stmt_bind_param($add_user_stmt, 'isssssssssss', $surgery_id, $user_type, $company_name, $first_name, $last_name, $dob, $family_members, $email, $mobile, $username, $password, $now_date_time);
			  mysqli_stmt_execute($add_user_stmt);
			  
			  if (mysqli_stmt_execute($add_user_stmt)) {
				  
				  $json_string = '{"result": [{"heading": "Log In Successful!", "message": "Proceed with getting doctors.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'"}]}';
				  
			  } else {
				  
				  $json_string = '{"result": [{"heading": "Log In Failed!", "message": "Please try again.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'"}]}';
				   
			  }
			  
		 }
		 
	 } else {
		 
		 	$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			if ($response == 'Unable to connect to database') {
				
				$json_string = '{"result": [{"heading": "No Response!", "message": "The surgery is not responding. Please try again or try another surgery."}]}';
			
			} else {
		
			if (count($response_array[0]) > 0) {
				
				
			
			if ($surgery_software == 'BestPractice') {
				 
				 $user_id = $response_array[0]['INTERNALID'];
				
			 } else {
				 
				 $user_id = $response_array[0]['user_id'];
		
			 }
			 
			 $json_string = '{"result": [{"heading": "Log In Successful!", "message": "Proceed with getting doctors.", "surgery_id": "'.$surgery_id.'", "patient_id": "'.$user_id.'"}]}';
			 
			 $stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "Android";
			$log_action = "Register/Log In";
			$log_action_comment = "";
			
			mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($stmt);
		
			mysqli_stmt_free_result($stmt);
			
			mysqli_close($mysqli);
			
			} else {
				
				if ($surgery_id == '7' || $surgery_id == '22' || $surgery_id == '29' || $surgery_id == '32' || $surgery_id == '33' || $surgery_id == '35' || $surgery_id == '36' || $surgery_id == '37' || $surgery_id == "43" || $surgery_id == "52" || $surgery_id == "53") {
				
					$jsonSendData='[{"medicareno":"'.$medicareno.'","medicarelineno":"'.$medicarelineno.'","username":"'.$username.'","email":"'.$email.'","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'", "town_suburb":"","company_name":"'.$company.'","mobile":"'.$mobile.'","middle_name":"","password":"'.$password.'","last_name":"'.$last_name.'","family_members":",,,*,,,*,,,*,,,*,,,*,,,","middle_name":"","number_street":"","phone":"'.$mobile.'","postcode":"","state":""}]';
				
					$post_array = array('addMedicareUser' => 'true', 'jsonSendData' => $jsonSendData);
				
				} else if ($surgery_id == '64') {
				
				$server_url = 'http://secure.docappointment.com.au/pracsoft.php';
				
				$jsonSendData='[{"username":"'.$username.'","email":"'.$email.'","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'", "town_suburb":"","company_name":"'.$company.'","mobile":"'.$mobile.'","middle_name":"","password":"'.$password.'","last_name":"'.$last_name.'","family_members":",,,*,,,*,,,*,,,*,,,*,,,","middle_name":"","number_street":"","phone":"'.$mobile.'","postcode":"","state":""}]';
				
				$post_array = array('addUser' => 'true', 'jsonSendData' => $jsonSendData);
				
				} else {
					
					$jsonSendData='[{"username":"'.$username.'","email":"'.$email.'","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'", "town_suburb":"","company_name":"'.$company.'","mobile":"'.$mobile.'","middle_name":"","password":"'.$password.'","last_name":"'.$last_name.'","family_members":",,,*,,,*,,,*,,,*,,,*,,,","middle_name":"","number_street":"","phone":"'.$mobile.'","postcode":"","state":""}]';
					
					$post_array = array('addUser' => 'true', 'jsonSendData' => $jsonSendData);
					
				}
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
				
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
				if (count($response_array[0]) > 0) {
				
					$json_string = '{"result": [{"heading": "'.$response_array[0]['heading'].'", "message": "'.$response_array[0]['result_string'].'"}]}';
			
				}
				
				$stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
				$now_date_time = date('Y-m-d H:i:s');
				$log_source = "Android";
				$log_action = "Register/Log In";
				$log_action_comment = $response;
				
				mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
			
				mysqli_stmt_execute($stmt);
			
				mysqli_stmt_free_result($stmt);
				
				mysqli_close($mysqli);
		
			}
			
			
		}
		 
		 
	 }
      
	
	echo $json_string;

?>



