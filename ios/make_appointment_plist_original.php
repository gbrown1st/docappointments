<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$patient_name = $_POST['first_name'].' '.$_POST['last_name'];
$company_name = $_POST['company_name'];
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];
$dob_array = explode('-', $dob);
$display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$appt_secs = $_POST['appt_secs'];
$appt_day_array = explode(' ', $_POST['appt_day']);
$appt_day = $appt_day_array[0];
$appt_id = $_POST['appt_id'];
$appt_time = $_POST['appt_time'];
$doctor_name = $_POST['doctor_name'];
$addUser = $_POST['addUser'];
$appoitment_type = 'Internet';

if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	$user_string = $first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	
} else {
	
	$appoitment_type = 'Internetwc';
	$user_string = $company_name.' - '.$first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
	  
	  $user_id = 0;
	
	  $patient_details_array = array();
	
	  if ($surgery_software == 'BestPractice') {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$user_id = $response_array[0]['INTERNALID'];

		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILE']);
	
		if ($addUser == 'yes') {
			
			$jsonSendData = '[{"email":"'.$email.'","postcode":"","mobile":"'.$mobile.'","last_name":"'.$last_name.'","number_street":"","HEADOFFAMILYID":'.$response_array[0]['INTERNALID'].',"town_suburb":"","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'","phone":"'.$mobile.'"}]';
			
			$addFamilyJsonSendData = $jsonSendData;
			
			$post_array = array('addFamilyMember' => 'true', 'jsonSendData' => $jsonSendData);
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			$response_array = json_decode($response, true);
		
		}
		
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILE']);
			
		}
		
		$jsonSendData = '[{"user_type":"'.$user_type.'","delete_from_day":"'.$now_date_time.'","appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","doctor_id":'.$doctor_id.',"appt_secs":'.$appt_secs.',"patient_name":"'.$patient_name.'","appt_time":"'.$appt_time.'","user_string":"'.$user_string.'","patient_id":'.$patient_id.',"patient_description":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"appt_id":'.$appt_id.'}]';
		
		//if ($user_type == 'employer') {
			
			$post_array = array('getInternetPatientIDEmployer' => 'true');
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$patient_array[] = $response_array[0]['INTERNALID'];
			
			$internet_employer_id = $response_array[0]['INTERNALID'];
			
		//} else {
			
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
		//}
		
		if ($user_type == 'patient') {
			
			$internet_employer_id = $internet_patient_id;
			
		}
		
			for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 }
				 
			}
	
	
	
		if ($user_type == 'employer') {
		
			$patient_description = $company_name.' - '.$patient_description;
			
		}
	
	  }
	
	if ($surgery_software == 'BestPractice') {
		
		$jsonSendData = '[{"patient_description":"*xxxxxxxxx*","appointment_description":"","patient_id":"'.$internet_employer_id.",".$internet_patient_id.'","user_type":"'.$user_type.'","appt_day":"'.$appt_day.'"}]';
		
	} else {
		
		$jsonSendData = '[{"appointment_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","patient_description":"*xxxxxxxxx*","user_type":"'.$user_type.'"}]';
		
	}
	
		$checkApp = $jsonSendData;
		
		$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
	
		$available = false;
		
		 for ($i=0;$i<count($response_array);$i++) {
			 
			 if ($response_array[$i]['ApptID'] == $appt_id) {
					 
					 $available = true;
					 
			}
			 
		 }
		
		if ($available) {
		
		 if ($surgery_software == 'BestPractice') {
			 
			if ($user_type == 'patient') {
			 
			 	$user_type = 1;
			 
		 	} else {
			 
			 	$user_type = 2;
			 
		 	}
		
			 $jsonSendData = '[{"user_type":"'.$user_type.'","delete_from_day":"'.$now_date_time.'","appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","doctor_id":'.$doctor_id.',"appt_secs":'.$appt_secs.',"patient_name":"'.$patient_name.'","appt_time":"'.$appt_time.'","user_string":"'.$user_string.'","patient_id":'.$patient_id.',"patient_description":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"appt_id":'.$appt_id.'}]';
		
		 } else {
			
			 $jsonSendData = '[{"appt_time":"'.$appt_time.'","user_type":"'.$user_type.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","delete_from_day":"'.$now_date_time.'","patient_name":"'.$patient_name.'","user_string":"'.$user_string.'","doctor_id":'.$doctor_id.',"appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","patient_description":"'.$user_string.'","appt_id":'.$appt_id.'}]';
			 
		 }
		  
		$post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		 $stmt = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date,
			json,
			json_response) 
		VALUES (?,?,?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time; Details: $patient_name [DOB: $display_dob] [Ph: $mobile]";
	
	mysqli_bind_param($stmt, 'iissssss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time, $jsonSendData, $response);

	mysqli_stmt_execute($stmt);

	mysqli_stmt_free_result($stmt);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		 
		  $plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$jsonSendData.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$response_array[0]['result_string'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

		} else {
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$checkApp.']]></string>
		<key>heading</key>
		<string><![CDATA[Appointment Unavailable]]></string>
		<key>message</key>
		<string><![CDATA[This appointment is no longer available.
		
Please choose another.]]></string>
	</dict>
</array>
</plist>';
			
		}
	
	echo $plist_string;

?>