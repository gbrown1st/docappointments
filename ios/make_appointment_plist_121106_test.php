<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');
$zedmed_now_date_time = $now_date_time.' 00:00:00';

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = '33';//$_POST['surgery_id'];
$doctor_id = '2';//$_POST['doctor_id'];
$appt_patient_id = '0';//$_POST['appt_patient_id'];
$first_name = 'Graham';//$_POST['first_name'];
$first_name = str_replace("'","''", $first_name);
$last_name = 'Brown';//$_POST['last_name'];
$last_name = str_replace("'","''", $last_name);
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];
$patient_name = $first_name.' '.$last_name;
$company_name = '';//$_POST['company_name'];
$user_type = 'patient';//$_POST['user_type'];
$dob = '1959-02-03';//$_POST['dob'];
$dob_array = explode('-', $dob);
$display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
$email = 'graham@mediarare.com.au';//$_POST['email'];
$mobile = '0448992599';//$_POST['mobile'];
$username = 'gb';//$_POST['username'];
$password = 'gb';//$_POST['password'];
$appt_length = (int) '900';//$_POST['appt_length'];
$appt_pos = (int) '0';//$_POST['appt_pos'];
$appt_secs = '53100';//$_POST['appt_secs'];
$appt_date = '2013-12-12';//$_POST['appt_day'];
$appt_day_array = explode(' ', $appt_date);
$appt_day = $appt_day_array[0];
$appt_date = str_replace('  0000', '.000', $appt_date);
$appt_date_time = trim('Thu 12 Dec 2013 2:45 PM');

$dateStringDate = strtotime($appt_date_time);
$appt_date_time = date("Y-m-d H:i:s.000",$dateStringDate);

$date = strtotime($appt_date_time);

$start_point = date("Y-m-d H:i:s",$date);
$end_point = date("Y-m-d H:i:s",$date + $appt_length);

$zedmed_appt_day = date("m/d/Y",$date);

$appt_day = substr($appt_date, 0, 10);
$appt_id = '-1';//$_POST['appt_id'];
$appt_time = 'Thu 12 Dec 2013 2:45 PM';//$_POST['appt_time'];
$doctor_name = 'Dr Peter Crawford';//$_POST['doctor_name'];
$addUser = 'no';//$_POST['addUser'];

$isInternetPatient = 'no';


//[{"user_type":"1","delete_from_day":"2012-10-25","appointment_description":"Internet","patient_email":"graham@mediarare.com.au","doctor_name":"Dr Graham Brown","appt_day":"2012-10-25","doctor_id":3,"appt_secs":28800,"patient_name":"Graham Brown","appt_time":"Thu 25 Oct 2012 8:00 AM","user_string":"Graham Brown [DOB: 03/02/1959] [Ph: 0448992599]","patient_id":,"patient_description":"","internet_patient_id":2,"appt_id":-1}]

	$appoitment_type = 'Internet';
	$user_string = $first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
	
/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);
	  
$stmt1 = mysqli_prepare($mysqli,
          "SELECT server_url.server_url, 
	server_url.server_wsdl_url, 
	surgeries.surgery_name, 
	surgeries.surgery_software, 
	surgeries.clinic_code, 
	surgeries.include_in_notifications, 
	surgery_notifications.surgery_notification_email, 
	surgery_notifications.surgery_notification_make_appointment
FROM server_url INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
	 LEFT OUTER JOIN surgery_notifications ON server_url.surgery_id = surgery_notifications.surgery_id
WHERE server_url.surgery_id =  ?");
	
	 mysqli_stmt_bind_param($stmt1, 'i', $surgery_id);

      mysqli_stmt_execute($stmt1);

      mysqli_stmt_bind_result($stmt1, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software, $row->clinic_code, $row->include_in_notifications, $row->surgery_notification_email, $row->surgery_notification_make_appointment);

      while (mysqli_stmt_fetch($stmt1)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  $clinic_code = $row->clinic_code;
		  $include_in_notifications = $row->include_in_notifications;
		  $surgery_notification_email = $row->surgery_notification_email;
		  $surgery_notification_make_appointment = $row->surgery_notification_make_appointment;
		  
      }
		
	if (in_array($surgery_id, $surgery_array)) {
		
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.doctor_code,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.appointment_length
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	} else {
			
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.doctor_code,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id,
	doctor_new_patients.multiple_appointments
FROM
	doctor_new_patients
WHERE
	surgery_id = ?
AND doctor_id = ?");
			
	}
	  

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id, $row->multiple_appointments);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  $doctor_code = $row->doctor_code;
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  $multiple_appointments = $row->multiple_appointments;
		  
      }
	  
	  if ($multiple_appointments != 'yes') {
		  
		  $multiple_appointments = 'no';
		  
	  }
	  
	  $stmt_zm_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.clinic_code
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_zm_doc, 'is', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_zm_doc);

      mysqli_stmt_bind_result($stmt_zm_doc, $row->clinic_code);

      while (mysqli_stmt_fetch($stmt_zm_doc)) {
		 
		  $clinic_code = $row->clinic_code;
		  
      }
	  
	   $user_id = 0;
	
	  $patient_details_array = array();
	  
	  if ($surgery_software == 'BestPractice' && !in_array($surgery_id, $surgery_array)) {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		print_r($response_array);
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$user_id = $response_array[0]['INTERNALID'];

		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILEPHONE']);
	
		print_r($patient_details_array);
		
		if ($addUser == 'yes') {
			
			$jsonSendData = '[{"email":"'.$email.'","postcode":"","mobile":"'.$mobile.'","last_name":"'.$last_name.'","number_street":"","HEADOFFAMILYID":'.$response_array[0]['INTERNALID'].',"town_suburb":"","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'","phone":"'.$mobile.'"}]';
			
			$addFamilyJsonSendData = $jsonSendData;
			
			$post_array = array('addFamilyMember' => 'true', 'jsonSendData' => $jsonSendData);
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			$response_array = json_decode($response, true);
		
		}
		
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILEPHONE']);
			
		}
		
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
			if ((int) $internet_patient_id == (int) $appt_patient_id) {
				
				$isInternetPatient = 'yes';
				
			} else {
				
				$isInternetPatient = 'no';
				
			}
		
			for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 }
				 
			}
	
	  }
	
	$available = false;
	
		 if ($surgery_software == 'BestPractice') {
			 
				//$jsonSendData = '[{"appt_id":'.$appt_id.',"appt_length":'.$appt_length.',"isInternetPatient":"'.$isInternetPatient.'","patient_id":'.$appt_patient_id.',"appt_time":'.$appt_secs.',"doctor_id":'.$doctor_id.',"appt_day":"'.$appt_day.'"}]';
				
				$jsonSendData = '[{"appt_id":-1,"appt_length":900,"isInternetPatient":"no","patient_id":0,"appt_time":54000,"doctor_id":178,"appt_day":"2013-12-12"}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
				
				echo 'checkAppointmentAvailability
				';
				print_r($post_array);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
				echo 'checkAppointmentAvailability response
				';
				print_r($response_array);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ($response_array[$i]['is_available'] == 1) {
							 
							 $available = true;
							 
					}
					 
				 }
	
			
		} else if ($surgery_software == 'PracSoft') {
			
				$jsonSendData = '[{"appt_date":"'.$appt_date_time.'","doctor_id":'.$doctor_id.'}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ($response_array[$i]['is_available'] == 0) {
							 
						$available = true;
							 
					}
					 
				 }
			
		} else if ($surgery_software == 'Practice 2000') {
			
				$jsonSendData = '[{"appt_pos":'.$appt_pos.',"internet_id":-1,"appt_date":"'.$appt_day.'","doctor_id":'.$doctor_id.'}]';
				
				$availableSendData = '[{"appt_pos":'.$appt_pos.',"internet_id":-1,"appt_date":"'.$appt_day.'","doctor_id":'.$doctor_id.'}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ((int) $response_array[$i]['is_available'] == 1) {
							 
						$available = true;
							 
					}
					 
				 }
			
		}
		
		
		
		if ($available || $surgery_software == 'Zedmed' || $surgery_software == 'Stat') {
			
			if ($surgery_software == 'BestPractice') {
				
					$user_type = 1;
					
					 if (in_array($surgery_id, $surgery_array)) {
						 
						$jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_email":"'.$email.'","appointment_description":"Internet","appt_secs":'.$appt_secs.',"appt_length":'.$appt_length.',"appt_day":"'.$appt_day.'","patient_description":"'.$patient_description.'","internet_patient_id":0,"doctor_id":'.$doctor_id.',"include_in_notifications":"'.$include_in_notifications.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","patient_name":"'.$patient_name.'","delete_from_day":"'.$now_date_time.'","user_type":"patient","patient_id":0,"appt_id":'.$appt_id.'}]';
						 
			
					 } else {
						 
						 $jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_id":'.$patient_id.',"appt_day":"'.$appt_day.'","appt_length":'.$appt_length.',"patient_email":"'.$email.'","user_string":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"patient_description":"'.$patient_description.'","delete_from_day":"'.$now_date_time.'","show_all_appointments":"'.$show_all_appointments.'","doctor_name":"'.$doctor_name.'","appointment_description":"Internet","appt_id":'.$appt_id.',"user_type":"patient","patient_name":"'.$patient_name.'","appt_secs":'.$appt_secs.',"doctor_id":'.$doctor_id.'}]';
						 
					 }
				
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				 
				 $request_data = $jsonSendData;
			
			 } else if ($surgery_software == 'PracSoft') {
				 
					 $jsonSendData = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$patient_description.'","doctor_code":"'.$doctor_code.'","appt_time":"'.$appt_time.'","multiple_appointments":"'.$multiple_appointments.'", "patient_description":"'.$patient_description.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","delete_from_day":"'.$now_date_time.'","appt_id":'.$appt_id.',"appointment_book_id":'.$appointment_book_id.',"patient_email":"'.$email.'","appt_day":"'.$appt_day.'","patient_name":"'.$patient_name.'","user_type":"'.$user_type.'","appointment_description":"","appt_date":"'.$appt_date_time.'"}]';
				
				 $post_array = array('makeNewAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				
			 } else if ($surgery_software == 'Zedmed') {
				  
				  $jsonSendData = '[{"appt_day":"'.$appt_date_time.'","doctor":"'.$doctor_id.'","appt_date":"'.$zedmed_appt_day.'","doctor_name":"'.$doctor_name.'","patient_email":"'.$email.'","patient_name":"'.$patient_name.'","appointment_description":"","patient_description":"'.$patient_description.'","delete_from_day":"'.$zedmed_now_date_time.'","user_string":"'.$user_string.'","start_point":"'.$start_point.'","appt_id":'.$appt_id.',"clinic_code":"'.$clinic_code.'","user_type":"'.$user_type.'","appt_time":"'.$appt_time.'","end_point":"'.$end_point.'"}]'; 
				  
				   $post_array = array('makeAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				  
			} else if ($surgery_software == 'Practice 2000') {
				  
				    $jsonSendData = '[{"patient_email":"'.$email.'","appt_pos":'.$appt_pos.',"doctor_name":"'.$doctor_name.'","appt_time":"'.$appt_date_time.'","show_all_appointments":"'.$show_all_appointments.'","appt_day":"'.$appt_time.'","include_in_notifications":"'.$include_in_notifications.'","appt_date":"'.$appt_day.'","appointment_description":"","user_string":"'.$user_string.'","doctor_id":'.$doctor_id.',"patient_description":"'.$patient_description.'","delete_from_day":"'.$now_date_time.'","doctor_code":"'.$doctor_code.'","user_type":"patient","patient_name":"'.$patient_name.'","appt_id":'.$appt_id.'}]';
					
				   $post_array = array('makeAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				  
			} else if ($surgery_software == 'Stat') {
					
				$dateTime = date("Ymd hi",strtotime($appt_time));	
	
				if ($multiple_appointments == 'yes') {
					
					$multiple_appointments = true;
					
				} else {
					
					$multiple_appointments = false;
					
				}
	
				$params = array ('resourceId'=>$doctor_id, 'dateTime'=>$dateTime, 'personString'=>$patient_description, 'allowMultipleAppointments'=>$multiple_appointments);
	
		try {
        
			$client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
			$result = $client->MakeAppointment($params);
			$result_code = (int) $result->MakeAppointmentResult;
						
				if ($result_code == 0) {
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Successful]]></string>
							<key>message</key>
							<string><![CDATA[The appointment has been saved.]]></string>
						</dict>
					</array>
					</plist>';
					
				} else if ($result_code == 9207) {
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Appointment Unavailable]]></string>
							<key>message</key>
							<string><![CDATA[This appointment is no longer available.
							
					Please choose another.]]></string>
						</dict>
					</array>
					</plist>';
					
				} else {
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Failed]]></string>
							<key>message</key>
							<string><![CDATA[The appointment could not be saved.]]></string>
						</dict>
					</array>
					</plist>';
								
				}
					   
			}
				catch (SoapFault $exception) {
					echo $exception->getMessage();
				}
			}

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		if ($response_array[0]['heading'] == 'Save Appointment Successful' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_make_appointment) > 0) {
			
			$email_message = $surgery_notification_make_appointment;
			$email_message = str_replace('[user]', $patient_name, $email_message);
			$email_message = str_replace('[doctor]', $doctor_name, $email_message);
			$email_message = str_replace('[appt_time]', $appt_date_time, $email_message);
			
			$email_message = str_replace('\r','\r\n', $email_message);
			
			$email_subject = $surgery_name . " Online Appointment Confirmation";
			
			//$email_message = wordwrap($email_message, 70, "\r\n");
			
			$headers = "Reply-To: $surgery_notification_email\r\n"; 
	
			if(mail($email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au')) {
						
				$response_array[0]['result_string'] .= "
					
An email notification has been sent to $email.";
						
			} else {
				
				$response_array[0]['result_string'] .= "
					
An email notification COULD NOT BE SENT to $email.

Please confirm your appointment.";

			}
			
			if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Success Notification";
				
				$email_message = "Patient: ".$patient_name."

Doctor: ".$doctor_name."

Time: ".$appt_date_time."

Status: Make Appointment Successful

Source: Apple iOS Mobile Device";

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
	
		} else if ($response_array[0]['heading'] == 'Save Appointment Failed' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_make_appointment) > 0) {
			
			if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Fail Notification";
				
				$email_message = "Patient: ".$patient_name."

Doctor: ".$doctor_name."

Time: ".$appt_date_time."

Status: Make Appointment Failed

Source: Apple iOS Mobile Device";

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
		}
		
		 $stmt3 = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date,
			json,
			json_response) 
		VALUES (?,?,?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time; Details: $patient_name [DOB: $dob] [Ph: $mobile]";
	
	mysqli_bind_param($stmt3, 'iissssss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time, $jsonSendData, $response);

	mysqli_stmt_execute($stmt3);

	mysqli_stmt_free_result($stmt1);
	mysqli_stmt_free_result($stmt2);
	mysqli_stmt_free_result($stmt_zm_doc);
	mysqli_stmt_free_result($stmt3);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		 
		  $plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$availableSendData.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$response_array[0]['result_string'].']]></string>
		<key>sql</key>
		<string><![CDATA['.$response_array[0]['extra_data'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

		} else {
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$jsonSendData.']]></string>
		<key>JSON</key>
		<string><![CDATA['.$check_response.']]></string>
		<key>heading</key>
		<string><![CDATA[Appointment Unavailable]]></string>
		<key>message</key>
		<string><![CDATA[This appointment is no longer available.
		
Please choose another.]]></string>
	</dict>
</array>
</plist>';
			
		}
	
		if ($surgery_software == 'Stat') {
			
			echo $stat_plist_string;
			
		} else {
			
			echo $plist_string;
			
		}

?>