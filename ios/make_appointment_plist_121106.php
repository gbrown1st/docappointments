<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');
$zedmed_now_date_time = $now_date_time.' 00:00:00';

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$appt_patient_id = $_POST['appt_patient_id'];
$patient_id = $_POST['patient_id'];
$first_name = $_POST['first_name'];
$first_name = str_replace("'","''", $first_name);
$last_name = $_POST['last_name'];
$last_name = str_replace("'","''", $last_name);
$medicareno = $_POST['medicareno'];
$medicarelineno = $_POST['medicarelineno'];
$patient_name = $first_name.' '.$last_name;
$company_name = $_POST['company_name'];
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];
$dob_array = explode('-', $dob);
$display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$appt_length = (int) $_POST['appt_length'];
$appt_pos = (int) $_POST['appt_pos'];
$appt_secs = $_POST['appt_secs'];
$appt_date = $_POST['appt_day'];
$appt_day_array = explode(' ', $appt_date);
$appt_day = $appt_day_array[0];
$appt_date = str_replace('  0000', '.000', $appt_date);
$appt_date_time = trim($_POST['appt_time']);

$dateStringDate = strtotime($appt_date_time);
$appt_date_time = date("Y-m-d H:i:s.000",$dateStringDate);

$date = strtotime($appt_date_time);

$start_point = date("Y-m-d H:i:s",$date);
$end_point = date("Y-m-d H:i:s",$date + $appt_length);

$zedmed_appt_day = date("m/d/Y",$date);

$appt_day = substr($appt_date, 0, 10);
$appt_id = $_POST['appt_id'];
$appt_time = $_POST['appt_time'];
$doctor_name = $_POST['doctor_name'];
$addUser = $_POST['addUser'];

$isInternetPatient = 'no';


//[{"user_type":"1","delete_from_day":"2012-10-25","appointment_description":"Internet","patient_email":"graham@mediarare.com.au","doctor_name":"Dr Graham Brown","appt_day":"2012-10-25","doctor_id":3,"appt_secs":28800,"patient_name":"Graham Brown","appt_time":"Thu 25 Oct 2012 8:00 AM","user_string":"Graham Brown [DOB: 03/02/1959] [Ph: 0448992599]","patient_id":,"patient_description":"","internet_patient_id":2,"appt_id":-1}]

	$appoitment_type = 'Internet';
	$user_string = $first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
	
/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);
	  
$stmt1 = mysqli_prepare($mysqli,
          "SELECT server_url.server_url, 
	server_url.server_wsdl_url, 
	surgeries.surgery_name, 
	surgeries.surgery_software, 
	surgeries.clinic_code, 
	surgeries.include_in_notifications, 
	surgery_notifications.surgery_notification_email, 
	surgery_notifications.surgery_notification_make_appointment
FROM server_url INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
	 LEFT OUTER JOIN surgery_notifications ON server_url.surgery_id = surgery_notifications.surgery_id
WHERE server_url.surgery_id =  ?");
	
	 mysqli_stmt_bind_param($stmt1, 'i', $surgery_id);

      mysqli_stmt_execute($stmt1);

      mysqli_stmt_bind_result($stmt1, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software, $row->clinic_code, $row->include_in_notifications, $row->surgery_notification_email, $row->surgery_notification_make_appointment);

      while (mysqli_stmt_fetch($stmt1)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  $clinic_code = $row->clinic_code;
		  $include_in_notifications = $row->include_in_notifications;
		  $surgery_notification_email = $row->surgery_notification_email;
		  $surgery_notification_make_appointment = $row->surgery_notification_make_appointment;
		  
      }
		
	if (in_array($surgery_id, $surgery_array)) {
		
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.doctor_code,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.appointment_length
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	} else {
			
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.doctor_code,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id,
	doctor_new_patients.multiple_appointments
FROM
	doctor_new_patients
WHERE
	surgery_id = ?
AND doctor_id = ?");
			
	}
	  

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id, $row->multiple_appointments);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  $doctor_code = $row->doctor_code;
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  $multiple_appointments = $row->multiple_appointments;
		  
      }
	  
	  if ($multiple_appointments != 'yes') {
		  
		  $multiple_appointments = 'no';
		  
	  }
	  
	  $stmt_zm_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.clinic_code
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_zm_doc, 'is', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_zm_doc);

      mysqli_stmt_bind_result($stmt_zm_doc, $row->clinic_code);

      while (mysqli_stmt_fetch($stmt_zm_doc)) {
		 
		  $clinic_code = $row->clinic_code;
		  
      }
	  
	   $user_id = 0;
	
	  $patient_details_array = array();
	  
	  if ($surgery_software == 'BestPractice' && !in_array($surgery_id, $surgery_array)) {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$user_id = $response_array[0]['INTERNALID'];

		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILE']);
	
		if ($addUser == 'yes') {
			
			$jsonSendData = '[{"email":"'.$email.'","postcode":"","mobile":"'.$mobile.'","last_name":"'.$last_name.'","number_street":"","HEADOFFAMILYID":'.$response_array[0]['INTERNALID'].',"town_suburb":"","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'","phone":"'.$mobile.'"}]';
			
			$addFamilyJsonSendData = $jsonSendData;
			
			$post_array = array('addFamilyMember' => 'true', 'jsonSendData' => $jsonSendData);
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			$response_array = json_decode($response, true);
		
		}
		
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILE']);
			
		}
		
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
			if ((int) $internet_patient_id == (int) $appt_patient_id) {
				
				$isInternetPatient = 'yes';
				
			} else {
				
				$isInternetPatient = 'no';
				
			}
		
			for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 }
				 
			}
	
	  }
	
	$available = false;
	
		 if ($surgery_software == 'BestPractice') {
			 
				$jsonSendData = '[{"appt_day":"'.$appt_day.'","appt_id":'.$appt_id.',"doctor_id":'.$doctor_id.',"appt_time":'.$appt_secs.',"patient_id":'.$appt_patient_id.',"isInternetPatient":"'.$isInternetPatient.'"}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
		
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ($response_array[$i]['is_available'] == 1) {
							 
							 $available = true;
							 
					}
					 
				 }
	
			
		} else if ($surgery_software == 'PracSoft') {
			
				$jsonSendData = '[{"appt_date":"'.$appt_date_time.'","doctor_id":'.$doctor_id.'}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ($response_array[$i]['is_available'] == 0) {
							 
						$available = true;
							 
					}
					 
				 }
			
		} else if ($surgery_software == 'Practice 2000') {
			
				$jsonSendData = '[{"appt_pos":'.$appt_pos.',"internet_id":-1,"appt_date":"'.$appt_day.'","doctor_id":'.$doctor_id.'}]';
				
				$availableSendData = '[{"appt_pos":'.$appt_pos.',"internet_id":-1,"appt_date":"'.$appt_day.'","doctor_id":'.$doctor_id.'}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ((int) $response_array[$i]['is_available'] == 1) {
							 
						$available = true;
							 
					}
					 
				 }
			
		}
		
		if ($available || $surgery_software == 'Zedmed' || $surgery_software == 'Stat' || $surgery_software == 'Medilink') {
			
			if ($surgery_software == 'BestPractice') {
				
					$user_type = 1;
					
					 if (in_array($surgery_id, $surgery_array)) {
						 
						$jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_email":"'.$email.'","appointment_description":"Internet","appt_secs":'.$appt_secs.',"appt_length":'.$appt_length.',"appt_day":"'.$appt_day.'","patient_description":"'.$patient_description.'","internet_patient_id":0,"doctor_id":'.$doctor_id.',"include_in_notifications":"'.$include_in_notifications.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","patient_name":"'.$patient_name.'","delete_from_day":"'.$now_date_time.'","user_type":"patient","patient_id":0,"appt_id":'.$appt_id.'}]';
						 
			
					 } else {
						 
						 $jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_id":'.$patient_id.',"appt_day":"'.$appt_day.'","appt_length":'.$appt_length.',"patient_email":"'.$email.'","user_string":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"patient_description":"'.$patient_description.'","delete_from_day":"'.$now_date_time.'","show_all_appointments":"'.$show_all_appointments.'","doctor_name":"'.$doctor_name.'","appointment_description":"Internet","appt_id":'.$appt_id.',"user_type":"patient","patient_name":"'.$patient_name.'","appt_secs":'.$appt_secs.',"doctor_id":'.$doctor_id.'}]';
						 
					 }
				
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				 
				 $request_data = $jsonSendData;
			
			 } else if ($surgery_software == 'PracSoft') {
				 
					 $jsonSendData = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$patient_description.'","doctor_code":"'.$doctor_code.'","appt_time":"'.$appt_time.'","multiple_appointments":"'.$multiple_appointments.'", "patient_description":"'.$patient_description.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","delete_from_day":"'.$now_date_time.'","appt_id":'.$appt_id.',"appointment_book_id":'.$appointment_book_id.',"patient_email":"'.$email.'","appt_day":"'.$appt_day.'","patient_name":"'.$patient_name.'","user_type":"'.$user_type.'","appointment_description":"","appt_date":"'.$appt_date_time.'"}]';
				
				 $post_array = array('makeNewAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				
			 } else if ($surgery_software == 'Zedmed') {
				  
				  $jsonSendData = '[{"appt_day":"'.$appt_date_time.'","doctor":"'.$doctor_id.'","appt_date":"'.$zedmed_appt_day.'","doctor_name":"'.$doctor_name.'","patient_email":"'.$email.'","patient_name":"'.$patient_name.'","appointment_description":"","patient_description":"'.$patient_description.'","delete_from_day":"'.$zedmed_now_date_time.'","user_string":"'.$user_string.'","start_point":"'.$start_point.'","appt_id":'.$appt_id.',"clinic_code":"'.$clinic_code.'","user_type":"'.$user_type.'","appt_time":"'.$appt_time.'","end_point":"'.$end_point.'"}]'; 
				  
				   $post_array = array('makeAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				  
			} else if ($surgery_software == 'Practice 2000') {
				  
				    $jsonSendData = '[{"patient_email":"'.$email.'","appt_pos":'.$appt_pos.',"doctor_name":"'.$doctor_name.'","appt_time":"'.$appt_date_time.'","show_all_appointments":"'.$show_all_appointments.'","appt_day":"'.$appt_time.'","include_in_notifications":"'.$include_in_notifications.'","appt_date":"'.$appt_day.'","appointment_description":"","user_string":"'.$user_string.'","doctor_id":'.$doctor_id.',"patient_description":"'.$patient_description.'","delete_from_day":"'.$now_date_time.'","doctor_code":"'.$doctor_code.'","user_type":"patient","patient_name":"'.$patient_name.'","appt_id":'.$appt_id.'}]';
					
				   $post_array = array('makeAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				  
			} else if ($surgery_software == 'Stat') {
					
				$dateTime = date("Ymd hi",strtotime($appt_time));	
				$date = strtotime($appt_time);
				$appt_date_time = date("D jS M Y g:i a", $date);
	
				if ($multiple_appointments == 'yes') {
					
					$multiple_appointments = true;
					
				} else {
					
					$multiple_appointments = false;
					
				}
	
				$params = array ('resourceId'=>$doctor_id, 'dateTime'=>$dateTime, 'personString'=>$patient_description, 'allowMultipleAppointments'=>$multiple_appointments);
	
		try {
        
			$client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
			$result = $client->MakeAppointment($params);
			$result_code = (int) $result->MakeAppointmentResult;
						
				if ($result_code == 0) {
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Successful]]></string>
							<key>message</key>
							<string><![CDATA[The appointment has been saved.]]></string>
						</dict>
					</array>
					</plist>';
					
				} else if ($result_code == 9207) {
					
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Appointment Unavailable]]></string>
							<key>message</key>
							<string><![CDATA[This appointment is no longer available.
							
					Please choose another.]]></string>
						</dict>
					</array>
					</plist>';
					
				} else {
					
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$result_code.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Failed]]></string>
							<key>message</key>
							<string><![CDATA[The appointment could not be saved.]]></string>
						</dict>
					</array>
					</plist>';
								
				}
					   
			}
				catch (SoapFault $exception) {
					echo $exception->getMessage();
				}
				
			} else if ($surgery_software == 'Medilink') {
				
				$client = new SoapClient($server_wsdl_url, array('login' => "admin", 'password' => "", "trace" => 1, "exception" => 0, 'cache_wsdl'   =>  WSDL_CACHE_BOTH, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
				
					$appt_time = date('c', $appt_date_time);
					
					if ($company_name == 'M') {
						$title = 'Mr';
					} else {
						$title = 'Ms';
					}
					
					if ($allow_multiple_appointments == 'yes') {
		
						$allow_multiple_appointments = true;
						
					} else {
						
						$allow_multiple_appointments = false;
						
					}
					
					$person = new stdClass();
					$person->Title = $title;
					$person->FirstName = $first_name;
					$person->LastName = $last_name;
					$person->DOB = date('c', strtotime($dob));
					$person->PhoneNumber = $mobile;
		
			try {
				
				$params = new stdClass;
				$params->ResourceId = $doctor_id;
				$appt_time = date('c', $dateStringDate);
				$params->DateTime = $appt_time;
				$params->AllowMultipleAppointments = $allow_multiple_appointments;
				$params->PersonString = $person;
		
				$methodName = "MakeAppointment";
				$resultName = $methodName . "Result";
				$result = $client->$methodName($params);
				$resultValue = $result->$resultName;
				
				if ($resultValue == 'Success') {
		
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$resultValue.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Successful]]></string>
							<key>message</key>
							<string><![CDATA[The appointment has been saved.]]></string>
						</dict>
					</array>
					</plist>';
		
				 } else if ($resultValue == 'MakeAppointmentNotAvailable') {
						
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$resultValue.']]></string>
							<key>heading</key>
							<string><![CDATA[Appointment Unavailable]]></string>
							<key>message</key>
							<string><![CDATA[This appointment is no longer available.
							
					Please choose another.]]></string>
						</dict>
					</array>
					</plist>';
						
				} else {
		
					$stat_plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
					<array>
						<dict>
							<key>Result Code</key>
							<string><![CDATA['.$resultValue.']]></string>
							<key>heading</key>
							<string><![CDATA[Save Appointment Failed]]></string>
							<key>message</key>
							<string><![CDATA[The appointment could not be saved.]]></string>
						</dict>
					</array>
					</plist>';
					
				}
				
			}
			catch (SoapFault $exception) {
				echo $exception->getMessage();
			}
	
			}

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		if (($response_array[0]['heading'] == 'Save Appointment Successful' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_make_appointment) > 0) || ($result_code == 0 && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_make_appointment) > 0) || ($resultValue == 'Success' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_cancel_appointment) > 0)) {
			
			$email_message = '<html><head><style type="text/css">body {margin-top:0;font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #333;}#content {width: 620px;}</style></head><body><div id="content">';
			
			if ($response_array[0]['extra_data'] != '' && $surgery_software == 'PracSoft') {
				$email_message .= "Your current appointment is on $appt_time with $doctor_name.
				
".$response_array[0]['extra_data'];
			} else if ($response_array[0]['extra_data'] == 1 && $surgery_software == 'BestPractice') {
				$email_message .= "Your current appointment is on $appt_time with $doctor_name.
				
".$response_array[0]['result_string'];
			} else {
				$email_message .= $surgery_notification_make_appointment;
			}
			$email_message = str_replace('[user]', $patient_name, $email_message);
			$email_message = str_replace('[doctor]', $doctor_name, $email_message);
			$email_message = str_replace('[appt_time]', $appt_date_time, $email_message);
			
			if ((int) $surgery_id != 43 && (int) $surgery_id != 51 && (int) $surgery_id != 90) {
			
			$email_message .= "

<hr size='1' noshade>
<h3 style='color:#1E6CB2;margin-top:20px;;margin-bottom:10px;'>Search Health Information</h3>
Would you like to find out more about your condition so you are better informed before your next appointment?

Visit <a href='http://www.mydr.com.au/?client=1'>myDr.com.au</a> to search independent Australian health information.

<a href='http://www.mydr.com.au/?client=1'><img width='310' height='60' alt='myDr logo' src='http://www.mydr.com.au/images/myDr_homepage_logo.png'></a>";

			}

			$email_message .='</div></body></html>';
			
			$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
			$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns

			$email_subject = $surgery_name . " Online Appointment Confirmation";
			
			//$email_message = wordwrap($email_message, 70, "\r\n");
			
			$headers = "Reply-To: $surgery_notification_email\n"; 
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: text/html";
	
			if(mail($email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au')) {
						
				$response_array[0]['result_string'] .= "
					
An email notification has been sent to $email.";
						
			} else {
				
				$response_array[0]['result_string'] .= "
					
An email notification COULD NOT BE SENT to $email.

Please confirm your appointment.";

			}
			
			if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Success Notification";
				
				$email_message = "Patient: ".$patient_description."

Doctor: ".$doctor_name."

Time: ".$appt_date_time."

Status: Make Appointment Successful

Source: Apple iOS Mobile Device";

				$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
				$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
	
		} else if ($response_array[0]['heading'] == 'Save Appointment Failed' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_make_appointment) > 0) {
			
			if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Fail Notification";
				
				$email_message = "Patient: ".$patient_description."

Doctor: ".$doctor_name."

Time: ".$appt_date_time."

Status: Make Appointment Failed

Source: Apple iOS Mobile Device";

				$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
				$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
		}
		
		 $stmt3 = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date,
			json,
			json_response) 
		VALUES (?,?,?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time; Surgery: ".$surgery_name."; Doctor: ".$doctor_name."; Details: $patient_name [DOB: $dob] [Ph: $mobile]";
	
	if ($response_array[0]['extra_data'] != '' && $surgery_software == 'PracSoft') {
				$log_action_comment .= "

Your current appointment is on $appt_time with $doctor_name.
				
".$response_array[0]['extra_data'];
			} else if ($response_array[0]['extra_data'] == 1 && $surgery_software == 'BestPractice') {
				$log_action_comment .= "
				
Your current appointment is on $appt_time with $doctor_name.
				
".$response_array[0]['result_string'];
			}
			
	mysqli_bind_param($stmt3, 'iissssss', $surgery_id, $patient_id, $log_source, $log_action, $log_action_comment, $now_date_time, $jsonSendData, $response);

	mysqli_stmt_execute($stmt3);

	mysqli_stmt_free_result($stmt1);
	mysqli_stmt_free_result($stmt2);
	mysqli_stmt_free_result($stmt_zm_doc);
	mysqli_stmt_free_result($stmt3);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		  
		  if ($response_array[0]['heading'] == 'Save Appointment Successful' && $response_array[0]['extra_data'] != '' && $surgery_software == 'PracSoft') {
		
$plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$request_data.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA[Your current appointment is on '.$appt_time.' with '.$doctor_name.'.
		
'.$response_array[0]['extra_data'].']]></string>
		<key>user_id</key>
		<string><![CDATA['.$patient_id.']]></string>
	</dict>';
	
	} else {
		
$plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$request_data.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA[Your current appointment is on '.$appt_time.' with '.$doctor_name.'.
		
'.$response_array[0]['result_string'].']]></string>
		<key>user_id</key>
		<string><![CDATA['.$patient_id.']]></string>
	</dict>';
	
	}
		 
		  
      }

	$plist_string .= '
</array>
</plist>';

		} else {
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$jsonSendData.']]></string>
		<key>JSON</key>
		<string><![CDATA['.$check_response.']]></string>
		<key>heading</key>
		<string><![CDATA[Appointment Unavailable]]></string>
		<key>message</key>
		<string><![CDATA[This appointment is no longer available.
		
Please choose another.]]></string>
	</dict>
</array>
</plist>';
			
		}
	
		if ($surgery_software == 'Stat' || $surgery_software == 'Medilink') {
			
			echo $stat_plist_string;
			
		} else {
			
			echo $plist_string;
			
		}

?>