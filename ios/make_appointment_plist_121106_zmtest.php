<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');
$zedmed_now_date_time = $now_date_time.' 00:00:00';

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = '44';//$_POST['surgery_id'];
$doctor_id = 'BB';//$_POST['doctor_id'];
$appt_patient_id = '';//$_POST['appt_patient_id'];
$first_name = 'Tricia';//$_POST['first_name'];
$last_name = 'Brown';//$_POST['last_name'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = 'Tricia';//$_POST['medicarelineno'];
$patient_name = 'Tricia Brown';//$_POST['first_name'].' '.$_POST['last_name'];
$company_name = '';//$_POST['company_name'];
$user_type = 'patient';//$_POST['user_type'];
$dob = '1953-03-25';//$_POST['dob'];
$dob_array = explode('-', $dob);
$display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
$email = 'tricia@mediarare.com.au';//$_POST['email'];
$mobile = '0408211198';//$_POST['mobile'];
$username = 'gb';//$_POST['username'];
$password = 'gb';//$_POST['password'];
$appt_length = (int) '600';//$_POST['appt_length'];
$appt_secs = '';//$_POST['appt_secs'];
$appt_date = '2012-12-17 13:00:00  0000';//$_POST['appt_day'];
$appt_date = str_replace('  0000', '.000', $appt_date);
$appt_date_time = trim('2012-12-17 09:10:00');

$date = strtotime($appt_date_time);
echo '$date = '.$date.'
';

$start_point = date("Y-m-d H:i:s",$date);
$end_point = date("Y-m-d H:i:s",$date + $appt_length);

$zedmed_appt_day = date("m/d/Y",$date);

echo '$zedmed_appt_day = '.$zedmed_appt_day.'
';
echo '$start_point = '.$start_point.'
';
echo '$end_point = '.$end_point.'
';

$appt_day = substr($appt_date, 0, 10);
$appt_id = '-1';//$_POST['appt_id'];
$appt_time = 'Mon 17 Dec 2012 9:10 AM';//$_POST['appt_time'];
$doctor_name = 'Dr Bruce Bond';//$_POST['doctor_name'];
$addUser = 'yes';//$_POST['addUser'];


//[{"user_type":"1","delete_from_day":"2012-10-25","appointment_description":"Internet","patient_email":"graham@mediarare.com.au","doctor_name":"Dr Graham Brown","appt_day":"2012-10-25","doctor_id":3,"appt_secs":28800,"patient_name":"Graham Brown","appt_time":"Thu 25 Oct 2012 8:00 AM","user_string":"Graham Brown [DOB: 03/02/1959] [Ph: 0448992599]","patient_id":,"patient_description":"","internet_patient_id":2,"appt_id":-1}]


if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	$user_string = $first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
	
} else {
	
	$appoitment_type = 'Internetwc';
	$user_string = $company_name.' - '.$first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt1 = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt1, 'i', $surgery_id);

      mysqli_stmt_execute($stmt1);

      mysqli_stmt_bind_result($stmt1, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt1)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

	  $stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.doctor_code,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id
FROM
	doctor_new_patients
	
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  $doctor_code = $row->doctor_code;
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  
      }
	  
	  $stmt_zm_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.clinic_code
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_zm_doc, 'is', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_zm_doc);

      mysqli_stmt_bind_result($stmt_zm_doc, $row->clinic_code);

      while (mysqli_stmt_fetch($stmt_zm_doc)) {
		 
		  $clinic_code = $row->clinic_code;
		  
      }
	  
	   $user_id = 0;
	
	  $patient_details_array = array();
	  
	  if ($surgery_software == 'BestPractice') {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$user_id = $response_array[0]['INTERNALID'];

		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILE']);
	
		if ($addUser == 'yes') {
			
			$jsonSendData = '[{"email":"'.$email.'","postcode":"","mobile":"'.$mobile.'","last_name":"'.$last_name.'","number_street":"","HEADOFFAMILYID":'.$response_array[0]['INTERNALID'].',"town_suburb":"","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'","phone":"'.$mobile.'"}]';
			
			$addFamilyJsonSendData = $jsonSendData;
			
			$post_array = array('addFamilyMember' => 'true', 'jsonSendData' => $jsonSendData);
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			$response_array = json_decode($response, true);
		
		}
		
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILE']);
			
		}
		
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
			if ((int) $internet_patient_id == (int) $appt_patient_id) {
				
				$isInternetPatient = 'yes';
				
			} else {
				
				$isInternetPatient = 'no';
				
			}
		
			for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 }
				 
			}
	
	  }
	
	$available = false;
	
		if ($surgery_software == 'BestPractice') {
			
			if ($surgery_id == "7" || $surgery_id == "22" || $surgery_id == "29" || $surgery_id == "32" || $surgery_id == "36" || $surgery_id == "43") {
				
				$jsonSendData = '[{"appt_day":"'.$appt_day.'","appt_id":'.$appt_id.',"doctor_id":'.$doctor_id.',"appt_time":'.$appt_secs.',"patient_id":'.$appt_patient_id.',"isInternetPatient":"'.$isInternetPatient.'"}]';
				
				$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
		
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
			
				 for ($i=0;$i<count($response_array);$i++) {
					
					 if ($response_array[$i]['is_available'] == 1) {
							 
							 $available = true;
							 
					}
					 
				 }
				
			 } else if ($surgery_software == 'PracSoft') {
				
				$jsonSendData = '[{"patient_description":"*xxxxxxxxx*","appointment_description":"Internet","patient_id":'.$internet_patient_id.',"user_type":"'.$user_type.'","appt_day":"'.$appt_day.'"}]';
				
				$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
			
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
				$check_response = $response;
		
				 for ($i=0;$i<count($response_array);$i++) {
					 
					 if ((int) $response_array[$i]['ApptID'] == (int) $appt_id) {
							 
							 $available = true;
							 
					}
					 
				 }
				
			}
			
			
		} else {
			
			if ($surgery_software == 'BestPractice' || $surgery_software == 'PracSoft') {
			
				if ($surgery_id == "13" || $surgery_id == "19" || $surgery_id == "41") {
					
					$jsonSendData = '[{"appt_date":"'.$appt_day.'","doctor_id":'.$doctor_id.'}]';
					
					$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
			
					$response = $r->send()->getBody();
					
					$response_array = json_decode($response, true);
				
					 for ($i=0;$i<count($response_array);$i++) {
						
						 if ($response_array[$i]['is_available'] == 0) {
								 
							$available = true;
								 
						}
						 
					 }
					
				} else {
					
					$jsonSendData = '[{"appointment_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","patient_description":"*xxxxxxxxx*","user_type":"'.$user_type.'"}]';	
						
					$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
			
					$response = $r->send()->getBody();
					
					$response_array = json_decode($response, true);
					
					$check_response = $response;
			
					 for ($i=0;$i<count($response_array);$i++) {
						 
						 if ((int) $response_array[$i]['ApptID'] == (int) $appt_id) {
								 
								 $available = true;
								 
						}
						 
					 }
					
				}
			
			}
		}
			
	
	
		if ($available || $surgery_software == 'Zedmed') {
			
			if ($surgery_software == 'BestPractice') {
				 
				$user_type = 1;

				    $jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_id":'.$patient_id.',"appt_day":"'.$appt_day.'","appt_length":'.$appt_length.',"patient_email":"'.$email.'","user_string":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"patient_description":"'.$patient_description.'","delete_from_day":"'.$now_date_time.'","show_all_appointments":"'.$show_all_appointments.'","doctor_name":"'.$doctor_name.'","appointment_description":"Internet","appt_id":'.$appt_id.',"user_type":"patient","patient_name":"'.$patient_name.'","appt_secs":'.$appt_secs.',"doctor_id":'.$doctor_id.'}]';
				  
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				 
				 $request_data = $jsonSendData;
			
			 } else if ($surgery_software == 'PracSoft') {
				 
				 if ($show_all_appointments == 'yes') {
					 
					 $jsonSendData = '[{"doctor_id":'.$doctor_id.',"user_string":"'.$user_string.'","doctor_code":"'.$doctor_code.'","appt_time":"'.$appt_time.'","patient_description":"'.$user_string.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","delete_from_day":"'.$now_date_time.'","appt_id":'.$appt_id.',"appointment_book_id":'.$appointment_book_id.',"patient_email":"'.$email.'","appt_day":"'.$appt_day.'","patient_name":"'.$patient_name.'","user_type":"'.$user_type.'","appointment_description":"","appt_date":"'.$appt_date_time.'"}]';
					
				 $post_array = array('makeNewAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 } else {
					 
					 $jsonSendData = '[{"appt_time":"'.$appt_time.'","user_type":"'.$user_type.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","delete_from_day":"'.$now_date_time.'","patient_name":"'.$patient_name.'","user_string":"'.$user_string.'","doctor_id":'.$doctor_id.',"appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","patient_description":"'.$user_string.'","appt_id":'.$appt_id.'}]';
					
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 }
				 
			  } else if ($surgery_software == 'Zedmed') {
				  
				  $jsonSendData = '[{"appt_day":"'.$appt_date_time.'","doctor":"'.$doctor_id.'","appt_date":"'.$zedmed_appt_day.'","doctor_name":"'.$doctor_name.'","patient_email":"'.$email.'","patient_name":"'.$patient_name.'","appointment_description":"","patient_description":"'.$user_string.'","delete_from_day":"'.$zedmed_now_date_time.'","user_string":"'.$user_string.'","start_point":"'.$start_point.'","appt_id":'.$appt_id.',"clinic_code":"'.$clinic_code.'","user_type":"'.$user_type.'","appt_time":"'.$appt_time.'","end_point":"'.$end_point.'"}]'; 
				  
				   $post_array = array('makeAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				   
				   print_r($post_array);
				   
			  }
	
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		 $stmt3 = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date,
			json,
			json_response) 
		VALUES (?,?,?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time; Details: $patient_name [DOB: $display_dob] [Ph: $mobile]";
	
	mysqli_bind_param($stmt3, 'iissssss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time, $jsonSendData, $response);

	mysqli_stmt_execute($stmt3);

	mysqli_stmt_free_result($stmt1);
	mysqli_stmt_free_result($stmt2);
	mysqli_stmt_free_result($stmt_zm_doc);
	mysqli_stmt_free_result($stmt3);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		 
		  $plist_string .= '
	<dict>
		<key>RequestData</key>
		<string><![CDATA['.$request_data.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$response_array[0]['result_string'].']]></string>
		<key>sql</key>
		<string><![CDATA['.$response_array[0]['extra_data'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

		} else {
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$checkApp.']]></string>
		<key>JSON</key>
		<string><![CDATA['.$check_response.']]></string>
		<key>heading</key>
		<string><![CDATA[Appointment Unavailable]]></string>
		<key>message</key>
		<string><![CDATA[This appointment is no longer available.
		
Please choose another.]]></string>
	</dict>
</array>
</plist>';
			
		}
	
	echo $plist_string;

?>