<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$company = '';//$_POST['company'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$patient_name = $first_name.' '.$last_name;
$user_type = $_POST['user_type'];

$dob = $_POST['dob'];
$pos = strrpos($dob, "-");
if ($pos !== false) { // note: three equal signs
   	$dob_array = explode('-', $dob);
	$dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
}

$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);
	
	$stat_surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT 
stat_doctors.surgery_id
FROM stat_doctors");
	
      mysqli_stmt_execute($stat_surgeries_stmt);

      mysqli_stmt_bind_result($stat_surgeries_stmt, $row->surgery_id);
	  
	  $stat_surgery_array = array();

      while (mysqli_stmt_fetch($stat_surgeries_stmt)) {
		 
		  $stat_surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($stat_surgeries_stmt);  
	  
$surgery_stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($surgery_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($surgery_stmt);

      mysqli_stmt_bind_result($surgery_stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($surgery_stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

    mysqli_stmt_free_result($surgery_stmt);
		
	 $login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($login_stmt);
		 
		 mysqli_stmt_bind_result($login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($login_stmt);

		 if (mysqli_stmt_num_rows($login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			
		mysqli_stmt_free_result($login_stmt);  
		
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
			
			
		 } else {
			 
			 $check_user_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where (user_username = ? or email = ?) and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($check_user_stmt, 'ssi', $username, $email, $surgery_id);
		 
		 mysqli_stmt_execute($check_user_stmt);
		 
		 mysqli_stmt_bind_result($check_user_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($check_user_stmt);

		 if (mysqli_stmt_num_rows($check_user_stmt) > 0) {
			 
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Username and/or Email In Use!]]></string>
						<key>message</key>
						<string><![CDATA[This username and/or email is in use. Please use a different username and/or email address.]]></string>
					</dict>
				</array>
			</plist>';
			
			mysqli_stmt_free_result($check_user_stmt);  
			 
		 } else {
			 
			  $add_user_stmt = mysqli_prepare($mysqli, "INSERT INTO users (surgery_id, user_type, first_name, last_name, dob, email, mobile, user_username, user_password, created_date) VALUES (?,?,?,?,?,?,?,?,AES_ENCRYPT(?, '0bfuscate'),?)");
			  
			  mysqli_stmt_bind_param($add_user_stmt, 'isssssssss', $surgery_id, $user_type, $first_name, $last_name, $dob, $email, $mobile, $username, $password, $now_date_time);
			  
			  if (mysqli_stmt_execute($add_user_stmt)) {
				  
				  $new_login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($new_login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($new_login_stmt);
		 
		 mysqli_stmt_bind_result($new_login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($new_login_stmt);

		 if (mysqli_stmt_num_rows($new_login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($new_login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			  
		 }
		  
		 mysqli_stmt_free_result($add_user_stmt);  
		 mysqli_stmt_free_result($new_login_stmt);  
		 
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
			
			  } else {
				  
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Failed!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has failed.
			
Please try again.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
				   
			  }
			  
		 }
	}
			  
			 $user_stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "iPhone";
			$log_action = "Register/Log In";
			$log_action_comment = $response;
			$log_action_comment = "Surgery: $surgery_name; Details: $patient_name [DOB: $dob] [Ph: $mobile]";
			
			mysqli_bind_param($user_stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($user_stmt);
			mysqli_stmt_free_result($user_stmt);
		
			mysqli_close($surgeries_stmt);
			mysqli_close($stat_surgeries_stmt);
			mysqli_close($surgery_stmt);
			mysqli_close($login_stmt);
			mysqli_close($add_user_stmt);
			mysqli_close($new_login_stmt);
			mysqli_close($user_stmt);
			mysqli_close($check_user_stmt);
			
			mysqli_close($mysqli);
			
	
	echo $plist_string;

?>



