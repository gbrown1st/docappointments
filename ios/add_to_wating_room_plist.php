<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$doctor_id = $_POST['doctor_id'];
$doctor_code = $_POST['doctor_code'];

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	server_url.server_wsdl_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  
      }
	
      mysqli_stmt_free_result($stmt);
	  
	  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';
	  
	  if ($surgery_software == 'Stat') {
		  
		  $dob = strtotime($dob);
		  $dob = date('Ymd', $dob);
		  $gender = strtoupper($gender);
		  $gender = $gender[0];
		  $params = array ('resourceId'=>$doctor_id, 'lastName'=>$last_name, 'firstName'=>$first_name, 'gender'=>$gender, 'dob'=>$dob);

		try {
			
		   $client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
		  
		  $result = $client->ArriveAppointment($params);
			
			$result_code = (int) $result->ArriveAppointmentResult;
			
				if ($result_code == 0) {
		
					$plist_string .= "
	<dict>
		<key>heading</key>
		<string><![CDATA[Your Arrival Has Been Noted]]></string>
		<key>message</key>
		<string><![CDATA[The front desk has been advised of your arrival.\n\nPlease take a seat.]]></string>
	</dict>";
		
				 } else {
		
					$plist_string .= "
	<dict>
		<key>heading</key>
		<string><![CDATA[No Appointment Found]]></string>
		<key>message</key>
		<string><![CDATA[We can't find an appointment for you with this doctor.\n\nPlease see the front desk.]]></string>
	</dict>";
					
				}
			
			}
		catch (SoapFault $exception) {
			echo $exception->getMessage();
		}
		  
	  } else if ($surgery_software == 'Medilink') {
		  
		try {
        $client = new SoapClient($server_wsdl_url, array('login' => "admin", 'password' => "", "trace" => 1, "exception" => 0, 'cache_wsdl'   =>  WSDL_CACHE_BOTH, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS)); 
		
		if ($gender == 'Male') {
			$gender = 'M';
		} else if ($gender == 'Female') {
			$gender = 'F';
		}
			  
		$params = new stdClass;
        $params->ResourceId = (int) $doctor_id;
        $params->FirstName = $first_name;
        $params->LastName = $last_name;
        $params->Gender = $gender;
		$params->DOB = date('c', strtotime($dob));
       	$methodName = "ArriveAppointment";
        $resultName = $methodName . "Result";
        $result = $client->$methodName($params);
		$resultValue = $result->$resultName;
			
				if ($resultValue == 'Success') {
		
					$plist_string .= "
	<dict>
		<key>heading</key>
		<string><![CDATA[Your Arrival Has Been Noted]]></string>
		<key>message</key>
		<string><![CDATA[The front desk has been advised of your arrival.\n\nPlease take a seat.]]></string>
	</dict>";
		
				 } else {
		
					$plist_string .= "
	<dict>
		<key>heading</key>
		<string><![CDATA[No Appointment Found]]></string>
		<key>message</key>
		<string><![CDATA[We can't find an appointment for you with this doctor.\n\nPlease see the front desk.]]></string>
	</dict>";
					
				}
			
    }
    catch (SoapFault $exception) {
        echo $exception->getMessage();
    }
		  
	  }
	  
	  else {
		  
		  if ($surgery_software == 'BestPractice') {
			  
			  if ($gender == 'M') {
				  
				  $gender = 'Male';
				  
			  } else {
				  
				  $gender = 'Female';
				  
			  }
			  
		  }
		 
		$post_array = array('addToWaitingRoom' => 'true', 'first_name' => $first_name, 'last_name' => $last_name, 'dob' => $dob, 'gender' => $gender, 'doctor_id' => $doctor_id, 'doctor_code' => $doctor_code);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
	
	for ($i=0;$i<count($response_array);$i++) {
		
		$message = str_replace("\'", "'", $response_array[0]['result_string']);
		$plist_string .= '
	<dict>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$message.']]></string>
	</dict>';
		
	}

}
	
		$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;
		
?>



