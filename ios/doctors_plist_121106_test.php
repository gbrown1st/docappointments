<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = '13';//$_GET['surgery_id'];

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);
	
	$stat_surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT 
stat_doctors.surgery_id
FROM stat_doctors");
	
      mysqli_stmt_execute($stat_surgeries_stmt);

      mysqli_stmt_bind_result($stat_surgeries_stmt, $row->surgery_id);
	  
	  $stat_surgery_array = array();

      while (mysqli_stmt_fetch($stat_surgeries_stmt)) {
		 
		  $stat_surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($stat_surgeries_stmt);
	    
$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	server_url.server_wsdl_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  
      }

      mysqli_stmt_free_result($stmt);

	  if (in_array($surgery_id, $surgery_array)) {
	  
	  $doc_loc_stmt = mysqli_prepare($mysqli,
	  "SELECT
	zedmed_doctors.doctor_id,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	zedmed_doctors.new_patients
FROM
	zedmed_doctors
INNER JOIN doctor_locations ON zedmed_doctors.surgery_id = doctor_locations.surgery_id
AND zedmed_doctors.doctor_id = doctor_locations.doctor_id
INNER JOIN surgery_locations ON doctor_locations.location_id = surgery_locations.surgery_location_id
WHERE
	zedmed_doctors.surgery_id = ?
AND online_status = 'yes'");

	  } else if (in_array($surgery_id, $stat_surgery_array)) {
	  
	  $doc_loc_stmt = mysqli_prepare($mysqli,
	  "SELECT
	stat_doctors.doctor_id,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	stat_doctors.new_patients
FROM
	stat_doctors
INNER JOIN doctor_locations ON stat_doctors.surgery_id = doctor_locations.surgery_id
AND stat_doctors.doctor_id = doctor_locations.doctor_id
INNER JOIN surgery_locations ON doctor_locations.surgery_id = surgery_locations.surgery_id
WHERE
	stat_doctors.surgery_id = ?
AND online_status = 'yes'");

	  } else {
		  
	  $doc_loc_stmt = mysqli_prepare($mysqli,
	  "SELECT
	doctor_locations.doctor_id,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	doctor_new_patients.new_patients
FROM
	doctor_locations
INNER JOIN surgery_locations ON doctor_locations.location_id = surgery_locations.surgery_location_id
LEFT OUTER JOIN doctor_new_patients ON doctor_locations.surgery_id = doctor_new_patients.surgery_id
AND doctor_locations.doctor_id = doctor_new_patients.doctor_id
WHERE
	doctor_locations.surgery_id = ?");
	
	  }
	
	  mysqli_stmt_bind_param($doc_loc_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($doc_loc_stmt);

      mysqli_stmt_bind_result($doc_loc_stmt, $row->doctor_id, $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, $row->surgery_postcode, $row->surgery_phone, $row->new_patients);
	  
	  $doc_loc_array = array();

      while (mysqli_stmt_fetch($doc_loc_stmt)) {
		  
		   if ($row->new_patients) {
			 
			  $new_patients = $row->new_patients;
			  
		  } else {
			  
			  $new_patients = 'yes';
		  }
		 
		  $doc_loc_array[] = array('doctor_id' => $row->doctor_id, 'new_patients' => $new_patients, 'surgery_address_1' => $row->surgery_address_1, 'surgery_address_2' => $row->surgery_address_2, 'surgery_suburb_town' => $row->surgery_suburb_town, 'surgery_state' => $row->surgery_state, 'surgery_postcode' => $row->surgery_postcode, 'surgery_phone' => $row->surgery_phone);
	
      }

      mysqli_stmt_free_result($doc_loc_stmt);
      
	
	if ($surgery_id == "7" || $surgery_id == "22" || $surgery_id == "29" || $surgery_id == "32" || $surgery_id == "33" || $surgery_id == "34" || $surgery_id == "35" || $surgery_id == "36" || $surgery_id == "37" || $surgery_id == "43" || $surgery_id == "52"  || $surgery_id == "53" || $surgery_id == "54") {
		
		$doctors_stmt = mysqli_prepare($mysqli,
	  "SELECT
	surgery_doctors.doctor_id,
	surgery_doctors.doctor_name,
	surgery_doctors.online_status
FROM
	surgery_doctors
WHERE
	surgery_doctors.surgery_id = ?
AND surgery_doctors.online_status = 'yes'");
	
	  mysqli_stmt_bind_param($doctors_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($doctors_stmt);

      mysqli_stmt_bind_result($doctors_stmt, $row->doctor_id, $row->doctor_name, $row->online_status);
	  
	  $response_array = array();

      while (mysqli_stmt_fetch($doctors_stmt)) {
		 
		  $response_array[] = array('DoctorID' => $row->doctor_id, 'DoctorApptID' => $row->doctor_id, 'DoctorCode' => $row->doctor_id, 'FullName' => $row->doctor_name);

      }

      mysqli_stmt_free_result($doc_loc_stmt);
	  
		mysqli_close($mysqli);
		
	} else if (in_array($surgery_id, $surgery_array)) {
		
		$doctors_stmt = mysqli_prepare($mysqli,
	  "SELECT
	zedmed_doctors.doctor_id,
	zedmed_doctors.doctor_name
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND online_status = 'yes'");
	
	  mysqli_stmt_bind_param($doctors_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($doctors_stmt);

      mysqli_stmt_bind_result($doctors_stmt, $row->doctor_id, $row->doctor_name);
	  
	  $response_array = array();

      while (mysqli_stmt_fetch($doctors_stmt)) {
		 
		  $response_array[] = array('DoctorID' => $row->doctor_id, 'DoctorApptID' => $row->doctor_id, 'DoctorCode' => $row->doctor_id, 'FullName' => $row->doctor_name);
	
      }

      mysqli_stmt_free_result($doc_loc_stmt);
	  
		mysqli_close($mysqli);
		
	} else if (in_array($surgery_id, $stat_surgery_array)) {
		
		$doctors_stmt = mysqli_prepare($mysqli,
	  "SELECT
	stat_doctors.doctor_id,
	stat_doctors.doctor_name
FROM
	stat_doctors
WHERE
	surgery_id = ?
AND online_status = 'yes'");
	
	  mysqli_stmt_bind_param($doctors_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($doctors_stmt);

      mysqli_stmt_bind_result($doctors_stmt, $row->doctor_id, $row->doctor_name);
	  
	  $response_array = array();

      while (mysqli_stmt_fetch($doctors_stmt)) {
		 
		  $response_array[] = array('DoctorID' => $row->doctor_id, 'DoctorApptID' => $row->doctor_id, 'DoctorCode' => $row->doctor_id, 'FullName' => $row->doctor_name);
	
      }

      mysqli_stmt_free_result($doc_loc_stmt);
	  
		mysqli_close($mysqli);
		
	} else {
		
		$post_array = array('getDoctors' => 'true');

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
	}
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA[First Available Appointments]]></string>
		<key>DoctorCode</key>
		<string><![CDATA[]]></string>
		<key>DoctorID</key>
		<string><![CDATA[-1]]></string>
		<key>DoctorApptID</key>
		<string><![CDATA[]]></string>
		<key>DoctorLocation</key>
		<string><![CDATA[The first available appointments in the next 2 days.]]></string>
		<key>DoctorNewPatients</key>
		<string><![CDATA[yes]]></string>
	</dict>';
	
      for ($i=0;$i<count($response_array);$i++) {
		  
		  if (count($response_array[$i]) == 7) {
						
							if ($response_array[$i]['surgery_id'] == '0') {
							   
							  continue;
							   
							}
						  
						  
						   if ((int) $response_array[$i]['surgery_id'] != (int) $surgery_id) {
							   
							  continue;
							   
						   }
					   
						}
		  
		  for ($n=0;$n<count($doc_loc_array);$n++) {
			  
			  if ($doc_loc_array[$n]['doctor_id'] == $response_array[$i]['DoctorID']) {
				  
				  $new_patients = $doc_loc_array[$n]['new_patients'];
				   
				  $doc_location = $surgery_name;
				  
				  $doc_location .= '
'.$doc_loc_array[$n]['surgery_address_1'];
				  
				  if (strlen($doc_loc_array[$n]['surgery_address_2']) > 2) {
					  
					  $doc_location .= '
'.$doc_loc_array[$i]['surgery_address_2'];
					  
				  }
				  
				  $doc_location .= '
'.$doc_loc_array[$n]['surgery_suburb_town'].' '.$doc_loc_array[$n]['surgery_state'].' '.$doc_loc_array[$n]['surgery_postcode'].' Phone: '.$doc_loc_array[$n]['surgery_phone'];

$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.$response_array[$i]['FullName'].']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.$response_array[$i]['DoctorCode'].']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.$response_array[$i]['DoctorID'].']]></string>
		<key>DoctorApptID</key>
		<string><![CDATA['.$response_array[$i]['DoctorApptID'].']]></string>
		<key>DoctorLocation</key>
		<string><![CDATA['.$doc_location.']]></string>
		<key>DoctorNewPatients</key>
		<string><![CDATA['.$new_patients.']]></string>
	</dict>';
			  
			  }
			  
		  }
		  
			
		  
      }

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

?>



