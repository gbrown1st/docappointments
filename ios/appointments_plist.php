<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];

$username = $_POST['username'];
$password = $_POST['password'];
$user_type = $_POST['user_type'];

$pracsoftDescrip = $_POST['pracsoftDescrip'];
$patient_string = str_replace("*", "", $pracsoftDescrip);
$appoitment_type = 'Internet';

$fromDate = $_POST['fromDate'];//'2011-12-25 13:00:00 +0000';
$toDate = $_POST['toDate'];//'2012-01-31 12:59:59 +0000';

$dateMonthYearArr = array();
$fromDateTS = strtotime($fromDate);
$toDateTS = strtotime($toDate);

for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
// use date() and $currentDateTS to format the dates in between
$currentDateStr = date("Y-m-d",$currentDateTS);
$dateMonthYearArr[] = $currentDateStr;
//print $currentDateStr."<br />";
}

$dates_array_1 = array();
$dates_array_2 = array();
$dates_array_3 = array();
$dates_array_4 = array();
$dates_array_5 = array();
$dates_array_6 = array();

for ($d=0;$d<count($dateMonthYearArr);$d++) {
	
	if ($d < 8) {
		
		$dates_array_1[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 8 && $d < 15) {
		
		$dates_array_2[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 15 && $d < 22) {
		
		$dates_array_3[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 22 && $d < 29) {
		
		$dates_array_4[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 29 && $d < 36) {
		
		$dates_array_5[] = $dateMonthYearArr[$d];
		
	} else if ($d >= 36 && $d < 42) {
		
		$dates_array_6[] = $dateMonthYearArr[$d];
		
	}
	
}

$comma_separated = implode("','", $dateMonthYearArr);
$comma_separated = "'".$comma_separated."'";

$comma_separated_1 = implode("','", $dates_array_1);
$comma_separated_1 = "'".$comma_separated_1."'";
$comma_separated_2 = implode("','", $dates_array_2);
$comma_separated_2 = "'".$comma_separated_2."'";
$comma_separated_3 = implode("','", $dates_array_3);
$comma_separated_3 = "'".$comma_separated_3."'";
$comma_separated_4 = implode("','", $dates_array_4);
$comma_separated_4 = "'".$comma_separated_4."'";
$comma_separated_5 = implode("','", $dates_array_5);
$comma_separated_5 = "'".$comma_separated_5."'";
$comma_separated_6 = implode("','", $dates_array_6);
$comma_separated_6 = "'".$comma_separated_6."'";


/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      	$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id
FROM
	doctor_new_patients
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->show_all_appointments, $row->appointment_book_id);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  if ($row->show_all_appointments) {
			 
			  $show_all_appointments = $row->show_all_appointments;
			  
		  } else {
			  
			  $show_all_appointments = 'no';
		  }
		  
		  if ($row->appointment_book_id) {
			 
			  $appointment_book_id = $row->appointment_book_id;
			  
		  } else {
			  
			  $appointment_book_id = 0;
		  }
		  
      }

	mysqli_stmt_free_result($stmt);
	mysqli_stmt_free_result($stmt2);

      mysqli_close($mysqli);
	  
	  $patients = '';
	  $patient_array = array();
	  $patient_details_array = array();
	  $AppInfo = '';
	  
	  if ($surgery_software == 'BestPractice') {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILEPHONE']);
		
		$getFamilyMembers= $_POST['getFamilyMembers'];
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_array[] = $response_array[$i]['INTERNALID'];
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILEPHONE']);
			
		}
	
		$post_array = array('getInternetPatientID' => 'true');
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$internet_patient_id = $response_array[0]['INTERNALID'];
		
		$patients = implode(",", $patient_array);
		
	}
	
	  	//$jsonSendData='[{"patient_id":"'.$patients.'","appointment_description":"Internet","patient_description":"","PractitionerID":'.$doctor_id.',"user_type":"'.$user_type.'","appt_day":"'.$comma_separated.'"}]';
		
	if ($surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22' || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36' || $surgery_id == '40' || $surgery_id == '41'  || $surgery_id == '42') {
			
			if ($show_all_appointments == 'yes') {
			
				 if ($surgery_software == 'BestPractice') {
					 
					 if (count($dates_array_1) > 0) {
						  
						 $jsonSendData1 = '[{"user_type":"patient","appt_day":"'.$comma_separated_1.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						$post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array1);
						$response1 = $r->send()->getBody();
					
					  }
					
					if (count($dates_array_2) > 0) {
							 
						 $jsonSendData2 = '[{"user_type":"patient","appt_day":"'.$comma_separated_2.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						$post_array2 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData2);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array2);
						$response2 = $r->send()->getBody();
					
				 }
				 
					if (count($dates_array_3) > 0) {
						 
						 $jsonSendData3 = '[{"user_type":"patient","appt_day":"'.$comma_separated_3.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						$post_array3 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData3);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array3);
						$response3 = $r->send()->getBody();
					
					 }
					 
					 if (count($dates_array_4) > 0) {
					
						 $jsonSendDat4 = '[{"user_type":"patient","appt_day":"'.$comma_separated_4.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						$post_array4 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData4);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array4);
						$response4 = $r->send()->getBody();
					
					  }
					
					 
					if (count($dates_array_5) > 0) {
						 
						 $jsonSendData5 = '[{"user_type":"patient","appt_day":"'.$comma_separated_5.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						 
						$post_array5 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData5);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array5);
						$response5 = $r->send()->getBody();
					
					 }
					
					 if (count($dates_array_6) > 0) {
						 
						 $jsonSendData6 = '[{"user_type":"patient","appt_day":"'.$comma_separated_6.'","patient_description":"'.$pracsoftDescrip.'","appointment_description":"Internet","doctor_id":'.$doctor_id.',"patient_id":"'.$patients.'"}]';
						$post_array6 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData6);
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array6);
						$response6 = $r->send()->getBody();
					
					 }
					
					
				 } else {
					 
		 			if (count($dates_array_1) > 0) {
						
						 $jsonSendData1 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_1.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array1 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData1);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array1);
						$response1 = $r->send()->getBody();
					
					}
					
					if (count($dates_array_2) > 0) {
						 
						 $jsonSendData2 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_2.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array2 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData2);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array2);
						$response2 = $r->send()->getBody();
					
					 }
					
					if (count($dates_array_3) > 0) {
						 
						 $jsonSendData3 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_3.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array3 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData3);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array3);
						$response3 = $r->send()->getBody();
					
					 }
					 
					if (count($dates_array_4) > 0) {
						 
						 $jsonSendData4 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_4.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array4 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData4);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array4);
						$response4 = $r->send()->getBody();
					
					 }
					
					if (count($dates_array_5) > 0) {
						
						 $jsonSendData5 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_5.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array5 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData5);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array5);
						$response5 = $r->send()->getBody();
					
					}
					
					if (count($dates_array_6) > 0) {
						 
						  $jsonSendData6 = '[{"doctor_id":'.$doctor_id.',"patient_description":"'.$pracsoftDescrip.'","patient_string":"'.$patient_string.'","appt_day":"'.$comma_separated_6.'","show_all_appointments":"'.$show_all_appointments.'"}]';
						 $post_array6 = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData6);
						 
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array6);
						$response6 = $r->send()->getBody();
					
					 }
					
				 }
				
				if (count($dates_array_1) > 0) {
					
					$response_array1 = json_decode($response1, true);
					
				}
				
				if (count($dates_array_2) > 0) {
					
					$response_array2 = json_decode($response2, true);
					
				}
				
				if (count($dates_array_3) > 0) {
					
					$response_array3 = json_decode($response3, true);
					
				}
				
				if (count($dates_array_4) > 0) {
					
					$response_array4 = json_decode($response4, true);
					
				}
				
				if (count($dates_array_5) > 0) {
					
					$response_array5 = json_decode($response5, true);
					
				}
				
				if (count($dates_array_6) > 0) {
					
					$response_array6 = json_decode($response6, true);
					
				}
				
				$response_array = array ();
				
				if (count($response_array1) > 0) {
					
					for ($i=0;$i<count($response_array1);$i++) {
						
						$date = strtotime($response_array1[$i]['When']);
						$response_array1[$i]['Date'] = $date;
						$response_array[] = $response_array1[$i];
						
					}
				
				}
				
				if (count($dates_array_2) > 0) {
				
					for ($i=0;$i<count($response_array2);$i++) {
						
						$date = strtotime($response_array2[$i]['When']);
						$response_array2[$i]['Date'] = $date;
						$response_array[] = $response_array2[$i];
						
					}
				
				}
				
				if (count($dates_array_3) > 0) {
				
					for ($i=0;$i<count($response_array3);$i++) {
						
						$date = strtotime($response_array3[$i]['When']);
						$response_array3[$i]['Date'] = $date;
						$response_array[] = $response_array3[$i];
						
					}
				
				}
				
				if (count($dates_array_4) > 0) {
					
					for ($i=0;$i<count($response_array4);$i++) {
						
						$date = strtotime($response_array4[$i]['When']);
						$response_array4[$i]['Date'] = $date;
						$response_array[] = $response_array4[$i];
						
					}
				
				}
				
				if (count($response_array5) > 0) {
				
					for ($i=0;$i<count($response_array5);$i++) {
						
						$date = strtotime($response_array5[$i]['When']);
						$response_array5[$i]['Date'] = $date;
						$response_array[] = $response_array5[$i];
						
					}
				
				}
				
				if (count($dates_array_6) > 0) {
					
					for ($i=0;$i<count($response_array6);$i++) {
						
						$date = strtotime($response_array6[$i]['When']);
						$response_array6[$i]['Date'] = $date;
						$response_array[] = $response_array6[$i];
						
					}
				
				}
			
			} else {
				
				$jsonSendData = '[{"patient_id":"'.$patients.'","user_type":"'.$user_type.'","PractitionerID":'.$doctor_id.',"appointment_description":"'.$appointment_description.'","appt_day":"'.$comma_separated.'","patient_description":"'.$pracsoftDescrip.'"}]';
				$post_array = array('getWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
			}
			
		} else {
			
			$jsonSendData = '[{"patient_id":"'.$patients.'","user_type":"'.$user_type.'","PractitionerID":'.$doctor_id.',"appointment_description":"'.$appointment_description.'","appt_day":"'.$comma_separated.'","patient_description":"'.$pracsoftDescrip.'"}]';
			$post_array = array('getWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
		}
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
		
		}
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		  
		  if ($response_array[$i]['Descrip'] != 'Unavailable' && $response_array[$i]['When'] != 'Unavailable' ) {
			  
			if ($response_array[$i]['Descrip'] == 'Available') {
				
				$response_array[$i]['Descrip'] = 'Internet';
					 
			}
				
		 $appInfo = '';
		  
		 $ApptDate_array = explode(" ", $response_array[$i]['When']);
		 
		 $date = strtotime($response_array[$i]['When']);

		 $appTime = date("D j M Y g:i A",$date);
		 
		  if ($surgery_software == 'BestPractice') {
			  
			  $appType = 'Internet';
				
			 for ($n=0;$n<count($patient_details_array);$n++) {
	
				 if ((int) $patient_details_array[$n]['patient_id'] == (int) $response_array[$i]['PatientID']) {
					 
					 $dob = $patient_details_array[$n]['dob'];
					 $dob_array = explode('-', $dob);
					 $display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
					 
					 $appInfo = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$display_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 
				 } 
				 
			 }
		 
		  } else {
			  
			  if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc' || $response_array[$i]['Descrip'] == 'Available') {
				  
				 $appType = 'Internet';
				  
			 } else {
				 
				 $appInfo = $response_array[$i]['Descrip'];
				 
			 }
			  
		  }
				  $plist_string .= '
			<dict>
				<key>JSON</key>
				<string><![CDATA['.$jsonSendData.']]></string>
				<key>PractitionerID</key>
				<string><![CDATA['.$response_array[$i]['PractitionerID'].']]></string>
				<key>PatientID</key>
				<string><![CDATA['.$response_array[$i]['PatientID'].']]></string>
				<key>ApptDate</key>
				<string><![CDATA['.$ApptDate_array[0].']]></string>
				<key>ApptDescription</key>
				<string><![CDATA['.$appTime.']]></string>
				<key>ApptDateTime</key>
				<string><![CDATA['.$response_array[$i]['When'].']]></string>
				<key>ApptInfo</key>
				<string><![CDATA['.$appInfo.']]></string>
				<key>ApptType</key>
				<string><![CDATA['.$appType.']]></string>
				<key>AppSecs</key>
				<string><![CDATA['.$response_array[$i]['AppSecs'].']]></string>
				<key>AppLength</key>
				<string><![CDATA['.$response_array[$i]['Length'].']]></string>
				<key>ApptID</key>
				<string><![CDATA['.$response_array[$i]['ApptID'].']]></string>
			</dict>';
			  }
	  
		  
	  }

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

?>



