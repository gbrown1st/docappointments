<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgery_state = $_GET['surgery_state'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surgeries.surgery_id,
	surgeries.surgery_name,
	surgeries.surgery_address_1,
	surgeries.surgery_address_2,
	surgeries.surgery_suburb_town,
	surgeries.surgery_state,
	surgeries.surgery_postcode,
	surgeries.contact_email,
	surgeries.contact_phone,
	surgeries.latitude,
	surgeries.longitude,
	surgery_types.surgery_type, 
	surgery_text.surgery_front_screen, 
	surgery_text.surgery_make_appointment_alert, 
	surgery_text.surgery_cancel_appointment_alert
FROM surgeries INNER JOIN surgery_types ON surgeries.surgery_type_id = surgery_types.surgery_type_id
	 INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
WHERE
	(surgery_status = 'Active' OR surgeries.surgery_id IN (9,23,27,29,40,42,114,116,107,124,128,138))
AND (surgery_state = '$surgery_state' OR surgery_state_2 = '$surgery_state')
ORDER BY
	surgeries.surgery_name ASC");


      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->surgery_id, $row->surgery_name, 
							  $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, 
							  $row->surgery_postcode, $row->contact_email, 
							  $row->contact_phone, $row->latitude, $row->longitude, 
							  $row->surgery_type, $row->surgery_front_screen, $row->surgery_make_appointment_alert, $row->surgery_cancel_appointment_alert);

	$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

      while (mysqli_stmt_fetch($stmt)) {
		  
		  $surgery_front_screen = str_replace("[surgery_name]", $row->surgery_name, $row->surgery_front_screen);
		  $surgery_front_screen = str_replace("[surgery_phone]", $row->contact_phone, $surgery_front_screen);
		  
		  if ((int) $row->surgery_id == 9 || (int) $surgery_id == 23 || (int) $surgery_id == 29 || (int) $surgery_id == 40 || (int) $row->surgery_id == 114 || (int) $row->surgery_id == 116 || (int) $row->surgery_id == 107 || (int) $surgery_id == 124 || (int) $surgery_id == 128 || (int) $row->surgery_id == 138) {
			  $row->surgery_state = '';
			  $row->surgery_postcode = '';
		  }
		  
		  $plist_string .= '
	<dict>
		<key>surgery_id</key>
		<string><![CDATA['.$row->surgery_id.']]></string>
		<key>surgery_name</key>
		<string><![CDATA['.$row->surgery_name.']]></string>
		<key>surgery_address_1</key>
		<string><![CDATA['.$row->surgery_address_1.']]></string>
		<key>surgery_address_2</key>
		<string><![CDATA['.$row->surgery_address_2.']]></string>
		<key>surgery_suburb_town</key>
		<string><![CDATA['.$row->surgery_suburb_town.']]></string>
		<key>surgery_state</key>
		<string><![CDATA['.$row->surgery_state.']]></string>
		<key>surgery_postcode</key>
		<string><![CDATA['.$row->surgery_postcode.']]></string>
		<key>contact_email</key>
		<string><![CDATA['.$row->contact_email.']]></string>
		<key>contact_phone</key>
		<string><![CDATA['.$row->contact_phone.']]></string>
		<key>latitude</key>
		<string><![CDATA['.$row->latitude.']]></string>
		<key>longitude</key>
		<string><![CDATA['.$row->longitude.']]></string>
		<key>surgery_type</key>
		<string><![CDATA['.$row->surgery_type.']]></string>
		<key>surgery_front_screen</key>
		<string><![CDATA['.$surgery_front_screen.']]></string>
		<key>surgery_make_appointment_alert</key>
		<string><![CDATA['.$row->surgery_make_appointment_alert.']]></string>
		<key>surgery_cancel_appointment_alert</key>
		<string><![CDATA['.$row->surgery_cancel_appointment_alert.']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
echo $plist_string;

?>



