<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];

$username = $_POST['username'];
$password = $_POST['password'];
$user_type = $_POST['user_type'];

$pracsoftDescrip = $_POST['pracsoftDescrip'];

if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	
} else {
	
	$appoitment_type = 'Internetwc';
	
}

$fromDate = $_POST['fromDate'];//'2011-12-25 13:00:00 +0000';
$toDate = $_POST['toDate'];//'2012-01-31 12:59:59 +0000';

$dateMonthYearArr = array();
$fromDateTS = strtotime($fromDate);
$toDateTS = strtotime($toDate);

for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
// use date() and $currentDateTS to format the dates in between
$currentDateStr = date("Y-m-d",$currentDateTS);
$dateMonthYearArr[] = $currentDateStr;
//print $currentDateStr."<br />";
}


$comma_separated = implode("','", $dateMonthYearArr);

$comma_separated = "'".$comma_separated."'";


/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
	  
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.show_all_appointments
FROM
	doctor_new_patients
WHERE
	surgery_id = 19
AND doctor_id = 3");

	  mysqli_stmt_bind_param($stmt, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->show_all_appointments);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $show_all_appointments = $row->show_all_appointments;
		  
      }

      mysqli_stmt_free_result($stmt2);
      mysqli_close($mysqli);
	  
	  $patients = '';
	  $patient_array = array();
	  $patient_details_array = array();
	  $AppInfo = '';
	  
	  if ($surgery_software == 'BestPractice') {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILEPHONE']);
		
		$getFamilyMembers= $_POST['getFamilyMembers'];
		$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
		$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$patient_array[] = $response_array[$i]['INTERNALID'];
			$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILEPHONE']);
			
		}
	
		$post_array = array('getInternetPatientID' => 'true');
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$patient_array[] = $response_array[0]['INTERNALID'];
		
		$internet_patient_id = $response_array[0]['INTERNALID'];
		
		if ($user_type == 'employer') {
			
			$post_array = array('getInternetPatientIDEmployer' => 'true');
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$patient_array[] = $response_array[0]['INTERNALID'];
			
			$internet_employer_id = $response_array[0]['INTERNALID'];
			
		}
		
		 $patients = implode(",", $patient_array);
		
	}
		  
	  	//$jsonSendData='[{"patient_id":"'.$patients.'","appointment_description":"Internet","patient_description":"","PractitionerID":'.$doctor_id.',"user_type":"'.$user_type.'","appt_day":"'.$comma_separated.'"}]';
		
		$jsonSendData = '[{"patient_id":"'.$patients.'","user_type":"'.$user_type.'","PractitionerID":'.$doctor_id.',"appointment_description":"'.$appointment_description.'","appt_day":"'.$comma_separated.'","patient_description":"'.$pracsoftDescrip.'"}]';
		  
		$post_array = array('getWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
		
		}
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		  
		 $appInfo = '';
		  
		 $ApptDate_array = explode(" ", $response_array[$i]['When']);
		 
		 $date = strtotime($response_array[$i]['When']);

		 $appTime = date("D j M Y g:i A",$date);
		 
		  if ($surgery_software == 'BestPractice') {
			  
			   if ($response_array[$i]['PatientID'] == $internet_patient_id) {
			 
			 		$appType = 'Internet';
			 
				 }
				 
				 if ($response_array[$i]['PatientID'] == $internet_employer_id) {
					 
					 $appType = 'Internetwc';
					 
				 }
				 
			 for ($n=0;$n<count($patient_details_array);$n++) {
	
				 if ($patient_details_array[$n]['patient_id'] == $response_array[$i]['PatientID']) {
					 
					 $dob = $patient_details_array[$n]['dob'];
					 $dob_array = explode('-', $dob);
					 $display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
					 
					 $appInfo = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$display_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 
				 } 
				 
			 }
		 
		  } else {
			  
			  if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc') {
				 
				 $appType = $response_array[$i]['Descrip'];
				  
			 } else {
				 
				 $appInfo = $response_array[$i]['Descrip'];
				 
			 }
			  
		  }
		
		
		  $plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$jsonSendData.']]></string>
		<key>PractitionerID</key>
		<string><![CDATA['.$response_array[$i]['PractitionerID'].']]></string>
		<key>PatientID</key>
		<string><![CDATA['.$response_array[$i]['PatientID'].']]></string>
		<key>ApptDate</key>
		<string><![CDATA['.$ApptDate_array[0].']]></string>
		<key>ApptDescription</key>
		<string><![CDATA['.$appTime.']]></string>
		<key>ApptDateTime</key>
		<string><![CDATA['.$response_array[$i]['When'].']]></string>
		<key>ApptInfo</key>
		<string><![CDATA['.$appInfo.']]></string>
		<key>ApptType</key>
		<string><![CDATA['.$appType.']]></string>
		<key>AppSecs</key>
		<string><![CDATA['.$response_array[$i]['AppSecs'].']]></string>
		<key>ApptID</key>
		<string><![CDATA['.$response_array[$i]['ApptID'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

?>



