<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

/* $latidute = '-37.703655';//$_GET['latitude'];
$longitude = '145.119904';//$_GET['longitude'];
$distance = '10';//$_GET['distance']; */

$latidute = $_GET['latitude'];
$longitude = $_GET['longitude'];
$distance = $_GET['distance'];

if ($distance == '1' || $distance == '5' || $distance == '10' ) {
	
	$limit = '20';
	
} else if ($distance == '25') {
	
	$limit = '50';
	
} else  {
	
	$limit = '100';
} 

$stmt = mysqli_prepare($mysqli,
          "SELECT
	`pharmacy_id` as pharmacy_id,
	`company_name` as company_name,
	`street` as street,
	`suburb` as suburb,
	`state` as state,
	`postcode` as postcode,
	`longitude` as longitude,
	`latitude` as latitude,
	`phone` as phone,
	`website` as website,
	`mobile` as mobile,
	`toll_free` as toll_free,
	`fax` as fax,
	`after_hours` as after_hours,
	`email` as email,
	(
		3959 * acos(
			cos(radians(?))* cos(radians(latitude))* cos(
				radians(longitude)- radians(?)
			)+ sin(radians(?))* sin(radians(latitude))
		)
	)AS distance
FROM
	`pharmacies`
WHERE `pharmacy_status` = 'active'
HAVING
	distance < ?
ORDER BY
	distance
LIMIT ?
");

	  mysqli_stmt_bind_param($stmt, 'dddii', $latidute, $longitude, $latidute, $distance, $limit);

      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->pharmacy_id, $row->company_name, 
							  $row->street, $row->suburb, $row->state, $row->postcode, 
							  $row->longitude, $row->latitude, 
							  $row->phone, $row->website, $row->mobile, 
							  $row->toll_free, $row->fax, $row->after_hours, 
							  $row->email,  $row->distance);

	$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

      while (mysqli_stmt_fetch($stmt)) {
		  
		  $plist_string .= '
	<dict>
		<key>pharmacy_id</key>
		<string><![CDATA['.$row->pharmacy_id.']]></string>
		<key>company_name</key>
		<string><![CDATA['.$row->company_name.']]></string>
		<key>street</key>
		<string><![CDATA['.$row->street.']]></string>
		<key>suburb</key>
		<string><![CDATA['.$row->suburb.']]></string>
		<key>state</key>
		<string><![CDATA['.$row->state.']]></string>
		<key>postcode</key>
		<string><![CDATA['.$row->postcode.']]></string>
		<key>latitude</key>
		<string><![CDATA['.$row->latitude.']]></string>
		<key>longitude</key>
		<string><![CDATA['.$row->longitude.']]></string>
		<key>phone</key>
		<string><![CDATA['.$row->phone.']]></string>
		<key>website</key>
		<string><![CDATA['.$row->website.']]></string>
		<key>mobile</key>
		<string><![CDATA['.$row->mobile.']]></string>
		<key>toll_free</key>
		<string><![CDATA['.$row->toll_free.']]></string>
		<key>fax</key>
		<string><![CDATA['.$row->fax.']]></string>
		<key>after_hours</key>
		<string><![CDATA['.$row->after_hours.']]></string>
		<key>email</key>
		<string><![CDATA['.$row->email.']]></string>
		<key>distance</key>
		<string><![CDATA['.$row->distance.']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';


echo $plist_string;

 mysqli_stmt_free_result($stmt);
 mysqli_close($mysqli);

?>



