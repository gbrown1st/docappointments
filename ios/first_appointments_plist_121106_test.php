<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$user_type = 'patient';//$_POST['user_type'];

$fromDate1 = strtotime('now');
$fromDate2 = strtotime('+1 day');

$appt_day_1 = date("Y-m-d",$fromDate1);
$zedmed_appt_day_1 = date("m/d/Y",$fromDate1);
$zedmed_start_point_1 = $appt_day_1.' 00.00.00';
$zedmed_end_point_1 = $appt_day_1.' 23.59.59';
$appt_day_2 = date("Y-m-d",$fromDate2);
$zedmed_appt_day_2 = date("m/d/Y",$fromDate2);
$zedmed_start_point_2 = $appt_day_2.' 00.00.00';
$zedmed_end_point_2 = $appt_day_2.' 23.59.59';

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);


$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
   
	 if (in_array($surgery_id, $surgery_array)) {
	  
$first_appt_stmt = mysqli_prepare($mysqli,
	  "SELECT
	zedmed_doctors.doctor_id,
	zedmed_doctors.doctor_name,
	zedmed_doctors.new_patients,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.appointment_length,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	doctor_locations.doctor_name
FROM
	zedmed_doctors
INNER JOIN surgery_locations ON zedmed_doctors.surgery_id = surgery_locations.surgery_id
INNER JOIN doctor_locations ON surgery_locations.surgery_id = doctor_locations.surgery_id
AND zedmed_doctors.doctor_id = doctor_locations.doctor_id
WHERE
	zedmed_doctors.surgery_id = ?
AND online_status = 'yes'");

	  } else {
		  
$first_appt_stmt = mysqli_prepare($mysqli,
          "SELECT
	doctor_locations.doctor_id,
	doctor_locations.doctor_name,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	doctor_new_patients.new_patients,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id,
	doctor_new_patients.appointment_length
FROM
	doctor_locations
INNER JOIN surgery_locations ON doctor_locations.location_id = surgery_locations.surgery_location_id
LEFT OUTER JOIN doctor_new_patients ON doctor_locations.surgery_id = doctor_new_patients.surgery_id
AND doctor_locations.doctor_id = doctor_new_patients.doctor_id
WHERE
	doctor_locations.surgery_id = ?
AND doctor_new_patients.show_all_appointments IS NOT NULL
ORDER BY
	doctor_locations.doctor_id ASC");
	
	  }
	
	mysqli_stmt_bind_param($first_appt_stmt, 'i', $surgery_id);

    mysqli_stmt_execute($first_appt_stmt);
	  
	mysqli_stmt_bind_result($first_appt_stmt, $row->doctor_id, $row->doctor_name, $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, $row->surgery_postcode, $row->surgery_phone, $row->new_patients, $row->show_all_appointments, $row->appointment_book_id, $row->appointment_length);

    while (mysqli_stmt_fetch($first_appt_stmt)) {
		 
		  $new_patients = $row->new_patients;
			
		  $show_all_appointments = $row->show_all_appointments;
			
		  $appointment_book_id = $row->appointment_book_id;
			
		  $appointment_length =  $row->appointment_length;
		  
		  $doctors_array[] = array("doctor_id" => $row->doctor_id, 'new_patients' => $new_patients, 'show_all_appointments' => $show_all_appointments, 'appointment_book_id' => $appointment_book_id, 'appointment_length' => $appointment_length, "doctor_name" => $row->doctor_name, 'surgery_address_1' => $row->surgery_address_1, 'surgery_address_2' => $row->surgery_address_2, 'surgery_suburb_town' => $row->surgery_suburb_town, 'surgery_state' => $row->surgery_state, 'surgery_postcode' => $row->surgery_postcode, 'surgery_phone' => $row->surgery_phone);
		  
      }
	
     mysqli_stmt_free_result($first_appt_stmt);
	 mysqli_close($mysqli);
	  
	  if ($surgery_software == 'BestPractice') {
		  
		 	$user_type = 1;
		
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
		} else {
			
			$internet_patient_id = '';
			
		}
	
		$number_of_doctors = 0;
		$doctor_string_array = array();
		$doctor_book_string_array = array();
		$doctor_length_string_array = array();
		$all_appts_doctor_string_array = array();
		$all_appts_book_string_array = array();
		$all_appts_length_string_array = array();
		$zedmed_doctors_array = array();
		
		for ($i=0;$i<count($doctors_array);$i++) {
			
			$zedmed_doctors_array[] = $doctors_array[$i]['doctor_id'];
			
			if ($doctors_array[$i]['show_all_appointments'] == 'yes') {
				
				$all_appts_doctor_string_array[] = $doctors_array[$i]['doctor_id'];
				$all_appts_book_string_array[] = $doctors_array[$i]['appointment_book_id'];
				$all_appts_length_string_array[] = $doctors_array[$i]['appointment_length'];
				$number_of_doctors ++;
				
			} else {
				
				$doctor_string_array[] = $doctors_array[$i]['doctor_id'];
				$doctor_book_string_array[] = $doctors_array[$i]['appointment_book_id'];
				$doctor_length_string_array[] = $doctors_array[$i]['appointment_length'];

			}
			
		}
		
		$doctor_string_comma_separated = implode(",", $doctor_string_array);
		$doctor_book_string_separated = implode(",", $doctor_book_string_array);
		$doctor_length_string_comma_separated = implode(",", $doctor_length_string_array);
		$all_appts_doctor_string_comma_separated = implode(",", $all_appts_doctor_string_array);
		$all_appts_book_string_comma_separated = implode(",", $all_appts_book_string_array);
		$all_appts_length_string_comma_separated = implode(",", $all_appts_length_string_array);
		$zedmed_doctors_comma_separated = implode("','", $zedmed_doctors_array);
		$zedmed_doctors_comma_separated = "'".$zedmed_doctors_comma_separated."'";
				
				if ($surgery_software == 'BestPractice') {
					
				$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_1.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				
				
				} else if ($surgery_software == 'PracSoft') {
					
					$jsonSendData = '[{"all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_length_string":"'.$all_appts_length_string_comma_separated.'","all_appts_book_string":"'.$all_appts_book_string_comma_separated.'","doctor_book_string":"'.$doctor_book_string_separated.'","patient_string":"","patient_description":"**","appt_day":"'.$appt_day_1.'","doctor_length_string":"'.$doctor_length_string_comma_separated.'","user_type":"'.$user_type.'"}]';
					
				} else if ($surgery_software == 'Zedmed') {
					
					$jsonSendData = '[{"patient_description":"*Internet* ","end_point":"'.$zedmed_end_point_1.'","doctors":"'.$zedmed_doctors_comma_separated.'","start_point":"'.$zedmed_start_point_1.'","appt_day":"'.$zedmed_appt_day_1.'"}]';
				
				}
				
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
			
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		if ($surgery_software == 'BestPractice') {
					
				$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_2.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				
				
				} else if ($surgery_software == 'PracSoft') {
					
					$jsonSendData = '[{"all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_length_string":"'.$all_appts_length_string_comma_separated.'","all_appts_book_string":"'.$all_appts_book_string_comma_separated.'","doctor_book_string":"'.$doctor_book_string_separated.'","patient_string":"","patient_description":"**","appt_day":"'.$appt_day_2.'","doctor_length_string":"'.$doctor_length_string_comma_separated.'","user_type":"'.$user_type.'"}]';
					
				} else if ($surgery_software == 'Zedmed') {
					
					$jsonSendData = '[{"patient_description":"*Internet* ","end_point":"'.$zedmed_end_point_2.'","doctors":"'.$zedmed_doctors_comma_separated.'","start_point":"'.$zedmed_start_point_2.'","appt_day":"'.$zedmed_appt_day_2.'"}]';
				
				}
				
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
			
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response_2 = $r->send()->getBody();
		
		$response_array_2 = json_decode($response_2, true);
		
		for ($i=0;$i<count($response_array_2);$i++) {
			
			$response_array[] = $response_array_2[$i];
			
		}
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
			
		
		}
		
	$new_response_array = array();
	
	for ($i=0;$i<count($response_array);$i++) {
		
		$duplicate = false;
		
		if ($i == 0) {
				
			$new_response_array[] = $response_array[$i];
				
		} else {
			
			for ($n=$i+1;$n<count($response_array);$n++) {
				
				if ($response_array[$n]['Date'] == $response_array[$i]['Date'] && $response_array[$n]['PractitionerID'] == $response_array[$i]['PractitionerID']) {
					$duplicate = true;
                 }
				
			}
			
			if(!$duplicate) {
				
				$new_response_array[] = $response_array[$i];
				
			}
			
		}
		
	}

	$response_array = $new_response_array;

			
	$count_first_appointments = 0;
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

     for ($i=0;$i<count($response_array);$i++) {
		  
		 if ($response_array[$i]['ApptID'] != '') {
			 
			 $now = time();
			 
			 $date = strtotime($response_array[$i]['When']);
			 
			if (substr($response_array[$i]['When'], 0, 11) != 'Unavailable' && substr($response_array[$i]['Descrip'], 0, 11) != 'Unavailable' && $count_first_appointments < 20 && $date > $now) {
			 
			 for ($n=0;$n<count($doctors_array);$n++) {
				 
				 if ($doctors_array[$n]['doctor_id'] == $response_array[$i]['PractitionerID']) {
					 
					 $doctor_name = str_replace('"',"'",$doctors_array[$n]['doctor_name']);
					 
					 $new_patients = $doctors_array[$n]['new_patients'];
					 
					 $doc_location = $surgery_name;
				  
				  $doc_location .= '
'.$doctors_array[$n]['surgery_address_1'];
				  
				  if (strlen($doctors_array[$n]['surgery_address_2']) > 2) {
					  
					  $doc_location .= '
'.$doctors_array[$i]['surgery_address_2'];
					  
				  }
				  
				  $doc_location .= '
'.$doctors_array[$n]['surgery_suburb_town'].' '.$doctors_array[$n]['surgery_state'].' '.$doctors_array[$n]['surgery_postcode'].' Phone: '.$doctors_array[$n]['surgery_phone'];

$appInfo = '';
			  
			 $ApptDate_array = explode(" ", $response_array[$i]['When']);
			 
			 $date = strtotime($response_array[$i]['When']);
	
			 $appTime = date("D j M Y g:i A",$date);
			 
			 if ($surgery_software == 'Practice 2000') {
					  
				$apptPos = $response_array[$i]['Pos'];
					  
			  } else {
					  
				$apptPos = '0'; 
				
			  }
			 
			  $plist_string .= '
		<dict>
			<key>JSON</key>
			<string><![CDATA['.$jsonSendData.']]></string>
			<key>PractitionerID</key>
			<string><![CDATA['.$response_array[$i]['PractitionerID'].']]></string>
			<key>DoctorNewPatients</key>
			<string><![CDATA['.$new_patients.']]></string>
			<key>PractitionerName</key>
			<string><![CDATA['.$doctor_name.']]></string>
			<key>DoctorLocation</key>
			<string><![CDATA['.$doc_location.']]></string>
			<key>PatientID</key>
			<string><![CDATA['.$response_array[$i]['PatientID'].']]></string>
			<key>ApptDate</key>
			<string><![CDATA['.$ApptDate_array[0].']]></string>
			<key>ApptDescription</key>
			<string><![CDATA['.$appTime.']]></string>
			<key>ApptInfo</key>
			<string><![CDATA['.$appInfo.']]></string>
			<key>ApptType</key>
			<string><![CDATA['.$appoitment_type.']]></string>
			<key>ApptPos</key>
			<string><![CDATA['.$apptPos.']]></string>
			<key>AppLength</key>
			<string><![CDATA['.$response_array[$i]['Length'].']]></string>
			<key>AppSecs</key>
			<string><![CDATA['.$response_array[$i]['AppSecs'].']]></string>
			<key>ApptID</key>
			<string><![CDATA['.$response_array[$i]['ApptID'].']]></string>
		</dict>';
					 
					  $count_first_appointments ++;
					  
				 }
			 	}
			}
		 }
	
      }

	$plist_string .= '
</array>
</plist>';

	echo $plist_string;
	

?>



