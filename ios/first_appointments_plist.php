<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$user_type = $_POST['user_type'];

$fromDate1 = strtotime('now');
$fromDate2 = strtotime('+1 day');
$fromDate3 = strtotime('+2 days');
$fromDate4 = strtotime('+3 days');
$fromDate5 = strtotime('+4 days');
$fromDate6 = strtotime('+5 days');

$appt_day_1 = date("Y-m-d",$fromDate1);
$appt_day_2 = date("Y-m-d",$fromDate2);
$appt_day_3 = date("Y-m-d",$fromDate3);
$appt_day_4 = date("Y-m-d",$fromDate4);
$appt_day_5 = date("Y-m-d",$fromDate5);
$appt_day_6 = date("Y-m-d",$fromDate6);

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$first_appt_stmt = mysqli_prepare($mysqli,
          "SELECT
	doctor_locations.doctor_id,
	doctor_locations.doctor_name,
	surgery_locations.surgery_address_1,
	surgery_locations.surgery_address_2,
	surgery_locations.surgery_suburb_town,
	surgery_locations.surgery_state,
	surgery_locations.surgery_postcode,
	surgery_locations.surgery_phone,
	doctor_new_patients.new_patients,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id
FROM
	doctor_locations
INNER JOIN surgery_locations ON doctor_locations.location_id = surgery_locations.surgery_location_id
LEFT OUTER JOIN doctor_new_patients ON doctor_locations.surgery_id = doctor_new_patients.surgery_id
AND doctor_locations.doctor_id = doctor_new_patients.doctor_id
WHERE
	doctor_locations.surgery_id = ?
ORDER BY
	doctor_locations.doctor_id ASC");
	
	mysqli_stmt_bind_param($first_appt_stmt, 'i', $surgery_id);

    mysqli_stmt_execute($first_appt_stmt);
	  
	mysqli_stmt_bind_result($first_appt_stmt, $row->doctor_id, $row->doctor_name, $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, $row->surgery_postcode, $row->surgery_phone, $row->new_patients, $row->show_all_appointments, $row->appointment_book_id);

    while (mysqli_stmt_fetch($first_appt_stmt)) {
		 
		 if ($row->new_patients) {
			 
			  $new_patients = $row->new_patients;
			  
		  } else {
			  
			  $new_patients = 'yes';
		  }
		  
		  if ($row->show_all_appointments) {
			 
			  $show_all_appointments = $row->show_all_appointments;
			  
		  } else {
			  
			  $show_all_appointments = 'no';
		  }
		  
		  if ($row->appointment_book_id) {
			 
			  $appointment_book_id = $row->appointment_book_id;
			  
		  } else {
			  
			  $appointment_book_id = 0;
		  }
		  
		  $doctors_array[] = array("doctor_id" => $row->doctor_id, 'new_patients' => $new_patients, 'show_all_appointments' => 'n', 'appointment_book_id' => $appointment_book_id, "doctor_name" => $row->doctor_name, 'surgery_address_1' => $row->surgery_address_1, 'surgery_address_2' => $row->surgery_address_2, 'surgery_suburb_town' => $row->surgery_suburb_town, 'surgery_state' => $row->surgery_state, 'surgery_postcode' => $row->surgery_postcode, 'surgery_phone' => $row->surgery_phone);
      }
	
      mysqli_stmt_free_result($first_appt_stmt);
	 
$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);
	  
	  if ($surgery_software == 'BestPractice') {
		  
		 	$user_type = 1;
		
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
		} else {
			
			$internet_patient_id = '';
			
		}
	
		$number_of_doctors = 0;
		$doctor_string_array = array();
		$all_appts_doctor_string_array = array();
		
		for ($i=0;$i<count($doctors_array);$i++) {
			
			if ($doctors_array[$i]['show_all_appointments'] == 'yes') {
				
				$all_appts_doctor_string_array[] = $doctors_array[$i]['doctor_id'];
				$number_of_doctors ++;
				
			} else {
				
				$doctor_string_array[] = $doctors_array[$i]['doctor_id'];

			}
			
		}
		
		
		$doctor_string_comma_separated = implode(",", $doctor_string_array);
		$all_appts_doctor_string_comma_separated = implode(",", $all_appts_doctor_string_array);
		
			
			if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
				
				$appt_date = $item['appt_day'];

				$patient_string = $item['patient_id'];
		
				$doctor_string = $item['doctor_string'];
		
				$all_appts_doctor_string = $item['all_appts_doctor_string'];

				$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_1.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
				
			} else {
		
				$jsonSendData = '[{"appt_day":"'.$appt_day_1.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
				$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
			
			}
			
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
			if (count($response_array) == 0) {
				
				if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
				
					$appt_date = $item['appt_day'];
	
					$patient_string = $item['patient_id'];
			
					$doctor_string = $item['doctor_string'];
			
					$all_appts_doctor_string = $item['all_appts_doctor_string'];
	
					$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_2.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
					
				} else {
			
					$jsonSendData = '[{"appt_day":"'.$appt_day_2.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
					$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
				
				}
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
				if (count($response_array) == 0) {
				
					if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
					
						$appt_date = $item['appt_day'];
		
						$patient_string = $item['patient_id'];
				
						$doctor_string = $item['doctor_string'];
				
						$all_appts_doctor_string = $item['all_appts_doctor_string'];
		
						$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_3.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
						
					} else {
				
						$jsonSendData = '[{"appt_day":"'.$appt_day_3.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
						$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
					
					}
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
		
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
				if (count($response_array) == 0) {
				
					if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
					
						$appt_date = $item['appt_day'];
		
						$patient_string = $item['patient_id'];
				
						$doctor_string = $item['doctor_string'];
				
						$all_appts_doctor_string = $item['all_appts_doctor_string'];
		
						$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_4.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
						
					} else {
				
						$jsonSendData = '[{"appt_day":"'.$appt_day_4.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
						$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
					
					}
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
			
					$response = $r->send()->getBody();
					
					$response_array = json_decode($response, true);
					
					if (count($response_array) == 0) {
				
						if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
						
							$appt_date = $item['appt_day'];
			
							$patient_string = $item['patient_id'];
					
							$doctor_string = $item['doctor_string'];
					
							$all_appts_doctor_string = $item['all_appts_doctor_string'];
			
							$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_5.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
							
						} else {
					
							$jsonSendData = '[{"appt_day":"'.$appt_day_5.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
							$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
						
						}
						
						$r = new HttpRequest($server_url, HttpRequest::METH_POST);
						$r->addPostFields($post_array);
				
						$response = $r->send()->getBody();
						
						$response_array = json_decode($response, true);
						
						if (count($response_array) == 0) {
				
							if ($surgery_id == '7' || $surgery_id == '13' || $surgery_id == '19' || $surgery_id == '22'  || $surgery_id == '26' || $surgery_id == '32' || $surgery_id == '36'  || $surgery_id == '41'  || $surgery_id == '42' ) {
							
								$appt_date = $item['appt_day'];
				
								$patient_string = $item['patient_id'];
						
								$doctor_string = $item['doctor_string'];
						
								$all_appts_doctor_string = $item['all_appts_doctor_string'];
				
								$jsonSendData = '[{"patient_id":"'.$internet_patient_id.'","patient_description":"**","patient_string":"","appt_day":"'.$appt_day_5.'","doctor_string":"'.$doctor_string_comma_separated.'","all_appts_doctor_string":"'.$all_appts_doctor_string_comma_separated.'"}]';
				$post_array = array('getAllDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
								
							} else {
						
								$jsonSendData = '[{"appt_day":"'.$appt_day_6.'","patient_description":"*xxxxxxxxx*","user_type":"patient","patient_id":"'.$internet_patient_id.'","appointment_description":"Internet"}]';
								$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);;
							
							}
							
							$r = new HttpRequest($server_url, HttpRequest::METH_POST);
							$r->addPostFields($post_array);
					
							$response = $r->send()->getBody();
							
							$response_array = json_decode($response, true);
							
						}
				
					}
				
				}
				
			}
			
		}
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
		
		}
		
	$count_first_appointments = 0;
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

     for ($i=0;$i<count($response_array);$i++) {
		  
		 if ($response_array[$i]['ApptID'] != '') {
			 
			 $now = time();
			 
			 $date = strtotime($response_array[$i]['When']);
			 
			if ($response_array[$i]['When'] != 'Unavailable' && $count_first_appointments < 20 && $date > $now) {
			 
			 for ($n=0;$n<count($doctors_array);$n++) {
				 
				 if ($doctors_array[$n]['doctor_id'] == $response_array[$i]['PractitionerID']) {
					 
					 $doctor_name = str_replace('"',"'",$doctors_array[$n]['doctor_name']);
					 
					 $new_patients = $doctors_array[$n]['new_patients'];
					 
					 $doc_location = $surgery_name;
				  
				  $doc_location .= '
'.$doctors_array[$n]['surgery_address_1'];
				  
				  if (strlen($doctors_array[$n]['surgery_address_2']) > 2) {
					  
					  $doc_location .= '
'.$doctors_array[$i]['surgery_address_2'];
					  
				  }
				  
				  $doc_location .= '
'.$doctors_array[$n]['surgery_suburb_town'].' '.$doctors_array[$n]['surgery_state'].' '.$doctors_array[$n]['surgery_postcode'].' Phone: '.$doctors_array[$n]['surgery_phone'];

$appInfo = '';
			  
			 $ApptDate_array = explode(" ", $response_array[$i]['When']);
			 
			 $date = strtotime($response_array[$i]['When']);
	
			 $appTime = date("D j M Y g:i A",$date);
			 
			  $plist_string .= '
		<dict>
			<key>JSON</key>
			<string><![CDATA['.$jsonSendData.']]></string>
			<key>PractitionerID</key>
			<string><![CDATA['.$response_array[$i]['PractitionerID'].']]></string>
			<key>DoctorNewPatients</key>
			<string><![CDATA['.$new_patients.']]></string>
			<key>PractitionerName</key>
			<string><![CDATA['.$doctor_name.']]></string>
			<key>DoctorLocation</key>
			<string><![CDATA['.$doc_location.']]></string>
			<key>PatientID</key>
			<string><![CDATA['.$response_array[$i]['PatientID'].']]></string>
			<key>ApptDate</key>
			<string><![CDATA['.$ApptDate_array[0].']]></string>
			<key>ApptDescription</key>
			<string><![CDATA['.$appTime.']]></string>
			<key>ApptInfo</key>
			<string><![CDATA['.$appInfo.']]></string>
			<key>ApptType</key>
			<string><![CDATA['.$appoitment_type.']]></string>
			<key>AppLength</key>
			<string><![CDATA['.$response_array[$i]['Length'].']]></string>
			<key>AppSecs</key>
			<string><![CDATA['.$response_array[$i]['AppSecs'].']]></string>
			<key>ApptID</key>
			<string><![CDATA['.$response_array[$i]['ApptID'].']]></string>
		</dict>';
					 
					  $count_first_appointments ++;
					  
				 }
			 	}
			}
		 }
	
      }

	$plist_string .= '
</array>
</plist>';

	echo $plist_string;
	

?>



