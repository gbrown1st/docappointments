<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = '88';//$_POST['surgery_id'];
$company = '';//$_POST['company'];
$first_name = 'Graham';//$_POST['first_name'];
$last_name = 'Brown';//$_POST['last_name'];
$user_type = 'patient';//$_POST['user_type'];


$dob = '1959-02-03';//$_POST['dob'];
$email = 'graham@mediarare.com.au';//$_POST['email'];
$mobile = '0448992599';//$_POST['mobile'];
$username = 'gb';//$_POST['username'];
$password = 'gb';//$_POST['password'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);
	
	$stat_surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT 
stat_doctors.surgery_id
FROM stat_doctors");
	
      mysqli_stmt_execute($stat_surgeries_stmt);

      mysqli_stmt_bind_result($stat_surgeries_stmt, $row->surgery_id);
	  
	  $stat_surgery_array = array();

      while (mysqli_stmt_fetch($stat_surgeries_stmt)) {
		 
		  $stat_surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($stat_surgeries_stmt);  
	  
$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

    mysqli_stmt_free_result($stmt);
	
		if ($surgery_software == 'BestPractice') {
		 	 
			$user_type = 1;
			
		}
			
	if (in_array($surgery_id, $surgery_array) || in_array($surgery_id, $stat_surgery_array)) {
		
		echo 'Line 119
		
		';
		 
		 $login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and user_password = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($login_stmt);
		 
		 mysqli_stmt_bind_result($login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($login_stmt);

		 if (mysqli_stmt_num_rows($login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			
			echo 'Line 140 user_id = '.$user_id.'
		
		';
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
			
			 mysqli_stmt_free_result($login_stmt);
			
		 } else {
			 
			 $company_name = '';
			 
			  $add_user_stmt = mysqli_prepare($mysqli, "INSERT INTO users (surgery_id, user_type, first_name, last_name, dob, email, mobile, user_username, user_password, created_date) VALUES (?,?,?,?,?,?,?,?,?,?)");
			  
			  mysqli_stmt_bind_param($add_user_stmt, 'isssssssss', $surgery_id, $user_type, $first_name, $last_name, $dob, $email, $mobile, $username, $password, $now_date_time);
			  
			  if (mysqli_stmt_execute($add_user_stmt)) {
				  
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
				  
			  } else {
				  
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Failed!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has failed.
			
Please try again.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
				   
			  }
			  
			  mysqli_stmt_free_result($add_user_stmt);
			  
		 }
		 
	 } else {
      
	$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
	$r = new HttpRequest($server_url, HttpRequest::METH_POST);
	$r->addPostFields($post_array);
	
	$response = $r->send()->getBody();
	
	$output = array();
	
	$response_array = json_decode($response, true);
	
	if ($response == 'Unable to connect to database') {
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[No Response!]]></string>
						<key>message</key>
						<string><![CDATA['.$surgery_name.' is not responding.
						
						Please try again or try another surgery.]]></string>
					</dict>
				</array>
			</plist>';

	} else {
	
		if (count($response_array[0]) > 0) {
			
			if ($surgery_software == 'BestPractice') {
		 
				 $user_id = $response_array[0]['INTERNALID'];
			
			 } else {
				 
				 $user_id = $response_array[0]['user_id'];
				 
			 }
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
				<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
				<plist version="1.0">
					<array>
						<dict>
							<key>heading</key>
							<string><![CDATA[Registration Successful!]]></string>
							<key>message</key>
							<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
				
				You can now make appointments with '.$surgery_name.'.]]></string>
							<key>patient_id</key>
							<string><![CDATA['.$response_array[0]['INTERNALID'].']]></string>
						</dict>
					</array>
				</plist>';
			
			 $stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "iPhone";
			$log_action = "Register/Log In";
			$log_action_comment = $response.' - original dob: '.$dob;
			
			mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($stmt);
		
			mysqli_stmt_free_result($stmt);
			
			mysqli_close($mysqli);
		
		} else {
			
			if (!in_array($surgery_id, $surgery_array)) {
				
				if ($surgery_software == 'BestPractice') {
			
					$jsonSendData= '[{"mobile":"'.$mobile.'","email":"'.$email.'","username":"'.$username.'","password":"'.$password.'","last_name":"'.$last_name.'","company_name":"","first_name":"'.$first_name.'","user_type":1,"dob":"'.$dob.'"}]';
				
					$post_array = array('addUser' => 'true', 'jsonSendData' => $jsonSendData);
				
				} else {
					
					$jsonSendData='[{"username":"'.$username.'","email":"'.$email.'","dob":"'.$dob.'","user_type":"patient","first_name":"'.$first_name.'", "town_suburb":"","company_name":"'.$company.'","mobile":"'.$mobile.'","middle_name":"","password":"'.$password.'","last_name":"'.$last_name.'","family_members":",,,*,,,*,,,*,,,*,,,*,,,","middle_name":"","number_street":"","phone":"'.$mobile.'","postcode":"","state":""}]';
					
					$post_array = array('addUser' => 'true', 'jsonSendData' => $jsonSendData);
					
				}
				
				$r = new HttpRequest($server_url, HttpRequest::METH_POST);
				$r->addPostFields($post_array);
				
				$response = $r->send()->getBody();
				
				$response_array = json_decode($response, true);
				
				if (count($response_array[0]) > 0) {
				
					$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
					<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
					<plist version="1.0">
						<array>
							<dict>
								<key>JSON</key>
								<string><![CDATA['.$jsonSendData.']]></string>
								<key>heading</key>
								<string><![CDATA['.$response_array[0]['heading'].']]></string>
								<key>message</key>
								<string><![CDATA['.$response_array[0]['result_string'].']]></string>
							</dict>
						</array>
					</plist>';
				
				}
			
			}
			
			 $stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "iPhone";
			$log_action = "Register/Log In";
			$log_action_comment = $response;
			
			mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($stmt);
		
			mysqli_stmt_free_result($stmt);
			
			mysqli_close($mysqli);
			
		}
	
	}
	
}

	echo $plist_string;

?>



