<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_GET['surgery_id'];

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	sponsors.sponsor_id,
	sponsors.surgery_id,
	sponsors.sponsor_name,
	sponsors.sponsor_address,
	sponsors.sponsor_phone,
	sponsors.sponsor_logo_url,
	sponsors.sponsor_text
FROM
	sponsors
WHERE
	sponsors.surgery_id = 13
ORDER BY
	sponsors.sponsor_name ASC");


      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->sponsor_id, $row->surgery_id, 
							  $row->sponsor_name, $row->sponsor_address, $row->sponsor_phone, $row->sponsor_logo_url, 
							  $row->sponsor_text);

	$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

      while (mysqli_stmt_fetch($stmt)) {
		  
		  $plist_string .= '
	<dict>
		<key>sponsor_id</key>
		<string><![CDATA['.$row->sponsor_id.']]></string>
		<key>surgery_id</key>
		<string><![CDATA['.$row->surgery_id.']]></string>
		<key>sponsor_name</key>
		<string><![CDATA['.$row->sponsor_name.']]></string>
		<key>sponsor_address</key>
		<string><![CDATA['.$row->sponsor_address.']]></string>
		<key>sponsor_phone</key>
		<string><![CDATA['.$row->sponsor_phone.']]></string>
		<key>sponsor_logo_url</key>
		<string><![CDATA['.$row->sponsor_logo_url.']]></string>
		<key>sponsor_text</key>
		<string><![CDATA['.$row->sponsor_text.']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
echo $plist_string;

?>



