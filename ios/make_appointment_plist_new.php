<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$appt_patient_id = $_POST['appt_patient_id'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$medicareno = $_POST['medicareno'];
$medicarelineno = $_POST['medicarelineno'];
$patient_name = $_POST['first_name'].' '.$_POST['last_name'];
$company_name = $_POST['company_name'];
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];
$dob_array = explode('-', $dob);
$display_dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$appt_secs = $_POST['appt_secs'];
$appt_date = $_POST['appt_day'];
$appt_date = str_replace('  0000', '', $appt_date);
$appt_date .='.000';
$appt_date_time = trim($_POST['appt_date_time']);
$appt_day_array = explode(' ', $appt_date_time);
$appt_day = $appt_day_array[0];
$appt_id = $_POST['appt_id'];
$appt_time = $_POST['appt_time'];
$doctor_name = $_POST['doctor_name'];
$addUser = $_POST['addUser'];


//[{"user_type":"1","delete_from_day":"2012-10-25","appointment_description":"Internet","patient_email":"graham@mediarare.com.au","doctor_name":"Dr Graham Brown","appt_day":"2012-10-25","doctor_id":3,"appt_secs":28800,"patient_name":"Graham Brown","appt_time":"Thu 25 Oct 2012 8:00 AM","user_string":"Graham Brown [DOB: 03/02/1959] [Ph: 0448992599]","patient_id":,"patient_description":"","internet_patient_id":2,"appt_id":-1}]


if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	$user_string = $first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
	
} else {
	
	$appoitment_type = 'Internetwc';
	$user_string = $company_name.' - '.$first_name.' '.$last_name.' [DOB: '.$display_dob.'] [Ph: '.$mobile.']';
	$patient_description = $user_string;
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt1 = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt1, 'i', $surgery_id);

      mysqli_stmt_execute($stmt1);

      mysqli_stmt_bind_result($stmt1, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt1)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

	  $stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.doctor_code,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id
FROM
	doctor_new_patients
	
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);

      mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id);

      while (mysqli_stmt_fetch($stmt2)) {
		 
		  $doctor_code = $row->doctor_code;
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  
      }
	  
	  $user_id = 0;
	
	  $patient_details_array = array();
	
	  if ($surgery_software == 'BestPractice') {
		  
		$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		$head_of_family = $response_array[0]['INTERNALID'];
		
		$user_id = $response_array[0]['INTERNALID'];

		$patient_details_array[] = array("patient_id" => $response_array[0]['INTERNALID'], "first_name" => $response_array[0]['FIRSTNAME'], "last_name" => $response_array[0]['SURNAME'], "dob" => $response_array[0]['DOB'], "mobile" => $response_array[0]['MOBILE']);
		
		for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					$nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					$nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					$patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					$patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					$patient_id = $patient_details_array[$n]['patient_id'];
					 
				 }
					 
			}
	
		if ($addUser == 'yes') {
			
			$getFamilyMembers= $_POST['getFamilyMembers'];
			$jsonSendData= '[{"HEADOFFAMILYID":'.$head_of_family.'}]';
			$post_array = array('getFamilyMembers' => 'true', 'jsonSendData' => $jsonSendData);
	
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
			
			$response = $r->send()->getBody();
			$response_array = json_decode($response, true);
			
			for ($i=0;$i<count($response_array);$i++) {
			
				$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILE']);
			
			}
		
			for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
					 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
					 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
					 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
					 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
					 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 } else {
					 
					 $jsonSendData = '[{"email":"'.$email.'","postcode":"","mobile":"'.$mobile.'","last_name":"'.$last_name.'","number_street":"","HEADOFFAMILYID":'.$response_array[0]['INTERNALID'].',"town_suburb":"","dob":"'.$display_dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'","phone":"'.$mobile.'"}]';
			
					$addFamilyJsonSendData = $jsonSendData;
					
					$post_array = array('addFamilyMember' => 'true', 'jsonSendData' => $jsonSendData);
				
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
					
					$response = $r->send()->getBody();
					$response_array = json_decode($response, true);
					
					for ($i=0;$i<count($response_array);$i++) {
			
						$patient_details_array[] = array("patient_id" => $response_array[$i]['INTERNALID'], "first_name" => $response_array[$i]['FIRSTNAME'], "last_name" => $response_array[$i]['SURNAME'], "dob" => $response_array[$i]['DOB'], "mobile" => $response_array[$i]['MOBILE']);
			
					}
		
					for ($n=0;$n<count($patient_details_array);$n++) {
				 
				 		if ($patient_details_array[$n]['first_name'] == $first_name && $patient_details_array[$n]['last_name'] == $last_name && $patient_details_array[$n]['dob'] == $dob) {
					 
							 $nice_dob_array = explode('-', $patient_details_array[$n]['dob']);
							 $nice_dob = $nice_dob_array[2].'/'.$nice_dob_array[1].'/'.$nice_dob_array[0];
							 $patient_description = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'].' [DOB: '.$nice_dob.'] [Ph: '.$patient_details_array[$n]['mobile'].']';
							 $patient_name = $patient_details_array[$n]['first_name'].' '.$patient_details_array[$n]['last_name'];
							 $patient_id = $patient_details_array[$n]['patient_id'];
					 
				 		}
					 
				 	}
				 
				}
			
			}
		
		}
		

			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
			if ((int) $internet_patient_id == (int) $appt_patient_id) {
				
				$isInternetPatient = 'yes';
				
			} else {
				
				$isInternetPatient = 'no';
				
			}
			
	  }
	  
	$available = false;
	
	if ($show_all_appointments == 'no') {
	
		if ($surgery_software == 'BestPractice') {
			
			$jsonSendData = '[{"patient_description":"*xxxxxxxxx*","appointment_description":"Internet","patient_id":'.$internet_patient_id.',"user_type":"'.$user_type.'","appt_day":"'.$appt_day.'"}]';
			
			//[{"patient_description":"*xxxxxxxxx*","appointment_description":"Internet","user_type":"patient","patient_id":"70570","appt_day":"2012-10-29"}]
			
		} else {
			
			$jsonSendData = '[{"appointment_description":"Internet","appt_day":"'.$appt_day.'","patient_description":"*xxxxxxxxx*","user_type":"'.$user_type.'"}]';
			
		}
		
			$checkApp = $jsonSendData;
			
			$post_array = array('getDayAppointments' => 'true', 'jsonSendData' => $jsonSendData);
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$check_response = $response;
	
			 for ($i=0;$i<count($response_array);$i++) {
				 
				 if ((int) $response_array[$i]['ApptID'] == (int) $appt_id) {
						 
						 $available = true;
						 
				}
				 
			 }
		 
	} else {
		
		if ($surgery_software == 'BestPractice') {
		
			$jsonSendData = '[{"appt_day":"'.$appt_day.'","appt_id":'.$appt_id.',"doctor_id":'.$doctor_id.',"appt_time":'.$appt_secs.',"patient_id":'.$appt_patient_id.',"isInternetPatient":"'.$isInternetPatient.'"}]';
			
		} else {
			
			$jsonSendData = '[{"appt_date":"'.$appt_date_time.'","doctor_id":'.$doctor_id.'}]';
			
		}
		
		$checkApp = $jsonSendData;
		
		$post_array = array('checkAppointmentAvailability' => 'true', 'jsonSendData' => $jsonSendData);
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
	
		 for ($i=0;$i<count($response_array);$i++) {
			
			 if ($response_array[$i]['is_available'] == 1) {
					 
					 $available = true;
					 
			}
			 
		 }
	
	}
	
		if ($available) {
			
			if ($surgery_software == 'BestPractice') {
				 
				$user_type = 1;
				
				// $jsonSendData = '[{"user_type":"'.$user_type.'","delete_from_day":"'.$now_date_time.'","appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","doctor_id":'.$doctor_id.',"appt_secs":'.$appt_secs.',"patient_name":"'.$patient_name.'","appt_time":"'.$appt_time.'","user_string":"'.$user_string.'","patient_id":'.$patient_id.',"patient_description":"'.$patient_description.'","internet_patient_id":'.$internet_patient_id.',"appt_id":'.$appt_id.'}]';
			
				 
				  $jsonSendData = '[{"appt_id":'.$appt_id.',"user_type":"patient","patient_name":"'.$patient_name.'","appt_secs":'.$appt_secs.',"appt_time":"'.$appt_time.'","patient_id":'.$patient_id.',"appt_day":"'.$appt_day.'","patient_email":"'.$email.'","appointment_description":"Internet","internet_patient_id":'.$internet_patient_id.',"patient_description":"'.$user_string.'","show_all_appointments":"'.$show_all_appointments.'","user_string":"'.$user_string.'","doctor_name":"'.$doctor_name.'","delete_from_day":"'.$now_date_time.'","doctor_id":'.$doctor_id.'}]';
		
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
			
			 } else {
				 
				 if ($show_all_appointments == 'yes') {
					 
					 $jsonSendData = '[{"doctor_id":'.$doctor_id.',"user_string":"'.$user_string.'","doctor_code":"'.$doctor_code.'","appt_time":"'.$appt_time.'","patient_description":"'.$user_string.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","delete_from_day":"'.$now_date_time.'","appt_id":'.$appt_id.',"appointment_book_id":'.$appointment_book_id.',"patient_email":"'.$email.'","appt_day":"'.$appt_day.'","patient_name":"'.$patient_name.'","user_type":"'.$user_type.'","appointment_description":"","appt_date":"'.$appt_date_time.'"}]';
					
				 $post_array = array('makeNewAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 } else {
					 
					 $jsonSendData = '[{"appt_time":"'.$appt_time.'","user_type":"'.$user_type.'","doctor_name":"'.$doctor_name.'","appt_day":"'.$appt_day.'","delete_from_day":"'.$now_date_time.'","patient_name":"'.$patient_name.'","user_string":"'.$user_string.'","doctor_id":'.$doctor_id.',"appointment_description":"'.$appoitment_type.'","patient_email":"'.$email.'","patient_description":"'.$user_string.'","appt_id":'.$appt_id.'}]';
					
				 $post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 }
				 
			 }

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		 $stmt3 = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date,
			json,
			json_response) 
		VALUES (?,?,?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time; Details: $patient_name [DOB: $display_dob] [Ph: $mobile]";
	
	mysqli_bind_param($stmt3, 'iissssss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time, $jsonSendData, $response);

	mysqli_stmt_execute($stmt3);

	mysqli_stmt_free_result($stmt1);
	mysqli_stmt_free_result($stmt2);
	mysqli_stmt_free_result($stmt3);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		 
		  $plist_string .= '
	<dict>
		<key>CheckApp</key>
		<string><![CDATA['.$checkApp.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$response_array[0]['result_string'].']]></string>
		<key>sql</key>
		<string><![CDATA['.$response_array[0]['extra_data'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

		} else {
			
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$checkApp.']]></string>
		<key>JSON</key>
		<string><![CDATA['.$check_response.']]></string>
		<key>heading</key>
		<string><![CDATA[Appointment Unavailable]]></string>
		<key>message</key>
		<string><![CDATA[This appointment is no longer available.
		
Please choose another.]]></string>
	</dict>
</array>
</plist>';
			
		}
	
	echo $plist_string;

?>