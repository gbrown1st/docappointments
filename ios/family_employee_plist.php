<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$company = $_POST['company'];
//$company = str_replace("'", "%27", $company);
//$company = str_replace("'", "%5", $company);
$first_name = $_POST['first_name'];
//$first_name = str_replace("'", "%27", $first_name);
//$first_name = str_replace("'", "%5", $first_name);
$last_name = $_POST['last_name'];
//$last_name = str_replace("'", "%27", $last_name);
//$last_name = str_replace("'", "%5", $last_name);
$user_type = $_POST['user_type'];
$dob = $_POST['dob'];
//$dob = str_replace("-", "%2D", $dob);
$email = $_POST['email'];
//$email = str_replace("@", "%40", $email);
//$email = str_replace(".", "%2E", $email);
//$email = str_replace("'", "%27", $email);
//$email = str_replace("'", "%5", $email);
$mobile = $_POST['mobile'];
$username = $_POST['username'];
//$username = str_replace("-", "%2D", $username);
//$username = str_replace("'", "%27", $username);
//$username = str_replace("'", "%5", $username);
$password = $_POST['password'];
//$password = str_replace("-", "%2D", $password);
//$password = str_replace("'", "%27", $password);
//$password = str_replace("'", "%5", $password);
/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);
		

	$post_array = array('login' => 'true', 'username' => $username, 'password' => $password);
	
	$r = new HttpRequest($server_url, HttpRequest::METH_POST);
	$r->addPostFields($post_array);
	
	$response = $r->send()->getBody();
	
	$response_array = json_decode($response, true);
	
	if ($response == 'Unable to connect to database') {
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
		<array>
			<dict>
				<key>heading</key>
				<string><![CDATA[No Response!]]></string>
				<key>message</key>
				<string><![CDATA['.$row->surgery_name.' is not responding.
				
				Please try again or try another surgery.]]></string>
			</dict>
		</array>
	</plist>';
	
	} else {

	if (count($response_array[0]) > 0) {
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
		<array>
			<dict>
				<key>heading</key>
				<string><![CDATA[Registration Successful!]]></string>
				<key>message</key>
				<string><![CDATA[Your registration with '.$row->surgery_name.' has been successful.
	
	You can now make appointments with '.$row->surgery_name.'.]]></string>
				<key>patient_id</key>
				<string><![CDATA['.$response_array[0]['INTERNALID'].']]></string>
			</dict>
		</array>
	</plist>';
		
	} else {
		
		if ($surgery_software == 'BestPractice') {
			
			if ($user_type == 'patient') {
			 
			 	$user_type = 1;
			 
			 } else {
				 
				 $user_type = 2;
				 
			 }
		}
		
		$addUser= $_POST['addUser'];
		$jsonSendData='[{"username":"'.$username.'","email":"'.$email.'","dob":"'.$dob.'","user_type":"'.$user_type.'","first_name":"'.$first_name.'", "town_suburb":"","company_name":"'.$company.'","mobile":"'.$mobile.'","middle_name":"","password":"'.$password.'","last_name":"'.$last_name.'","family_members":",,,*,,,*,,,*,,,*,,,*,,,","middle_name":"","number_street":"","phone":"'.$mobile.'","postcode":"","state":""}]';
		
		$post_array = array('addUser' => $addUser, 'jsonSendData' => $jsonSendData);
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		if (count($response_array[0]) > 0) {
		
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
		<array>
			<dict>
				<key>JSON</key>
				<string><![CDATA['.$jsonSendData.']]></string>
				<key>heading</key>
				<string><![CDATA['.$response_array[0]['heading'].']]></string>
				<key>message</key>
				<string><![CDATA['.$response_array[0]['result_string'].']]></string>
			</dict>
		</array>
	</plist>';
	
		}

	}
	
	
	}
	echo $plist_string;

?>



