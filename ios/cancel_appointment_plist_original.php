<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$doctor_name = $_POST['doctor_name'];
$patient_id = $_POST['patient_id'];
$user_type = $_POST['user_type'];
$email = $_POST['email'];
$appt_length = $_POST['appt_length'];
$appt_secs = $_POST['appt_secs'];
$appt_day_array = explode(' ', $_POST['appt_day']);
$appt_day = $appt_day_array[0];
$appt_id = $_POST['appt_id'];
$appt_time = $_POST['appt_time'];
$appt_info = $_POST['appt_info'];

if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	
} else {
	
	$appoitment_type = 'Internetwc';
	
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

      mysqli_stmt_free_result($stmt);
 
	  $patient_details_array = array();
	
	  if ($surgery_software == 'BestPractice') {
		  
		if ($user_type == 'employer') {
			
			$post_array = array('getInternetPatientIDEmployer' => 'true');
			
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$patient_array[] = $response_array[0]['INTERNALID'];
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
		} else {
			
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
			
		}
	
		$jsonSendData = '[{"user_string":"'.$appt_info.'","appointment_description":"'.$appoitment_type.'","patient_id":'.$patient_id.',"internet_patient_id":'.$internet_patient_id.',"appt_secs":'.$appt_secs.',"patient_email":"'.$email.'","doctor_name":"'.$doctor_name.'","appt_time":"'.$appt_time.'","user_type":"'.$user_type.'","patient_name":"'.$appt_info.'","appt_id":'.$appt_id.',"patient_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","delete_from_day":"'.$now_date_time.'","doctor_id":'.$doctor_id.'}]';
	
		$post_array = array('cancelAppointment' => 'true', 'jsonSendData' => $jsonSendData);

	  } else {
		
		$jsonSendData = '[{"user_type":"'.$user_type.'","appointment_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","doctor_id":'.$doctor_id.',"patient_name":"'.$appt_info.'","user_string":"'.$appt_info.'","doctor_name":"'.$doctor_name.'","patient_email":"'.$email.'","patient_description":"'.$appoitment_type.'","appt_id":'.$appt_id.',"appt_time":"'.$appt_time.'","delete_from_day":"'.$now_date_time.'"}]';
		
		$post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
		
	  }
		
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		
		 $stmt = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date) 
		VALUES (?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time";
	
	mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);

	mysqli_stmt_execute($stmt);

	mysqli_stmt_free_result($stmt);
	
	mysqli_close($mysqli);
	
		$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


      for ($i=0;$i<count($response_array);$i++) {
		 
		  $plist_string .= '
	<dict>
		<key>JSON</key>
		<string><![CDATA['.$jsonSendData.']]></string>
		<key>heading</key>
		<string><![CDATA['.$response_array[0]['heading'].']]></string>
		<key>message</key>
		<string><![CDATA['.$response_array[0]['result_string'].']]></string>
	</dict>';
      }

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

?>