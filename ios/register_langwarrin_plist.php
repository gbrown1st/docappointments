<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$company = '';//$_POST['company'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$patient_name = $first_name.' '.$last_name;
$user_type = $_POST['user_type'];

$dob = $_POST['dob'];
$pos = strrpos($dob, "-");
if ($pos !== false) { // note: three equal signs
   	$dob_array = explode('-', $dob);
	$dob = $dob_array[2].'/'.$dob_array[1].'/'.$dob_array[0];
}

$email = $_POST['email'];
$mobile = $_POST['mobile'];
$username = $_POST['username'];
$password = $_POST['password'];
$medicareno = '';//$_POST['medicareno'];
$medicarelineno = '';//$_POST['medicarelineno'];

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surgeries.surgery_id,
	surgeries.surgery_name,
	surgeries.surgery_address_1,
	surgeries.surgery_address_2,
	surgeries.surgery_suburb_town,
	surgeries.surgery_state,
	surgeries.surgery_postcode,
	surgeries.contact_email,
	surgeries.contact_phone,
	surgeries.latitude,
	surgeries.longitude,
	surgery_types.surgery_type, 
	surgery_text.surgery_front_screen, 
	surgery_text.surgery_make_appointment_alert, 
	surgery_text.surgery_cancel_appointment_alert
FROM surgeries INNER JOIN surgery_types ON surgeries.surgery_type_id = surgery_types.surgery_type_id
	 INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
WHERE
	surgeries.surgery_id = ?");

mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->surgery_id, $row->surgery_name, 
							  $row->surgery_address_1, $row->surgery_address_2, $row->surgery_suburb_town, $row->surgery_state, 
							  $row->surgery_postcode, $row->contact_email, 
							  $row->contact_phone, $row->latitude, $row->longitude, 
							  $row->surgery_type, $row->surgery_front_screen, $row->surgery_make_appointment_alert, $row->surgery_cancel_appointment_alert);
							  
	while (mysqli_stmt_fetch($stmt)) {
		 
		  $surgery_id = $row->surgery_id;
		  $surgery_name = $row->surgery_name;
		  $surgery_address_1 = $row->surgery_address_1;
		  $surgery_address_2 = $row->surgery_address_2;
		  $surgery_suburb_town = $row->surgery_suburb_town;
		  $surgery_state = $row->surgery_state;
		  $surgery_postcode = $row->surgery_postcode;
		  $contact_email = $row->contact_email;
		  $contact_phone = $row->contact_phone;
		  $latitude = $row->latitude;
		  $longitude = $row->longitude;
		  $surgery_type = $row->surgery_type;
		  $surgery_front_screen = str_replace("[surgery_name]", $row->surgery_name, $row->surgery_front_screen);
		  $surgery_front_screen = str_replace("[surgery_phone]", $row->contact_phone, $surgery_front_screen);
		  $surgery_front_screen = str_replace("DocAppointment", "Langwarrin Medical Clinic Online", $surgery_front_screen);
		  $surgery_make_appointment_alert = $row->surgery_make_appointment_alert;
		  $surgery_cancel_appointment_alert = $row->surgery_cancel_appointment_alert;
		  
      }
		
	 $login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($login_stmt);
		 
		 mysqli_stmt_bind_result($login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($login_stmt);

		 if (mysqli_stmt_num_rows($login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			
		mysqli_stmt_free_result($login_stmt);  
		
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
						<key>surgery_id</key>
						<string><![CDATA['.$surgery_id.']]></string>
						<key>surgery_name</key>
						<string><![CDATA['.$surgery_name.']]></string>
						<key>surgery_address_1</key>
						<string><![CDATA['.$surgery_address_1.']]></string>
						<key>surgery_address_2</key>
						<string><![CDATA['.$surgery_address_2.']]></string>
						<key>surgery_suburb_town</key>
						<string><![CDATA['.$surgery_suburb_town.']]></string>
						<key>surgery_state</key>
						<string><![CDATA['.$surgery_state.']]></string>
						<key>surgery_postcode</key>
						<string><![CDATA['.$surgery_postcode.']]></string>
						<key>contact_email</key>
						<string><![CDATA['.$contact_email.']]></string>
						<key>contact_phone</key>
						<string><![CDATA['.$contact_phone.']]></string>
						<key>latitude</key>
						<string><![CDATA['.$latitude.']]></string>
						<key>longitude</key>
						<string><![CDATA['.$longitude.']]></string>
						<key>surgery_type</key>
						<string><![CDATA['.$surgery_type.']]></string>
						<key>surgery_front_screen</key>
						<string><![CDATA['.$surgery_front_screen.']]></string>
						<key>surgery_make_appointment_alert</key>
						<string><![CDATA['.$surgery_make_appointment_alert.']]></string>
						<key>surgery_cancel_appointment_alert</key>
						<string><![CDATA['.$surgery_cancel_appointment_alert.']]></string>
					</dict>
				</array>
			</plist>';
			
			
		 } else {
			 
			 $check_user_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where (user_username = ? or email = ?) and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($check_user_stmt, 'ssi', $username, $email, $surgery_id);
		 
		 mysqli_stmt_execute($check_user_stmt);
		 
		 mysqli_stmt_bind_result($check_user_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($check_user_stmt);

		 if (mysqli_stmt_num_rows($check_user_stmt) > 0) {
			 
			$plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Username and/or Email In Use!]]></string>
						<key>message</key>
						<string><![CDATA[This username and/or email is in use. Please use a different username and/or email address.]]></string>
					</dict>
				</array>
			</plist>';
			
			mysqli_stmt_free_result($check_user_stmt);  
			 
		 } else {
			 
			  $add_user_stmt = mysqli_prepare($mysqli, "INSERT INTO users (surgery_id, user_type, first_name, last_name, dob, email, mobile, user_username, user_password, created_date) VALUES (?,?,?,?,?,?,?,?,AES_ENCRYPT(?, '0bfuscate'),?)");
			  
			  mysqli_stmt_bind_param($add_user_stmt, 'isssssssss', $surgery_id, $user_type, $first_name, $last_name, $dob, $email, $mobile, $username, $password, $now_date_time);
			  
			  if (mysqli_stmt_execute($add_user_stmt)) {
				  
				  $new_login_stmt = mysqli_prepare($mysqli, "SELECT user_id from users where user_username = ? and AES_DECRYPT(user_password, '0bfuscate') = ? and surgery_id = ? and user_status = 'active'");
		 
		 mysqli_stmt_bind_param($new_login_stmt, 'ssi', $username, $password, $surgery_id);
		 
		 mysqli_stmt_execute($new_login_stmt);
		 
		 mysqli_stmt_bind_result($new_login_stmt, $row->user_id);
		 
		 mysqli_stmt_store_result($new_login_stmt);

		 if (mysqli_stmt_num_rows($new_login_stmt) > 0) {
			 
			  while (mysqli_stmt_fetch($new_login_stmt)) {
		 
				  $user_id = $row->user_id;
			  }
			  
		 }
		  
		 mysqli_stmt_free_result($add_user_stmt);  
		 mysqli_stmt_free_result($new_login_stmt);  
		 
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Successful!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has been successful.
			
You can now make appointments with '.$surgery_name.'.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
						<key>surgery_id</key>
						<string><![CDATA['.$surgery_id.']]></string>
						<key>surgery_name</key>
						<string><![CDATA['.$surgery_name.']]></string>
						<key>surgery_address_1</key>
						<string><![CDATA['.$surgery_address_1.']]></string>
						<key>surgery_address_2</key>
						<string><![CDATA['.$surgery_address_2.']]></string>
						<key>surgery_suburb_town</key>
						<string><![CDATA['.$surgery_suburb_town.']]></string>
						<key>surgery_state</key>
						<string><![CDATA['.$surgery_state.']]></string>
						<key>surgery_postcode</key>
						<string><![CDATA['.$surgery_postcode.']]></string>
						<key>contact_email</key>
						<string><![CDATA['.$contact_email.']]></string>
						<key>contact_phone</key>
						<string><![CDATA['.$contact_phone.']]></string>
						<key>latitude</key>
						<string><![CDATA['.$latitude.']]></string>
						<key>longitude</key>
						<string><![CDATA['.$longitude.']]></string>
						<key>surgery_type</key>
						<string><![CDATA['.$surgery_type.']]></string>
						<key>surgery_front_screen</key>
						<string><![CDATA['.$surgery_front_screen.']]></string>
						<key>surgery_make_appointment_alert</key>
						<string><![CDATA['.$surgery_make_appointment_alert.']]></string>
						<key>surgery_cancel_appointment_alert</key>
						<string><![CDATA['.$surgery_cancel_appointment_alert.']]></string> 
					</dict>
				</array>
			</plist>';
			
			  } else {
				  
				  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
			<plist version="1.0">
				<array>
					<dict>
						<key>heading</key>
						<string><![CDATA[Registration Failed!]]></string>
						<key>message</key>
						<string><![CDATA[Your registration with '.$surgery_name.' has failed.
			
Please try again.]]></string>
						<key>patient_id</key>
						<string><![CDATA['.$user_id.']]></string>
					</dict>
				</array>
			</plist>';
				   
			  }
			  
		 }
	}
			  
			 $user_stmt = mysqli_prepare($mysqli,
				  "INSERT INTO user_log (
					surgery_id, 
					user_id, 
					log_source, 
					log_action, 
					log_action_comment,
					log_action_date) 
				VALUES (?,?,?,?,?,?)");
			
			$now_date_time = date('Y-m-d H:i:s');
			$log_source = "iPhone";
			$log_action = "Register/Log In";
			$log_action_comment = $response;
			$log_action_comment = "Surgery: $surgery_name; Details: $patient_name [DOB: $dob] [Ph: $mobile]";
			
			mysqli_bind_param($user_stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);
		
			mysqli_stmt_execute($user_stmt);
			mysqli_stmt_free_result($user_stmt);
		
			mysqli_stmt_free_result($stmt);
			mysqli_close($login_stmt);
			mysqli_close($add_user_stmt);
			mysqli_close($new_login_stmt);
			mysqli_close($user_stmt);
			mysqli_close($check_user_stmt);
			
			mysqli_close($mysqli);
			
	
	echo $plist_string;

?>



