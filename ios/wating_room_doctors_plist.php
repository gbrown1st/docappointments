<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_GET['surgery_id'];
$location = $_GET['location'];

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	server_url.server_wsdl_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  
      }
	
      mysqli_stmt_free_result($stmt);
	  
	  $plist_string = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';
	
 if ($surgery_software == 'Stat') {
		
		try {
			
		   $client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
	
			$result = $client->GetOnlineResourceListWithAppointments();
			
		$response_array = $result->GetOnlineResourceListWithAppointmentsResult->DocAppResourceArray->DocAppResource;

if (count($response_array) == 0) {
	
	$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA[No doctors with appointments]]></string>
		<key>DoctorCode</key>
		<string><![CDATA[0]]></string>
		<key>DoctorID</key>
		<string><![CDATA[0]]></string>
	</dict>';
	
}

if (is_array($response_array)) {
	
	for ($i=0;$i<count($response_array);$i++) {
	
		$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.str_replace('"',"'",trim($response_array[$i]->ResourceName)).']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.trim($response_array[$i]->ResourceId).']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.trim($response_array[$i]->ResourceId).']]></string>
	</dict>';
      
		}
	
} else {
	
		$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.str_replace('"',"'",trim($response_array->ResourceName)).']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.trim($response_array->ResourceId).']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.trim($response_array->ResourceId).']]></string>
	</dict>';
		
}
			
		}
		catch (SoapFault $exception) {
			echo $exception->getMessage();
		}
		  
	  } 
	  
	  else if ($surgery_software == 'Medilink') {
	
	try {
		
		  $client = new SoapClient($server_wsdl_url, array('login' => "admin", 'password' => "", "trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS)); 
		  
          $methodName = "GetOnlineResourceListWithAppointments";
          $resultName = $methodName . "Result";
          $params = new stdClass;
          $result = $client->$methodName($params);
          $resultValue = $result->$resultName;
          $resourceArray = "ResourceArray";
          $resource = "Resource";
          $id = "Id";
          $resourceName = "ResourceName";
          $title = "Title";
          $firstName = "FirstName";
          $lastName = "LastName";
          $response_array = array();
         
          	  foreach($resultValue->$resourceArray->$resource as $value)
				  {
						$resourceObject = new StdClass();
					  	$resourceObject->ResourceId = $value->$id;
					  	$resourceObject->ResourceName = $value->$resourceName->$title . ' ' . $value->$resourceName->$firstName . ' ' . $value->$resourceName->$lastName;
					   $response_array[] = $resourceObject;
				  }
        
		if (count($response_array) == 0) {
	
	$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA[No doctors with appointments]]></string>
		<key>DoctorCode</key>
		<string><![CDATA[0]]></string>
		<key>DoctorID</key>
		<string><![CDATA[0]]></string>
	</dict>';
	
}
           for ($i=0;$i<count($response_array);$i++) {
	
		$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.str_replace('"',"'",trim($response_array[$i]->ResourceName)).']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.trim($response_array[$i]->ResourceId).']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.trim($response_array[$i]->ResourceId).']]></string>
	</dict>';
      
		}
	
   }
	
	
    catch (SoapFault $exception) {
        echo $exception->getMessage();
    }

}
else {
		   
		$post_array = array('getApptDoctors' => 'true');

		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);
		
		$response = $r->send()->getBody();
		$response_array = json_decode($response, true);
		
		if (count($response_array) == 0) {
	
	$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA[No doctors with appointments]]></string>
		<key>DoctorCode</key>
		<string><![CDATA[0]]></string>
		<key>DoctorID</key>
		<string><![CDATA[0]]></string>
	</dict>';
	
}
	
for ($i=0;$i<count($response_array);$i++) {
	
	if (strlen($location) > 0) {
		
		if ($response_array[$i]['Location'] == $location) {

$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.str_replace('"',"'",trim($response_array[$i]['FullName'])).']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.trim($response_array[$i]['DoctorCode']).']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.$response_array[$i]['DoctorID'].']]></string>
	</dict>';
	
		}
		
	} else {
		$plist_string .= '
	<dict>
		<key>SurgeryName</key>
		<string><![CDATA['.$surgery_name.']]></string>
		<key>FullName</key>
		<string><![CDATA['.str_replace('"',"'",trim($response_array[$i]['FullName'])).']]></string>
		<key>DoctorCode</key>
		<string><![CDATA['.trim($response_array[$i]['DoctorCode']).']]></string>
		<key>DoctorID</key>
		<string><![CDATA['.$response_array[$i]['DoctorID'].']]></string>
	</dict>';
	}
	
	
}


}

	$plist_string .= '
</array>
</plist>';

	
	echo $plist_string;

		
?>



