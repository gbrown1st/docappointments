<?php
$code_version = '170315';

require_once "configbp.php";

if (isset($_GET['showVersion']))
{
	echo $code_version;
}

if (isset($_POST['addPatient']))

{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$title_code = $item['title_code'];
			$first_name = $item['first_name'];
			$middle_name = $item['middle_name'];
			$last_name = $item['last_name'];
			$preferred_name = $item['preferred_name'];
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$medicare_number = $item['medicare_number'];
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$atsic_code = (int) $item['atsic_code'];
			$sms_code = (int) $item['sms_code'];
			$nok_title = $item['nok_title'];
			$nok_title_code = $item['nok_title_code'];
			$nok_first_name = $item['nok_first_name'];
			$nok_last_name = $item['nok_last_name'];
			$nok_address = $item['nok_address'];
			$nok_city = $item['nok_city'];
			$nok_postcode = $item['nok_postcode'];
			$nok_phone = $item['nok_phone'];
			$nok_alternate_phone = $item['nok_alternate_phone'];
			$nok_relationship = $item['nok_relationship'];
			$ec_title = $item['ec_title'];
			$ec_title_code = $item['ec_title_code'];
			$ec_first_name = $item['ec_first_name'];
			$ec_last_name = $item['ec_last_name'];
			$ec_address = $item['ec_address'];
			$ec_city = $item['ec_city'];
			$ec_postcode = $item['ec_postcode'];
			$ec_phone = $item['ec_phone'];
			$ec_alternate_phone = $item['ec_alternate_phone'];
			$ec_relationship = $item['ec_relationship'];
	
		}
		
		$patient_sql = "SELECT 
	dbo.PATIENTS.INTERNALID
FROM
dbo.PATIENTS
WHERE     
(dbo.PATIENTS.FIRSTNAME = UPPER('$first_name')) 
AND (dbo.PATIENTS.SURNAME = UPPER('$last_name')) 
AND CONVERT(char(10), dbo.PATIENTS.DOB,126) = '$dob'
AND (dbo.PATIENTS.SEXCODE = $gender_code)";
                      
     $patient_rs = odbc_exec($Connect,$patient_sql);
	
		$p = array();
		$StringResult = new stdClass();
		
	if( odbc_num_rows( $patient_rs ) > 0) {
		
		while ($patient_row = odbc_fetch_object($patient_rs)) {
			
			$patient_id = (int) $patient_row->INTERNALID;
			
		}   
		
		$StringResult->result_string = "A patient with your details already exists."; 
		$StringResult->heading = "Patient Already Exists";
		
	} else {
		
		$addPatientSQL = "DECLARE @RC int

		EXECUTE @RC = [BPSPatients].[dbo].[BP_AddPatient]  

		   @TITLECODE = $title_code
		  ,@FIRSTNAME = N'$first_name'
		  ,@MIDDLENAME = N'$middle_name'
		  ,@SURNAME = N'$last_name'
		  ,@PREFERREDNAME = N'$preferred_name'
		  ,@ADDRESS1 = N'$address_1'
		  ,@ADDRESS2 = N'$address_2'
		  ,@CITY = N'$city'
		  ,@POSTCODE = N'$postcode'
		  ,@POSTALADDRESS = N'$postal_address'
		  ,@POSTALCITY = N'$postal_city'
		  ,@POSTALPOSTCODE = N'$postal_postcode'
		  ,@DOB = N'$dob'
		  ,@SEXCODE = $gender_code
		  ,@HOMEPHONE = N'$home_phone'
		  ,@WORKPHONE = N'$work_phone'
		  ,@MOBILEPHONE = N'$mobile_phone'
		  ,@MEDICARENO = N'$medicare_number'
		  ,@MEDICARELINENO = N'$medicare_line_number'
		  ,@MEDICAREEXPIRY = N'$medicare_expiry'
		  ,@PENSIONCODE = $pension_code
		  ,@PENSIONNO = N'$pension_number'
		  ,@PENSIONEXPIRY = N'$pension_expiry'
		  ,@DVACODE = $dva_code
		  ,@DVANO = N'$dva_number'
		  ,@RECORDNO = N''
		  ,@EXTERNALID = N''
		  ,@EMAIL = N'$email'		  
		  ,@HEADOFFAMILYID = 0
		  ,@ETHNICCODE = $atsic_code
  		  ,@CONSENTSMSREMINDER = $sms_code

		select 'new_patient_id' = @RC";

		$add_user_rs = odbc_exec($Connect,$addPatientSQL);
		
			$result = new stdClass();
		
			while ($add_user_row = odbc_fetch_object($add_user_rs)) {
				$result = $add_user_row;
			}
			foreach ($result as $key => $value) {
           		$patient_id = (int) $value;
		    }
		    
		    if ($patient_id > 0) {
				
		$nok_message_subject = "1stAvailable Next of Kin";
		$nok_message_body = "Title: $nok_title;   ";
		$nok_message_body .= "First name: $nok_first_name;   ";
		$nok_message_body .= "Last name: $nok_last_name;   ";
		$nok_message_body .= "Address: $nok_address;   ";
		$nok_message_body .= "City: $nok_city;   ";
		$nok_message_body .= "Postcode: $nok_postcode;   ";
		$nok_message_body .= "Contact phone: $nok_phone;   ";
		$nok_message_body .= "Alternate contact: $nok_alternate_phone;   ";
		$nok_message_body .= "Relationship: $nok_relationship;   ";

		$message_sql = "DECLARE	@return_value int
		
		EXEC	@return_value = [dbo].[message_add]
				@subject = N'$nok_message_subject',
				@message = N'$nok_message_body',
				@fromid = $bp_practicemanager_id,
				@userid = $bp_practicemanager_id,
				@patid = $patient_id,
				@loginid = $bp_practicemanager_id
		
		SELECT	'add_message_result' = @return_value";

		$message_rs = odbc_exec($Connect,$message_sql);
		
			while ($message_row = odbc_fetch_object($message_rs)) {

				$message_success  = (int) $message_row->add_message_result;

			}
			
			if ($message_success == 0) {
		
				$ec_message_subject = "1stAvailable Emergency Contact";
				$ec_message_body = "Title: $ec_title;   ";
				$ec_message_body .= "First name: $ec_first_name;   ";
				$ec_message_body .= "Last name: $ec_last_name;   ";
				$ec_message_body .= "Address: $ec_address;   ";
				$ec_message_body .= "City: $ec_city;   ";
				$ec_message_body .= "Postcode: $ec_postcode;   ";
				$ec_message_body .= "Contact phone: $ec_phone;   ";
				$ec_message_body .= "Alternate contact: $ec_alternate_phone;   ";
				$ec_message_body .= "Relationship: $ec_relationship;   ";
				
				$message2_sql = "DECLARE	@return_value int
		
		EXEC	@return_value = [dbo].[message_add]
				@subject = N'1stAvailable Emergency Contact',
				@message = N'$ec_message_body',
				@fromid = $bp_practicemanager_id,
				@userid = $bp_practicemanager_id,
				@patid = $patient_id,
				@loginid = $bp_practicemanager_id
		
		SELECT	'add_message_result' = @return_value";
		
				$message2_rs = odbc_exec($Connect,$message2_sql);
		
			}
				
					$StringResult->result_string = "Your details have been saved."; 
					$StringResult->heading = "Patient Details Saved";

			} else {

					$StringResult->result_string = "Your details could not be saved."; 
					$StringResult->heading = "Patient Details Failed";

			}
			
		}


		$p[] = $StringResult;

		echo json_encode($p);

	}
	

if (isset($_POST['updateExistingPatient']))

{
		
		$jsonString = urldecode($_POST['jsonSendData']);
		
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {
			
			$patient_id = (int) $item['patient_id'];
			$title_code = $item['title_code'];
			$first_name = $item['first_name'];
			$middle_name = $item['middle_name'];
			$last_name = $item['last_name'];
			$preferred_name = $item['preferred_name'];
			$address_1 = $item['address_1'];
			$address_2 = $item['address_2'];
			$city = $item['city'];
			$postcode = $item['postcode'];
			$postal_address = $item['postal_address'];
			$postal_city = $item['postal_city'];
			$postal_postcode = $item['postal_postcode'];
			$dob = $item['dob'];
			$home_phone = $item['home_phone'];
			$work_phone = $item['work_phone'];
			$mobile_phone = $item['mobile_phone'];
			$email = $item['email'];
			$gender_code = $item['gender_code'];
			$medicare_number = $item['medicare_number'];
			$medicare_line_number = $item['medicare_line_number'];
			$medicare_expiry = $item['medicare_expiry'];
			$pension_code = $item['pension_code'];
			$dva_code = $item['dva_code'];
			$pension_number = $item['pension_number'];
			$pension_expiry = $item['pension_expiry'];
			$dva_number = $item['dva_number'];
			$head_of_family = $item['head_of_family'];
			$atsic_code = (int) $item['atsic_code'];
			$sms_code = (int) $item['sms_code'];
			$nok_title = $item['nok_title'];
			$nok_title_code = $item['nok_title_code'];
			$nok_first_name = $item['nok_first_name'];
			$nok_last_name = $item['nok_last_name'];
			$nok_address = $item['nok_address'];
			$nok_city = $item['nok_city'];
			$nok_postcode = $item['nok_postcode'];
			$nok_phone = $item['nok_phone'];
			$nok_alternate_phone = $item['nok_alternate_phone'];
			$nok_relationship = $item['nok_relationship'];
			$ec_title = $item['ec_title'];
			$ec_title_code = $item['ec_title_code'];
			$ec_first_name = $item['ec_first_name'];
			$ec_last_name = $item['ec_last_name'];
			$ec_address = $item['ec_address'];
			$ec_city = $item['ec_city'];
			$ec_postcode = $item['ec_postcode'];
			$ec_phone = $item['ec_phone'];
			$ec_alternate_phone = $item['ec_alternate_phone'];
			$ec_relationship = $item['ec_relationship'];
	
		}
		
		$p = array();
		$StringResult = new stdClass();
		
		$addPatientSQL = "DECLARE @RC int

		EXECUTE @RC = [BPSPatients].[dbo].[BP_UpdatePatient]  
		
		   @PATIENTID = $patient_id
		  ,@TITLECODE = $title_code
		  ,@FIRSTNAME = N'$first_name'
		  ,@MIDDLENAME = N'$middle_name'
		  ,@SURNAME = N'$last_name'
		  ,@PREFERREDNAME = N'$preferred_name'
		  ,@ADDRESS1 = N'$address_1'
		  ,@ADDRESS2 = N'$address_2'
		  ,@CITY = N'$city'
		  ,@POSTCODE = N'$postcode'
		  ,@POSTALADDRESS = N'$postal_address'
		  ,@POSTALCITY = N'$postal_city'
		  ,@POSTALPOSTCODE = N'$postal_postcode'
		  ,@DOB = N'$dob'
		  ,@SEXCODE = $gender_code
		  ,@HOMEPHONE = N'$home_phone'
		  ,@WORKPHONE = N'$work_phone'
		  ,@MOBILEPHONE = N'$mobile_phone'
		  ,@MEDICARENO = N'$medicare_number'
		  ,@MEDICARELINENO = N'$medicare_line_number'
		  ,@MEDICAREEXPIRY = N'$medicare_expiry'
		  ,@PENSIONCODE = $pension_code
		  ,@PENSIONNO = N'$pension_number'
		  ,@PENSIONEXPIRY = N'$pension_expiry'
		  ,@DVACODE = $dva_code
		  ,@DVANO = N'$dva_number'
		  ,@RECORDNO = N''
		  ,@EXTERNALID = N''
		  ,@EMAIL = N'$email'
		  ,@HEADOFFAMILYID = $head_of_family
		  ,@ETHNICCODE = $atsic_code
  		  ,@CONSENTSMSREMINDER = $sms_code

		select 'update_patient_id' = @RC";

		$add_user_rs = odbc_exec($Connect,$addPatientSQL);
		
			while ($add_user_row = odbc_fetch_object($add_user_rs)) {

				$add_update_success  = (int) $add_user_row->update_patient_id;

			}
			
			if ($add_update_success > 0) {
				
		$nok_message_subject = "1stAvailable Next of Kin";
		$nok_message_body = "Title: $nok_title;   ";
		$nok_message_body .= "First name: $nok_first_name;   ";
		$nok_message_body .= "Last name: $nok_last_name;   ";
		$nok_message_body .= "Address: $nok_address;   ";
		$nok_message_body .= "City: $nok_city;   ";
		$nok_message_body .= "Postcode: $nok_postcode;   ";
		$nok_message_body .= "Contact phone: $nok_phone;   ";
		$nok_message_body .= "Alternate contact: $nok_alternate_phone;   ";
		$nok_message_body .= "Relationship: $nok_relationship;   ";

		$message_sql = "DECLARE	@return_value int
		
		EXEC	@return_value = [dbo].[message_add]
				@subject = N'$nok_message_subject',
				@message = N'$nok_message_body',
				@fromid = $bp_practicemanager_id,
				@userid = $bp_practicemanager_id,
				@patid = $patient_id,
				@loginid = $bp_practicemanager_id
		
		SELECT	'add_message_result' = @return_value";

		$message_rs = odbc_exec($Connect,$message_sql);
		
			while ($message_row = odbc_fetch_object($message_rs)) {

				$message_success  = (int) $message_row->add_message_result;

			}
			
			if ($message_success == 0) {
		
				$ec_message_subject = "1stAvailable Emergency Contact";
				$ec_message_subject = "1stAvailable Emergency Contact";
				$ec_message_body = "Title: $ec_title;   ";
				$ec_message_body .= "First name: $ec_first_name;   ";
				$ec_message_body .= "Last name: $ec_last_name;   ";
				$ec_message_body .= "Address: $ec_address;   ";
				$ec_message_body .= "City: $ec_city;   ";
				$ec_message_body .= "Postcode: $ec_postcode;   ";
				$ec_message_body .= "Contact phone: $ec_phone;   ";
				$ec_message_body .= "Alternate contact: $ec_alternate_phone;   ";
				$ec_message_body .= "Relationship: $ec_relationship;   ";
				
				$message2_sql = "DECLARE	@return_value int
		
		EXEC	@return_value = [dbo].[message_add]
				@subject = N'1stAvailable Emergency Contact',
				@message = N'$ec_message_body',
				@fromid = $bp_practicemanager_id,
				@userid = $bp_practicemanager_id,
				@patid = $patient_id,
				@loginid = $bp_practicemanager_id
		
		SELECT	'add_message_result' = @return_value";
		
				$message2_rs = odbc_exec($Connect,$message2_sql);
		
			}
		
					$StringResult->result_string = "Your details have been updated."; 
					$StringResult->heading = "Update Patient Details Succeeded";

			} else {

					$StringResult->result_string = "Your details could not be saved."; 
					$StringResult->heading = "Update Patient Details Failed";

			}

		$p[] = $StringResult;

		echo json_encode($p);

	}
	
	
if(isset($_POST['getExistingPatientDetails']))
{
		$jsonString = urldecode($_POST['jsonSendData']);
  		$data = json_decode($jsonString, true);

		foreach ($data as $item) {

			$patient_id = (int) $item['patient_id'];
			
		}
  		
	$get_patient_sql = "SELECT dbo.PATIENTS.INTERNALID, dbo.PATIENTS.TITLECODE, dbo.PATIENTS.FIRSTNAME, 
dbo.PATIENTS.MIDDLENAME, dbo.PATIENTS.SURNAME,  

dbo.PATIENTS.PREFERREDNAME, dbo.PATIENTS.ADDRESS1, dbo.PATIENTS.ADDRESS2, dbo.PATIENTS.CITY, dbo.PATIENTS.POSTCODE,  

dbo.PATIENTS.POSTALADDRESS, dbo.PATIENTS.POSTALCITY, dbo.PATIENTS.POSTALPOSTCODE, dbo.PATIENTS.DOB, 
dbo.PATIENTS.SEXCODE,  

                      dbo.PATIENTS.HOMEPHONE, dbo.PATIENTS.WORKPHONE, dbo.PATIENTS.MOBILEPHONE, dbo.PATIENTS.MEDICARENO, 

dbo.PATIENTS.MEDICARELINENO, dbo.PATIENTS.MEDICAREEXPIRY, dbo.PATIENTS.PENSIONCODE, dbo.PATIENTS.PENSIONNO,  

dbo.PATIENTS.PENSIONEXPIRY, dbo.PATIENTS.DVACODE, dbo.PATIENTS.DVANO, dbo.PATIENTS.RECORDNO, dbo.PATIENTS.USERID,  

                      dbo.PATIENTS.EXTERNALID, dbo.PATIENTS.HEADOFFAMILYID, 
                      dbo.PATIENTS.ETHNICCODE, dbo.PATIENTS.CONSENTSMSREMINDER, dbo.EMAIL.EMAIL,
                      (SELECT TOP 1 MESSAGE
FROM         dbo.MESSAGES
WHERE     (INTERNALID = $patient_id) AND (SUBJECT = '1stAvailable Next of Kin')
ORDER BY CREATED DESC) as NEXTOFKIN,
(SELECT TOP 1 MESSAGE
FROM         dbo.MESSAGES
WHERE     (INTERNALID = $patient_id) AND (SUBJECT = '1stAvailable Emergency Contact')
ORDER BY CREATED DESC) as EMERGENCYCONTACT

FROM         dbo.PATIENTS LEFT OUTER JOIN

                      dbo.EMAIL ON dbo.PATIENTS.INTERNALID = dbo.EMAIL.INTERNALID

WHERE     (dbo.PATIENTS.INTERNALID = $patient_id)";

  $get_patient_rs = odbc_exec($Connect,$get_patient_sql);

			while ($row = odbc_fetch_object($get_patient_rs)) 

			{
				$Appointment = new StdClass();
				$Appointment->INTERNALID = (int) $row->INTERNALID;
				$Appointment->TITLECODE = $row->TITLECODE;
				$Appointment->FIRSTNAME = $row->FIRSTNAME;
				$Appointment->MIDDLENAME = $row->MIDDLENAME;
				$Appointment->SURNAME = $row->SURNAME;
				$Appointment->PREFERREDNAME = $row->PREFERREDNAME;
				$Appointment->ADDRESS1 = $row->ADDRESS1;
				$Appointment->ADDRESS2 = $row->ADDRESS2;
				$Appointment->CITY = $row->CITY;
				$Appointment->POSTCODE = $row->POSTCODE;
				$Appointment->POSTALADDRESS = $row->POSTALADDRESS;
				$Appointment->POSTALCITY = $row->POSTALCITY;
				$Appointment->POSTALPOSTCODE = $row->POSTALPOSTCODE;
				$Appointment->DOB = $row->DOB;
				$Appointment->SEXCODE = (int) $row->SEXCODE;
				$Appointment->HOMEPHONE = $row->HOMEPHONE;
				$Appointment->WORKPHONE = $row->WORKPHONE;
				$Appointment->MOBILEPHONE = $row->MOBILEPHONE;
				$Appointment->MEDICARENO = $row->MEDICARENO;
				$Appointment->MEDICARELINENO = $row->MEDICARELINENO;
				$Appointment->MEDICAREEXPIRY = $row->MEDICAREEXPIRY;
				$Appointment->PENSIONCODE = (int) $row->PENSIONCODE;
				$Appointment->PENSIONNO = $row->PENSIONNO;
				$Appointment->PENSIONEXPIRY = $row->PENSIONEXPIRY;
				$Appointment->DVACODE = (int) $row->DVACODE;
				$Appointment->DVANO = $row->DVANO;
				$Appointment->RECORDNO = $row->RECORDNO;
				$Appointment->EXTERNALID = $row->EXTERNALID;
				$Appointment->HEADOFFAMILYID = (int) $row->HEADOFFAMILYID;
				$Appointment->ATSI = $row->ETHNICCODE;
				$Appointment->SMS = $row->CONSENTSMSREMINDER;
				$Appointment->EMAIL = $row->EMAIL;
				$Appointment->NEXTOFKIN = $row->NEXTOFKIN;
				$Appointment->EMERGENCYCONTACT = $row->EMERGENCYCONTACT;
				
				$p[] = $Appointment;
			}

	echo json_encode($p);
}

if(isset($_POST['getBestPracticeUsers']))
{

	$sql = "SELECT     dbo.TITLES.TITLE, dbo.USERS.FIRSTNAME, dbo.USERS.SURNAME, dbo.USERGROUPS.GROUPNAME, dbo.USERS.USERID
FROM         dbo.USERGROUPS INNER JOIN
                      dbo.USERS ON dbo.USERGROUPS.GROUPCODE = dbo.USERS.GROUPCODE INNER JOIN
                      dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
WHERE     (dbo.USERS.RECORDSTATUS = 1)
ORDER BY dbo.USERS.USERID, dbo.USERS.SURNAME, dbo.USERS.FIRSTNAME";

	$rs = odbc_exec($Connect,$sql);
	$p = array();
	
	while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->GROUPNAME = $row->GROUPNAME;
			$Appointment->UserID = (int) $row->USERID;
			$Appointment->FullName = trim($row->TITLE).' '.trim($row->FIRSTNAME).' '.trim($row->SURNAME);
			
			$p[] = $Appointment;
			
		}
		
	echo json_encode($p);
}

if(isset($_POST['getApptDoctors']))
{
	$fromDate = strtotime('now');
	$today = date("Y-m-d",$fromDate);
	
	$sql = "SELECT DISTINCT dbo.TITLES.TITLE, dbo.USERS.FIRSTNAME, dbo.USERS.SURNAME, dbo.USERS.USERID
FROM         dbo.APPOINTMENTS INNER JOIN
                      dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
                      dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
WHERE     (dbo.APPOINTMENTS.APPOINTMENTDATE = CONVERT(DATETIME, '$today', 102))";

	$rs = odbc_exec($Connect,$sql);
	$p = array();
	
	while ($row = odbc_fetch_object($rs)) {

			$Appointment = new StdClass();
			$Appointment->PractitionerID = (int) $row->USERID;
			$Appointment->DoctorID = (int) $row->USERID;
			$Appointment->DoctorCode = $row->USERID;
			if ($row->TITLE == '' && $row->FIRSTNAME == '') {
				$Appointment->FullName = trim($row->SURNAME);
			} else {
				$Appointment->FullName = trim($row->TITLE).' '.trim($row->FIRSTNAME).' '.trim($row->SURNAME);
			}
			
			$p[] = $Appointment;
			
		}
		
	echo json_encode($p);
}

if(isset($_POST['addToWaitingRoom']))
{
	$first_name = strtoupper(trim($_POST['first_name']));
	$last_name = strtoupper(trim($_POST['last_name']));
	
	$dob = $_POST['dob'];
	$gender = $_POST['gender'];
	
	$fromDate = strtotime('now');
	$today = date("Y-m-d",$fromDate);
	
	$timeFrom = time() - strtotime("today") - 7200;
	$timeTo = time() - strtotime("today") + 7200;
	
		
	$p = array();
	$StringResult = new StdClass();
	
	$patient_sql = "SELECT 
	dbo.PATIENTS.INTERNALID, dbo.PATIENTS.UPDATED
FROM
dbo.PATIENTS INNER JOIN dbo.SEX ON dbo.PATIENTS.SEXCODE = dbo.SEX.SEXCODE
WHERE     
(UPPER(dbo.PATIENTS.FIRSTNAME) = '$first_name') 
AND (UPPER(dbo.PATIENTS.SURNAME) = '$last_name') 
AND CONVERT(char(10), dbo.PATIENTS.DOB,126) = '$dob'
AND (dbo.SEX.SEX = '$gender')";

     $patient_rs = odbc_exec($Connect,$patient_sql);
     
	if( odbc_num_rows( $patient_rs ) > 0) {
		
		while ($patient_row = odbc_fetch_object($patient_rs)) {
			$patient_id = (int) $patient_row->INTERNALID;
			$last_updated = strtotime($patient_row->UPDATED);
			
		}    
		
		$last_six_months = strtotime("-6 months");
		
		$appointment_sql = "SELECT RECORDID, USERID, INTERNALID, REASON, 
APPOINTMENTDATE, APPOINTMENTTIME, RECORDSTATUS, APPOINTMENTLENGTH
FROM APPOINTMENTS
WHERE     
	INTERNALID = $patient_id
	AND APPOINTMENTDATE = '$today'
	AND APPOINTMENTTIME >= $timeFrom
	AND APPOINTMENTTIME <= $timeTo
	AND RECORDSTATUS = 1";
	
	 $appointment_rs = odbc_exec($Connect,$appointment_sql);
	 
		if( odbc_num_rows( $appointment_rs ) > 0) {
			
			while ($appointment_row = odbc_fetch_object($appointment_rs)) {
				
				$app_patient_id = (int) $appointment_row->INTERNALID;
				$app_id = (int) $appointment_row->RECORDID;
				$app_len = (int) $appointment_row->APPOINTMENTLENGTH;
				$app_date = $appointment_row->APPOINTMENTDATE;
				$app_time = (int) $appointment_row->APPOINTMENTTIME;
				$reason =  $appointment_row->REASON;
				
			}    
			
			
			$sql_wait = "DECLARE @RC int
				DECLARE @aptid int
				DECLARE @loginid int
				
				-- TODO: Set parameter values here.
				
				EXECUTE @RC = [BPSPatients].[dbo].[BP_ArriveAppointment] 
				   $app_id
				  ,0"; 
				 
							$rs_wait = odbc_exec($Connect,$sql_wait);
											
											
								if  ($rs_wait) {
									
										if ($last_six_months > $last_updated) {
						
											$StringResult->result_string = "The front desk has been advised of your arrival.\n\nIt has been more than six months since your details were updated.\nPlease contact the reception desk so you can update your details.";
													
										} else {
														
											$StringResult->result_string = "The front desk has been advised of your arrival.\n\nPlease take a seat."; 
														
										}
										$StringResult->heading = "Your Arrival Has Been Noted"; 
										
									} else {
										
										$StringResult->result_string = "We can\'t find a matching appointment for you with this doctor.\n\nPlease see the front desk.";  
										$StringResult->heading = "No Appointment Found";
										
									} 
				
		} else {
			
			$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
		}
	
	} else {
		
		$StringResult->result_string = "We can\'t find a matching appointment for you with this 
doctor.\n\nPlease see the front desk.";  
		$StringResult->heading = "No Appointment Found";
		
	}          
	
	
	$p[] = $StringResult;
	echo json_encode($p);

} 

?>
