<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

if (isset($_GET['surgery_id'])) {
	$surgery_id = $_GET['surgery_id'];
	setcookie("surgery_id", $surgery_id, time()+90*60*60*24*30);		
} else {
	$surgery_id = $_COOKIE['surgery_id'];
}

mysql_select_db($database_approval_rs, $approval_rs);
$query_surgery_test_rs = sprintf("SELECT
	surgeries.surgery_name,
	surgeries.contact_phone,
	surgery_text.surgery_front_screen
FROM
	surgeries
INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
WHERE
	surgeries.surgery_id = %s", GetSQLValueString($surgery_id, "int"));
$surgery_test_rs = mysql_query($query_surgery_test_rs, $approval_rs) or die(mysql_error());
$row_surgery_test_rs = mysql_fetch_assoc($surgery_test_rs);
$totalRows_surgery_test_rs = mysql_num_rows($surgery_test_rs);

$surgery_name = '<strong>'.$row_surgery_test_rs['surgery_name'].'</strong>';
$surgery_phone = '<strong>'.$row_surgery_test_rs['contact_phone'].'</strong>';
$front_screen_text = $row_surgery_test_rs['surgery_front_screen'];
$surgery_logo = 'logos/logo_'.$surgery_id.'.png';

$front_screen_text = str_replace('[surgery_name]', $surgery_name, $front_screen_text);
$front_screen_text = str_replace('[surgery_phone]', $surgery_phone, $front_screen_text);

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = '';
  $logout_url_array = explode('=1&logout_url=',$_REQUEST['accesscheck']);
	if (count($logout_url_array) > 0) {
		echo $logout_url_array[1];
		header("Location: ".$logout_url_array[1]);
	}
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "user_id";
  $MM_redirectLoginSuccess = "appointments.php";
  $MM_redirectLoginFailed = "login.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_approval_rs, $approval_rs);
  	
  $LoginRS__query=sprintf("SELECT user_username, AES_DECRYPT(user_password, '0bfuscate') as user_password, user_id FROM users_new WHERE user_username=%s AND user_password=AES_DECRYPT(user_password, '0bfuscate') = %s",
  GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $approval_rs) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
    
    $loginStrGroup  = mysql_result($LoginRS,0,'user_id');
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	
	$_SESSION['user_id'] = $loginStrGroup;	       

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
		header("Location: " . $MM_redirectLoginSuccess);
	  }
	  else {
		header("Location: ". $MM_redirectLoginFailed );
	  }
}
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<base href="https://secure.docappointment.com.au/appointments/" />
<title>DocAppointment Log In</title>
<link href="css/styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }
   
   function getLostPassword() {
	var email = document.getElementById('email').value;
	request.open("POST", "get_lost_password.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send("email="+email);
	var strResult=request.responseText;
	var alert_row = document.getElementById('alert_row');
	var alert_display = document.getElementById('alert_display');
	alert_display.style.display = "block";
	alert_display.innerHTML = strResult;
	
}

</script>

</head>

<body>
<div id="wrapper">
<div id="header"><img src="images/header.png" width="800" height="100"></div>
<div id="login_text">
<p><strong>Welcome to <?php echo $row_surgery_test_rs['surgery_name']; ?> Online Appointments</strong></p>
<p><img name="" src="<?php echo $surgery_logo; ?>" alt=""><?php echo $front_screen_text; ?></p>
</div>
<div id="content">
<div id="login">
<form style="width:60px;float:right;margin-right:20px;" name="go_to_register_form" method="post" action="register.php?surgery_id=<?php echo $surgery_id; ?>"><input name="Register" value="Register" type="submit"></form>
<p>To begin please log in below or if you haven't got a log in ID click the 'Register' button and complete the form.</p>
<form action="<?php echo $loginFormAction; ?>" method="POST" name="login_form">
  <table width="300" border="0" cellspacing="0" cellpadding="8">
    <tr>
      <th colspan="2">DocAppointment Log In</th>
    </tr>
    <tr>
      <td width="100" align="right"><strong>Username:</strong></td>
      <td width="200"><input type="text" name="username" id="username"></td>
    </tr>
    <tr>
      <td align="right"><strong>Password</strong>:</td>
      <td><input type="password" name="password" id="password"></td>
    </tr>
    <tr>
      <td colspan="2" align="right"> <input type="submit" name="login_btn" id="login_btn" value="Log In"></td>
    </tr>
  </table>
</form>
</div>
<div id="recover_password">
<div style="display:none;" id="alert_display"></div>
<form action="" method="POST" onSubmit="getLostPassword();return false;">
<table width="300" border="0" cellspacing="0" cellpadding="8">
  <tr>
    <th colspan="2">Can't Remember Your Password?</th>
    </tr>
  <tr>
    <td colspan="2">Enter your email address and click the &quot;Get Lost Password&quot; button and we'll email it to you.</td>
  </tr>
  <tr>
    <td align="right"><strong>Email</strong>:</td>
    <td><input name="email" type="text" id="email" size="30"></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><input type="submit" name="lost_password_btn" id="lost_password_btn" value="Get Lost Password"></td>
  </tr>
</table>
</form>
</div>
</div>
</div>
</body>
</html>
<?php
mysql_free_result($surgery_test_rs);
?>
