<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

?>
<?php
if (isset($_GET['surgery_id'])) {
  $surgery_id = $_GET['surgery_id'];
}
mysql_select_db($database_approval_rs, $approval_rs);
$query_surgery_test_rs = sprintf("SELECT
	surgeries.surgery_name,
	surgeries.contact_phone,
	surgery_text.surgery_front_screen
FROM
	surgeries
INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
WHERE
	surgeries.surgery_id = %s", GetSQLValueString($surgery_id, "int"));
$surgery_test_rs = mysql_query($query_surgery_test_rs, $approval_rs) or die(mysql_error());
$row_surgery_test_rs = mysql_fetch_assoc($surgery_test_rs);
$totalRows_surgery_test_rs = mysql_num_rows($surgery_test_rs);

$surgery_name = '<strong>'.$row_surgery_test_rs['surgery_name'].'</strong>';
$surgery_phone = '<strong>'.$row_surgery_test_rs['contact_phone'].'</strong>';
$front_screen_text = $row_surgery_test_rs['surgery_front_screen'];
$surgery_logo = 'logos/logo_'.$surgery_id.'.png';

$front_screen_text = str_replace('[surgery_name]', $surgery_name, $front_screen_text);
$front_screen_text = str_replace('[surgery_phone]', $surgery_phone, $front_screen_text);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>DocAppointment User Registration</title>
<link href="css/styles.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

var testNoFirst = Math.floor((Math.random() * 20) + 1);
var testNoSecond = Math.floor((Math.random() * 20) + 1);

var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }
 
function checkDate(field)
  {
    var allowBlank = false;
    var minYear = 1902;
    var maxYear = (new Date()).getFullYear();

    var errorMsg = "";

	re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
	
    if(field.value != '') {
      if(regs = field.value.match(re)) {
        if(regs[1] < 1 || regs[1] > 31) {
          errorMsg = "Invalid value for day: " + regs[1];
        } else if(regs[2] < 1 || regs[2] > 12) {
          errorMsg = "Invalid value for month: " + regs[2];
        } else if(regs[3] < minYear || regs[3] > maxYear) {
          errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear + " and " + maxYear;
        }
      } else {
        errorMsg = "- Invalid date format: " + field.value;
      }
    } else if(!allowBlank) {
      errorMsg = "- Date of birth is required.";
    }

    return errorMsg;
	
  }
  
  function checkEmail(field) {

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	var errorMsg = "";
	
	if (!filter.test(field.value)) {
		errorMsg = "- Email address is required.";
	} 
	if (!filter.test(field.value)) {
		errorMsg = "- Please provide a valid email address.";
	}
   
   return errorMsg;
   
 }
 
 function checkAddAnswer(field) {

	var question = testNoFirst + testNoSecond;
    var answer = parseInt(field.value);
 	var errorMsg = "";
	
	if (answer != question) {
		errorMsg = "- Please answer the test sum correctly.";
	}
   
   return errorMsg;
   
 }

function registerUser() {
	var errors = "";
	
	var first_name = document.getElementById('first_name');
	if (first_name.value == '') {
		errors+='- First name is required.\n';
	} 
	
	var last_name = document.getElementById('last_name');
	if (last_name.value == '') {
		errors+='- Last name is required.\n';
	} 
	
	var dob = document.getElementById('dob');
	var validDOB = checkDate(dob);
	if (validDOB != "") {
		errors+=validDOB+'\n';
	} 
	
	var email = document.getElementById('email');
	var validEmail = checkEmail(email);
	if (validEmail != "") {
		errors+=validEmail+'\n';
	} 

	var mobile = document.getElementById('mobile');
	if (mobile.value == '') {
		errors+='- Mobile is required.\n';
	} 
	
	var username = document.getElementById('user_username');
	if (username.value == '') {
		errors+='- Username is required.\n';
	} 
	
	var password = document.getElementById('user_password');
	if (password.value == '') {
		errors+='- Password is required.\n';
	} 
	var confirm_password = document.getElementById('confirm_password');
	if (confirm_password.value == '') {
		errors+='- Confirm password is required.\n';
	}
	 
	if (confirm_password.value != password.value && password.value != '' && confirm_password.value != '') {
		errors+='- Confirm password must match password.\n';
	}
	
	var validAnswer = checkAddAnswer(add_response);
	if (validAnswer != "") {
		errors+=validAnswer+'\n';
	} 
	
	if (errors.length > 0) {
		
		alert('The following error(s) occurred:\n'+errors);
		
  	} else {
		
		request.open("POST", "register_request.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		var params = "";
		request.send("first_name="+first_name.value+"&last_name="+last_name.value+"&dob="+dob.value+"&email="+email.value+"&mobile="+mobile.value+"&username="+username.value+"&password="+password.value+"");
		var strResult=request.responseText;
		var alert_display = document.getElementById('alert_display');
		alert_display.style.display = "block";
		alert_display.innerHTML = strResult;
		
	} 

}

</script>
</head>

<body>

<div id="wrapper">
<div id="header"><img src="images/header.png" width="800" height="100"></div>
<div id="login_text">
<p><strong>Welcome to <?php echo $row_surgery_test_rs['surgery_name']; ?> Online Appointments</strong></p>
<p><img name="" src="<?php echo $surgery_logo; ?>" alt=""><?php echo $front_screen_text; ?></p>
</div>
<div id="content">
<div style="display:none;" id="alert_display"></div>
  <form name="register_form"  method="POST" onSubmit="registerUser();return false;">
    <table border="0" align="center" cellpadding="8" cellspacing="0">
      <tr valign="baseline">
        <th colspan="2" nowrap><h2>New DocAppointment User</h2></th>
        </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>First Name:</strong></td>
        <td><input name="first_name" type="text" id="first_name" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Last Name:</strong></td>
        <td><input name="last_name" type="text" id="last_name" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Date of Birth:</strong></td>
        <td><input placeholder="DD/MM/YYYY" name="dob" type="text" id="dob" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Email:</strong></td>
        <td><input name="email" type="text" id="email" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Mobile:</strong></td>
        <td><input name="mobile" type="text" id="mobile" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Username:</strong></td>
        <td><input name="user_username" type="text" id="user_username" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td nowrap align="right"><strong>Password:</strong></td>
        <td><input name="user_password" type="password" id="user_password" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td align="right" nowrap><strong>Confirm Password:</strong></td>
        <td><input name="confirm_password" type="password" id="confirm_password" value="" size="32"></td>
      </tr>
      <tr valign="baseline">
        <td align="right" nowrap><strong>What is <script type="text/javascript">
document.write(testNoFirst);
</script> + <script type="text/javascript">
document.write(testNoSecond);
</script>?
 </strong></td>
        <td align="left" nowrap><input size="12" type="text" name="add_response" id="add_response"></td>
      </tr>
      <tr valign="baseline">
        <td colspan="2" align="right" nowrap><a style="margin-right:20px;" href="login.php">
          <input type="hidden" name="surgery_id" id="surgery_id" value="<?php echo $surgery_id; ?>">
          Cancel Registration</a><input name="register_user_btn" type="submit" id="register_user_btn" value="Register"></td>
        </tr>
    </table>
  </form>
</div>
</div>
</body>
</html>