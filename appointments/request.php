<?php

if(isset($_POST['getDoctors']))
{
	$post_array = array('getDoctors' => 'true');
}

if(isset($_POST['getDoctorsAdmin']))
{
	$post_array = array('getDoctors' => 'true');
}

if(isset($_POST['getAllDayAppointments']))
{
	$getDayAppointments= $_POST['getAllDayAppointments'];
	$jsonSendData= $_POST['jsonSendData'];
	$post_array = array('getAllDayAppointments' => $getDayAppointments, 'jsonSendData' => $jsonSendData);
}

if(isset($_POST['checkAppointmentAvailability']))
{
	$checkAppointmentAvailability= $_POST['checkAppointmentAvailability'];
	$jsonSendData= $_POST['jsonSendData'];
	$post_array = array('checkAppointmentAvailability' => $checkAppointmentAvailability, 'jsonSendData' => $jsonSendData);
}

if(isset($_POST['getAllWeekAppointments']))
{
	$getAllWeekAppointments= $_POST['getAllWeekAppointments'];
	$jsonSendData= $_POST['jsonSendData'];
	$post_array = array('getAllWeekAppointments' => $getAllWeekAppointments, 'jsonSendData' => $jsonSendData);
}

if(isset($_POST['makeAppointmentRequest']))
{
	$makeAppointmentRequest= $_POST['makeAppointmentRequest'];
	$jsonSendData= $_POST['jsonSendData'];
	$post_array = array('makeAppointmentRequest' => $makeAppointmentRequest, 'jsonSendData' => $jsonSendData);
}

if(isset($_POST['cancelAppointment']))
{
	$cancelAppointment= $_POST['cancelAppointment'];
	$jsonSendData= $_POST['jsonSendData'];
	$post_array = array('cancelAppointment' => $cancelAppointment, 'jsonSendData' => $jsonSendData);
}


if(isset($_POST['makePassword']))
{
	$makePassword= $_POST['makePassword'];
	$post_array = array('makePassword' => $makePassword);
}

$r = new HttpRequest('https://oakleigh.docappointment.com.au:8443/cmcd_bp_json.php', HttpRequest::METH_POST);
$r->addPostFields($post_array);

try {
    echo $r->send()->getBody();
} catch (HttpException $ex) {
    echo $ex;
}
?>