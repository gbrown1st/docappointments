<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

if (isset($_GET['logout']) && $_GET['logout']==1) {
		
	session_unset ();
				
} 

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strusers_new, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users_new based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrusers_new = Explode(",", $strusers_new); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrusers_new)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users_new based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strusers_new == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = $_GET['logout_url'];
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedusers_new, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  
  header("Location: ". $MM_restrictGoTo); 
  exit;
}

$MM_authorizedusers_new = "";
$MM_donotCheckaccess = "true";

$set_user_surgery = false;

$user_id = $_SESSION['user_id'];

if (isset($_COOKIE['surgery_id'])) {
	$surgery_id = $_COOKIE['surgery_id'];
	$set_user_surgery = true;
} else {
	if (isset($_GET['set_surgery'])) {
		$surgery_id = $_GET['surgery_id'];
		if ($_GET['set_surgery'] == "1") {
			setcookie("surgery_id", $surgery_id, time()+7*60*60*24*30);
			$set_user_surgery = true;
		} else {
			setcookie("surgery_id", "", time()-(60*60*24));
		}
	} else {
		header("Location: surgeries.php?change_surgery=1"); 
	}
}

if ($set_user_surgery) {
	
	mysql_select_db($database_approval_rs, $approval_rs);
	$set_user_surgery_sql = sprintf("replace into user_surgeries (surgery_id, user_id) values (%s, %s)", GetSQLValueString($surgery_id, "int"), GetSQLValueString($user_id, "int"));
	
	$set_user_surgery_rs = mysql_query($set_user_surgery_sql, $approval_rs) or die(mysql_error());
	
	if (!$set_user_surgery_rs) {
		header("Location: surgeries.php?change_surgery=1"); 
	}

}

mysql_select_db($database_approval_rs, $approval_rs);
$query_surgery_details_rs = sprintf("SELECT
	surgeries.contact_phone,
	surgeries.surgery_name,
	surgeries.surgery_address_1,
	surgeries.surgery_address_2,
	surgeries.surgery_suburb_town,
	surgeries.surgery_state,
	surgeries.surgery_postcode,
	surgeries.surgery_software,
	surgery_text.surgery_front_screen,
	server_url.log_out_url
FROM
	surgeries
INNER JOIN surgery_text ON surgeries.surgery_id = surgery_text.surgery_id
INNER JOIN server_url ON surgeries.surgery_id = server_url.surgery_id
WHERE
	surgeries.surgery_id = %s", GetSQLValueString($surgery_id, "int"));
$surgery_details_rs = mysql_query($query_surgery_details_rs, $approval_rs) or die(mysql_error());
$row_surgery_details_rs = mysql_fetch_assoc($surgery_details_rs);
$totalRows_surgery_details_rs = mysql_num_rows($surgery_details_rs);
echo $surgery_software;

$surgery_software = $row_surgery_details_rs['surgery_software'];

mysql_select_db($database_approval_rs, $approval_rs);

if ($surgery_software == 'Stat') {
	$query_doctors_rs = sprintf("SELECT
	stat_doctors.doctor_id,
	stat_doctors.doctor_name,
	stat_doctors.new_patients,
	stat_doctors.show_all_appointments,
	stat_doctors.clinic_code,
	stat_doctors.multiple_appointments,
	stat_doctors.allow_prescription_requests
FROM
	stat_doctors
WHERE
	stat_doctors.surgery_id = %s
AND online_status = 'yes'", GetSQLValueString($surgery_id, "int"));
} else {
	$query_doctors_rs = sprintf("SELECT
	zedmed_doctors.doctor_id,
	zedmed_doctors.doctor_name,
	zedmed_doctors.doctor_code,
	zedmed_doctors.new_patients,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.multiple_appointments,
	zedmed_doctors.allow_prescription_requests,
	zedmed_doctors.clinic_code,
	zedmed_doctors.appointment_length,
	zedmed_doctors.appointment_book_id
FROM
	zedmed_doctors
WHERE
	zedmed_doctors.surgery_id = %s
AND online_status = 'yes'", GetSQLValueString($surgery_id, "int"));
}

$doctors_rs = mysql_query($query_doctors_rs, $approval_rs) or die(mysql_error());
$doctors_array = array();
while ($row_doctors_rs = mysql_fetch_assoc($doctors_rs)) {
	$doctors_array[] = array("doctor_id"=>$row_doctors_rs['doctor_id'], "doctor_name"=>$row_doctors_rs['doctor_name'], "doctor_code"=>$row_doctors_rs['doctor_code'], "new_patients"=>$row_doctors_rs['new_patients'], "show_all_appointments"=>$row_doctors_rs['show_all_appointments'], "multiple_appointments"=>$row_doctors_rs['multiple_appointments'], "allow_prescription_requests"=>$row_doctors_rs['allow_prescription_requests'], "clinic_code"=>$row_doctors_rs['clinic_code'], "appointment_length"=>$row_doctors_rs['appointment_length'], "appointment_book_id"=>$row_doctors_rs['appointment_book_id']);
}

mysql_select_db($database_approval_rs, $approval_rs);
$query_user_details = sprintf("SELECT first_name, last_name, dob, email, mobile FROM users_new WHERE user_id = %s", GetSQLValueString($user_id, "int"));
$user_details = mysql_query($query_user_details, $approval_rs) or die(mysql_error());
$row_user_details = mysql_fetch_assoc($user_details);
$email = $row_user_details['email'];
$totalRows_user_details = mysql_num_rows($user_details);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $row_surgery_details_rs['surgery_name']; ?> DocAppointment</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700">
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cuprum:700">
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100">

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">


    <!-- Respomsive slider -->
    <link href="css/responsive-calendar.css" rel="stylesheet">
    
<link href="css/styles.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="themes/base/jquery.ui.all.css">

<script src="ui/jquery.ui.core.js" type="text/javascript"></script> 
<script src="ui/jquery.ui.dialog.js" type="text/javascript"></script>

<!--[if lte IE 7]>

<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script type="text/javascript">

var selected_dr;
var selected_dr_id;
var selected_dr_row;
var selected_appt_title;
var selected_appt_description;
var selected_appt_time;
var selected_appt_date = new Date();
var override_get_appointments = false;
var reset_selected_date = false;
var month;
var year;
var number_of_doctors = <?php echo $totalRows_doctors_rs; ?>;
var selected_patient;
var appt_scroll = 0;
	
var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }

var ALERT_TITLE = "DocAppointment Alert!";
var ALERT_BUTTON_TEXT = "OK";

if(document.getElementById) {
	window.alert = function(txt) {
		createCustomAlert(txt);
	}
}

function createCustomAlert(txt) {
	d = document;

	if(d.getElementById("modalContainer")) return;

	mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
	mObj.id = "modalContainer";
	mObj.style.height = d.documentElement.scrollHeight + "px";
	
	alertObj = mObj.appendChild(d.createElement("div"));
	alertObj.id = "alertBox";
	if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
	alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
	alertObj.style.visiblity="visible";

	h1 = alertObj.appendChild(d.createElement("h1"));
	h1.appendChild(d.createTextNode(ALERT_TITLE));

	msg = alertObj.appendChild(d.createElement("p"));
	//msg.appendChild(d.createTextNode(txt));
	msg.innerHTML = txt;

	btn = alertObj.appendChild(d.createElement("a"));
	btn.id = "closeBtn";
	btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
	btn.href = "#";
	btn.focus();
	btn.onclick = function() { removeCustomAlert();return false; }

	alertObj.style.display = "block";
	
}

function removeCustomAlert() {
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}

function setSelectedDoctor(dr_row, dr_name, doctor_id) {
	
	selected_dr_row = dr_row;
	selected_dr = dr_name;
	selected_dr_id = doctor_id;
	
	for (i=0;i<number_of_doctors + 1;i++) {
		
		var clear_row = document.getElementById("dr_"+i);
		clear_row.style.color = "#999999";
		clear_row.style.backgroundColor = "#FFFFFF";
		
		if (clear_row.getAttribute('doctor_id') == selected_dr_id) {
			selected_dr_row = "dr_"+i;
		}
		
	}
	
	var set_row = document.getElementById(selected_dr_row);
	set_row.style.color = "#EF2A21";
	set_row.style.backgroundColor = "#E3E8E4";
	
}

function getFirstAvailableAppointments() {
	
		var theDiv = document.getElementById('apDiv1');
		theDiv.style.right = '-1000px';

		$('#calendar').responsiveCalendar('clearAll');
		
		var appt_container = document.getElementById("appt_list");
		
		appt_container.innerHTML = '<div style="width:65%;margin-left:auto;margin-right:auto;margin-top:10px;text-align:center;color: rgb(239, 42, 33);"><p>Fetching appointments...please wait.</p><img width="41" height="39" src="images/loading.gif"></div>';
			
		setSelectedDoctor("dr_0", "", "");
		
		request.open("POST", "first_appointments.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_id=<?php echo $surgery_id; ?>");
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		
		if (objLength(apptJSON) == 0) {
		
			appt_container.innerHTML = '<div style="width:65%;margin-left:auto;margin-right:auto;margin-top:10px;text-align:center;color: rgb(239, 42, 33);"><p>No appointments.</p></div>';
			
		} else {
			
			appt_container.innerHTML = '';
			
		}
		
		for (i=0;i<apptJSON.length;i++) {
			
				var appt_description = "<strong>"+apptJSON[i]['appt_description'] + "</strong><br />Available with "+apptJSON[i]['doctor_name']+" (" + apptJSON[i]['appt_length'] / 60 + " mins)";
				
			appt_container.innerHTML +=	'<div onClick="showBox(\'make\','+apptJSON[i]['appt_id']+','+apptJSON[i]['appt_length']+','+apptJSON[i]['patient_id']+','+apptJSON[i]['appt_secs']+','+apptJSON[i]['appt_pos']+',\''+apptJSON[i]['doctor_name']+'\',\''+apptJSON[i]['doctor_id']+'\',\''+apptJSON[i]['appt_description']+'\',\''+apptJSON[i]['appt_date']+'\',\''+apptJSON[i]['appt_date_time']+'\',\''+apptJSON[i]['new_patients']+'\');" class="appointment" id="appt_' + i + '">' + appt_description + '</div>';
				
		}
	
}

function getDayAppointments(dr, date) {
	
		var theDiv = document.getElementById('apDiv1');
		theDiv.style.right = '-1000px';
		
		var appt_container = document.getElementById("appt_list");
		
		appt_container.innerHTML = '<div style="width:65%;margin-left:auto;margin-right:auto;margin-top:10px;text-align:center;color: rgb(239, 42, 33);"><p>Fetching appointments...please wait.</p><img width="41" height="39" src="images/loading.gif"></div>';
		
		request.open("POST", "day_appointments.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_id=<?php echo $surgery_id; ?>&doctor_id="+dr+"&apptDate="+date+"&u="+<?php echo $user_id; ?>);
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		var appt_description = '';
		
		appt_container.innerHTML = '';
		
		for (i=0;i<apptJSON.length;i++) {
			
			if (apptJSON[i]['appt_info'] != "") {
				
				appt_description = '<strong>' + apptJSON[i]['appt_description'] + '</strong><br /><span style="color: rgb(239, 42, 33); font-size:12px;">'+apptJSON[i]['appt_info']+'</span>';
				appt_container.innerHTML +=	'<div onClick="showBox(\'cancel\','+apptJSON[i]['appt_id']+','+apptJSON[i]['appt_length']+','+apptJSON[i]['patient_id']+','+apptJSON[i]['appt_secs']+','+apptJSON[i]['appt_pos']+',\''+apptJSON[i]['doctor_name']+'\',\''+apptJSON[i]['doctor_id']+'\',\''+apptJSON[i]['appt_description']+'\',\''+apptJSON[i]['appt_date']+'\',\''+apptJSON[i]['appt_date_time']+'\',\''+apptJSON[i]['appt_info']+'\',\''+apptJSON[i]['new_patients']+'\');" class="appointment" id="appt_' + i + '">' + appt_description + '</div>';
				
			} else {
				
				appt_description = "<strong>"+apptJSON[i]['appt_description'] + "</strong><br />Available with "+apptJSON[i]['doctor_name']+" (" + apptJSON[i]['appt_length'] / 60 + " mins)";
				appt_container.innerHTML +=	'<div onClick="showBox(\'make\','+apptJSON[i]['appt_id']+','+apptJSON[i]['appt_length']+','+apptJSON[i]['patient_id']+','+apptJSON[i]['appt_secs']+','+apptJSON[i]['appt_pos']+',\''+apptJSON[i]['doctor_name']+'\',\''+apptJSON[i]['doctor_id']+'\',\''+apptJSON[i]['appt_description']+'\',\''+apptJSON[i]['appt_date']+'\',\''+apptJSON[i]['appt_date_time']+'\',\'\',\''+apptJSON[i]['new_patients']+'\');" class="appointment" id="appt_' + i + '">' + appt_description + '</div>';
				
			}
				
		}
		
		var appt_list = document.getElementById("appt_list");
		appt_list.scrollTop = appt_scroll; 
		appt_scroll = 0;
	
	
}

function triggerAppointments() {
	
	getAppointments(selected_dr_row, selected_dr, selected_dr_id) ;
	
}

function objLength(obj){
  var i=0;
  for (var x in obj){
    if(obj.hasOwnProperty(x)){
      i++;
    }
  } 
  return i;
}

function getAppointments(dr_row, dr_name, doctor_id) {
	
	if (override_get_appointments) {
		
		override_get_appointments = false;
		return;
		
	}
	
	
	var theDiv = document.getElementById('apDiv1');
	theDiv.style.right = '-1000px';
	
	$('#calendar').responsiveCalendar('clearAll');
	
	var appt_container = document.getElementById("appt_list");
		
	appt_container.innerHTML = '<div style="width:65%;margin-left:auto;margin-right:auto;margin-top:10px;text-align:center;color: rgb(239, 42, 33);"><p>Fetching appointments...please wait.</p><img width="41" height="39" src="images/loading.gif"></div>';
	
	setSelectedDoctor(dr_row, dr_name, doctor_id);
	
	request.open("POST", "appointments_dates.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send("surgery_id=<?php echo $surgery_id; ?>&doctor_id="+doctor_id+"&month="+(month+1)+"&year="+year+"&u=<?php echo $user_id; ?>");
		
	var strResult=request.responseText;
	
	var apptJSON = JSON.parse(strResult);
	
	if (objLength(apptJSON) == 0) {
		
		appt_container.innerHTML = '<div style="width:65%;margin-left:auto;margin-right:auto;margin-top:10px;text-align:center;color: rgb(239, 42, 33);"><p>No appointments.</p></div>';
		
	} else {
		
		appt_container.innerHTML = '';
		
	}
	
		
	$('#calendar').responsiveCalendar('edit', apptJSON);
		
}

$(function() {
	$( "#dialog-confirm" ).dialog({
	autoOpen: false,
	resizable: false,
	height:290,
	modal: true,
	buttons: {
	"Yes": function() {
	location.assign("appointments.php?surgery_id="+selected_surgery_id+"&set_surgery=1");
	},
	"No": function() {
	location.assign("appointments.php?surgery_id="+selected_surgery_id+"&set_surgery=0");
	},
	Cancel: function() {
	$( this ).dialog( "close" );
	}
	}
	});
});

function updateTransition(proceess_type, appt_id, appt_length, patient_id, appt_secs, appt_pos, doctor_id, appt_description, appt_date, appt_date_time, new_patients) {
	
	var theDiv = document.getElementById('apDiv1');

	var str = appt_description;
	var res = str.toString().split(" "); 
	
	var number_of_doctors = <?php echo $totalRows_doctors_rs; ?>;
	
	selected_appt_date = res[0] + " " + res[1] + " " + res[2] + " " + res[3];
	selected_appt_time = res[4] + " " + res[5];
	
	if (proceess_type == "make") {
		
		var appt_descr = "<h2>Are you sure you would like to book this appointment?</h2><p><strong>Doctor: </strong>" + selected_dr + "</p><p><strong>Date: </strong>" + selected_appt_date + "</p><p><strong>Time: </strong>" + selected_appt_time + "</p>";
		
		var fm_buttons = '<div class="button_holder"><div onclick="addFamilyMember()" id="add_family_member_btn" class="book_buttons">Add Family Member</div><div id="edit_family_member_btn" onclick="editFamilyMember()" class="book_buttons">Edit Family Member</div></div>';
		
		var fm_form = '<div id="family_member_form_holder" style="display:none;"></div>';
	
		var appointment_for = "<p style='margin-top:10px;'><select onchange='selectFamilyMember();' id='appointment_for'><option user_type='none' value='0'>You can add or update family members here:</option><option user_type='user' f='<?php echo $row_user_details['first_name']; ?>' l='<?php echo $row_user_details['last_name']; ?>' d='<?php echo $row_user_details['dob']; ?>' m='<?php echo $row_user_details['mobile']; ?>' value='<?php echo $user_id; ?>'><?php echo $row_user_details['first_name']; ?> <?php echo $row_user_details['last_name']; ?></option></select></p>";
		
		var book_buttons = '<div style="margin-top:10px;background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div><div class="button_holder"><div onclick="showBox()" id="cancel_btn" class="book_buttons">Cancel</div><div id="book_btn" onclick="makeAppointment('+appt_id+','+appt_length+','+patient_id+','+appt_secs+','+appt_pos+',\''+doctor_id+'\',\''+appt_description+'\',\''+appt_date+'\',\''+appt_date_time+'\')" class="book_buttons">Book Appointment</div></div>';
		
		var pr_buttons = '<div style="margin-top:48px;background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div><div class="button_holder" id="pr_display" style="display:none;"><div id="show_pr_btn" onclick="showPR()"  style="float: right; margin-top: 0px;margin-bottom: 10px;" class="book_buttons">Request Prescription</div><div id="pr_form" style="margin-top: 14px;clear:right;display:none;"></div></div>';
				
		theDiv.innerHTML = '<div id="close_iframe" onclick="showBox()">CLOSE</div><div style="background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div>'+appt_descr+''+fm_buttons+appointment_for+fm_form+book_buttons+pr_buttons;
		
		getFamilyMembers () ;
		getPrDoctors();
	
	} else if (proceess_type == "prescription") {
		
		var appt_descr = "<h2>Prescription Request</h2><p><strong>Doctor: </strong>" + selected_dr + "</p>";
		
		var fm_buttons = '<div class="button_holder"><div onclick="addFamilyMember()" id="add_family_member_btn" class="book_buttons">Add Family Member</div><div id="edit_family_member_btn" onclick="editFamilyMember()" class="book_buttons">Edit Family Member</div></div>';
		
		var fm_form = '<div id="family_member_form_holder" style="display:none;"></div>';
	
		var appointment_for = "<p style='margin-top:10px;'><select onchange='selectFamilyMember();' id='appointment_for'><option user_type='none' value='0'>Add or update a family member:</option><option user_type='user' f='<?php echo $row_user_details['first_name']; ?>' l='<?php echo $row_user_details['last_name']; ?>' d='<?php echo $row_user_details['dob']; ?>' m='<?php echo $row_user_details['mobile']; ?>' value='<?php echo $user_id; ?>'><?php echo $row_user_details['first_name']; ?> <?php echo $row_user_details['last_name']; ?></option></select></p>";
		
		var pr_buttons = '<div style="margin-top:48px;background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div><div class="button_holder" id="pr_display" style="display:block;"><div id="show_pr_btn" onclick="showPR()"  style="float: right; margin-top: 0px;margin-bottom: 10px;" class="book_buttons">Request Prescription</div><div id="pr_form" style="margin-top: 14px;clear:right;display:none;"></div></div>';
				
		theDiv.innerHTML = '<div id="close_iframe" onclick="showBox()">CLOSE</div><div style="background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div>'+appt_descr+''+fm_buttons+appointment_for+fm_form+pr_buttons;
		
		getFamilyMembers () ;
		getPrDoctors();
		showPR();
		
		for (i=0;i<number_of_doctors + 1;i++) {
			var clear_row = document.getElementById("dr_"+i);
			clear_row.style.color = "#999999";
			clear_row.style.backgroundColor = "#FFFFFF";
		}
		
		var set_row = document.getElementById(selected_dr_row);
		set_row.style.color = "#EF2A21";
		set_row.style.backgroundColor = "#E3E8E4";
		
		var pr_dr_select = document.getElementById("pr_dr_select");
		
		for (i=0;i<pr_dr_select.length;i++) {
			
			if (pr_dr_select.options[i].innerHTML == selected_dr) {
				
				pr_dr_select.selectedIndex = i;
				
			} 
		}
		
	
	} else {
		
		var appt_descr = "<h2>Are you sure you would like to cancel this appointment?</h2><p><strong>Patient: </strong>" + selected_patient + "</p><p><strong>Doctor: </strong>" + selected_dr + "</p><p><strong>Date: </strong>" + selected_appt_date + "</p><p><strong>Time: </strong>" + selected_appt_time + "</p>";
		
		var book_buttons = '<div style="margin-top:10px;background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div><div class="button_holder"><div onclick="showBox()" id="cancel_btn" class="book_buttons" style="width:80px">Close</div><div id="book_btn" onclick="cancelAppointment(\''+proceess_type+'\','+appt_id+','+appt_length+','+patient_id+','+appt_secs+','+appt_pos+',\''+doctor_id+'\',\''+appt_description+'\',\''+appt_date+'\',\''+appt_date_time+'\')" class="book_buttons" style="width:160px">Cancel Appointment</div></div>';
				
		theDiv.innerHTML = '<div id="close_iframe" onclick="showBox()">CLOSE</div><div style="background-color: #CCCCCC;clear: right;height: 1px; margin-bottom:10px;"></div>'+appt_descr+''+book_buttons;
		
	}
	
}

function cancelFamilyMember() {
	
	var family_member_form = document.getElementById("family_member_form_holder");
	family_member_form.style.display = "none";
	
}

function selectFamilyMember() {
	
	var appointment_for = document.getElementById("appointment_for");
	var family_member_form = document.getElementById("family_member_form_holder");
	
	family_member_form.style.display = "none";
	
}

function checkDate(field)
  {
    var allowBlank = false;
    var minYear = 1902;
    var maxYear = (new Date()).getFullYear();

    var errorMsg = "";

	re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
	
    if(field.value != '') {
      if(regs = field.value.match(re)) {
        if(regs[1] < 1 || regs[1] > 31) {
          errorMsg = "Invalid value for day: " + regs[1];
        } else if(regs[2] < 1 || regs[2] > 12) {
          errorMsg = "Invalid value for month: " + regs[2];
        } else if(regs[3] < minYear || regs[3] > maxYear) {
          errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear + " and " + maxYear;
        }
      } else {
        errorMsg = "- Invalid date format: " + field.value;
      }
    } else if(!allowBlank) {
      errorMsg = "- Date of birth is required.";
    }

    return errorMsg;
	
  }
  
  function getFamilyMembers () {
	  
	  	var appointment_for = document.getElementById("appointment_for");
		
		var appointment_for_html = appointment_for.innerHTML;
		
		appointment_for.selectedIndex = 1;
		
	  	var u = appointment_for.value;
	  
		request.open("POST", "family_members.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("u="+u);
			
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		
		var fms = "";
		
		if (apptJSON.length > 0) {
			
			for (i=0;i<apptJSON.length;i++) {
				
				fms +=	"<option user_type='fm' fid='"+apptJSON[i]['family_member_id']+"' f='"+apptJSON[i]['first_name']+"' l='"+apptJSON[i]['last_name']+"' d='"+apptJSON[i]['dob']+"' m='"+apptJSON[i]['mobile']+"' value='"+apptJSON[i]['user_id']+"'>"+apptJSON[i]['first_name']+" "+apptJSON[i]['last_name']+"</option>";
					
			}
			
			appointment_for_html += fms; 
			
			appointment_for.innerHTML = appointment_for_html;
		
		}
		
  }
  
  function newFamilyMember () {
	  
	  	var appointment_for = document.getElementById("appointment_for");
		
		appointment_for.selectedIndex = 1;
		
	  	var u = appointment_for.value;
	  	var f = document.getElementById("first_name").value;
		var l = document.getElementById("last_name").value;
		var dob = document.getElementById("dob");
		var d = document.getElementById("dob").value;
		var m = document.getElementById("mobile").value;
		
		var errors = "";
	
		if (f == '') {
			errors+='- First name is required.<br />';
		} 
		
		if (l == '') {
			errors+='- Last name is required.<br />';
		} 
		
		var validDOB = checkDate(dob);
		if (validDOB != "") {
			errors+=validDOB+'<br />';
		} 
		
		if (m == '') {
			errors+='- Mobile is required.<br />';
		} 
		
	  	if (errors.length > 0) {
		
			ALERT_TITLE = 'Missing Information!';
			alert('The following error(s) occurred:<br />'+errors);
		
		} else {
			
			request.open("POST", "new_fm.php", false);
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			request.send("u="+u+"&f="+f+"&l="+l+"&d="+d+"&m="+m);
			
			var strResult=request.responseText;
			var apptJSON = JSON.parse(strResult);
			
			ALERT_TITLE = apptJSON[0]['heading'];
			alert(apptJSON[0]['message']);
			showBox();
		}
	  
  }
  
  function saveFamilyMember () {
	  
	  	var appointment_for = document.getElementById("appointment_for");
		
	  	var u = appointment_for.value;
		var t = appointment_for.options[appointment_for.selectedIndex].getAttribute('user_type');
	  	var f = document.getElementById("first_name").value;
		var l = document.getElementById("last_name").value;
		var dob = document.getElementById("dob");
		var d = document.getElementById("dob").value;
		var m = document.getElementById("mobile").value;
		
		var errors = "";
	
		if (f == '') {
			errors+='- First name is required.<br />';
		} 
		
		if (l == '') {
			errors+='- Last name is required.<br />';
		} 
		
		var validDOB = checkDate(dob);
		if (validDOB != "") {
			errors+=validDOB+'<br />';
		} 
		
		if (m == '') {
			errors+='- Mobile is required.<br />';
		} 
		
	  	if (errors.length > 0) {
			
			ALERT_TITLE = 'Missing Information!';
			alert('The following error(s) occurred:<br />'+errors);
		
		} else {
			
			var url = "";
			
			if (t == "user") {
				url = "edit_user_fm.php";
			} else {
				u =  appointment_for.options[appointment_for.selectedIndex].getAttribute('fid');
				url = "edit_fm.php";
			}
			
			request.open("POST", url, false);
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			request.send("u="+u+"&f="+f+"&l="+l+"&d="+d+"&m="+m);
			
			var strResult=request.responseText;
			var apptJSON = JSON.parse(strResult);
			
			if (t == "user") {
				
				ALERT_TITLE = apptJSON[0]['heading'];
				alert(apptJSON[0]['message'] + "\n\nYou have changed the logged in user's details. The page must reload.");
				window.location.assign(window.location.pathname)
				
			} else {
				
				showBox();
				ALERT_TITLE = apptJSON[0]['heading'];
				alert(apptJSON[0]['message']);
			}
		}
	  
  }
  
  
function addFamilyMember() {
	
	var appointment_for = document.getElementById("appointment_for");
	var family_member_form = document.getElementById("family_member_form_holder");
	
	var thePRDiv = document.getElementById('pr_form');
	thePRDiv.style.display = 'none';
	
	appointment_for.selectedIndex = 0;
	 
		var fm_form = '<form action="" method="get" name="family_member_form" style="padding-left: 20px;">';
  fm_form += '<table border="0" cellspacing="0" cellpadding="8">';
    fm_form += '<tr>';
      fm_form += '<th colspan="2" align="center" id="family_edit_add">Add Family Member</th>';
      fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td><label>First Name:';
        fm_form += '<input type="text" name="first_name" id="first_name">';
      fm_form += '</label></td>';
      fm_form += '<td><label>Last Name:';
        fm_form += '<input type="text" name="last_name" id="last_name">';
      fm_form += '</label></td>';
    fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td><label>Date of Birth:';
        fm_form += '<input type="text" name="dob" id="dob">';
      fm_form += '</label></td>';
      fm_form += '<td><label>Mobile Number:';
        fm_form += '<input type="text" name="mobile" id="mobile">';
      fm_form += '</label></td>';
    fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td></td>';
      fm_form += '<td style="padding-right: 32px;"><div onclick="cancelFamilyMember()" id="cancel_family_member_btn" class="book_buttons">Cancel</div><div onclick="newFamilyMember()" id="save_family_member_btn" class="book_buttons">Save</div></td>';
    fm_form += '</tr>';
  fm_form += '</table>';
fm_form += '</form>';
		
		family_member_form.innerHTML = fm_form;
		family_member_form.style.display = "block";
	
}

function editFamilyMember() {
	
	var appointment_for = document.getElementById("appointment_for");
	var family_member_form = document.getElementById("family_member_form_holder");
	
	var thePRDiv = document.getElementById('pr_form');
	thePRDiv.style.display = 'none';
	
	var f = appointment_for.options[appointment_for.selectedIndex].getAttribute('f');
	var l = appointment_for.options[appointment_for.selectedIndex].getAttribute('l');
	var d = appointment_for.options[appointment_for.selectedIndex].getAttribute('d');
	var m = appointment_for.options[appointment_for.selectedIndex].getAttribute('m');
		
	if (appointment_for.selectedIndex == 0) {
		
		family_member_form.style.display = "none";
		ALERT_TITLE = "No Family Member";
		alert("Please select a family member.");
		
	} else {
		
		var fm_form = '<form action="" method="get" name="family_member_form" style="padding-left: 20px;">';
  fm_form += '<table border="0" cellspacing="0" cellpadding="8">';
    fm_form += '<tr>';
      fm_form += '<th colspan="2" align="center" id="family_edit_add">Edit Family Member</th>';
      fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td><label>First Name:';
        fm_form += '<input type="text" name="first_name" id="first_name" value="'+f+'">';
      fm_form += '</label></td>';
      fm_form += '<td><label>Last Name:';
        fm_form += '<input type="text" name="last_name" id="last_name" value="'+l+'">';
      fm_form += '</label></td>';
    fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td><label>Date of Birth:';
        fm_form += '<input type="text" name="dob" id="dob" value="'+d+'">';
      fm_form += '</label></td>';
      fm_form += '<td><label>Mobile Number:';
        fm_form += '<input type="text" name="mobile" id="mobile" value="'+m+'">';
      fm_form += '</label></td>';
    fm_form += '</tr>';
    fm_form += '<tr>';
      fm_form += '<td></td>';
      fm_form += '<td style="padding-right: 32px;"><div onclick="cancelFamilyMember()" id="cancel_family_member_btn" class="book_buttons">Cancel</div><div onclick="saveFamilyMember()" id="save_family_member_btn" class="book_buttons">Save</div></td>';
    fm_form += '</tr>';
  fm_form += '</table>';
fm_form += '</form>';
		
		family_member_form.innerHTML = fm_form;
		family_member_form.style.display = "block";
		
	}
	
	
}

function makeAppointment(appt_id, appt_length, patient_id, appt_secs, appt_pos, doctor_id, appt_description, appt_date, appt_date_time) {
	
	var appt_list = document.getElementById("appt_list");
    appt_scroll = appt_list.scrollTop; 
	
	var appointment_for = document.getElementById("appointment_for");
	var u = appointment_for.value;
	var t = appointment_for.options[appointment_for.selectedIndex].getAttribute('user_type');
	var f = appointment_for.options[appointment_for.selectedIndex].getAttribute('f');
	var l = appointment_for.options[appointment_for.selectedIndex].getAttribute('l');
	var d = appointment_for.options[appointment_for.selectedIndex].getAttribute('d');
	var m = appointment_for.options[appointment_for.selectedIndex].getAttribute('m');
	
	if (appointment_for.selectedIndex === 0) {
		
		ALERT_TITLE = "No Family Member";
		alert("Please select a family member.");
		
	} else {
		
		var theDiv = document.getElementById('apDiv1');
		theDiv.style.right = '-1000px';
		
		request.open("POST", "make_appointment.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_id=<?php echo $surgery_id; ?>&doctor_id="+doctor_id+"&appt_patient_id="+patient_id+"&appt_id="+appt_id+"&appt_time="+appt_description+"&appt_date_time="+appt_date_time+"&appt_day="+appt_date+"&appt_length="+appt_length+"&appt_secs="+appt_secs+"&appt_pos="+appt_pos+"&first_name="+f+"&last_name="+l+"&dob="+d+"&mobile="+m+"&email=graham@mediarare.com.au&doctor_name="+selected_dr);
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		reset_selected_date = true;
	
		getAppointments(selected_dr_row, selected_dr, selected_dr_id);
		
		ALERT_TITLE = apptJSON[0]['heading'];
		alert(apptJSON[0]['message']);
		getDayAppointments(doctor_id, appt_date)
		
	}
	
}

function cancelAppointment(proceess_type, appt_id, appt_length, patient_id, appt_secs, appt_pos, doctor_id, appt_description, appt_date, appt_date_time) {

		var theDiv = document.getElementById('apDiv1');
		theDiv.style.right = '-1000px';
		
		var appt_list = document.getElementById("appt_list");
    	appt_scroll = appt_list.scrollTop; 
		
		request.open("POST", "cancel_appointment.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_id=<?php echo $surgery_id; ?>&doctor_id="+doctor_id+"&patient_id="+patient_id+"&appt_id="+appt_id+"&appt_info="+proceess_type+"&appt_time="+appt_description+"&appt_day="+appt_date+"&appt_length="+appt_length+"&appt_secs="+appt_secs+"&appt_pos="+appt_pos+"&email=graham@mediarare.com.au&doctor_name="+selected_dr);
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		reset_selected_date = true;
		getAppointments(selected_dr_row, selected_dr, selected_dr_id);
		ALERT_TITLE = apptJSON[0]['heading'];
		alert(apptJSON[0]['message']);
		getDayAppointments(doctor_id, appt_date)
		
}

function showBox(proceess_type, appt_id, appt_length, patient_id, appt_secs, appt_pos, doctor_name, doctor_id, appt_description, appt_date, appt_date_time, patient, new_patients) {
	
	if (new_patients == "no") {
		
		if (!newPatient(doctor_id)) {
			
			return;
		}
		
	}
	
	selected_dr = doctor_name;
	selected_dr_id = doctor_id;
	selected_patient = patient;
	
	var theDiv = document.getElementById('apDiv1');
	if (proceess_type == "prescription" || proceess_type == "make") {
		theDiv.style.right = '20px';
	} else {
		
		if (theDiv.style.right == '-1000px') {
			theDiv.style.right = '20px';
		} else {
			theDiv.style.right = '-1000px';
		}
	
	}
	
	updateTransition(proceess_type, appt_id, appt_length, patient_id, appt_secs, appt_pos, doctor_id, appt_description, appt_date, appt_date_time, new_patients);
	
}

function getPrDoctors() {

		hasPRDoctors = false;
		
		request.open("POST", "pr_doctors.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_id=<?php echo $surgery_id; ?>");
		
		var theDiv = document.getElementById('pr_display');
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		
		
		if (apptJSON.length > 1) {
			
			var pr_dr_select_string = '<select style="display:block;" id="pr_dr_select" onchange="">';
			
			for (i=0;i<apptJSON.length;i++) {
				
				pr_dr_select_string += '<option value="'+apptJSON[i]['doctor_id']+'">'+apptJSON[i]['doctor_name']+'</option>';
				
			}
			
			pr_dr_select_string += '</select>';
			
			var pr_patient_select_string = '<select style="display:block;margin-top:10px;" id="pr_patient_select" onchange="">';
			
			pr_patient_select_string += '<option value="0">Select the family member for the prescription:</option>';
			
			var selectobject=document.getElementById("appointment_for");
			
			for (var i=1; i<selectobject.length; i++){
				
				if (i==1) {
					pr_patient_select_string += '<option value="'+selectobject.options[i].value+'">'+selectobject.options[i].text+'</option>';
				} else {
					pr_patient_select_string += '<option value="'+selectobject.options[i].getAttribute('fid')+'">'+selectobject.options[i].text+'</option>';
				}
				
			}
			
			pr_patient_select_string += '</select>';
			
			  var fm_form = '<table border="0" cellspacing="0" cellpadding="8" style="margin-top: 10px; margin-bottom: 10px; width: 100%; font-weight: bold; font-size: 12px;">';
				fm_form += '<tr>';
				  fm_form += '<td>Medication:</td>';
				   fm_form += '</tr>';
				  fm_form += '<tr>';
					fm_form += '<td><input type="text" name="pr_medication" id="pr_medication" style="width:98%;border:1px solid #ccc"></td>';
				  fm_form += '</tr>';
				  fm_form += '<tr>';
				  fm_form += '<td>Dose:</td>';
				   fm_form += '</tr>';
				  fm_form += '<tr>';
					fm_form += '<td><input type="text" name="pr_dose" id="pr_dose" style="width:98%;border:1px solid #ccc"></td>';
				fm_form += '</tr>';
			  fm_form += '</table>';
			  fm_form += '<div style="margin:0px"><div class="book_buttons" id="cancel_pr_btn" onclick="cancelPR()" style="float:left;background-color:#ef2a21;border:1px solid #ef2a21;">Cancel Request</div><div class="book_buttons" id="make_pr_btn" onclick="makePR()" style="float: right;;background-color:#ef2a21;border:1px solid #ef2a21;">Send Request</div></div>';
			
			var thePRDiv = document.getElementById('pr_form');
			thePRDiv.innerHTML = pr_dr_select_string + pr_patient_select_string + fm_form;
			
			theDiv.style.display = 'block';
			
		} else {
			
			theDiv.style.display = 'none';
			
		}
		
}

function showDocPR(dr_row, dr_name, doctor_id) {
	
	selected_dr_row = dr_row;
	selected_dr = dr_name;
	selected_dr_id = doctor_id;
	override_get_appointments = true;
	showBox("prescription",-1,600,0,null,0,"","","","","","");
	
}

function showPR() {

	var thePRDiv = document.getElementById('pr_form');
	var family_member_form = document.getElementById("family_member_form_holder");
	family_member_form.style.display = 'none';
	
	document.getElementById("pr_dr_select").selectedIndex = 0;
	document.getElementById("pr_patient_select").selectedIndex = 0;
	document.getElementById("pr_medication").value = "";
	document.getElementById("pr_dose").value = "";
	
	if (thePRDiv.style.display == 'none') {
		thePRDiv.style.display = 'block';
	} else {
		thePRDiv.style.display = 'none';
	}
	
	
}

function cancelPR() {
	
	var theDiv = document.getElementById('apDiv1');
	theDiv.style.right = '-1000px';
	var thePRDiv = document.getElementById('pr_form');
	thePRDiv.style.display = 'none';
	
}

function makePR() {
	
	var appointment_for = document.getElementById("appointment_for");
	var user_id = appointment_for.options[1].getAttribute('value');
	var pr_dr_select = document.getElementById("pr_dr_select");
	var doctor_id = pr_dr_select.value;
	var pr_patient_select = document.getElementById("pr_patient_select");
	var patient = pr_patient_select.options[pr_patient_select.selectedIndex].innerHTML;
	var medication = document.getElementById("pr_medication").value;
	var dose = document.getElementById("pr_dose").value;
			
	var errors = "";
	
		if (pr_dr_select.selectedIndex == 0) {
			errors+='- Select your doctor.<br />';
		} 
		
		if (pr_patient_select.selectedIndex == 0) {
			errors+='- Select a family member.<br />';
		} 
		
		if (medication == '') {
			errors+='- Enter a medication.<br />';
		} 
		
		if (dose == '') {
			errors+='- Enter a dose.<br />';
		} 
		
	  	if (errors.length > 0) {
			
			ALERT_TITLE = 'Missing Information!';
			alert('The following error(s) occurred:<br />'+errors);
		
		} else {
				
			request.open("POST", "pr_request.php", false);
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			request.send("surgery_id=<?php echo $surgery_id; ?>&doctor_id="+doctor_id+"&user_id="+user_id+"&patient="+patient+"&medication="+medication+"&dose="+dose);
				
			var strResult=request.responseText;
			var apptJSON = JSON.parse(strResult);
			
			cancelPR();
			
			ALERT_TITLE = apptJSON[0]['heading'];
			alert(apptJSON[0]['message']);
	
		}
	
}

function newPatient(doctor_id) {
	
	
    var doctor_code = prompt("This doctor only sees existing patients.\n\nPlease enter your doctor's online code.\n\nYou can get your doctor's online code by\nringing the surgery and asking for it.\n\n", "Enter you doctor's online code.");
	
		if (doctor_code.toString() != doctor_id.toString()) {
			alert("The code you have entered is incorrect. Please try again.");
			return false;
		} else {
			return true;
		}
	
}


</script>

</head>

<body onLoad="getFirstAvailableAppointments()">

<div class="container">

<div id="content">
<div id="apDiv1" style="right:-1000px;">
<div id="close_iframe" onclick="showBox()">CLOSE</div></div>
</div>

  <div id="header"><img src="images/header.png" alt="Insert Logo Here" name="Insert_logo" width="800" height="101" id="Insert_logo" /> 
    <!-- end .header --></div>
    <div id="log_out" style="width: 780px; margin-left: auto; margin-right: auto; text-align: right; font-size: 14px; margin-bottom: 10px;"><a href="appointments.php?logout=1&logout_url=<?php echo $row_surgery_details_rs['log_out_url']; ?>">Log out</a></div>
  <div class="sidebar1">
  <div id="dr_list">
  <p><strong>Select a doctor from the list below.</strong></p>
  <p>Scroll to see all doctors.</p>
  <p>Click 'First Available Appointment' to see all available appointments for the next two days.</p>
  <p>Click doctor's name to have calendar  show which days have appointments for that doctor.</p>
  <p>Click the 'Request Prescription' button to request a script.</p>
  <?php 
  $row_number = 1;
  ?>
  <div id="dr_0" doctor_id="0" style="color:#EF2A21; background-color:#E3E8E4;" onClick="getFirstAvailableAppointments()" class="doctor">First Available Appointments</div>
  <?php 
  for ($i=0;$i<count($doctors_array);$i++) {
	  
	  if ($doctors_array[$i]['allow_prescription_requests'] == 'yes') {
	  	$pr_string = '<div onclick="showDocPR(\'dr_'.$row_number.'\', \''.$doctors_array[$i]['doctor_name'].'\', \''.$doctors_array[$i]['doctor_id'].'\')"  style="position: relative; right: -120px; padding-top: 6px; margin-top: 4px; height: 32px;" class="book_buttons" id="pr_'.$row_number.'">Request Prescription</div>';
	  }
	  
	?>
  <div doctor_id="<?php echo $doctors_array[$i]['doctor_id']; ?>"  id="dr_<?php echo $row_number; ?>" onClick="getAppointments('dr_<?php echo $row_number; ?>', '<?php echo $doctors_array[$i]['doctor_name']; ?>', '<?php echo $doctors_array[$i]['doctor_id']; ?>' )" class="doctor"><?php echo $doctors_array[$i]['doctor_name']; ?><?php echo $pr_string; ?></div>
 <?php 
 $row_number ++;
 } ?>
</div>
  <!-- end .sidebar1 --></div>
  <div class="content">
  
      <!-- Responsive calendar - START -->
    	<div class="responsive-calendar" id="calendar">
        <div class="controls">
            <a class="pull-left" data-go="prev"><div class="btn btn-primary">Prev</div></a>
            <h4><span data-head-year></span> <span data-head-month></span></h4>
            <a class="pull-right" data-go="next"><div class="btn btn-primary">Next</div></a>
        </div><hr/>
        <div class="day-headers">
          <div class="day header">Mon</div>
          <div class="day header">Tue</div>
          <div class="day header">Wed</div>
          <div class="day header">Thu</div>
          <div class="day header">Fri</div>
          <div class="day header">Sat</div>
          <div class="day header">Sun</div>
        </div>
        <div class="days" data-group="days">
      </div>
      <!-- Responsive calendar - END -->
    </div>
    <script src="js/jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/responsive-calendar.js"></script>
    <script type="text/javascript">
      $(document).ready(function () {
		  
			  function addLeadingZero(num) {
				if (num < 10) {
				  return "0" + num;
				} else {
				  return "" + num;
				}
			  }


        $(".responsive-calendar").responsiveCalendar({
			onInit: function() { month = this.currentMonth;year = this.currentYear;},
			onMonthChange: function() { month = this.currentMonth;year = this.currentYear;triggerAppointments();}
        });
      });
    </script>
    <!-- end .content --></div>
  <div class="sidebar2">
  <div id="appt_list">
  
    </div>
  </div>

  <!-- end .container --></div>
  <div id="apDiv1" style="right:-1000px;">
<div id="close_iframe" onclick="showBox()">CLOSE</div></div>
</div>
</body>
</html>