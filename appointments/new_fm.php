<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$user_id = $_POST['u'];
$first_name = $_POST['f'];
$first_name = ucfirst($first_name); 
$last_name = $_POST['l'];
$last_name = ucfirst($last_name); 
$dob = $_POST['d'];
$mobile = $_POST['m'];
$mobile = str_replace(' ', '', $mobile);

$message_array = array();
		
$sql = sprintf("replace into family_members (user_id, first_name, last_name, dob, mobile) values (%s,%s,%s,%s,%s)", GetSQLValueString($user_id, "int"), GetSQLValueString($first_name, "text"), GetSQLValueString($last_name, "text"), GetSQLValueString($dob, "text"), GetSQLValueString($mobile, "text"));
	
		mysql_select_db($database_approval_rs, $approval_rs);
  		$rs = mysql_query($sql, $approval_rs) or die(mysql_error());
	 
		if ($rs) {
			
			$message_array[] = array("heading"=>"Family Member Added.", "message"=>"Your family member has been added.");
			
		} else {
			
			$message_array[] = array("heading"=>"Family Member Not Added.", "message"=>"Your family member could not be added.");
			
		}
		
		echo json_encode($message_array);
	
?>