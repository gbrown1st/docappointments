<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$user_id = $_POST['u'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	family_members.family_member_id,
	family_members.user_id,
	family_members.first_name,
	family_members.last_name,
	family_members.dob,
	family_members.mobile
FROM
	family_members
WHERE
	family_members.user_id = ?
ORDER BY
	family_members.last_name ASC,
	family_members.first_name ASC");
	
      mysqli_stmt_bind_param($stmt, 'i', $user_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->family_member_id, $row->user_id, $row->first_name, $row->last_name, $row->dob, $row->mobile);


	$fm_array = array();

      while (mysqli_stmt_fetch($stmt)) {
	
			$fm_array[] = array("family_member_id"=>$row->family_member_id, "user_id"=>$row->user_id, "first_name"=>$row->first_name, "last_name"=>$row->last_name, "dob"=>$row->dob, "mobile"=>$row->mobile);
	
      }


      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
	echo json_encode($fm_array);

?>



