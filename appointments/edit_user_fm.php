<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$user_id = $_POST['u'];
$first_name = $_POST['f'];
$last_name = $_POST['l'];
$dob = $_POST['d'];
$mobile = $_POST['m'];

$message_array = array();

$sql = sprintf("update users_new set first_name = %s, last_name = %s, dob = %s, mobile = %s
WHERE user_id = %s", GetSQLValueString($first_name, "text"), GetSQLValueString($last_name, "text"), GetSQLValueString($dob, "text"), GetSQLValueString($mobile, "text"), GetSQLValueString($user_id, "int"));
	
		mysql_select_db($database_approval_rs, $approval_rs);
  		$rs = mysql_query($sql, $approval_rs) or die(mysql_error());
	 
		if ($rs) {
			
			$message_array[] = array("heading"=>"Family Member Updated.", "message"=>"The family member details have been updated.");
			
		} else {
			
			$message_array[] = array("heading"=>"Family Member Not Updated.", "message"=>"The family member details could not be updated.\n\nPlease try again.");
			
		}
		
		echo json_encode($message_array);
	
?>