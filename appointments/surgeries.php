<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

if (isset($_GET['change_surgery'])) {
	if ($_GET['change_surgery'] == '1') {
		setcookie("surgery_id", "", time()-(60*60*24));
	}
}
if (isset($_COOKIE['surgery_id'])) {
	header("Location: appointments.php" );
}

if (isset($_GET['user_id'])) {
	$_SESSION['user_id'] = $_GET['user_id'];
}

$MM_authorizedusers_new = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strusers_new, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users_new based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrusers_new = Explode(",", $strusers_new); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrusers_new)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users_new based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strusers_new == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedusers_new, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_approval_rs, $approval_rs);
$query_states_rs = "SELECT DISTINCT 	surgeries.surgery_state FROM 	surgeries WHERE 	surgery_status = 'Active' AND surgeries.surgery_state <> '' ORDER BY 	surgeries.surgery_state ASC";
$states_rs = mysql_query($query_states_rs, $approval_rs) or die(mysql_error());
$totalRows_states_rs = mysql_num_rows($states_rs);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>DocAppointment</title>

<link href="css/styles.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script type="text/javascript">

var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }


function getSurgeries(dr_row, surgery_state) {
	
		request.open("POST", "state_surgeries.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("surgery_state="+surgery_state);
		
		var strResult=request.responseText;
		var apptJSON = JSON.parse(strResult);
		
		var theDiv = document.getElementById("surgery_list");
		
		var surgery_list = "<p><strong>Select a surgery from the list below: </strong></p>";
		
		var row_number = 1;
		
		for (i=0;i<apptJSON.length;i++) {
			
			surgery_list += '<div id="surgery_'+row_number+'" onClick="selectSurgery('+apptJSON[i]['surgery_id']+')" class="doctor"><h2 style="font-size: 13px; margin: 0px;">'+apptJSON[i]['surgery_name']+'</h2><p style="font-weight: normal; margin: 0px;">'+apptJSON[i]['display_address_1']+'<br />'+apptJSON[i]['display_address_2']+'</p></div>';
			
		}
		
		theDiv.innerHTML = surgery_list;
		
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

  $(function() {
	$( "#dialog-confirm" ).dialog({
	autoOpen: false,
	resizable: false,
	height:290,
	modal: true,
	buttons: {
	"Yes": function() {
	location.assign("appointments.php?surgery_id="+selected_surgery_id+"&set_surgery=1");
	},
	"No": function() {
	location.assign("appointments.php?surgery_id="+selected_surgery_id+"&set_surgery=0");
	},
	Cancel: function() {
	$( this ).dialog( "close" );
	}
	}
	});
});

var selected_surgery_id;

function selectSurgery(surgery_id) {
	selected_surgery_id = surgery_id;
	$( "#dialog-confirm" ).dialog( "open" );
}
</script>

</head>

<body onLoad="MM_preloadImages('images/AustraliaMapGreyWARed.gif','images/AustraliaMapGreyNTRed.gif','images/AustraliaMapGreySARed.gif','images/AustraliaMapGreyQLDRed.gif','images/AustraliaMapGreyNSWRed.gif','images/AustraliaMapGreyVICRed.gif','images/AustraliaMapGreyTASRed.gif','images/AustraliaMapGreyACTRed.gif')">
<div id="wrapper">
<div id="dialog-confirm" title="Remember this as your surgery?">
<p>To see appointments for this surgery and to have DocAppointment remember this surgery as your surgery click 'Yes'.<br /><br />To see see appointments for this surgery just this time click 'No'.<br /><br />To choose another surgery or state click 'Cancel'.</p>
</div>
<div id="header"><img src="images/header.png" width="800" height="100"></div>
<div id="surgery_details">

</div> 
<div id="content">
<div id="state_list">
  <p><strong>Select a state from the list below to find DocAppointments surgeries there: </strong></p>
  <?php 
  $row_number = 1;
  ?>
  <?php 
  while ($row_states_rs = mysql_fetch_assoc($states_rs)) {?>
  <div id="dr_<?php echo $row_number; ?>" onClick="getSurgeries('dr_<?php echo $row_number; ?>', '<?php echo $row_states_rs['surgery_state']; ?>')" class="doctor">
    <?php echo $row_states_rs['surgery_state']; ?> </div>
 <?php 
 $row_number ++;
 } ?>
 <p><strong>Or click  a state on the map below: </strong></p>
 <img name="map_image" id="map_image" src="images/AustraliaMapGrey.gif" width="304" height="285" usemap="#Map">
 <map name="Map">
   
   <area shape="poly" coords="116,38,95,33,39,76,3,101,15,144,14,193,41,198,83,189,115,171" href="#" alt="Western Australia" onClick="getSurgeries(8,'WA')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyWARed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="121,34,122,118,182,119,182,49,168,41,175,21,135,11" href="#" onClick="getSurgeries(2,'NT')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyNTRed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="124,128,122,168,201,226,201,126" href="#" onClick="getSurgeries(4,'SA')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreySARed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="188,54" href="#">
   <area shape="poly" coords="188,53,188,120,208,121,208,147,276,145,296,138,278,102,244,59,216,4,205,57" href="#" onClick="getSurgeries(2,'QLD')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyQLDRed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="207,149" href="#">
   <area shape="poly" coords="259,193,258,202,266,208,272,202,268,193" href="#" onClick="getSurgeries(0,'ACT')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyACTRed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="208,150,210,185,236,207,253,205,273,219,275,196,294,167,295,151,293,143,260,152" href="#" onClick="getSurgeries(1,'NSW')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyNSWRed.gif',1)" onMouseOut="MM_swapImgRestore()">
   <area shape="poly" coords="207,191,209,233,244,234,268,223,253,211,239,212,227,208" href="#" onClick="getSurgeries(7,'VIC')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyVICRed.gif',1)" onMouseOut="MM_swapImgRestore()">
   
   <area shape="poly" coords="233,254,237,269,249,281,259,269,260,254" href="#" onClick="getSurgeries(5,'TAS')" onMouseOver="MM_swapImage('map_image','','images/AustraliaMapGreyTASRed.gif',1)" onMouseOut="MM_swapImgRestore()">
 </map>

</div>
<div id="surgery_list">
<p><strong>Please select a state...</strong></p>
</div>

</div>
</body>
</html>
<?php
mysql_free_result($states_rs);
?>
