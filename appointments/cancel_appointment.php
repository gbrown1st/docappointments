<?php

date_default_timezone_set('Australia/Hobart');
$now_date_time = date('Y-m-d');

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$doctor_name = $_POST['doctor_name'];
$doctor_name = str_replace("''","'", $doctor_name);
$patient_id = $_POST['patient_id'];
$user_type = 'patient';
$email = $_POST['email'];
$appt_length = $_POST['appt_length'];
$appt_pos = $_POST['appt_pos'];
$appt_secs = $_POST['appt_secs'];
$appt_day_array = explode(' ', $_POST['appt_day']);
$appt_day = $appt_day_array[0];
$appt_id = $_POST['appt_id'];
$appt_time = $_POST['appt_time'];
$appt_info = $_POST['appt_info'];

$date = strtotime($appt_time);

$start_point = date("Y-m-d H:i:s",$date);
$end_point = date("Y-m-d H:i:s",$date + $appt_length);

if ($user_type == 'patient') {
	
	$appoitment_type = 'Internet';
	
} else {
	
	$appoitment_type = 'Internetwc';
	
}

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgeries_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	zedmed_doctors.surgery_id
FROM
	zedmed_doctors");
	
      mysqli_stmt_execute($surgeries_stmt);

      mysqli_stmt_bind_result($surgeries_stmt, $row->surgery_id);
	  
	  $surgery_array = array();

      while (mysqli_stmt_fetch($surgeries_stmt)) {
		 
		  $surgery_array[] = $row->surgery_id;
		  
      }

      mysqli_stmt_free_result($surgeries_stmt);


$stmt = mysqli_prepare($mysqli,
          "SELECT server_url.server_url, 
	server_url.server_wsdl_url, 
	surgeries.surgery_name, 
	surgeries.surgery_software, 
	surgeries.clinic_code, 
	surgeries.include_in_notifications, 
	surgery_notifications.surgery_notification_email, 
	surgery_notifications.surgery_notification_cancel_appointment
FROM server_url INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
	 LEFT OUTER JOIN surgery_notifications ON server_url.surgery_id = surgery_notifications.surgery_id
WHERE server_url.surgery_id =  ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software, $row->clinic_code, $row->include_in_notifications, $row->surgery_notification_email, $row->surgery_notification_cancel_appointment);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
		  $clinic_code = $row->clinic_code;
		  $include_in_notifications = $row->include_in_notifications;
		  $surgery_notification_email = $row->surgery_notification_email;
		  $surgery_notification_cancel_appointment = $row->surgery_notification_cancel_appointment;
		  
      }
	  
      mysqli_stmt_free_result($stmt);
	  
	  if (in_array($surgery_id, $surgery_array)) {
		
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.doctor_code,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.multiple_appointments,
	zedmed_doctors.appointment_length
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	} else {
			
		$stmt2 = mysqli_prepare($mysqli,	  
	  "SELECT
	doctor_new_patients.doctor_code,
	doctor_new_patients.show_all_appointments,
	doctor_new_patients.appointment_book_id,
	doctor_new_patients.multiple_appointments
FROM
	doctor_new_patients
WHERE
	surgery_id = ?
AND doctor_id = ?");
			
	}
	  

	  mysqli_stmt_bind_param($stmt2, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt2);
	  
	  if (in_array($surgery_id, $surgery_array)) {
		  
		  mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id, $row->multiple_appointments, $row->appointment_length);

			  while (mysqli_stmt_fetch($stmt2)) {
				 
				  $doctor_code = $row->doctor_code;
				  $show_all_appointments = $row->show_all_appointments;
				  $appointment_book_id = $row->appointment_book_id;
				  $multiple_appointments = $row->multiple_appointments;
				  $appointment_length = $row->appointment_length;
				  
			  }
		  
	  } else {
		  
		   mysqli_stmt_bind_result($stmt2, $row->doctor_code, $row->show_all_appointments, $row->appointment_book_id, $row->multiple_appointments);

			  while (mysqli_stmt_fetch($stmt2)) {
				 
				  $doctor_code = $row->doctor_code;
				  $show_all_appointments = $row->show_all_appointments;
				  $appointment_book_id = $row->appointment_book_id;
				  $multiple_appointments = $row->multiple_appointments;
				  
			  }
		  
	  }

	  
	  mysqli_stmt_free_result($stmt2);
	  
	  if ($multiple_appointments != 'yes') {
		  
		  $multiple_appointments = 'no';
		  
	  }
	  
	  $stmt_zm_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.clinic_code,
	zedmed_doctors.show_all_appointments
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	  mysqli_stmt_bind_param($stmt_zm_doc, 'is', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_zm_doc);

      mysqli_stmt_bind_result($stmt_zm_doc, $row->clinic_code, $row->show_all_appointments);

      while (mysqli_stmt_fetch($stmt_zm_doc)) {
		 
		  $clinic_code = $row->clinic_code;
		  $show_all_appointments = $row->show_all_appointments;
		  
      }
 
 	 mysqli_stmt_free_result($stmt_zm_doc);
	 
	  $patient_details_array = array();
	
	  if ($surgery_software == 'BestPractice') {
		  
			$post_array = array('getInternetPatientID' => 'true');
		
			$r = new HttpRequest($server_url, HttpRequest::METH_POST);
			$r->addPostFields($post_array);
	
			$response = $r->send()->getBody();
			
			$response_array = json_decode($response, true);
			
			$internet_patient_id = $response_array[0]['INTERNALID'];
	
	 if (in_array($surgery_id, $surgery_array)) {
		 
		$jsonSendData = '[{"appt_time":"'.$appt_time.'","patient_email":"'.$email.'","appointment_description":"'.$appoitment_type.'","appt_secs":'.$appt_secs.',"internet_patient_id":0,"appt_length":'.$appt_length.',"patient_description":"'.$appt_info.'","doctor_id":'.$doctor_id.',"appt_day":"'.$appt_day.'","include_in_notifications":"'.$include_in_notifications.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","patient_name":"'.$appt_info.'","delete_from_day":"'.$now_date_time.'","user_type":"patient","patient_id":0,"appt_id":'.$appt_id.'}]';
		
	} else {
		
		$jsonSendData = '[{"user_string":"'.$appt_info.'","appointment_description":"'.$appoitment_type.'","patient_id":'.$patient_id.',"internet_patient_id":'.$internet_patient_id.',"appt_length":'.$appt_length.',"appt_secs":'.$appt_secs.',"patient_email":"'.$email.'","doctor_name":"'.$doctor_name.'","show_all_appointments":"'.$show_all_appointments.'","appt_time":"'.$appt_time.'","user_type":"'.$user_type.'","patient_name":"'.$appt_info.'","appt_id":'.$appt_id.',"patient_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","delete_from_day":"'.$now_date_time.'","doctor_id":'.$doctor_id.'}]';
		
	}
		
		$post_array = array('cancelAppointment' => 'true', 'jsonSendData' => $jsonSendData);

	   } else if ($surgery_software == 'PracSoft') {
		
		if (in_array($surgery_id, $surgery_array)) {
				
				$jsonSendData = '[{"patient_description":"Internet","doctor_id":'.$doctor_id.',"delete_from_day":"'.$now_date_time.'","patient_email":"'.$email.'","patient_name":"'.$appt_info.'","appt_id":'.$appt_id.',"appointment_description":"Internet","appt_day":"'.$appt_day.'","doctor_name":"'.$doctor_name.'","user_type":"patient","show_all_appointments":"'.$show_all_appointments.'","appt_date":"'.$appt_day.'","include_in_notifications":"'.$include_in_notifications.'","user_string":"'.$appt_info.'","appt_time":"'.$appt_time.'"}]';
				
				$post_array = array('cancelExistingAppointment' => 'true', 'jsonSendData' => $jsonSendData);
				
			} else {
				
				$jsonSendData = '[{"user_type":"'.$user_type.'","appointment_description":"'.$appoitment_type.'","appt_day":"'.$appt_day.'","doctor_id":'.$doctor_id.',"patient_name":"'.$appt_info.'","user_string":"'.$appt_info.'","doctor_name":"'.$doctor_name.'","patient_email":"'.$email.'","patient_description":"'.$appoitment_type.'","appt_id":'.$appt_id.',"appt_time":"'.$appt_time.'","delete_from_day":"'.$now_date_time.'"}]';
				
				$post_array = array('makeAppointmentRequest' => 'true', 'jsonSendData' => $jsonSendData);
				
			}
		
	  } else if ($surgery_software == 'Zedmed') {
		   
		  $jsonSendData = '[{"end_point":"'.$end_point.'","patient_email":"'.$email.'","doctor":"'.$doctor_id.'","doctor_name":"'.$doctor_name.'","start_point":"'.$start_point.'","patient_description":"'.$appt_info.'","appt_id":'.$appt_id.',"clinic_code":"'.$clinic_code.'","appt_time":"'.$appt_time.'","show_all_appointments":"'.$show_all_appointments.'"}]';
		
		$post_array = array('cancelAppointment' => 'true', 'jsonSendData' => $jsonSendData);
		  
	  } else if ($surgery_software == 'Practice 2000') {
		 
		   $jsonSendData = '[{"patient_email":"'.$email.'","appt_pos":'.$appt_pos.',"doctor_name":"'.$doctor_name.'","appt_time":"'.$appt_time.'","show_all_appointments":"'.$show_all_appointments.'","appt_day":"'.$appt_time.'","include_in_notifications":"'.$include_in_notifications.'","appt_date":"'.$appt_day.'","appointment_description":"Internet","user_string":"'.$appt_info.'","doctor_id":'.$doctor_id.',"patient_description":"Internet","delete_from_day":"'.$now_date_time.'","doctor_code":"'.$doctor_code.'","user_type":"patient","patient_name":"'.$appt_info.'","appt_id":'.$appt_id.'}]';
		
		$post_array = array('cancelAppointment' => 'true', 'jsonSendData' => $jsonSendData);
		  
	  } else if ($surgery_software == 'Stat') {
		   
		$params = array ('appointmentId'=>$appt_id);
	
		try {
			
		   $client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
	
			$result = $client->CancelAppointment($params) ;
			
			$result_code = (int) $result->CancelAppointmentResult;
			
			if ($result_code == 0) {
	
				$stat_json_string = '{"result": [{"heading": "Cancel Appointment Successful", "message": "Your appointment has been cancelled."}]}';
	
			 } else {
	
				$stat_json_string = '{"result": [{"heading": "Cancel Appointment Failed", "message": "Your appointment could not be cancelled."}]}';
				
			}
			
		   
		}
		catch (SoapFault $exception) {
			echo $exception->getMessage();
		}
		  
	  }
		
		$r = new HttpRequest($server_url, HttpRequest::METH_POST);
		$r->addPostFields($post_array);

		$response = $r->send()->getBody();
		
		$response_array = json_decode($response, true);
		
		if ($response_array[0]['heading'] == 'Cancel Appointment Successful' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_cancel_appointment) > 0) {
			
			$email_message = '<html><head><style type="text/css">body {margin-top:0;font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #333;}#content {width: 620px;}</style></head><body><div id="content">';
			
			$email_message .= $surgery_notification_cancel_appointment;
			$email_message = str_replace('[user]', $appt_info, $email_message);
			$email_message = str_replace('[doctor]', $doctor_name, $email_message);
			$email_message = str_replace('[appt_time]', $appt_time, $email_message);
			
			$email_message = str_replace('\r','\r\n', $email_message);
			
			$email_message .= "

<hr size='1' noshade>
<h3 style='color:#1E6CB2;margin-top:20px;;margin-bottom:10px;'>Search Health Information</h3>
Would you like to find out more about your condition so you are better informed before your next appointment?

Visit <a href='http://www.mydr.com.au/?client=1'>myDr.com.au</a> to search independent Australian health information.

<a href='http://www.mydr.com.au/?client=1'><img width='310' height='60' alt='myDr logo' src='http://www.mydr.com.au/images/myDr_homepage_logo.png'></a>";

			$email_message .='</div></body></html>';

			$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
			$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns
			
			$email_subject = $surgery_name . " Online Appointment Cancellation";
			
			//$email_message = wordwrap($email_message, 70, "\r\n");
			
			$headers = "Reply-To: $surgery_notification_email\n"; 
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: text/html";
	
			if(mail($email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au')) {
						
				$response_array[0]['result_string'] .= "
					
An email cancellation notification has been sent to $email.";
						
			} else {
				
				$response_array[0]['result_string'] .= "
					
An email notification COULD NOT BE SENT to $email.

Please confirm your appointment cancellation.";

			}
			
		if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Cancel Success Notification";
				
				$email_message = "Patient: ".$appt_info."

Doctor: ".$doctor_name."

Time: ".$appt_time."

Status: Cancel Appointment Successful

Source: Android Mobile Device";

				$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
				$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
			
		} else if ($response_array[0]['heading'] == 'Cancel Appointment Failed' && strlen($surgery_notification_email) > 0 && strlen($surgery_notification_cancel_appointment) > 0) {
			
			if ($include_in_notifications == 'y') {
				
				$email_subject = $surgery_name . " Online Appointment Cancel Fail Notification";
				
				$email_message = "Patient: ".$appt_info."

Doctor: ".$doctor_name."

Time: ".$appt_time."

Status: Cancel Appointment Failed

Source: Android Mobile Device";

				$email_message = str_replace(chr(10), "<br>", $email_message); //remove carriage returns
				$email_message = str_replace(chr(13), "<br>", $email_message); //remove carriage returns

				mail($surgery_notification_email, $email_subject, $email_message, $headers, '-f do-not-reply@docappointments.com.au');

			}
		}
		
		 $stmt = mysqli_prepare($mysqli,
          "INSERT INTO user_log (
			surgery_id, 
			user_id, 
			log_source, 
			log_action, 
			log_action_comment,
			log_action_date) 
		VALUES (?,?,?,?,?,?)");
	
	$now_date_time = date('Y-m-d H:i:s');
	$log_source = "iPhone";
	$log_action = $response_array[0]['heading'];
	$log_action_comment = "Appointment ID: $appt_id; Time: $appt_time";
	
	mysqli_bind_param($stmt, 'iissss', $surgery_id, $user_id, $log_source, $log_action, $log_action_comment, $now_date_time);

	mysqli_stmt_execute($stmt);

	mysqli_stmt_free_result($stmt);
	
	mysqli_close($mysqli);
	
	if ($surgery_software == 'Stat') {
		
		echo $stat_json_string;
		
	} else {
		
		$message_array = array();
	
		$message_array[] = array("heading"=>$response_array[0]['heading'], "message"=>$response_array[0]['result_string']);
		
		echo json_encode($message_array);
		
	}
	
	
	

?>