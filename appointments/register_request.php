<?php require_once('../Connections/approval_rs.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

		$surgery_id = $_POST['surgery_id'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$dob = $_POST['dob'];
		$email = $_POST['email'];
		$mobile = $_POST['mobile'];
		$username = $_POST['username'];
		$password = $_POST['password'];
	
		/*
		$surgery_id = '19';
		$user_type = 'patient';
		$first_name = 'Graham';
		$last_name = 'Brown';
		$dob = '03/02/1959';
		$email = 'graham@mediarare.com.au';
		$mobile = '0448992599';
		$username = 'gb';
		$password = 'gb';
		*/
			
	$responseText = "";
	
	$checkusers_newQL = sprintf("SELECT users_new.user_id, users_new.user_username, users_new.email FROM users_new WHERE user_username = %s OR email = %s",
					   GetSQLValueString($username, "text"),
					   GetSQLValueString($email, "text"));
	
	mysql_select_db($database_approval_rs, $approval_rs);
	$checkUser_rs = mysql_query($checkusers_newQL, $approval_rs) or die(mysql_error());
	
	 if (mysql_num_rows($checkUser_rs) > 0) {
		 
		 $responseText .= "<h3>Username and/or Email Unavailable</h3>";
		 
		 $username_unavailable = false;
		 $email_unavailable = false;
		 
		 while ($row = mysql_fetch_assoc($checkUser_rs)) 
		
			{
				if ($username == $row['user_username']) {
					
					$username_unavailable = true;
					
				}
				
				if ($email == $row['email']) {
					
					$email_unavailable = true;
					
				}
			}
			
		if ($username_unavailable) {
			$responseText .= "<p>This username is in use. Please use a different username address.</p>"; 
		} 
		
		if ($email_unavailable) {
			$responseText .= "<p>This email is in use. Please use a different email address.</p>"; 
		}

	 } else {
	
	  $insertSQL = sprintf("INSERT INTO users_new (surgery_id, first_name, last_name, dob, email, mobile, user_username, user_password, created_date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
					   GetSQLValueString($surgery_id, "int"),
					   GetSQLValueString($first_name, "text"),
					   GetSQLValueString($last_name, "text"),
					   GetSQLValueString($dob, "text"),
					   GetSQLValueString($email, "text"),
					   GetSQLValueString($mobile, "text"),
					   GetSQLValueString($username, "text"),
					   GetSQLValueString($password, "text"),
                       GetSQLValueString($now_date_time, "date"));

	  mysql_select_db($database_approval_rs, $approval_rs);
	  $rs = mysql_query($insertSQL, $approval_rs) or die(mysql_error());
  		
			if ($rs) {
				
				$checkusers_newQL = sprintf("SELECT users_new.user_id FROM users_new WHERE user_username = %s", GetSQLValueString($username, "text"), GetSQLValueString($surgery_id, "int"));
					   
				mysql_select_db($database_approval_rs, $approval_rs);
  				$checkUser_rs = mysql_query($checkusers_newQL, $approval_rs) or die(mysql_error());
				
				while ($row = mysql_fetch_assoc($checkUser_rs)) 
		
				{
					$_SESSION['user_id'] = $row['user_id'];	 
				}
				
				$responseText .= "<h3>Registration Successful</h3><p>Your DocAppointment user registration has been succesful.</p><p>You can now user DocAppointment.</p><p>Click the link to <a href='login.php'> log in.</a></p>"; 
				
			} else {
				
				$responseText .= "<h3>Registration Failed</h3><p>Your DocAppointment user registration has failed.</p>"; 
			
			} 
			
	 }
			
		echo $responseText;
	
?>