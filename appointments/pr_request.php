<?php require_once('../Connections/approval_rs.php'); ?>
<?php
date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$user_id = $_POST['user_id'];
$patient = $_POST['patient'];
$medication = $_POST['medication'];
$dose = $_POST['dose'];

$message_array = array();
$sql = sprintf("insert into prescription_requests_new (surgery_id, doctor_id, user_id, pr_patient, medication, dose, request_date) values (%s, %s, %s, AES_ENCRYPT(%s, '0bfuscate'), AES_ENCRYPT(%s, '0bfuscate'), AES_ENCRYPT(%s, '0bfuscate'), %s)",
					   GetSQLValueString($surgery_id, "int"), GetSQLValueString($doctor_id, "text"), GetSQLValueString($user_id, "int"), GetSQLValueString($patient, "text"), GetSQLValueString($medication, "text"), GetSQLValueString($dose, "text"), GetSQLValueString($now_date_time, "text"));

 mysql_select_db($database_approval_rs, $approval_rs);
 $rs = mysql_query($sql, $approval_rs) or die(mysql_error());
$message_array = array();

			if ($rs) {
				$message_array[] = array("heading"=>"Prescription Request Successful", "message"=>"The prescription request has been sent.");
			} else {
				$message_array[] = array("heading"=>"Prescription Request Failed.", "message"=>"The prescription request could not be sent.");
			} 
		
		echo json_encode($message_array);
	
?>