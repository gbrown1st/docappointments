<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
$surgery_id = '22';
$doctor_id = '3';
$user_type = 'patient';//$_POST['user_type'];

$appt_day = "'2014-10-17'";

$user_id = '1';
*/
$surgery_id = $_POST['surgery_id'];
$doctor_id = $_POST['doctor_id'];
$user_type = 'patient';//$_POST['user_type'];

$appt_day = "'".$_POST['apptDate']."'";

$user_id = $_POST['u'];


$zedmed_appt_day = date("m/d/Y",strtotime($appt_day));

$zedmed_start_point = $zedmed_appt_day.' 00.00.00';
$zedmed_end_point = $zedmed_appt_day.' 23.59.59';


if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}



$fm_stmt = mysqli_prepare($mysqli,
          "SELECT
	users_new.first_name,
	users_new.last_name,
	users_new.dob,
	users_new.mobile
FROM
	users_new
WHERE
	users_new.user_id = ?
UNION
	SELECT
		family_members.first_name,
		family_members.last_name,
		family_members.dob,
		family_members.mobile
	FROM
		family_members
	WHERE
		family_members.user_id = ?");
	
      mysqli_stmt_bind_param($fm_stmt, 'ii', $user_id, $user_id);

      mysqli_stmt_execute($fm_stmt);

      mysqli_stmt_bind_result($fm_stmt, $row->first_name, $row->last_name, $row->dob, $row->mobile);


	$fm_array = array();

      while (mysqli_stmt_fetch($fm_stmt)) {
	
			$fm_array[] = '*'.$row->first_name.' '.$row->last_name.' [DOB: '.$row->dob.'] [Ph: '.$row->mobile.']*';
	
      }


      mysqli_stmt_free_result($fm_stmt);
	  
	$pracsoftDescrip = implode(",", $fm_array);
	
	$patient_string = str_replace(", ", ",", $pracsoftDescrip);
	$patient_string = str_replace("*", "", $patient_string);
	$appoitment_type = 'Internet';
	$patient_description = '*Internet*,'.$pracsoftDescrip;
	 
$stmt = mysqli_prepare($mysqli,
          "SELECT
	server_url.server_url,
	server_url.server_wsdl_url,
	surgeries.surgery_name,
	surgeries.surgery_software
FROM
	server_url
INNER JOIN surgeries ON server_url.surgery_id = surgeries.surgery_id
WHERE
	server_url.surgery_id = ?");
	
	 mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->server_url, $row->server_wsdl_url, $row->surgery_name, $row->surgery_software);

      while (mysqli_stmt_fetch($stmt)) {
		 
		  $server_url = $row->server_url;
		  $server_wsdl_url = $row->server_wsdl_url;
		  $surgery_name = $row->surgery_name;
		  $surgery_software = $row->surgery_software;
      }

	mysqli_stmt_free_result($stmt);
	
	if ($surgery_software == 'Stat') {
		
		$stmt_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	stat_doctors.doctor_name,
	stat_doctors.new_patients,
	stat_doctors.show_all_appointments,
	0 as appointment_book_id,
	0 as appointment_length
FROM
	stat_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	} else {
		
		$stmt_doc = mysqli_prepare($mysqli,	  
	  "SELECT
	zedmed_doctors.doctor_name,
	zedmed_doctors.show_all_appointments,
	zedmed_doctors.appointment_book_id,
	zedmed_doctors.appointment_length,
	zedmed_doctors.new_patients
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND doctor_id = ?");

	}
	
	  mysqli_stmt_bind_param($stmt_doc, 'ii', $surgery_id, $doctor_id);

      mysqli_stmt_execute($stmt_doc);

      mysqli_stmt_bind_result($stmt_doc, $row->doctor_name, $row->show_all_appointments, $row->appointment_book_id, $row->appointment_length, $row->new_patients);

      while (mysqli_stmt_fetch($stmt_doc)) {
		  
		  $doctor_name = $row->doctor_name;
		  $show_all_appointments = $row->show_all_appointments;
		  $appointment_book_id = $row->appointment_book_id;
		  $appointment_length = $row->appointment_length;
		  $new_patients = $row->new_patients;
	
      }
	  
	  mysqli_stmt_free_result($stmt_doc);
	  
				
				if ($surgery_software == 'BestPractice') {
					
					$jsonSendData = '[{"appt_day":"'.$appt_day.'","user_type":"patient","patient_id":"0","patient_description":"'.$patient_description.'","doctor_id":'.$doctor_id.',"appointment_description":"Internet","show_all_appointments":"'.$show_all_appointments.'"}]';
						  
					$post_array = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
				
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
			
					$response = $r->send()->getBody();
					
					$response_array = json_decode($response, true);
			
				} else if ($surgery_software == 'PracSoft') {
					
					$jsonSendData = '[{"appointment_book_id":'.$appointment_book_id.',"appointment_length":'.$appointment_length.',"doctor_id":'.$doctor_id.',"patient_description":"'.$patient_description.'","patient_string":"'.$patient_string.'","appt_day":"'.$appt_day.'","show_all_appointments":"'.$show_all_appointments.'","user_type":"patient"}]';
					
					$post_array = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
					
					$r = new HttpRequest($server_url, HttpRequest::METH_POST);
					$r->addPostFields($post_array);
			
					$response = $r->send()->getBody();
					
					$response_array = json_decode($response, true);
					
				}  else if ($surgery_software == 'Zedmed') {
				
					$jsonSendData = '[{"clinic_code":"'.$clinic_code.'","start_point":"'.$zedmed_start_point.'","patient_description":"'.$patient_description.'","appt_day":"'.$appt_day.'","doctor":"'.$doctor_id.'","end_point":"'.$zedmed_end_point.'"}]';
					
					$post_array = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 } else if ($surgery_software == 'Practice 2000') {
				
					$jsonSendData = '[{"patient_string":"'.$patient_string.'","show_all_appointments":"'.$show_all_appointments.'","patient_description":"'.$patient_description.'","appt_day":"'.$appt_day.'","internet_id": -1,"doctor_id":"'.$doctor_id.'","user_type":"patient"}]';
					
					 $post_array = array('getAllWeekAppointments' => 'true', 'jsonSendData' => $jsonSendData);
					 
				 } else if ($surgery_software == 'Stat') {
					
					$appt_day = str_replace("'", "", $appt_day);
					$appt_day = date("Ymd",strtotime($appt_day));
					
					$people = str_replace("*", "", $patient_description);
					
					$people_array = explode(',', $people);
					
					$params = array ('resourceId'=>$doctor_id, 'dateFrom'=>$appt_day, 'dateTo'=>$appt_day, 'personStringArray'=>$people_array);
					
					try {
						
					   	$client = new SoapClient($server_wsdl_url, array("trace" => 1, "exception" => 1, 'cache_wsdl'   =>  WSDL_CACHE_BOTH));
				
						$result = $client->GetAppointmentsWeek($params) ;
						
						$appointment_array = $result->GetAppointmentsWeekResult->DocAppAppointmentArray->DocAppAppointment;
						
						$stat_response_array = array();
						
						for ($i=0;$i<count($appointment_array);$i++) {
								
								$stat_response_array[$i]['ApptID'] = (int) $appointment_array[$i]->AppointmentId;
								$stat_response_array[$i]['PractitionerID'] = (int) $appointment_array[$i]->ResourceId;
								$stat_response_array[$i]['PatientID'] = 0;
								$stat_response_array[$i]['When'] = date("Y-m-d H:i.00.000",strtotime($appointment_array[$i]->AppointmentTime));
								$stat_response_array[$i]['Length'] = (int) $appointment_array[$i]->Length * 60;
								$stat_response_array[$i]['AppSecs'] =  0;
								$stat_response_array[$i]['Descrip'] =  $appointment_array[$i]->Description;
								$date = strtotime($stat_response_array[$i]['When']);
								$stat_response_array[$i]['Date'] = $date;
											
						}
						
					}
					catch (SoapFault $exception) {
						echo $exception->getMessage();
					}
				}
		
		if ($surgery_software == 'Stat') {
			
			$response_array = array();
			$response_array = $stat_response_array;
			
		}
		
		for ($i=0;$i<count($response_array);$i++) {
			
			$date = strtotime($response_array[$i]['When']);
			$response_array[$i]['Date'] = $date;
		}
		
		if (count($response_array) > 0) {
	
			$sortArray = array();
	
			foreach($response_array as $appointment){
				foreach($appointment as $key=>$value){
					if(!isset($sortArray[$key])){
						$sortArray[$key] = array();
					}
					$sortArray[$key][] = $value;
				}
			}
	
			$orderby = "Date"; //change this to whatever key you want from the array
			
			array_multisort($sortArray[$orderby],SORT_ASC,$response_array); 
		
		}
	
	//print_r($response_array);
	$appointments_array = array();
	
	$now = time();
	
      for ($i=0;$i<count($response_array);$i++) {
		  
			 $date = strtotime($response_array[$i]['When']);
			 
			if (substr($response_array[$i]['When'], 0, 11) != 'Unavailable' && substr($response_array[$i]['Descrip'], 0, 11) != 'Unavailable' && $date > $now) {
				
				if (substr($response_array[$i]['Descrip'], 0, 9) == 'Available') {
				
					$response_array[$i]['Descrip'] = 'Internet';
					 
				}
				
		 	 $appInfo = '';
			  
			 $ApptDate_array = explode(" ", $response_array[$i]['When']);
			 
			 $date = strtotime($response_array[$i]['When']);
	
			 $appTime = date("D j M Y g:i A",$date);
			 
			 if ($surgery_software == 'BestPractice') {
			  
			  $appType = 'Internet';
			  
			  if ($response_array[$i]['Descrip'] == 'Internet') {
					  
					  $appInfo = '';
					  
				  } else {
					  
					  $appInfo = $response_array[$i]['Descrip'];
					  
				}
			
		  } else  if ($surgery_software == 'PracSoft') {
			  
			  if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc' || $response_array[$i]['Descrip'] == 'Available') {
				  
				 $appType = 'Internet';
				  
			 } else {
				 
				 $appInfo = $response_array[$i]['Descrip'];
				 
			 }
			  
		  } else  if ($surgery_software == 'Practice 2000' || $surgery_software == 'Zedmed' || $surgery_software == 'Stat') {
			  
			   if ($response_array[$i]['Descrip'] == 'Internet' || $response_array[$i]['Descrip'] == 'Internetwc' || $response_array[$i]['Descrip'] == 'Available') {
				  
				 	$appType = 'Internet';
				  
				 } else {
					 
					 $appInfo = $response_array[$i]['Descrip'];
					 
				 }
		 
		  }
		
			 
				if ($surgery_software == 'Practice 2000') {
						  
					$apptPos = $response_array[$i]['Pos'];
					$appt_length = $response_array[$i]['Length'] * 60;
						  
				} else {
						  
					$apptPos = '0';
					$appt_length = $response_array[$i]['Length'];
					
				}
			
				 if (($surgery_id == '42' && (int) $response_array[$i]['AppSecs'] >= 64800)) {
					 
					 continue;
					 
				 } else {
			 
			$small_appt_time = str_replace(':00.000','',$response_array[$i]['When']);
			$appt_time_array = explode(' ',$small_appt_time);
			
			$date_array = explode('-',$appt_time_array[0]);
			$time_array = explode(':',$appt_time_array[1]);
			
			$year = (int) $date_array[0];
			$month = (int) $date_array[1] - 1;
			$day = (int) $date_array[2];
			
			$hour = (int) $time_array[0];
			$minute = (int) $time_array[1];
			
	$appointments_array[] = array("year"=>$year, "month"=>$month, "day"=>$day, "hour"=>$hour, "minute"=>$minute, "doctor_id"=>$response_array[$i]['PractitionerID'], "doctor_name"=>$doctor_name, "patient_id"=>$response_array[$i]['PatientID'], "appt_date"=>$appt_time_array[0], "appt_description"=>$appTime, "appt_date_time"=>$response_array[$i]['When'], "appt_info"=>$appInfo, "appt_type"=>$appType, "appt_length"=>$appt_length, "appt_pos"=>$apptPos, "appt_secs"=>$response_array[$i]['AppSecs'], "appt_id"=>$response_array[$i]['ApptID'], "new_patients"=>$new_patients);
	
			
				 	}
					 
				}
			}
	
	
	echo json_encode($appointments_array);
	
?>


