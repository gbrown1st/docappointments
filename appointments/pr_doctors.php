<?php

$mysqli = new mysqli('localhost', 'root', '@sqlda129', 'docappointment');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$surgery_id = $_POST['surgery_id'];

$surgery_stmt = mysqli_prepare($mysqli,
          "SELECT
	surgeries.surgery_software
FROM
	surgeries
WHERE
	surgery_id = ?");
	
	  mysqli_stmt_bind_param($surgery_stmt, 'i', $surgery_id);

      mysqli_stmt_execute($surgery_stmt);

      $rows = array();
	  
      mysqli_stmt_bind_result($surgery_stmt, $row->surgery_software);
	  
	  while (mysqli_stmt_fetch($surgery_stmt)) {

		$surgery_software = $row->surgery_software;
	
      }
	  
	mysqli_stmt_free_result($surgery_stmt);  
	
	if ($surgery_software == 'Stat') {
		$stmt = mysqli_prepare($mysqli,
          "SELECT
	stat_doctors.doctor_id,
	stat_doctors.doctor_name
FROM
	stat_doctors
WHERE
	surgery_id = ?
AND allow_prescription_requests = 'yes'
ORDER BY
	stat_doctors.doctor_name ASC");

	} else {
		$stmt = mysqli_prepare($mysqli,
          "SELECT
	zedmed_doctors.doctor_id,
	zedmed_doctors.doctor_name
FROM
	zedmed_doctors
WHERE
	surgery_id = ?
AND allow_prescription_requests = 'yes'
ORDER BY
	zedmed_doctors.doctor_name ASC");

	}

	  mysqli_stmt_bind_param($stmt, 'i', $surgery_id);

      mysqli_stmt_execute($stmt);

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->doctor_id, $row->doctor_name);


	$pr_doctors_array = array();
	
	$pr_doctors_array[] = array("doctor_id"=>"0", "doctor_name"=>'Select your prescription request doctor:');

      while (mysqli_stmt_fetch($stmt)) {

		$pr_doctors_array[] = array("doctor_id"=>$row->doctor_id, "doctor_name"=>$row->doctor_name);
	
      }


      mysqli_stmt_free_result($stmt);
      mysqli_close($mysqli);

		
	echo json_encode($pr_doctors_array);

?>



